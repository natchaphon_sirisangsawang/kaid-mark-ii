﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void DayNightCycle::Update()
extern void DayNightCycle_Update_m0C25023C3F2BE55D8394DFAD4D5BCCE163317571 (void);
// 0x00000002 System.Void DayNightCycle::ChangeTime()
extern void DayNightCycle_ChangeTime_m7EB3321F875846760487296086DF3621DA714889 (void);
// 0x00000003 System.Void DayNightCycle::.ctor()
extern void DayNightCycle__ctor_m5EDE9A8D722F9FD94C7D511C707D3CFDB3ABD6C8 (void);
// 0x00000004 System.Void WaterManager::Awake()
extern void WaterManager_Awake_m9E6149DE853EF8F04B99CEBF9621926BD93684E4 (void);
// 0x00000005 System.Void WaterManager::Update()
extern void WaterManager_Update_m2845FC186C1A7C567F74799E81314268852F6B95 (void);
// 0x00000006 System.Void WaterManager::.ctor()
extern void WaterManager__ctor_m3329A005F2509CFB87BA80071501999B51095416 (void);
// 0x00000007 System.Void WaveManager::Awake()
extern void WaveManager_Awake_m62ED0E86417DC404A2CB4301389740B6124EFD78 (void);
// 0x00000008 System.Void WaveManager::Update()
extern void WaveManager_Update_m179CA7CE42049BFC0291C7EC3AF0D77D7FB9B301 (void);
// 0x00000009 System.Single WaveManager::GetWaveHeight(System.Single)
extern void WaveManager_GetWaveHeight_m08B1C320B5B3F4106C5912C6865B08488CC183C9 (void);
// 0x0000000A System.Void WaveManager::.ctor()
extern void WaveManager__ctor_mBDC5BC3BFE97218B2ADA2160C31B34682498DCFF (void);
// 0x0000000B System.Void WaterReflection::OnWillRenderObject()
extern void WaterReflection_OnWillRenderObject_mA8126C59F0AA196FB9356E67B1E3EC2A8C8CEE74 (void);
// 0x0000000C System.Void WaterReflection::OnDisable()
extern void WaterReflection_OnDisable_m0FBE16036955394DE08D1D84231C9978CC58D70F (void);
// 0x0000000D System.Void WaterReflection::UpdateCameraModes(UnityEngine.Camera,UnityEngine.Camera)
extern void WaterReflection_UpdateCameraModes_mA407B437EFF18CFEE10A5B5AB080446350D257ED (void);
// 0x0000000E System.Void WaterReflection::CreateMirrorObjects(UnityEngine.Camera,UnityEngine.Camera&)
extern void WaterReflection_CreateMirrorObjects_mF9AA9BBC3E767667BA6BF1AFE390CDC9537C63D8 (void);
// 0x0000000F System.Single WaterReflection::sgn(System.Single)
extern void WaterReflection_sgn_m95E5C8AE0595A89E79BBFEF34070081D829A23A9 (void);
// 0x00000010 UnityEngine.Vector4 WaterReflection::CameraSpacePlane(UnityEngine.Camera,UnityEngine.Vector3,UnityEngine.Vector3,System.Single)
extern void WaterReflection_CameraSpacePlane_m175AAD5A32459E659B994B1DD97DD87AA79F614E (void);
// 0x00000011 System.Void WaterReflection::CalculateObliqueMatrix(UnityEngine.Matrix4x4&,UnityEngine.Vector4)
extern void WaterReflection_CalculateObliqueMatrix_m357CED7D00638688AECE4AF9514B088F6887ABAE (void);
// 0x00000012 System.Void WaterReflection::CalculateReflectionMatrix(UnityEngine.Matrix4x4&,UnityEngine.Vector4)
extern void WaterReflection_CalculateReflectionMatrix_mF66BFCC859AC9FEDA112C41643F6ABA335787CAD (void);
// 0x00000013 System.Void WaterReflection::.ctor()
extern void WaterReflection__ctor_m7B8DE8B3F86141802475B009C5E3AC8342BE312F (void);
// 0x00000014 System.Void WaterReflection::.cctor()
extern void WaterReflection__cctor_mE97A05A6742F123C183BC939586B59393F869B24 (void);
// 0x00000015 System.Void CardBluePrint::.ctor()
extern void CardBluePrint__ctor_m6F2F9B910B7B2AE9DFD18F5E89D6D5750CE2A3D5 (void);
// 0x00000016 System.Void ElementBluePrint::.ctor()
extern void ElementBluePrint__ctor_m84BBC6026AB88DDBC06918F594967E444C2764B9 (void);
// 0x00000017 System.Void SkillBluePrint::.ctor()
extern void SkillBluePrint__ctor_m581922088833BC48E4F1CBDE2F003D76A0959897 (void);
// 0x00000018 System.Void Bullet_F13::Start()
extern void Bullet_F13_Start_mD127A0DD8AF669A549D1A3C42D26ED1DD2052AB0 (void);
// 0x00000019 System.Void Bullet_F13::Update()
extern void Bullet_F13_Update_mA5E5F41024D7CD6D7C13FF93E001B23E3338581D (void);
// 0x0000001A System.Void Bullet_F13::OnDrawGizmosSelected()
extern void Bullet_F13_OnDrawGizmosSelected_mB208EE1054DB3FA44817CB8542D4760BCF67CA88 (void);
// 0x0000001B System.Void Bullet_F13::.ctor()
extern void Bullet_F13__ctor_mC1FBE3B05B7BC50C9BCE337370EBBCF9F2B44FFD (void);
// 0x0000001C System.Void Bullet_F81::Start()
extern void Bullet_F81_Start_mD1E95E1CF0D3FEAF51E17214B6357136C170E699 (void);
// 0x0000001D System.Void Bullet_F81::Update()
extern void Bullet_F81_Update_mF17E98C0F357AE9EF1F88B0777472E9BF5487CC0 (void);
// 0x0000001E System.Void Bullet_F81::OnDrawGizmosSelected()
extern void Bullet_F81_OnDrawGizmosSelected_m85176FD1CC2BDA897D53DB2CF5899A244C4542E3 (void);
// 0x0000001F System.Void Bullet_F81::.ctor()
extern void Bullet_F81__ctor_m57FE37CC0A00C7811FEEE929715B386D1C1E8B22 (void);
// 0x00000020 System.Void Bullet_P32::Start()
extern void Bullet_P32_Start_m09D1FE8339CB537CCB371D12A4A72EB317C0A12C (void);
// 0x00000021 System.Void Bullet_P32::Update()
extern void Bullet_P32_Update_mB39B26D371938503D61AE471F1F6EAC9C8EEE63A (void);
// 0x00000022 System.Void Bullet_P32::OnDrawGizmosSelected()
extern void Bullet_P32_OnDrawGizmosSelected_m413006E0ABB213202A26E5A825914742A7DCFFEE (void);
// 0x00000023 System.Void Bullet_P32::.ctor()
extern void Bullet_P32__ctor_m9A4A6D8F64D0E9493FB97235FD3F2A59DB8FD8A6 (void);
// 0x00000024 System.Void Bullet_Z1::Start()
extern void Bullet_Z1_Start_m80B201D494B0FC5DFF158E99C92C31E983F6762C (void);
// 0x00000025 System.Void Bullet_Z1::Update()
extern void Bullet_Z1_Update_m85CF42F158B46630C326885EB2E4D2718CB0F078 (void);
// 0x00000026 System.Void Bullet_Z1::OnDrawGizmosSelected()
extern void Bullet_Z1_OnDrawGizmosSelected_m232511525BE5F218A0F62A8E25CB6185C4875995 (void);
// 0x00000027 System.Void Bullet_Z1::.ctor()
extern void Bullet_Z1__ctor_mB915962289836878CF61A1A72E3C3EE22293552E (void);
// 0x00000028 System.Void Bullet_Z23::Start()
extern void Bullet_Z23_Start_m6FC25F184D891B1BF89B9EC84C4AC71F4412CD59 (void);
// 0x00000029 System.Void Bullet_Z23::Update()
extern void Bullet_Z23_Update_mEFE8EFC6F10825767E2792060EB2AACD9B95E7E1 (void);
// 0x0000002A System.Void Bullet_Z23::OnDrawGizmosSelected()
extern void Bullet_Z23_OnDrawGizmosSelected_mD2D81A888B8F331D6AB0BE8E8279B7D9F2F5E32B (void);
// 0x0000002B System.Void Bullet_Z23::.ctor()
extern void Bullet_Z23__ctor_m73738B84EC10C746F476ACCC3A07F002DD981F2B (void);
// 0x0000002C CinemachineSwitcher CinemachineSwitcher::get_Instance()
extern void CinemachineSwitcher_get_Instance_mF6ABBBE7B4E38636BEB09D7ADF3940BA80DC808B (void);
// 0x0000002D System.Void CinemachineSwitcher::set_Instance(CinemachineSwitcher)
extern void CinemachineSwitcher_set_Instance_mC4980BCB3320D1E7B59956E65C60DCA174B641EA (void);
// 0x0000002E System.Void CinemachineSwitcher::Awake()
extern void CinemachineSwitcher_Awake_m25CB21057BF4834495973CB7BBA4E68865BBB801 (void);
// 0x0000002F System.Void CinemachineSwitcher::Start()
extern void CinemachineSwitcher_Start_m5DE5EDDABC85812FDA214459EBD9F0FAD2658BDA (void);
// 0x00000030 System.Void CinemachineSwitcher::AdsReward()
extern void CinemachineSwitcher_AdsReward_mBD399E9DD2A6B31B5B4B95D95357F2C7D0F4FA27 (void);
// 0x00000031 System.Void CinemachineSwitcher::GameOverCam()
extern void CinemachineSwitcher_GameOverCam_mDF1566D5ED0F4F43F71DEE6EA6F9597763E3D35E (void);
// 0x00000032 System.Collections.IEnumerator CinemachineSwitcher::WaitToStart()
extern void CinemachineSwitcher_WaitToStart_mFB3F4280F74BD988595B53EE4F855CCDBA6708F1 (void);
// 0x00000033 System.Void CinemachineSwitcher::HideCanvas()
extern void CinemachineSwitcher_HideCanvas_m1EB69E3F766DC9792A4F54EF2C22F257335AC661 (void);
// 0x00000034 System.Void CinemachineSwitcher::SwitchLeftCamera()
extern void CinemachineSwitcher_SwitchLeftCamera_mE27C16222721F42BD5DF4AE7E7E89A2B479A04C0 (void);
// 0x00000035 System.Void CinemachineSwitcher::SwitchFontCamera()
extern void CinemachineSwitcher_SwitchFontCamera_m59A0CFCD4B5BE80C3B763F614675AE06D616A81C (void);
// 0x00000036 System.Collections.IEnumerator CinemachineSwitcher::WaitForCam()
extern void CinemachineSwitcher_WaitForCam_m20F16C627FA98D38C064729772CE31961D3735AE (void);
// 0x00000037 System.Void CinemachineSwitcher::SwitchTopCamera()
extern void CinemachineSwitcher_SwitchTopCamera_mC4B609132602A7E361F6BD11F5AC626A872BD7AA (void);
// 0x00000038 System.Void CinemachineSwitcher::SwitchRightCamera()
extern void CinemachineSwitcher_SwitchRightCamera_mB37954856A657DD5B2AB594446F941C46509E46B (void);
// 0x00000039 System.Collections.IEnumerator CinemachineSwitcher::WaitForShowCanvas()
extern void CinemachineSwitcher_WaitForShowCanvas_m66CF0EE7E5A38923F80DA4040DD48571364BE4AD (void);
// 0x0000003A System.Void CinemachineSwitcher::.ctor()
extern void CinemachineSwitcher__ctor_m125076B67A450514B9B0DEF65A389DFB41FB2D46 (void);
// 0x0000003B InputManager InputManager::get_Instance()
extern void InputManager_get_Instance_mC5B4EA7BB8A909DB0FDD5D9CBF67B3A1CC40181D (void);
// 0x0000003C System.Void InputManager::set_Instance(InputManager)
extern void InputManager_set_Instance_mB94B3A652E1A869CBB1FFBBE8CA25842E26A3C34 (void);
// 0x0000003D System.Void InputManager::add_OnStartTouch(InputManager/StartTouch)
extern void InputManager_add_OnStartTouch_mCF106264B78FB07537E71524C99EE140805FF25D (void);
// 0x0000003E System.Void InputManager::remove_OnStartTouch(InputManager/StartTouch)
extern void InputManager_remove_OnStartTouch_m24C6AC368EC9AA24B40A5D1F3EF7B07A72766C7D (void);
// 0x0000003F System.Void InputManager::add_OnEndTouch(InputManager/EndTouch)
extern void InputManager_add_OnEndTouch_m50D940EFE21F790A04BCF54CE88FDF55F953CDFF (void);
// 0x00000040 System.Void InputManager::remove_OnEndTouch(InputManager/EndTouch)
extern void InputManager_remove_OnEndTouch_m3B6F0D7B26BC509080CA8F30E8D80E0FCA5423D2 (void);
// 0x00000041 System.Void InputManager::Awake()
extern void InputManager_Awake_m56AAFE728DF63B4B922FC31449C82EC6CFB0D946 (void);
// 0x00000042 System.Void InputManager::OnEnable()
extern void InputManager_OnEnable_m584432543637338E8BBECE286DF8167F10309018 (void);
// 0x00000043 System.Void InputManager::OnDisable()
extern void InputManager_OnDisable_m93B2282C567FD565E189E8F08A5BBF08EED61478 (void);
// 0x00000044 System.Void InputManager::Start()
extern void InputManager_Start_m99C6C0D9277906D13076BCD14E7BE2DDAF88FF08 (void);
// 0x00000045 System.Void InputManager::StartTouchPrimary(UnityEngine.InputSystem.InputAction/CallbackContext)
extern void InputManager_StartTouchPrimary_m8AABC7B12D09F2ED23825E1CCAD1A40F15ADCE47 (void);
// 0x00000046 System.Void InputManager::EndTouchPrimary(UnityEngine.InputSystem.InputAction/CallbackContext)
extern void InputManager_EndTouchPrimary_m6532C782DC9F442C8272E685D963D478C255AA35 (void);
// 0x00000047 UnityEngine.Vector2 InputManager::PrimaryPosition()
extern void InputManager_PrimaryPosition_m6F82DF759B187A46A0A049D5F7B15C3E106438DD (void);
// 0x00000048 System.Void InputManager::.ctor()
extern void InputManager__ctor_mB533F16325A793C9274F6CA3804EBCE27AD700A7 (void);
// 0x00000049 System.Void InputManager::<Start>b__17_0(UnityEngine.InputSystem.InputAction/CallbackContext)
extern void InputManager_U3CStartU3Eb__17_0_m6D32B313D99F02F691422CF3763619364E7A8936 (void);
// 0x0000004A System.Void InputManager::<Start>b__17_1(UnityEngine.InputSystem.InputAction/CallbackContext)
extern void InputManager_U3CStartU3Eb__17_1_m3F85443FB5E717AC691727FFF4C91444705B47AD (void);
// 0x0000004B UnityEngine.InputSystem.InputActionAsset MobileInput::get_asset()
extern void MobileInput_get_asset_m8065F2CC02A1444F16ADA2794E8071D867C46888 (void);
// 0x0000004C System.Void MobileInput::.ctor()
extern void MobileInput__ctor_m4DDB04139A6D7296830473E18185DA65099FAB86 (void);
// 0x0000004D System.Void MobileInput::Dispose()
extern void MobileInput_Dispose_m0CB0FBDC49ECFF0B6A98A66419805962C27B68A2 (void);
// 0x0000004E System.Nullable`1<UnityEngine.InputSystem.InputBinding> MobileInput::get_bindingMask()
extern void MobileInput_get_bindingMask_m0F596245A82CAB9AAAB735DC0D28E5B4BD4A8BD5 (void);
// 0x0000004F System.Void MobileInput::set_bindingMask(System.Nullable`1<UnityEngine.InputSystem.InputBinding>)
extern void MobileInput_set_bindingMask_mD9523881398BCAFA7CB7B3A66A0CAE5787705CCA (void);
// 0x00000050 System.Nullable`1<UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputDevice>> MobileInput::get_devices()
extern void MobileInput_get_devices_m74DB85C25EB1629E6EDEB33FD0D3D2A362307EB4 (void);
// 0x00000051 System.Void MobileInput::set_devices(System.Nullable`1<UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputDevice>>)
extern void MobileInput_set_devices_m39041648627016BD8A9B579D906D7ECB268B2F8B (void);
// 0x00000052 UnityEngine.InputSystem.Utilities.ReadOnlyArray`1<UnityEngine.InputSystem.InputControlScheme> MobileInput::get_controlSchemes()
extern void MobileInput_get_controlSchemes_mB1AB9A0279747DE3E5B20C983485A3AEF64FC462 (void);
// 0x00000053 System.Boolean MobileInput::Contains(UnityEngine.InputSystem.InputAction)
extern void MobileInput_Contains_m0F8F2B0431E6E73175F69C173DD2F445316CEA98 (void);
// 0x00000054 System.Collections.Generic.IEnumerator`1<UnityEngine.InputSystem.InputAction> MobileInput::GetEnumerator()
extern void MobileInput_GetEnumerator_m2E60ADD04AAD89BB1E230C5190708B4C669D0D26 (void);
// 0x00000055 System.Collections.IEnumerator MobileInput::System.Collections.IEnumerable.GetEnumerator()
extern void MobileInput_System_Collections_IEnumerable_GetEnumerator_m718E93A97714E3C66F70E97FB2E5140212B1D4AA (void);
// 0x00000056 System.Void MobileInput::Enable()
extern void MobileInput_Enable_mB764A4D6375832398D0A79024AB57088FE3854F0 (void);
// 0x00000057 System.Void MobileInput::Disable()
extern void MobileInput_Disable_mD3641EE7154939C47A0691630D2B97E83878C5BF (void);
// 0x00000058 MobileInput/TouchActions MobileInput::get_Touch()
extern void MobileInput_get_Touch_mE6235E514104B07DAB37A8ADEA5DA6D0B4DD660F (void);
// 0x00000059 System.Void SwipeDetection::Awake()
extern void SwipeDetection_Awake_m1F16EE2EC922E7C6087AB6C76AB37166B978AE50 (void);
// 0x0000005A System.Void SwipeDetection::OnEnable()
extern void SwipeDetection_OnEnable_m8F56324CB51CEA6AB5AE215FF93D6354D59EDB74 (void);
// 0x0000005B System.Void SwipeDetection::OnDisable()
extern void SwipeDetection_OnDisable_m55567A728958F6000E8FA0CBE12600CC0CE90D23 (void);
// 0x0000005C System.Void SwipeDetection::SwipeStart(UnityEngine.Vector2,System.Single)
extern void SwipeDetection_SwipeStart_mF76804A19BA6BEADB9F5F86288D6E263CBEF6CB7 (void);
// 0x0000005D System.Void SwipeDetection::SwipeEnd(UnityEngine.Vector2,System.Single)
extern void SwipeDetection_SwipeEnd_m58C2324A595B121023B3F2CEA1E8126DB4DA5C60 (void);
// 0x0000005E System.Void SwipeDetection::DetectSwipe()
extern void SwipeDetection_DetectSwipe_m3F29BC8BF7CC981CB377FF27B55FFB479FDF41C7 (void);
// 0x0000005F System.Void SwipeDetection::SwipeDirection(UnityEngine.Vector2)
extern void SwipeDetection_SwipeDirection_m860B495376A9B2D25B0520C9E8F2D706A8887692 (void);
// 0x00000060 System.Void SwipeDetection::.ctor()
extern void SwipeDetection__ctor_m2C4513A493B17DBB06F395DCEA18547D30EC2D58 (void);
// 0x00000061 UnityEngine.Vector3 Utils::ScreenToWorld(UnityEngine.Camera,UnityEngine.Vector3)
extern void Utils_ScreenToWorld_m0DC4BED80C5AD99D0B2950C6BF74F1AE186539E8 (void);
// 0x00000062 System.Void Utils::.ctor()
extern void Utils__ctor_mC41E39AE5559F8129A8C15EFD3E22EF8ACEDCCAE (void);
// 0x00000063 System.Void ScreenCapture::Start()
extern void ScreenCapture_Start_mE754AB7DB07B1B52F9528011C8394B374ACF6CE5 (void);
// 0x00000064 System.String ScreenCapture::CreateFileName(System.Int32,System.Int32)
extern void ScreenCapture_CreateFileName_m70DCE68DAC386836200A8F1BEC91721F20210989 (void);
// 0x00000065 System.Void ScreenCapture::CaptureScreenshot()
extern void ScreenCapture_CaptureScreenshot_mE2E816A00BCB0CDC06E3650079C466729D0B0E4D (void);
// 0x00000066 System.Void ScreenCapture::TakeScreenShot()
extern void ScreenCapture_TakeScreenShot_mAE70B259C4E62A028B502F320DAAF09D79ACCEEB (void);
// 0x00000067 System.Collections.IEnumerator ScreenCapture::ImageShowCase()
extern void ScreenCapture_ImageShowCase_m7C1802B6832D044746834F97B1D7D55505D707AA (void);
// 0x00000068 System.Void ScreenCapture::ShareImage()
extern void ScreenCapture_ShareImage_m9227EEB4098F09988F9995027C2422525349B905 (void);
// 0x00000069 System.Void ScreenCapture::.ctor()
extern void ScreenCapture__ctor_mF1A1D405C141990DEFD5166FA5370FD91EE8F6D4 (void);
// 0x0000006A System.Void CubeButton::OnMouseDown()
extern void CubeButton_OnMouseDown_m7B567FA80595EF0E1614FEA98BDC304BF13D5320 (void);
// 0x0000006B System.Void CubeButton::.ctor()
extern void CubeButton__ctor_mCBB82476431D9EC9DAEBDD69FC1DE33C4CCE5D7B (void);
// 0x0000006C System.Void LongClickButton::OnPointerDown(UnityEngine.EventSystems.PointerEventData)
extern void LongClickButton_OnPointerDown_m396A66D12FFF14E5AC982CA62ADC8A75DE29821A (void);
// 0x0000006D System.Void LongClickButton::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void LongClickButton_OnPointerUp_m55F255A996E879C45277323F539735150523F911 (void);
// 0x0000006E System.Void LongClickButton::Update()
extern void LongClickButton_Update_m62D430D348D82E24ECD02DC67D58BEE330F77237 (void);
// 0x0000006F System.Collections.IEnumerator LongClickButton::DelayClick()
extern void LongClickButton_DelayClick_m57566FE8544386D752B832895C7A3C424E60F152 (void);
// 0x00000070 System.Void LongClickButton::Reset()
extern void LongClickButton_Reset_mDA906B766DB3C337E865915B83EB4FCA0E1223C6 (void);
// 0x00000071 System.Void LongClickButton::DestroyCard()
extern void LongClickButton_DestroyCard_mCFD62BA1BC8B1C5EBF53BE5E56C428E2AE058FD6 (void);
// 0x00000072 System.Void LongClickButton::.ctor()
extern void LongClickButton__ctor_m62A293FBCE550D5C9975FD1BCED8445F9D5CCBD3 (void);
// 0x00000073 UnityEngine.Transform[] EnemyWayPoint::get_WayPoints()
extern void EnemyWayPoint_get_WayPoints_mA9B0EC26719F8C9A85CE11979FA70ACAA767FC8D (void);
// 0x00000074 System.Void EnemyWayPoint::set_WayPoints(UnityEngine.Transform[])
extern void EnemyWayPoint_set_WayPoints_m29C19EC458381493C0C989548754E56303BB6B6F (void);
// 0x00000075 EnemyWayPoint EnemyWayPoint::get_Instance()
extern void EnemyWayPoint_get_Instance_m9E8A4D26FF4B696112549EDA6B14E2A419990393 (void);
// 0x00000076 System.Void EnemyWayPoint::set_Instance(EnemyWayPoint)
extern void EnemyWayPoint_set_Instance_m7FCF1F5029861DE0A65F2271EB287F6CC3D0C4F6 (void);
// 0x00000077 System.Void EnemyWayPoint::Awake()
extern void EnemyWayPoint_Awake_m56E825FF651D08C2088FD8E9A804B95F603689B9 (void);
// 0x00000078 System.Void EnemyWayPoint::.ctor()
extern void EnemyWayPoint__ctor_m3490E4C651E5A136BF1702E2A9E75642B9A65AFE (void);
// 0x00000079 System.Void Chest::Init(System.Single,System.Single,System.Int32,Manager.Element)
extern void Chest_Init_mE065D87A18D700C18CA776CBAD844B41E792C41A (void);
// 0x0000007A System.Void Chest::Update()
extern void Chest_Update_m4FA8556D3034321A01625325CE1F0612CB333866 (void);
// 0x0000007B System.Void Chest::TakeDamage(System.Single)
extern void Chest_TakeDamage_m32038BCB9444386B50AE02229FA8B9028DBEDE36 (void);
// 0x0000007C System.Void Chest::.ctor()
extern void Chest__ctor_m107B91B4F8435DE9E7C45BC7C0707B14BE3632D2 (void);
// 0x0000007D System.Void CrossDive::Init(System.Single,System.Single,System.Int32,Manager.Element)
extern void CrossDive_Init_m4814A2EF01947AB112E05F3F198160FB857476C1 (void);
// 0x0000007E System.Void CrossDive::Update()
extern void CrossDive_Update_m23A7D345248DBA6A2E84C48C2BA4D2B26787CDC4 (void);
// 0x0000007F System.Void CrossDive::TakeDamage(System.Single)
extern void CrossDive_TakeDamage_mD319E8B592E1C76C2ED73341E85F530A1CD23F4A (void);
// 0x00000080 System.Void CrossDive::.ctor()
extern void CrossDive__ctor_mA643E63EF6E24F3330AE6441506D650376DDB96B (void);
// 0x00000081 System.Void Eclipseside::Init(System.Single,System.Single,System.Int32,Manager.Element)
extern void Eclipseside_Init_m9DE15A51131D42E6A01C72CE8BF80EF401100358 (void);
// 0x00000082 System.Void Eclipseside::Update()
extern void Eclipseside_Update_mBF4E50D9FC1476C35795D9AF6E37684D488B691D (void);
// 0x00000083 System.Void Eclipseside::TakeDamage(System.Single)
extern void Eclipseside_TakeDamage_m818F719D07F247CE231CAFCA1D2FF75EAB984A76 (void);
// 0x00000084 System.Void Eclipseside::.ctor()
extern void Eclipseside__ctor_m7CA9E4526EAF88914CED49C54D8F749479DF83C6 (void);
// 0x00000085 System.Void Embio::Init(System.Single,System.Single,System.Int32,Manager.Element)
extern void Embio_Init_m22EE30C4B7BEBF452ADABFB88BAF3D7820F20C9A (void);
// 0x00000086 System.Void Embio::Update()
extern void Embio_Update_m79A2048FC2AD6AE31D1EF609F75CE77FCA177B96 (void);
// 0x00000087 System.Void Embio::TakeDamage(System.Single)
extern void Embio_TakeDamage_mDF3BE2419C8EB9673B8AC5B6A0BE84507A43A105 (void);
// 0x00000088 System.Void Embio::.ctor()
extern void Embio__ctor_m24F040E5B257FBF6A4839D473D345D35FF2E5318 (void);
// 0x00000089 System.Void Kana::Init(System.Single,System.Single,System.Int32,Manager.Element)
extern void Kana_Init_mC88B141257F094D7A4D528CC491DFB1D9BD3160F (void);
// 0x0000008A System.Void Kana::Update()
extern void Kana_Update_mB003DAF267B2758BBC811A04420512A022EF9882 (void);
// 0x0000008B System.Void Kana::TakeDamage(System.Single)
extern void Kana_TakeDamage_mF5BFD7BF789316E305D1D4B759CC4ECA5979E78D (void);
// 0x0000008C System.Void Kana::.ctor()
extern void Kana__ctor_mA72390CC4E4F7C29C8EEEB5DF61A31ED0FE76A7E (void);
// 0x0000008D System.Void Shiro::Init(System.Single,System.Single,System.Int32,Manager.Element)
extern void Shiro_Init_m5DB57855BC3CA04029B5F227B84895D4221F8055 (void);
// 0x0000008E System.Void Shiro::Update()
extern void Shiro_Update_mFD63634086788B962AE85CF60C820134C64121A6 (void);
// 0x0000008F System.Void Shiro::TakeDamage(System.Single)
extern void Shiro_TakeDamage_mCC810C83FFBF232F847D07DD1F910983B9B8E6A9 (void);
// 0x00000090 System.Void Shiro::.ctor()
extern void Shiro__ctor_mFCE0E57D1815B837736164675C8247246E3C80C9 (void);
// 0x00000091 System.Void PopUpDamage::Start()
extern void PopUpDamage_Start_m961A2CFD842EB20B0136E7AA58D4097AA385F977 (void);
// 0x00000092 System.Void PopUpDamage::Update()
extern void PopUpDamage_Update_m267C109471D594CCFC3AEDF5F24583AE032E0480 (void);
// 0x00000093 System.Void PopUpDamage::SetDamageText(System.Single)
extern void PopUpDamage_SetDamageText_m20DE6CDDA6356C942C236CCD17F926C0E26EBFB7 (void);
// 0x00000094 System.Void PopUpDamage::.ctor()
extern void PopUpDamage__ctor_m4903615A6D2049D6E46B34F6A425ABB4E393FE00 (void);
// 0x00000095 System.Void IDamageable::Start()
// 0x00000096 System.Void IDamageable::Update()
// 0x00000097 System.Void FirebaseManager::ClearUserData()
extern void FirebaseManager_ClearUserData_mD89AD374A9CDD4D89E8EE4B096BFB071F205A206 (void);
// 0x00000098 System.Void FirebaseManager::OpenLoginPanel()
extern void FirebaseManager_OpenLoginPanel_m03F016EB02757294F66B14D5B05A3AC5ED36870C (void);
// 0x00000099 System.Void FirebaseManager::OpenSignUpPanel()
extern void FirebaseManager_OpenSignUpPanel_mDEB14871C0EBFFD03CE8CFDE9467FBB43D6D73B5 (void);
// 0x0000009A System.Void FirebaseManager::OpenMenuPanel()
extern void FirebaseManager_OpenMenuPanel_mB431E4ABBC4285FA5B93B850A42FA27C816E950D (void);
// 0x0000009B System.Void FirebaseManager::OpenForgetPasswordPanel()
extern void FirebaseManager_OpenForgetPasswordPanel_m7D006EACB2B60F75E06E6D089969F894F2AEB1FF (void);
// 0x0000009C System.Void FirebaseManager::Start()
extern void FirebaseManager_Start_mC613ACB1AF5C9327DFD21634676C02D3991E5571 (void);
// 0x0000009D System.Void FirebaseManager::SaveStateRememberMe()
extern void FirebaseManager_SaveStateRememberMe_m510B158BB8236151DBB6417C8F5ED16D908AFB02 (void);
// 0x0000009E System.Collections.IEnumerator FirebaseManager::CheckAlreadyName(System.String)
extern void FirebaseManager_CheckAlreadyName_m25B27E898AA4C9F8DA81920ED46EE6B052C2DDE5 (void);
// 0x0000009F System.Void FirebaseManager::SignUpUser()
extern void FirebaseManager_SignUpUser_m108DBA3676562F47C9FD9C5CD17E9039BD23FA19 (void);
// 0x000000A0 System.Void FirebaseManager::CreateUser(System.String,System.String,System.String)
extern void FirebaseManager_CreateUser_mA84786334174BCC8FE2DF2DDD0357E89029138D7 (void);
// 0x000000A1 System.Void FirebaseManager::UpdateUserProfile(System.String,System.String)
extern void FirebaseManager_UpdateUserProfile_mD09EC38F532181435E4ED5151CC52367E43962B5 (void);
// 0x000000A2 System.Void FirebaseManager::LoginUser()
extern void FirebaseManager_LoginUser_m2B5D47B80CDE60E55E793786177DDC35E5FD0F93 (void);
// 0x000000A3 System.Void FirebaseManager::SignInUser(System.String,System.String)
extern void FirebaseManager_SignInUser_mB8701CF8750D17E6CA49F57E7D1456E2119BAB6E (void);
// 0x000000A4 System.Void FirebaseManager::WriteNewUser(System.String,System.String,System.String)
extern void FirebaseManager_WriteNewUser_m1C4B00BD9B783B7A5E4F608A0F247CFD07B0F66C (void);
// 0x000000A5 System.Void FirebaseManager::WriteOldUser(System.String,System.String,System.String)
extern void FirebaseManager_WriteOldUser_m488ADB3E281DD8F23C56F7BC9CC87857CEAFECFD (void);
// 0x000000A6 System.Void FirebaseManager::ForgetPass()
extern void FirebaseManager_ForgetPass_m953784A0B10809BE2D5A2F27BD65951302492CE8 (void);
// 0x000000A7 System.Void FirebaseManager::SendPasswordResetEmail(System.String)
extern void FirebaseManager_SendPasswordResetEmail_m6B355D137B2B60EF488CD2AC200035966C4E6CC1 (void);
// 0x000000A8 System.String FirebaseManager::GetErrorMessage(Firebase.Auth.AuthError)
extern void FirebaseManager_GetErrorMessage_m6C1AA969EE15A97C868DC7757BB10BEBEDBEF318 (void);
// 0x000000A9 System.Void FirebaseManager::ShowNotification(System.String,System.String)
extern void FirebaseManager_ShowNotification_mF62AC4770A509F9C26420EECE955311813C5C3FD (void);
// 0x000000AA System.Collections.IEnumerator FirebaseManager::CloseNotification()
extern void FirebaseManager_CloseNotification_m909268520C1AA67BBB1CC394D2A6BC6E63520F43 (void);
// 0x000000AB System.Void FirebaseManager::InitializeFirebase()
extern void FirebaseManager_InitializeFirebase_m0AB60DA55A5299AEFEEB03BAA7AAAA2CEDB11E90 (void);
// 0x000000AC System.Void FirebaseManager::AuthStateChanged(System.Object,System.EventArgs)
extern void FirebaseManager_AuthStateChanged_m918C661027778087914DAF004BDD83A730CD7F3B (void);
// 0x000000AD System.Void FirebaseManager::OnDestroy()
extern void FirebaseManager_OnDestroy_mA9EBED6EECE1364720C35962CDD7567AF2B71B0E (void);
// 0x000000AE System.Void FirebaseManager::Logout()
extern void FirebaseManager_Logout_m2A7271F20B8490BB33CD79E54CFF27C4024F5F09 (void);
// 0x000000AF System.Void FirebaseManager::DeleteUser()
extern void FirebaseManager_DeleteUser_m4EBA59F2A09549EE164E845BFA7C0F565501B833 (void);
// 0x000000B0 System.Void FirebaseManager::CheckID()
extern void FirebaseManager_CheckID_m403FAB3FD33179843578B306FFC2B6FEECC0E3B2 (void);
// 0x000000B1 System.Void FirebaseManager::LateUpdate()
extern void FirebaseManager_LateUpdate_m80D016FED347E580CBEBB8A79EDA3A63195727D7 (void);
// 0x000000B2 System.Void FirebaseManager::ExitGame()
extern void FirebaseManager_ExitGame_m1184C846093F039C598268587F880B6B7FCE43D0 (void);
// 0x000000B3 System.Void FirebaseManager::PlayAsGuest()
extern void FirebaseManager_PlayAsGuest_m853322836EADD190A1F745585BEC9D31342659C3 (void);
// 0x000000B4 System.Void FirebaseManager::TransferID()
extern void FirebaseManager_TransferID_m1447677FF66D860B182C84D1187A9AB735141C75 (void);
// 0x000000B5 System.Void FirebaseManager::TransferData(System.String,System.String,System.String)
extern void FirebaseManager_TransferData_mBD424AE5151A98C7FF2E62FCA22D275306FA3AF3 (void);
// 0x000000B6 System.Collections.IEnumerator FirebaseManager::CheckAlreadyNameIdTransfer(System.String)
extern void FirebaseManager_CheckAlreadyNameIdTransfer_m5BFE3B6936231D494314BE845F42A5AF4AC949FB (void);
// 0x000000B7 System.Collections.IEnumerator FirebaseManager::LoadUserScoreFromDB()
extern void FirebaseManager_LoadUserScoreFromDB_mD894FC790D4E61DCE6BA00C7CD7FDDAA39CD1E36 (void);
// 0x000000B8 System.Collections.IEnumerator FirebaseManager::DownloadImage(System.String)
extern void FirebaseManager_DownloadImage_m7181991C7853AADCE4D73A9D254FD97D898B378A (void);
// 0x000000B9 System.Void FirebaseManager::UploadPhotoUrl()
extern void FirebaseManager_UploadPhotoUrl_m0B34CAE805E4FFBC822E118875EF7A0E9E5B10CC (void);
// 0x000000BA System.Void FirebaseManager::RefreshPhoto()
extern void FirebaseManager_RefreshPhoto_m2AC1AC744922F23EBAD47CE7F82252FCFE3568E8 (void);
// 0x000000BB System.Void FirebaseManager::OpenLeaderBoard()
extern void FirebaseManager_OpenLeaderBoard_mFFE6757D86E27742C1685C757A3FBBB6FA395646 (void);
// 0x000000BC System.Collections.IEnumerator FirebaseManager::LoadLeaderBoardData()
extern void FirebaseManager_LoadLeaderBoardData_m06C184117B561767D286D1B7BD89173A9A4AB3CD (void);
// 0x000000BD System.Void FirebaseManager::.ctor()
extern void FirebaseManager__ctor_mB894832C008369C3C25D3B0DF9CDF84481C89506 (void);
// 0x000000BE System.Void FirebaseManager::<Start>b__48_0(System.Threading.Tasks.Task`1<Firebase.DependencyStatus>)
extern void FirebaseManager_U3CStartU3Eb__48_0_m36581C2AEC12E1C90BB72B87048D131734928B3C (void);
// 0x000000BF System.Void FirebaseManager::<SignInUser>b__55_0(System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser>)
extern void FirebaseManager_U3CSignInUserU3Eb__55_0_mC0D0D74A9B0AC51CEA0C4944D148B7D608C0EDA1 (void);
// 0x000000C0 System.Void FirebaseManager::<SendPasswordResetEmail>b__59_0(System.Threading.Tasks.Task)
extern void FirebaseManager_U3CSendPasswordResetEmailU3Eb__59_0_m91D2331DFDE02DA0307FB03795621C211246F4A2 (void);
// 0x000000C1 System.Void FirebaseManager::<DeleteUser>b__67_0(System.Threading.Tasks.Task)
extern void FirebaseManager_U3CDeleteUserU3Eb__67_0_mBFE0C01FAAF9354F30490CB854BF25624839A5D9 (void);
// 0x000000C2 System.Void FirebaseManager::<PlayAsGuest>b__71_0(System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser>)
extern void FirebaseManager_U3CPlayAsGuestU3Eb__71_0_m0D1455F6948BC1B608F5C3150AEEFD920DE04982 (void);
// 0x000000C3 System.Void RankData::NewRankData(System.Int32,System.String,System.Int32,System.String)
extern void RankData_NewRankData_m90E3025AD61654DA9BB167577B1EAC595EE311D9 (void);
// 0x000000C4 System.Collections.IEnumerator RankData::DownloadImage(System.String)
extern void RankData_DownloadImage_mB3634704E17275CFFC430EA174C96A48CDFCA196 (void);
// 0x000000C5 System.Void RankData::.ctor()
extern void RankData__ctor_mCE49986E1CB806794090039EB62946C655982EF4 (void);
// 0x000000C6 System.Void RankManager::Start()
extern void RankManager_Start_m1879556A68EA7C24A1E7F166E654327CE0A08184 (void);
// 0x000000C7 System.Void RankManager::UpdateToDB()
extern void RankManager_UpdateToDB_mA569F162B13FDE577289EDF4C6D20F0792B09F07 (void);
// 0x000000C8 System.Collections.IEnumerator RankManager::CheckDamageOverCurrentScore(System.Int32)
extern void RankManager_CheckDamageOverCurrentScore_mCB8486642C343A640FA9437DE98B3B4D87616034 (void);
// 0x000000C9 System.Collections.IEnumerator RankManager::CheckTimeSurvivedOverCurrentScore(System.Single)
extern void RankManager_CheckTimeSurvivedOverCurrentScore_m04A388299D839D31AAD14DF4E736989BDB46EFA0 (void);
// 0x000000CA System.Collections.IEnumerator RankManager::UpdateDamageDone(System.Int32)
extern void RankManager_UpdateDamageDone_mD0D78C65C914F189377894AE8F83E0788D6B2683 (void);
// 0x000000CB System.Collections.IEnumerator RankManager::UpdateWaveSurvived(System.Int32)
extern void RankManager_UpdateWaveSurvived_mF9400B91A4798D5BBE0AFCF5267A32977115979C (void);
// 0x000000CC System.Collections.IEnumerator RankManager::UpdateTimeSurvived(System.Single)
extern void RankManager_UpdateTimeSurvived_m6B3EFE193E94C4FBCCA5FA99EF20AE3D7417235F (void);
// 0x000000CD System.Collections.IEnumerator RankManager::LoadUserScoreFromDB()
extern void RankManager_LoadUserScoreFromDB_m0C1F32AE08783D1EFE74247850F9423A856FA627 (void);
// 0x000000CE System.Collections.IEnumerator RankManager::LoadLeaderBoardData()
extern void RankManager_LoadLeaderBoardData_mEA3E6CB77B24A2D858BF2423FCB7DCD52FC032B6 (void);
// 0x000000CF System.Collections.IEnumerator RankManager::DownloadImage(System.String)
extern void RankManager_DownloadImage_m9D8910B3E34BE8AEB8BDC0DD2E91135FF024FED7 (void);
// 0x000000D0 System.Void RankManager::OpenLeaderBoard()
extern void RankManager_OpenLeaderBoard_mE438584CE2BB3B1561E81F68893F7EA87AC4D54E (void);
// 0x000000D1 System.Void RankManager::InitializeFirebase()
extern void RankManager_InitializeFirebase_m93EA5CB6A0753B8C0476A92EAAC9E1F4ADFE3433 (void);
// 0x000000D2 System.Void RankManager::AuthStateChanged(System.Object,System.EventArgs)
extern void RankManager_AuthStateChanged_mFD4C507AB0443DD59C44AD5D2C47E795383DA6E6 (void);
// 0x000000D3 System.Void RankManager::OnDestroy()
extern void RankManager_OnDestroy_mE0A63330921A2CD1B8992EFE5CB30DD984DA6E4C (void);
// 0x000000D4 System.Void RankManager::.ctor()
extern void RankManager__ctor_mABC316C9D683BDCF250052EF5C2FAD05EAD53377 (void);
// 0x000000D5 System.Void AdsManager::Start()
extern void AdsManager_Start_m561A6D94B810C2CBBAD6235533FEF81D3FCB93BA (void);
// 0x000000D6 System.Void AdsManager::ShowBannerAd()
extern void AdsManager_ShowBannerAd_mEE21D50E8CF7E7E4C29DDDCBE56409C7B5B79783 (void);
// 0x000000D7 System.Void AdsManager::HideBanner()
extern void AdsManager_HideBanner_m649BED280175777ACA3807ACB1B332B37E2236D7 (void);
// 0x000000D8 System.Void AdsManager::ShowAds(System.String)
extern void AdsManager_ShowAds_m8F51286E3D51611E15459A5635394278B8DB192D (void);
// 0x000000D9 System.Void AdsManager::ShowMenuAds()
extern void AdsManager_ShowMenuAds_m1210026A28F80B54E21D1D89209CB54FDE13F4AE (void);
// 0x000000DA System.Void AdsManager::OnUnityAdsReady(System.String)
extern void AdsManager_OnUnityAdsReady_mA76941ECD72FD15E03D0B7DBF10365BF5AD72764 (void);
// 0x000000DB System.Void AdsManager::OnUnityAdsDidError(System.String)
extern void AdsManager_OnUnityAdsDidError_m1908BC190CA0D84743BF3480F0D10B4202F9E207 (void);
// 0x000000DC System.Void AdsManager::OnUnityAdsDidStart(System.String)
extern void AdsManager_OnUnityAdsDidStart_mF0AB782E7E55745E4D5A5A1EDFDB13CF64F73B15 (void);
// 0x000000DD System.Void AdsManager::OnUnityAdsDidFinish(System.String,UnityEngine.Advertisements.ShowResult)
extern void AdsManager_OnUnityAdsDidFinish_mC9DD4B662622209033D8314AD626FC82ACA736F0 (void);
// 0x000000DE System.Void AdsManager::.ctor()
extern void AdsManager__ctor_m1324EE3D9841045940C44D5E0379750DBE0C0A53 (void);
// 0x000000DF AnimationManager AnimationManager::get_Instance()
extern void AnimationManager_get_Instance_m6006600F80794DD76BB6AC3235F42B08C76DFC21 (void);
// 0x000000E0 System.Void AnimationManager::set_Instance(AnimationManager)
extern void AnimationManager_set_Instance_m129B4A95F6E1A05D296C65F5FF4654A34E191C1A (void);
// 0x000000E1 System.Void AnimationManager::Awake()
extern void AnimationManager_Awake_m627DE07938651E02F0A44463872756F99CEBE1A0 (void);
// 0x000000E2 System.Void AnimationManager::MapUp()
extern void AnimationManager_MapUp_m0671E4730102F8A1A16189D0519CD1DECF12FB83 (void);
// 0x000000E3 System.Void AnimationManager::MapDown()
extern void AnimationManager_MapDown_m888E8697FF4886EF0F124BE1DBF6E60FF2B3018E (void);
// 0x000000E4 System.Void AnimationManager::FireworkItemClick()
extern void AnimationManager_FireworkItemClick_m9D08BDDF76F297BA01437D256E0A8AFC77C53037 (void);
// 0x000000E5 System.Void AnimationManager::FireworkItemClicked()
extern void AnimationManager_FireworkItemClicked_m2F47BFF9156FAECAF864BC25CBF9E85195419EC9 (void);
// 0x000000E6 System.Void AnimationManager::CookieItemClick()
extern void AnimationManager_CookieItemClick_mA61DD11F51A285F81B5B49CDC4873B7FEC43AA68 (void);
// 0x000000E7 System.Void AnimationManager::CookieItemClicked()
extern void AnimationManager_CookieItemClicked_m784832DFA3744E37046384144043421A354A969E (void);
// 0x000000E8 System.Void AnimationManager::CandyItemClick()
extern void AnimationManager_CandyItemClick_mC5637379CA1202E055174E2C0B3EF094E02E6BEF (void);
// 0x000000E9 System.Void AnimationManager::CandyItemClicked()
extern void AnimationManager_CandyItemClicked_m2E8C80CC584738CC7C27DE953214E38E2B34B38C (void);
// 0x000000EA System.Void AnimationManager::ScreenShotClick()
extern void AnimationManager_ScreenShotClick_m3898AC476C591FB1745516AFDC8EB22502279752 (void);
// 0x000000EB System.Void AnimationManager::ScreenShotClicked()
extern void AnimationManager_ScreenShotClicked_m930B9D597CC234711BD297210DA32E7F26E23698 (void);
// 0x000000EC System.Void AnimationManager::LeaderBoardClick()
extern void AnimationManager_LeaderBoardClick_mEFE537E4AE47C191A838A194165A325D1D3CCD64 (void);
// 0x000000ED System.Void AnimationManager::LeaderBoardClicked()
extern void AnimationManager_LeaderBoardClicked_mBEFB624ED531785691AB117629A43766714FBBD9 (void);
// 0x000000EE System.Void AnimationManager::NoticeTimerIn()
extern void AnimationManager_NoticeTimerIn_mC8DC954F73922F7206D8A90992002523045F427C (void);
// 0x000000EF System.Void AnimationManager::NoticeTimerOut()
extern void AnimationManager_NoticeTimerOut_m6221B0A1F27D0377F1B16A1DEE3443E91F444ADA (void);
// 0x000000F0 System.Void AnimationManager::NoticeIn()
extern void AnimationManager_NoticeIn_m6CBA2BFB6C0BF4D1A111DAECD8101CC088255948 (void);
// 0x000000F1 System.Void AnimationManager::NoticeOut()
extern void AnimationManager_NoticeOut_m21DB4E8741E8B7A9467F17B6057AD243692D324B (void);
// 0x000000F2 System.Void AnimationManager::.ctor()
extern void AnimationManager__ctor_m0C4622326DC614221E503860BBCC73F91BFB9F20 (void);
// 0x000000F3 System.Void HeroUI::Awake()
extern void HeroUI_Awake_m346912E3285BAE99C967A6AFCD65747A22734273 (void);
// 0x000000F4 System.Void HeroUI::SetTarget(Map.Area)
extern void HeroUI_SetTarget_mE184F5ACE462C0BEC4348DA2AC654926CFFC401F (void);
// 0x000000F5 System.Void HeroUI::HideHeroUI()
extern void HeroUI_HideHeroUI_mCD64F46AC83B9BF1FEBBB53D306002EA79703119 (void);
// 0x000000F6 System.Collections.IEnumerator HeroUI::ChangeButtonColor()
extern void HeroUI_ChangeButtonColor_m57C3A34E9EFD62ECB84BB942B1369D742BD003B3 (void);
// 0x000000F7 System.Void HeroUI::Upgrade()
extern void HeroUI_Upgrade_m2200F32D9546740ACE0947DB0A1273DC4E6178FB (void);
// 0x000000F8 System.Void HeroUI::UpdateSlider()
extern void HeroUI_UpdateSlider_m6BC29E2EA6BE802981C4FDC3C4ABB6FD32CF254A (void);
// 0x000000F9 System.Void HeroUI::UpgradeHero()
extern void HeroUI_UpgradeHero_m27EE9516C460948699817C75002BAAAAFB525DFE (void);
// 0x000000FA System.Void HeroUI::UpdateLevel()
extern void HeroUI_UpdateLevel_mF08433D72311D0D92ED645915129D17A643C67EB (void);
// 0x000000FB System.Void HeroUI::EvoHero()
extern void HeroUI_EvoHero_mE05637FF195C442F64ADC9FADE12043944EA0BDF (void);
// 0x000000FC System.Collections.IEnumerator HeroUI::WaitForUpdateUi()
extern void HeroUI_WaitForUpdateUi_mB163302A7AC7999CDB49AD6C3F1E8D1891C6F1A4 (void);
// 0x000000FD System.Void HeroUI::SellHero()
extern void HeroUI_SellHero_mD120DEC2BCD2425BF46A97E768B4B161389296CB (void);
// 0x000000FE System.Void HeroUI::CheckHeroType()
extern void HeroUI_CheckHeroType_mD7BB6E270BA8AEEFCE513EF1792E95B5A2703ABF (void);
// 0x000000FF System.Void HeroUI::ShowAttackRange()
extern void HeroUI_ShowAttackRange_m8D2084F32ACD2685C52EAE3C749F3AE3D9F945E7 (void);
// 0x00000100 System.Void HeroUI::.ctor()
extern void HeroUI__ctor_m6EAC57B737C83863D29DF818694B7AA43AF62F94 (void);
// 0x00000101 System.Boolean HeroUI::<SetTarget>b__19_0(CardBluePrint)
extern void HeroUI_U3CSetTargetU3Eb__19_0_m1F49EFF4785EA6875D6F430195C5647A22F490D4 (void);
// 0x00000102 System.Boolean HeroUI::<EvoHero>b__26_0(CardBluePrint)
extern void HeroUI_U3CEvoHeroU3Eb__26_0_m890A8A5597D9EAAC6DAF8ED08FE6C8F3DF37798B (void);
// 0x00000103 ItemManager ItemManager::get_Instance()
extern void ItemManager_get_Instance_mC07F08CD08726E948EF32F708CA18982076190B6 (void);
// 0x00000104 System.Void ItemManager::set_Instance(ItemManager)
extern void ItemManager_set_Instance_m91F8EBA1EDB2F3C0C38B96B94567147A357531FC (void);
// 0x00000105 System.Void ItemManager::add_OnCandyUsed(System.Action)
extern void ItemManager_add_OnCandyUsed_mE11A67279CF917C592370DC3E5F19F43624F4B82 (void);
// 0x00000106 System.Void ItemManager::remove_OnCandyUsed(System.Action)
extern void ItemManager_remove_OnCandyUsed_m095FF8D6FEB3DE6137B118DB77A0E92606FFEF72 (void);
// 0x00000107 System.Void ItemManager::add_OnCookieUsed(System.Action)
extern void ItemManager_add_OnCookieUsed_m28C6881C57300A2FB437BFF027FF42500973E98E (void);
// 0x00000108 System.Void ItemManager::remove_OnCookieUsed(System.Action)
extern void ItemManager_remove_OnCookieUsed_mAC2B81471A8D57878164BE8A6905EB88B9AC9CF3 (void);
// 0x00000109 System.Void ItemManager::Awake()
extern void ItemManager_Awake_m38A9CB7617956210243C27F71C25FE547791586C (void);
// 0x0000010A System.Void ItemManager::FireWork()
extern void ItemManager_FireWork_m3633E8176747DF6245F055249A1C8F6E0C0684AC (void);
// 0x0000010B System.Void ItemManager::Cookie()
extern void ItemManager_Cookie_m016B249CE01E73A1E38AEA6E590D24485BFBE547 (void);
// 0x0000010C System.Collections.IEnumerator ItemManager::ActiveCookie30s()
extern void ItemManager_ActiveCookie30s_m2159094A265F281AD920E8F646A10FF9F2B429A3 (void);
// 0x0000010D System.Void ItemManager::Candy()
extern void ItemManager_Candy_mC7F7ECAB66A3949773BD0ECEC85C9199572FF69C (void);
// 0x0000010E System.Collections.IEnumerator ItemManager::ActiveCandy30s()
extern void ItemManager_ActiveCandy30s_mD5662C359943A8F22218D041056106C2FAFB2AF6 (void);
// 0x0000010F System.Void ItemManager::.ctor()
extern void ItemManager__ctor_mB4E542587417D4CED86516E00930C40F85219460 (void);
// 0x00000110 System.Void MainMenuManager::StartGame()
extern void MainMenuManager_StartGame_m2211BA6F6E02EDDCD05A6319868508E6E1F8C96B (void);
// 0x00000111 System.Void MainMenuManager::OpenUploadPhoto()
extern void MainMenuManager_OpenUploadPhoto_mAB911DA65BD02BFF796FC696813B2EDDE1269E6C (void);
// 0x00000112 System.Void MainMenuManager::.ctor()
extern void MainMenuManager__ctor_m39846598ADB8889025A11166F08497A1E99D13BF (void);
// 0x00000113 TimeManager TimeManager::get_Instance()
extern void TimeManager_get_Instance_m19259A61D1B020A332EC31181A9E667EF907151A (void);
// 0x00000114 System.Void TimeManager::set_Instance(TimeManager)
extern void TimeManager_set_Instance_mB699CF404EFA8C02A254633270C30C6E0EA0EFF6 (void);
// 0x00000115 System.Void TimeManager::Awake()
extern void TimeManager_Awake_mFBAAE1A728FE924382001A6378AE45F9F592999D (void);
// 0x00000116 System.Void TimeManager::Start()
extern void TimeManager_Start_m266A4C8445E0E6B1356A27AFF828649C364A62D3 (void);
// 0x00000117 System.Void TimeManager::Update()
extern void TimeManager_Update_m2FE11D93837B4183FDA835BBBACACE91FEFC9B3A (void);
// 0x00000118 System.Void TimeManager::Pause()
extern void TimeManager_Pause_m9479102F27E8F107AFEF234A8FE989E5BE6F96D0 (void);
// 0x00000119 System.Void TimeManager::Resume()
extern void TimeManager_Resume_mFA56BDC4CEE7EBBE2A7812CB150FED8606D18D95 (void);
// 0x0000011A System.Void TimeManager::GameOver()
extern void TimeManager_GameOver_m2C2A1E99AE84267A588678B82ED69C806A6D8B94 (void);
// 0x0000011B System.Collections.IEnumerator TimeManager::WaitForPanel()
extern void TimeManager_WaitForPanel_m68280F25263044850C9A68C113FDEC174C3D7625 (void);
// 0x0000011C System.Collections.IEnumerator TimeManager::WaitForUnPause()
extern void TimeManager_WaitForUnPause_mB05F71F812DAD1687CFF92931741779EA69EB465 (void);
// 0x0000011D System.Void TimeManager::AdsReward()
extern void TimeManager_AdsReward_mDC1E38660B356F05B0FCFD1A4C329B5675F59E93 (void);
// 0x0000011E System.Void TimeManager::.ctor()
extern void TimeManager__ctor_m29682988877F236DB723B6DA48DA89855B9934A7 (void);
// 0x0000011F System.Void MapGenerate::Start()
extern void MapGenerate_Start_m87A416E3BD8D9823C663C02B505EBEAB634B5BAB (void);
// 0x00000120 System.Collections.Generic.List`1<UnityEngine.GameObject> MapGenerate::getTopEdgeAreas()
extern void MapGenerate_getTopEdgeAreas_m99A213FEAACEAB2E074D44484E1EC3B47CD5DE22 (void);
// 0x00000121 System.Collections.Generic.List`1<UnityEngine.GameObject> MapGenerate::getBottomEdgeAreas()
extern void MapGenerate_getBottomEdgeAreas_m4275F34434287FC2E2BD013A524AC33258712FBE (void);
// 0x00000122 System.Void MapGenerate::GenerateMap()
extern void MapGenerate_GenerateMap_mD2373E341C842CE69C9F57746801194E8BC0718A (void);
// 0x00000123 System.Void MapGenerate::.ctor()
extern void MapGenerate__ctor_m8EF35BCBED274A480CD54BF60D4B67CC3BE770DD (void);
// 0x00000124 System.Void SafeAreaSetter::Start()
extern void SafeAreaSetter_Start_m83780A11588277507500E26788A2EA69E09FF392 (void);
// 0x00000125 System.Void SafeAreaSetter::ApplySafeArea()
extern void SafeAreaSetter_ApplySafeArea_m4F4F38103E2C3689B92117BF87F0E7559B48CB25 (void);
// 0x00000126 System.Void SafeAreaSetter::Update()
extern void SafeAreaSetter_Update_mF64C75E3889BDD924F5886A3AFF934BC395A5E01 (void);
// 0x00000127 System.Void SafeAreaSetter::.ctor()
extern void SafeAreaSetter__ctor_mA93E55DED3B5E17188E4D3CC093573F5E1BC696B (void);
// 0x00000128 ChainSkill ChainSkill::get_Instance()
extern void ChainSkill_get_Instance_mFF57A1E3160BADBC15485C65C3459453C8388C8E (void);
// 0x00000129 System.Void ChainSkill::set_Instance(ChainSkill)
extern void ChainSkill_set_Instance_m463EFBE4C3064E87C11ECFB2FBD865ADF6E335A5 (void);
// 0x0000012A System.Void ChainSkill::Awake()
extern void ChainSkill_Awake_mC5E9C708E5F6FCA441E0790222686142EC016586 (void);
// 0x0000012B System.Void ChainSkill::Start()
extern void ChainSkill_Start_mC8744523813A83A5A94B9B7699588D8798CE64B5 (void);
// 0x0000012C System.Void ChainSkill::GetCompGameOver()
extern void ChainSkill_GetCompGameOver_mEB67BD090A0FA99296477DE170E104A3B1E95C99 (void);
// 0x0000012D System.Void ChainSkill::CheckSkillChainActive()
extern void ChainSkill_CheckSkillChainActive_m123F7C619409D75D13F8E7BA55EA763832AFC328 (void);
// 0x0000012E System.Void ChainSkill::AddHeroToField(Manager.HeroBluePrint)
extern void ChainSkill_AddHeroToField_mC4E2F4D3A707D7483A35840CCEA93D7A95AE5C42 (void);
// 0x0000012F System.Void ChainSkill::RemoveHeroInField(Manager.HeroBluePrint)
extern void ChainSkill_RemoveHeroInField_mE4877B0CA855F3D4E2F8B7E9160A06D40EF9B317 (void);
// 0x00000130 System.Void ChainSkill::SetBuff(SkillBluePrint)
extern void ChainSkill_SetBuff_m3D524A1E8D7D21AC8C6BA9C2D4BD93AFC53B6B6E (void);
// 0x00000131 System.Void ChainSkill::RemoveBuff(SkillBluePrint)
extern void ChainSkill_RemoveBuff_m86021B3D4839A440243460AAA9362C7FD1B1C480 (void);
// 0x00000132 System.Void ChainSkill::FindBuild(System.Collections.Generic.List`1<Manager.HeroBluePrint>,SkillBluePrint,System.Collections.Generic.List`1<Manager.HeroName>)
extern void ChainSkill_FindBuild_m413B5AD731302D27C5D979F21B8B6EC6FE0313F6 (void);
// 0x00000133 System.Void ChainSkill::FindElementBuild()
extern void ChainSkill_FindElementBuild_m2348A7F0B91B94352F188F89AEB72CFA296CAD76 (void);
// 0x00000134 System.Void ChainSkill::.ctor()
extern void ChainSkill__ctor_m956A177A4F7FECBCB2AB5FD95BC7D286B636CACF (void);
// 0x00000135 System.Void ChatController::OnEnable()
extern void ChatController_OnEnable_mBC3BFC05A1D47069DFA58A4F0E39CFF8FAD44FF2 (void);
// 0x00000136 System.Void ChatController::OnDisable()
extern void ChatController_OnDisable_m268A7488A3FDEB850FC3BC91A0BBDA767D42876B (void);
// 0x00000137 System.Void ChatController::AddToChatOutput(System.String)
extern void ChatController_AddToChatOutput_m43856EEA133E04C24701A2616E7F10D7FFA68371 (void);
// 0x00000138 System.Void ChatController::.ctor()
extern void ChatController__ctor_m3B66A5F749B457D865E8BDA1DE481C8CF1158026 (void);
// 0x00000139 System.Void DropdownSample::OnButtonClick()
extern void DropdownSample_OnButtonClick_m97641451A2EAC4FC6682897A948A37FDD6BF9EA6 (void);
// 0x0000013A System.Void DropdownSample::.ctor()
extern void DropdownSample__ctor_m87664E3DC75A1A9AA19FEE6ED6B993AB1E50E2B9 (void);
// 0x0000013B System.Void EnvMapAnimator::Awake()
extern void EnvMapAnimator_Awake_mFFC04BC5320F83CA8C45FF36A11BF43AC17C6B93 (void);
// 0x0000013C System.Collections.IEnumerator EnvMapAnimator::Start()
extern void EnvMapAnimator_Start_mC0D348CAB0F0DC920EF3D3A008688B533F66D1BE (void);
// 0x0000013D System.Void EnvMapAnimator::.ctor()
extern void EnvMapAnimator__ctor_mC151D673A394E2E7CEA8774C4004985028C5C3EC (void);
// 0x0000013E System.Char TMPro.TMP_DigitValidator::Validate(System.String&,System.Int32&,System.Char)
extern void TMP_DigitValidator_Validate_m5D303EB8CD6E9E7D526D633BA1021884051FE226 (void);
// 0x0000013F System.Void TMPro.TMP_DigitValidator::.ctor()
extern void TMP_DigitValidator__ctor_m1E838567EF38170662F1BAF52A847CC2C258333E (void);
// 0x00000140 System.Char TMPro.TMP_PhoneNumberValidator::Validate(System.String&,System.Int32&,System.Char)
extern void TMP_PhoneNumberValidator_Validate_mF0E90A277E9E91BC213DD02DC60088D03C9436B1 (void);
// 0x00000141 System.Void TMPro.TMP_PhoneNumberValidator::.ctor()
extern void TMP_PhoneNumberValidator__ctor_mB3C36CAAE3B52554C44A1D19194F0176B5A8EED3 (void);
// 0x00000142 TMPro.TMP_TextEventHandler/CharacterSelectionEvent TMPro.TMP_TextEventHandler::get_onCharacterSelection()
extern void TMP_TextEventHandler_get_onCharacterSelection_m90C39320C726E8E542D91F4BBD690697349F5385 (void);
// 0x00000143 System.Void TMPro.TMP_TextEventHandler::set_onCharacterSelection(TMPro.TMP_TextEventHandler/CharacterSelectionEvent)
extern void TMP_TextEventHandler_set_onCharacterSelection_m5ED7658EB101C6740A921FA150DE18C443BDA0C4 (void);
// 0x00000144 TMPro.TMP_TextEventHandler/SpriteSelectionEvent TMPro.TMP_TextEventHandler::get_onSpriteSelection()
extern void TMP_TextEventHandler_get_onSpriteSelection_m0E645AE1DFE19B011A3319474D0CF0DA612C8B7B (void);
// 0x00000145 System.Void TMPro.TMP_TextEventHandler::set_onSpriteSelection(TMPro.TMP_TextEventHandler/SpriteSelectionEvent)
extern void TMP_TextEventHandler_set_onSpriteSelection_m4AFC6772C3357218956A5D33B3CD19F3AAF39788 (void);
// 0x00000146 TMPro.TMP_TextEventHandler/WordSelectionEvent TMPro.TMP_TextEventHandler::get_onWordSelection()
extern void TMP_TextEventHandler_get_onWordSelection_mA42B89A37810FB659FCFA8539339A3BB8037203A (void);
// 0x00000147 System.Void TMPro.TMP_TextEventHandler::set_onWordSelection(TMPro.TMP_TextEventHandler/WordSelectionEvent)
extern void TMP_TextEventHandler_set_onWordSelection_m4A839FAFFABAFECD073B82BA8826E1CD033C0076 (void);
// 0x00000148 TMPro.TMP_TextEventHandler/LineSelectionEvent TMPro.TMP_TextEventHandler::get_onLineSelection()
extern void TMP_TextEventHandler_get_onLineSelection_mB701B6C713AD4EFC61E1B30A564EE54ADE31F58D (void);
// 0x00000149 System.Void TMPro.TMP_TextEventHandler::set_onLineSelection(TMPro.TMP_TextEventHandler/LineSelectionEvent)
extern void TMP_TextEventHandler_set_onLineSelection_m0B2337598350E51D0A17B8FCB3AAA533F312F3DA (void);
// 0x0000014A TMPro.TMP_TextEventHandler/LinkSelectionEvent TMPro.TMP_TextEventHandler::get_onLinkSelection()
extern void TMP_TextEventHandler_get_onLinkSelection_mF5C3875D661F5B1E3712566FE16D332EA37D15BB (void);
// 0x0000014B System.Void TMPro.TMP_TextEventHandler::set_onLinkSelection(TMPro.TMP_TextEventHandler/LinkSelectionEvent)
extern void TMP_TextEventHandler_set_onLinkSelection_m200566EDEB2C9299647F3EEAC588B51818D360A5 (void);
// 0x0000014C System.Void TMPro.TMP_TextEventHandler::Awake()
extern void TMP_TextEventHandler_Awake_m43EB03A4A6776A624F79457EC49E78E7B5BA1C70 (void);
// 0x0000014D System.Void TMPro.TMP_TextEventHandler::LateUpdate()
extern void TMP_TextEventHandler_LateUpdate_mE1D989C40DA8E54E116E3C60217DFCAADD6FDE11 (void);
// 0x0000014E System.Void TMPro.TMP_TextEventHandler::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextEventHandler_OnPointerEnter_m8FC88F25858B24CE68BE80C727A3F0227A8EE5AC (void);
// 0x0000014F System.Void TMPro.TMP_TextEventHandler::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextEventHandler_OnPointerExit_mBB2A74D55741F631A678A5D6997D40247FE75D44 (void);
// 0x00000150 System.Void TMPro.TMP_TextEventHandler::SendOnCharacterSelection(System.Char,System.Int32)
extern void TMP_TextEventHandler_SendOnCharacterSelection_m78983D3590F1B0C242BEAB0A11FDBDABDD1814EC (void);
// 0x00000151 System.Void TMPro.TMP_TextEventHandler::SendOnSpriteSelection(System.Char,System.Int32)
extern void TMP_TextEventHandler_SendOnSpriteSelection_m10C257A74F121B95E7077F7E488FBC52380A6C53 (void);
// 0x00000152 System.Void TMPro.TMP_TextEventHandler::SendOnWordSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventHandler_SendOnWordSelection_m95A4E5A00E339E5B8BA9AA63B98DB3E81C8B8F66 (void);
// 0x00000153 System.Void TMPro.TMP_TextEventHandler::SendOnLineSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventHandler_SendOnLineSelection_mE85BA6ECE1188B666FD36839B8970C3E0130BC43 (void);
// 0x00000154 System.Void TMPro.TMP_TextEventHandler::SendOnLinkSelection(System.String,System.String,System.Int32)
extern void TMP_TextEventHandler_SendOnLinkSelection_m299E6620DFD835C8258A3F48B4EA076C304E9B77 (void);
// 0x00000155 System.Void TMPro.TMP_TextEventHandler::.ctor()
extern void TMP_TextEventHandler__ctor_m6345DC1CEA2E4209E928AD1E61C3ACA4227DD9B8 (void);
// 0x00000156 System.Collections.IEnumerator TMPro.Examples.Benchmark01::Start()
extern void Benchmark01_Start_mE7E5146B0D8D926CC410CAA48F3B617A0A7D955C (void);
// 0x00000157 System.Void TMPro.Examples.Benchmark01::.ctor()
extern void Benchmark01__ctor_mB92568AA7A9E13B92315B6270FCA23584A7D0F7F (void);
// 0x00000158 System.Collections.IEnumerator TMPro.Examples.Benchmark01_UGUI::Start()
extern void Benchmark01_UGUI_Start_m2F8F9E1798943DA8086A7C7E73BA9D7C67482BD4 (void);
// 0x00000159 System.Void TMPro.Examples.Benchmark01_UGUI::.ctor()
extern void Benchmark01_UGUI__ctor_mACFE8D997EAB50FDBD7F671C1A25A892D9F78376 (void);
// 0x0000015A System.Void TMPro.Examples.Benchmark02::Start()
extern void Benchmark02_Start_m315C0DF3A9AC66B4F5802FDAED056E7E5F3D2046 (void);
// 0x0000015B System.Void TMPro.Examples.Benchmark02::.ctor()
extern void Benchmark02__ctor_m526B38D3B7E75905208E4E57B168D8AC1900F4E5 (void);
// 0x0000015C System.Void TMPro.Examples.Benchmark03::Awake()
extern void Benchmark03_Awake_mD23429C1EE428EEF4ED9A753385BF194BAA41A77 (void);
// 0x0000015D System.Void TMPro.Examples.Benchmark03::Start()
extern void Benchmark03_Start_m19E638DED29CB7F96362F3C346DC47217C4AF516 (void);
// 0x0000015E System.Void TMPro.Examples.Benchmark03::.ctor()
extern void Benchmark03__ctor_m89F9102259BE1694EC418E9023DC563F3999B0BE (void);
// 0x0000015F System.Void TMPro.Examples.Benchmark04::Start()
extern void Benchmark04_Start_mEABD467C12547066E33359FDC433E8B5BAD43DB9 (void);
// 0x00000160 System.Void TMPro.Examples.Benchmark04::.ctor()
extern void Benchmark04__ctor_m5015375E0C8D19060CB5DE14CBF4AC02DDD70E7C (void);
// 0x00000161 System.Void TMPro.Examples.CameraController::Awake()
extern void CameraController_Awake_m420393892377B9703EC97764B34E32890FC5283E (void);
// 0x00000162 System.Void TMPro.Examples.CameraController::Start()
extern void CameraController_Start_m32D90EE7232BE6DE08D224F824F8EB9571655610 (void);
// 0x00000163 System.Void TMPro.Examples.CameraController::LateUpdate()
extern void CameraController_LateUpdate_m6D81DEBA4E8B443CF2AD8288F0E76E3A6B3B5373 (void);
// 0x00000164 System.Void TMPro.Examples.CameraController::GetPlayerInput()
extern void CameraController_GetPlayerInput_m6695FE20CFC691585A6AC279EDB338EC9DD13FE3 (void);
// 0x00000165 System.Void TMPro.Examples.CameraController::.ctor()
extern void CameraController__ctor_m09187FB27B590118043D4DC7B89B93164124C124 (void);
// 0x00000166 System.Void TMPro.Examples.ObjectSpin::Awake()
extern void ObjectSpin_Awake_mC1EB9630B6D3BAE645D3DD79C264F71F7B18A1AA (void);
// 0x00000167 System.Void TMPro.Examples.ObjectSpin::Update()
extern void ObjectSpin_Update_mD39DCBA0789DC0116037C442F0BA1EE6752E36D3 (void);
// 0x00000168 System.Void TMPro.Examples.ObjectSpin::.ctor()
extern void ObjectSpin__ctor_m1827B9648659746252026432DFED907AEC6007FC (void);
// 0x00000169 System.Void TMPro.Examples.ShaderPropAnimator::Awake()
extern void ShaderPropAnimator_Awake_mE04C66A41CA53AB73733E7D2CCD21B30A360C5A8 (void);
// 0x0000016A System.Void TMPro.Examples.ShaderPropAnimator::Start()
extern void ShaderPropAnimator_Start_mC5AC59C59AF2F71E0CF65F11ACD787488140EDD2 (void);
// 0x0000016B System.Collections.IEnumerator TMPro.Examples.ShaderPropAnimator::AnimateProperties()
extern void ShaderPropAnimator_AnimateProperties_mAF49CD157AD41377CE00AA10F0C06C8BF5AA0469 (void);
// 0x0000016C System.Void TMPro.Examples.ShaderPropAnimator::.ctor()
extern void ShaderPropAnimator__ctor_mAA626BC8AEEB00C5AE362FE8690D3F2200CE6E64 (void);
// 0x0000016D System.Void TMPro.Examples.SimpleScript::Start()
extern void SimpleScript_Start_m75CD9CEDCAFA9A991753478D7C28407C7329FB8F (void);
// 0x0000016E System.Void TMPro.Examples.SimpleScript::Update()
extern void SimpleScript_Update_mE8C3930DCC1767C2DF59B622FABD188E6FB91BE5 (void);
// 0x0000016F System.Void TMPro.Examples.SimpleScript::.ctor()
extern void SimpleScript__ctor_mEB370A69544227EF04C48C604A28502ADAA73697 (void);
// 0x00000170 System.Void TMPro.Examples.SkewTextExample::Awake()
extern void SkewTextExample_Awake_m6FBA7E7DC9AFDD3099F0D0B9CDE91574551105DB (void);
// 0x00000171 System.Void TMPro.Examples.SkewTextExample::Start()
extern void SkewTextExample_Start_m536F82F3A5D229332694688C59F396E07F88153F (void);
// 0x00000172 UnityEngine.AnimationCurve TMPro.Examples.SkewTextExample::CopyAnimationCurve(UnityEngine.AnimationCurve)
extern void SkewTextExample_CopyAnimationCurve_m555177255F5828DBC7E677ED06F7EFFC052886B3 (void);
// 0x00000173 System.Collections.IEnumerator TMPro.Examples.SkewTextExample::WarpText()
extern void SkewTextExample_WarpText_m0E0C46988600673F0E5DFA3133534DC6CA5950D3 (void);
// 0x00000174 System.Void TMPro.Examples.SkewTextExample::.ctor()
extern void SkewTextExample__ctor_m945059906734DD38CF860CD32B3E07635D0E1F86 (void);
// 0x00000175 System.Void TMPro.Examples.TMP_ExampleScript_01::Awake()
extern void TMP_ExampleScript_01_Awake_m381EF5B50E1D012B8CA1883DEFF604EEAE6D95F4 (void);
// 0x00000176 System.Void TMPro.Examples.TMP_ExampleScript_01::Update()
extern void TMP_ExampleScript_01_Update_m31611F22A608784F164A24444195B33B714567FB (void);
// 0x00000177 System.Void TMPro.Examples.TMP_ExampleScript_01::.ctor()
extern void TMP_ExampleScript_01__ctor_m3641F2E0D25B6666CE77773A4A493C4C4D3D3A12 (void);
// 0x00000178 System.Void TMPro.Examples.TMP_FrameRateCounter::Awake()
extern void TMP_FrameRateCounter_Awake_m958B668086DB4A38D9C56E3F8C2DCCB6FF11FAA3 (void);
// 0x00000179 System.Void TMPro.Examples.TMP_FrameRateCounter::Start()
extern void TMP_FrameRateCounter_Start_mC642CA714D0FB8482D2AC6192D976DA179EE3720 (void);
// 0x0000017A System.Void TMPro.Examples.TMP_FrameRateCounter::Update()
extern void TMP_FrameRateCounter_Update_m5E41B55573D86C3D345BFE11C489B00229BCEE21 (void);
// 0x0000017B System.Void TMPro.Examples.TMP_FrameRateCounter::Set_FrameCounter_Position(TMPro.Examples.TMP_FrameRateCounter/FpsCounterAnchorPositions)
extern void TMP_FrameRateCounter_Set_FrameCounter_Position_mA72ECDE464E3290E4F533491FC8113B8D4068BE1 (void);
// 0x0000017C System.Void TMPro.Examples.TMP_FrameRateCounter::.ctor()
extern void TMP_FrameRateCounter__ctor_mC79D5BF3FAE2BB7D22D9955A75E3483725BA619C (void);
// 0x0000017D System.Void TMPro.Examples.TMP_TextEventCheck::OnEnable()
extern void TMP_TextEventCheck_OnEnable_mE6D5125EEE0720E6F414978A496066E7CF1592DF (void);
// 0x0000017E System.Void TMPro.Examples.TMP_TextEventCheck::OnDisable()
extern void TMP_TextEventCheck_OnDisable_m94F087C259890C48D4F5464ABA4CD1D0E5863A9A (void);
// 0x0000017F System.Void TMPro.Examples.TMP_TextEventCheck::OnCharacterSelection(System.Char,System.Int32)
extern void TMP_TextEventCheck_OnCharacterSelection_mDA044F0808D61A99CDA374075943CEB1C92C253D (void);
// 0x00000180 System.Void TMPro.Examples.TMP_TextEventCheck::OnSpriteSelection(System.Char,System.Int32)
extern void TMP_TextEventCheck_OnSpriteSelection_mE8FFA550F38F5447CA37205698A30A41ADE901E0 (void);
// 0x00000181 System.Void TMPro.Examples.TMP_TextEventCheck::OnWordSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventCheck_OnWordSelection_m6037166D18A938678A2B06F28A5DCB3E09FAC61B (void);
// 0x00000182 System.Void TMPro.Examples.TMP_TextEventCheck::OnLineSelection(System.String,System.Int32,System.Int32)
extern void TMP_TextEventCheck_OnLineSelection_mD2BAA6C8ABD61F0442A40C5448FA7774D782C363 (void);
// 0x00000183 System.Void TMPro.Examples.TMP_TextEventCheck::OnLinkSelection(System.String,System.String,System.Int32)
extern void TMP_TextEventCheck_OnLinkSelection_m184B11D5E35AC4EA7CDCE3340AB9FEE3C14BF41C (void);
// 0x00000184 System.Void TMPro.Examples.TMP_TextEventCheck::.ctor()
extern void TMP_TextEventCheck__ctor_mA5E82EE7CE8F8836FC6CF3823CBC7356005B898B (void);
// 0x00000185 System.Void TMPro.Examples.TMP_TextInfoDebugTool::.ctor()
extern void TMP_TextInfoDebugTool__ctor_mF6AA30660FBD4CE708B6147833853498993CB9EE (void);
// 0x00000186 System.Void TMPro.Examples.TMP_TextSelector_A::Awake()
extern void TMP_TextSelector_A_Awake_mD7252A5075E30E3BF9BEF4353F7AA2A9203FC943 (void);
// 0x00000187 System.Void TMPro.Examples.TMP_TextSelector_A::LateUpdate()
extern void TMP_TextSelector_A_LateUpdate_mDBBC09726332EDDEAF7C30AB6C08FB33261F79FD (void);
// 0x00000188 System.Void TMPro.Examples.TMP_TextSelector_A::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_A_OnPointerEnter_mC19B85FB5B4E6EB47832C39F0115A513B85060D0 (void);
// 0x00000189 System.Void TMPro.Examples.TMP_TextSelector_A::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_A_OnPointerExit_mE51C850F54B18A5C96E3BE015DFBB0F079E5AE66 (void);
// 0x0000018A System.Void TMPro.Examples.TMP_TextSelector_A::.ctor()
extern void TMP_TextSelector_A__ctor_mEB83D3952B32CEE9A871EC60E3AE9B79048302BF (void);
// 0x0000018B System.Void TMPro.Examples.TMP_TextSelector_B::Awake()
extern void TMP_TextSelector_B_Awake_m7E413A43C54DF9E0FE70C4E69FC682391B47205A (void);
// 0x0000018C System.Void TMPro.Examples.TMP_TextSelector_B::OnEnable()
extern void TMP_TextSelector_B_OnEnable_m0828D13E2D407B90038442228D54FB0D7B3D29FB (void);
// 0x0000018D System.Void TMPro.Examples.TMP_TextSelector_B::OnDisable()
extern void TMP_TextSelector_B_OnDisable_m2B559A85B52C8CAFC7350CC7B4F8E5BC773EF781 (void);
// 0x0000018E System.Void TMPro.Examples.TMP_TextSelector_B::ON_TEXT_CHANGED(UnityEngine.Object)
extern void TMP_TextSelector_B_ON_TEXT_CHANGED_m1597DBE7C7EBE7CFA4DC395897A4779387B59910 (void);
// 0x0000018F System.Void TMPro.Examples.TMP_TextSelector_B::LateUpdate()
extern void TMP_TextSelector_B_LateUpdate_m8BA10E368C7F3483E9EC05589BBE45D7EFC75691 (void);
// 0x00000190 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerEnter(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerEnter_m0A0064632E4C0E0ADCD4358AD5BC168BFB74AC4D (void);
// 0x00000191 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerExit(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerExit_m667659102B5B17FBAF56593B7034E5EC1C48D43F (void);
// 0x00000192 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerClick(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerClick_m8DB2D22EE2F4D3965115791C81F985D82021469F (void);
// 0x00000193 System.Void TMPro.Examples.TMP_TextSelector_B::OnPointerUp(UnityEngine.EventSystems.PointerEventData)
extern void TMP_TextSelector_B_OnPointerUp_m303500BE309B56F3ADCE8B461CEC50C8D5ED70BC (void);
// 0x00000194 System.Void TMPro.Examples.TMP_TextSelector_B::RestoreCachedVertexAttributes(System.Int32)
extern void TMP_TextSelector_B_RestoreCachedVertexAttributes_m3D637CF2C6CA663922EBE56FFD672BE582E9F1F2 (void);
// 0x00000195 System.Void TMPro.Examples.TMP_TextSelector_B::.ctor()
extern void TMP_TextSelector_B__ctor_mE654F9F1570570C4BACDA79640B8DB3033D91C33 (void);
// 0x00000196 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Awake()
extern void TMP_UiFrameRateCounter_Awake_m83DBE22B6CC551BE5E385048B92067679716179E (void);
// 0x00000197 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Start()
extern void TMP_UiFrameRateCounter_Start_m59DA342A492C9EF8AD5C4512753211BF3796C944 (void);
// 0x00000198 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Update()
extern void TMP_UiFrameRateCounter_Update_m28FC233C475AA15A3BE399BF9345977089997749 (void);
// 0x00000199 System.Void TMPro.Examples.TMP_UiFrameRateCounter::Set_FrameCounter_Position(TMPro.Examples.TMP_UiFrameRateCounter/FpsCounterAnchorPositions)
extern void TMP_UiFrameRateCounter_Set_FrameCounter_Position_m2025933902F312C0EC4B6A864196A8BA8D545647 (void);
// 0x0000019A System.Void TMPro.Examples.TMP_UiFrameRateCounter::.ctor()
extern void TMP_UiFrameRateCounter__ctor_m5774BF4B9389770FC34991095557B24417600F84 (void);
// 0x0000019B System.Void TMPro.Examples.TMPro_InstructionOverlay::Awake()
extern void TMPro_InstructionOverlay_Awake_m2444EA8749D72BCEA4DC30A328E3745AB19062EC (void);
// 0x0000019C System.Void TMPro.Examples.TMPro_InstructionOverlay::Set_FrameCounter_Position(TMPro.Examples.TMPro_InstructionOverlay/FpsCounterAnchorPositions)
extern void TMPro_InstructionOverlay_Set_FrameCounter_Position_m62B897E4DB2D6B1936833EC54D9C246D68D50EEB (void);
// 0x0000019D System.Void TMPro.Examples.TMPro_InstructionOverlay::.ctor()
extern void TMPro_InstructionOverlay__ctor_m7AB5B851B4BFB07547E460D6B7B9D969DEB4A7CF (void);
// 0x0000019E System.Void TMPro.Examples.TeleType::Awake()
extern void TeleType_Awake_m099FCB202F2A8299B133DC56ECB9D01A16C08DFE (void);
// 0x0000019F System.Collections.IEnumerator TMPro.Examples.TeleType::Start()
extern void TeleType_Start_m72DE9DE597F4FA0B1CA02115CBC1E74EB53F63F7 (void);
// 0x000001A0 System.Void TMPro.Examples.TeleType::.ctor()
extern void TeleType__ctor_m12A79B34F66CBFDCA8F582F0689D1731586B90AF (void);
// 0x000001A1 System.Void TMPro.Examples.TextConsoleSimulator::Awake()
extern void TextConsoleSimulator_Awake_mA51EE182A528CCA5CAEA8DBE8AD30E43FDFAE7B0 (void);
// 0x000001A2 System.Void TMPro.Examples.TextConsoleSimulator::Start()
extern void TextConsoleSimulator_Start_m429701BE4C9A52AA6B257172A4D85D58E091E7DA (void);
// 0x000001A3 System.Void TMPro.Examples.TextConsoleSimulator::OnEnable()
extern void TextConsoleSimulator_OnEnable_m3397FBCDA8D9DA264D2149288BE4DCF63368AB50 (void);
// 0x000001A4 System.Void TMPro.Examples.TextConsoleSimulator::OnDisable()
extern void TextConsoleSimulator_OnDisable_m43DF869E864789E215873192ECFCDC51ABA79712 (void);
// 0x000001A5 System.Void TMPro.Examples.TextConsoleSimulator::ON_TEXT_CHANGED(UnityEngine.Object)
extern void TextConsoleSimulator_ON_TEXT_CHANGED_m51F06EE5DD9B32FEFB529743F209C6E6C41BE3B8 (void);
// 0x000001A6 System.Collections.IEnumerator TMPro.Examples.TextConsoleSimulator::RevealCharacters(TMPro.TMP_Text)
extern void TextConsoleSimulator_RevealCharacters_mE8E415644F7BD2056D0809F7BB0FA9A2F9FE8534 (void);
// 0x000001A7 System.Collections.IEnumerator TMPro.Examples.TextConsoleSimulator::RevealWords(TMPro.TMP_Text)
extern void TextConsoleSimulator_RevealWords_mFDBC863D30BC63ADCF2860F303AF252E27D0F4F4 (void);
// 0x000001A8 System.Void TMPro.Examples.TextConsoleSimulator::.ctor()
extern void TextConsoleSimulator__ctor_m4719FB9D4D89F37234757D93876DF0193E8E2848 (void);
// 0x000001A9 System.Void TMPro.Examples.TextMeshProFloatingText::Awake()
extern void TextMeshProFloatingText_Awake_m679E597FF18E192C1FBD263D0E5ECEA392412046 (void);
// 0x000001AA System.Void TMPro.Examples.TextMeshProFloatingText::Start()
extern void TextMeshProFloatingText_Start_m68E511DEEDA883FE0F0999B329451F3A8A7269B1 (void);
// 0x000001AB System.Collections.IEnumerator TMPro.Examples.TextMeshProFloatingText::DisplayTextMeshProFloatingText()
extern void TextMeshProFloatingText_DisplayTextMeshProFloatingText_m3C381B8A53C58CF001D7A9212B8BDA6F368CC2C7 (void);
// 0x000001AC System.Collections.IEnumerator TMPro.Examples.TextMeshProFloatingText::DisplayTextMeshFloatingText()
extern void TextMeshProFloatingText_DisplayTextMeshFloatingText_m22691E6EA41B7FCF782B03954865D8E3B890E4DF (void);
// 0x000001AD System.Void TMPro.Examples.TextMeshProFloatingText::.ctor()
extern void TextMeshProFloatingText__ctor_m968E2691E21A93C010CF8205BC3666ADF712457E (void);
// 0x000001AE System.Void TMPro.Examples.TextMeshProFloatingText::.cctor()
extern void TextMeshProFloatingText__cctor_m5FF3949FBDB0FD13908FB7349F64DDB3012512B8 (void);
// 0x000001AF System.Void TMPro.Examples.TextMeshSpawner::Awake()
extern void TextMeshSpawner_Awake_mDF8CCC9C6B7380D6FA027263CDC3BC00EF75AEFB (void);
// 0x000001B0 System.Void TMPro.Examples.TextMeshSpawner::Start()
extern void TextMeshSpawner_Start_m7CC21883CA786A846A44921D2E37C201C25439BA (void);
// 0x000001B1 System.Void TMPro.Examples.TextMeshSpawner::.ctor()
extern void TextMeshSpawner__ctor_m1255777673BE591F62B4FC55EB999DBD7A7CB5A1 (void);
// 0x000001B2 System.Void TMPro.Examples.VertexColorCycler::Awake()
extern void VertexColorCycler_Awake_m84B4548078500DA811F3ADFF66186373BE8EDABC (void);
// 0x000001B3 System.Void TMPro.Examples.VertexColorCycler::Start()
extern void VertexColorCycler_Start_mC3FF90808EDD6A02F08375E909254778D5268B66 (void);
// 0x000001B4 System.Collections.IEnumerator TMPro.Examples.VertexColorCycler::AnimateVertexColors()
extern void VertexColorCycler_AnimateVertexColors_m6960F777E4876DFCA726BCAE7A8163850D58FA42 (void);
// 0x000001B5 System.Void TMPro.Examples.VertexColorCycler::.ctor()
extern void VertexColorCycler__ctor_m09990A8066C8A957A96CDBEBDA980399283B45E9 (void);
// 0x000001B6 System.Void TMPro.Examples.VertexJitter::Awake()
extern void VertexJitter_Awake_m9C5586A35BD9C928455D6479C93C1CE095447D8F (void);
// 0x000001B7 System.Void TMPro.Examples.VertexJitter::OnEnable()
extern void VertexJitter_OnEnable_m97ED60DBD350C72D1436ADFB8009A6F33A78C825 (void);
// 0x000001B8 System.Void TMPro.Examples.VertexJitter::OnDisable()
extern void VertexJitter_OnDisable_mF8D32D6E02E41A73C3E958FB6EE5D1D659D2A846 (void);
// 0x000001B9 System.Void TMPro.Examples.VertexJitter::Start()
extern void VertexJitter_Start_m8A0FED7ED16F90DBF287E09BFCBD7B26E07DBF97 (void);
// 0x000001BA System.Void TMPro.Examples.VertexJitter::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexJitter_ON_TEXT_CHANGED_mC7AB0113F6823D4FF415A096314B55D9C1BE9549 (void);
// 0x000001BB System.Collections.IEnumerator TMPro.Examples.VertexJitter::AnimateVertexColors()
extern void VertexJitter_AnimateVertexColors_mECAE037FC0CBA52CAC71C0B61E88829FF18BCC16 (void);
// 0x000001BC System.Void TMPro.Examples.VertexJitter::.ctor()
extern void VertexJitter__ctor_m550C9169D6FCD6F60D6AABCB8B4955DF58A12DCE (void);
// 0x000001BD System.Void TMPro.Examples.VertexShakeA::Awake()
extern void VertexShakeA_Awake_m005C6F9EE8A8FD816CD3A736D6AF199CD2B615A2 (void);
// 0x000001BE System.Void TMPro.Examples.VertexShakeA::OnEnable()
extern void VertexShakeA_OnEnable_m9F60F3951A7D0DF4201A0409F0ADC05243D324EA (void);
// 0x000001BF System.Void TMPro.Examples.VertexShakeA::OnDisable()
extern void VertexShakeA_OnDisable_m59B86895C03B278B2E634A7C68CC942344A8D2D2 (void);
// 0x000001C0 System.Void TMPro.Examples.VertexShakeA::Start()
extern void VertexShakeA_Start_m3E623C28F873F54F4AC7579433C7C50B2A3319DE (void);
// 0x000001C1 System.Void TMPro.Examples.VertexShakeA::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexShakeA_ON_TEXT_CHANGED_m7AD31F3239351E0916E4D02148F46A80DC148D7E (void);
// 0x000001C2 System.Collections.IEnumerator TMPro.Examples.VertexShakeA::AnimateVertexColors()
extern void VertexShakeA_AnimateVertexColors_mC451E2732A4E3E90E2553811AD75903EEF974599 (void);
// 0x000001C3 System.Void TMPro.Examples.VertexShakeA::.ctor()
extern void VertexShakeA__ctor_m7E6FD1700BD08616532AF22B3B489A18B88DFB62 (void);
// 0x000001C4 System.Void TMPro.Examples.VertexShakeB::Awake()
extern void VertexShakeB_Awake_m99C9A0474DBFE462DC88C898F958C1805376F9D5 (void);
// 0x000001C5 System.Void TMPro.Examples.VertexShakeB::OnEnable()
extern void VertexShakeB_OnEnable_m64299258797D745CDFE7F3CBEC4708BDBC7C3971 (void);
// 0x000001C6 System.Void TMPro.Examples.VertexShakeB::OnDisable()
extern void VertexShakeB_OnDisable_m07D520A8D7BCD8D188CE9F5CC7845F47D5AD6EF4 (void);
// 0x000001C7 System.Void TMPro.Examples.VertexShakeB::Start()
extern void VertexShakeB_Start_m9642281210FA5F701A324B78850331E4638B2DD1 (void);
// 0x000001C8 System.Void TMPro.Examples.VertexShakeB::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexShakeB_ON_TEXT_CHANGED_m5650D69D258D0EEF35AF390D6D5086B327B308A5 (void);
// 0x000001C9 System.Collections.IEnumerator TMPro.Examples.VertexShakeB::AnimateVertexColors()
extern void VertexShakeB_AnimateVertexColors_mDF7A7E7028361D288DF588D9301541E4FA1EFA87 (void);
// 0x000001CA System.Void TMPro.Examples.VertexShakeB::.ctor()
extern void VertexShakeB__ctor_m605A778C36B506B5763A1AE17B01F8DCCBCD51EC (void);
// 0x000001CB System.Void TMPro.Examples.VertexZoom::Awake()
extern void VertexZoom_Awake_m1B5D386B98CF2EB05A8155B238D6F6E8275D181C (void);
// 0x000001CC System.Void TMPro.Examples.VertexZoom::OnEnable()
extern void VertexZoom_OnEnable_m7F980FC038FC2534C428A5FD33E3E13AEAEB4EEC (void);
// 0x000001CD System.Void TMPro.Examples.VertexZoom::OnDisable()
extern void VertexZoom_OnDisable_m880C38B62B4BB05AF49F12F75B83FFA2F0519884 (void);
// 0x000001CE System.Void TMPro.Examples.VertexZoom::Start()
extern void VertexZoom_Start_m10A182DCEF8D430DADAFBFFA3F04171F8E60C84C (void);
// 0x000001CF System.Void TMPro.Examples.VertexZoom::ON_TEXT_CHANGED(UnityEngine.Object)
extern void VertexZoom_ON_TEXT_CHANGED_mB696E443C61D2212AC93A7615AA58106DD25250F (void);
// 0x000001D0 System.Collections.IEnumerator TMPro.Examples.VertexZoom::AnimateVertexColors()
extern void VertexZoom_AnimateVertexColors_mEFBA6C940DFECB485236C247358A32011A7963ED (void);
// 0x000001D1 System.Void TMPro.Examples.VertexZoom::.ctor()
extern void VertexZoom__ctor_mE7B36F2D3EC8EF397AB0AD7A19E988C06927A3B2 (void);
// 0x000001D2 System.Void TMPro.Examples.WarpTextExample::Awake()
extern void WarpTextExample_Awake_mEDB072A485F3F37270FC736E1A201624D696B4B0 (void);
// 0x000001D3 System.Void TMPro.Examples.WarpTextExample::Start()
extern void WarpTextExample_Start_m36448AEA494658D7C129C3B71BF3A64CC4DFEDC4 (void);
// 0x000001D4 UnityEngine.AnimationCurve TMPro.Examples.WarpTextExample::CopyAnimationCurve(UnityEngine.AnimationCurve)
extern void WarpTextExample_CopyAnimationCurve_m744026E638662DA48AC81ED21E0589E3E4E05FAB (void);
// 0x000001D5 System.Collections.IEnumerator TMPro.Examples.WarpTextExample::WarpText()
extern void WarpTextExample_WarpText_mA29C98CF3B92F253C2DD2BADFC76618F8324BEF2 (void);
// 0x000001D6 System.Void TMPro.Examples.WarpTextExample::.ctor()
extern void WarpTextExample__ctor_m62299DFDB704027267321007E16DB87D93D0F099 (void);
// 0x000001D7 System.Single Map.Area::get_CurrentHeroAmount()
extern void Area_get_CurrentHeroAmount_mF7EE46F068C4AF7A0C0EE036CF6853C75337B957 (void);
// 0x000001D8 System.Void Map.Area::set_CurrentHeroAmount(System.Single)
extern void Area_set_CurrentHeroAmount_m7D64716F3C5D20FC426581D30EED068869264294 (void);
// 0x000001D9 System.Single Map.Area::get_CurrentSubHeroAmount()
extern void Area_get_CurrentSubHeroAmount_mB1AE1AEACA136C7E29DD9855DCF10ACC8B7C3D66 (void);
// 0x000001DA System.Void Map.Area::set_CurrentSubHeroAmount(System.Single)
extern void Area_set_CurrentSubHeroAmount_m3E01B5026F9398B8D8B3012ED50B3BAE83B620E6 (void);
// 0x000001DB System.Boolean Map.Area::get_moreThanHeroLimit()
extern void Area_get_moreThanHeroLimit_mDB9A2215AD1ABF9BB60CCBFC30DC3816CDF736D2 (void);
// 0x000001DC System.Boolean Map.Area::get_moreThanSubHeroLimit()
extern void Area_get_moreThanSubHeroLimit_mAD4ED3CE6380F3E01555DC4CFF6A36DBF27CCF3E (void);
// 0x000001DD System.Void Map.Area::Start()
extern void Area_Start_m737D962CA274329A6A24C6E4EC0A41CCE87DDC31 (void);
// 0x000001DE UnityEngine.Vector3 Map.Area::GetPosition()
extern void Area_GetPosition_m0DCF0740581DF32E8F70FD8F873EA94E4574A48D (void);
// 0x000001DF UnityEngine.Vector3 Map.Area::GetEvoPosition()
extern void Area_GetEvoPosition_mD3B273FE99D8AC70F52BC0A05B3651D5658577C2 (void);
// 0x000001E0 System.Void Map.Area::BuildHero(Manager.HeroBluePrint)
extern void Area_BuildHero_m114CE898FD89E055F3BDB6B11402915B15E252F3 (void);
// 0x000001E1 System.Void Map.Area::UpgradeHero()
extern void Area_UpgradeHero_m4BA42283AF51359CCEF2D273974E66FDAA111196 (void);
// 0x000001E2 System.Void Map.Area::EvoHero()
extern void Area_EvoHero_m0D5410A9FF26B54AF75E8B2912EC4F436CED9C41 (void);
// 0x000001E3 System.Void Map.Area::SellHero()
extern void Area_SellHero_m9900D32E6F213DA261F2236921ABC3D4A2D53CA9 (void);
// 0x000001E4 System.Void Map.Area::OnMouseDown()
extern void Area_OnMouseDown_m46157C8A055549E5354058828A86CA41D798B875 (void);
// 0x000001E5 System.Void Map.Area::.ctor()
extern void Area__ctor_mD2D681B0DD7E6A0FE5C9A5D587C11DE5A12DFB8F (void);
// 0x000001E6 SimpleJSON.JSONNodeType SimpleJSON.JSONNode::get_Tag()
// 0x000001E7 SimpleJSON.JSONNode SimpleJSON.JSONNode::get_Item(System.Int32)
extern void JSONNode_get_Item_m9B4DBE85D7742250EF918F4450E3CE020AF7C807 (void);
// 0x000001E8 System.Void SimpleJSON.JSONNode::set_Item(System.Int32,SimpleJSON.JSONNode)
extern void JSONNode_set_Item_m62DFE58028E55800BF80EFE6E0CE4CDE6DF00CA7 (void);
// 0x000001E9 SimpleJSON.JSONNode SimpleJSON.JSONNode::get_Item(System.String)
extern void JSONNode_get_Item_mB39E17FF10DD6C0D91FCF1EF485A73F434B34F1D (void);
// 0x000001EA System.Void SimpleJSON.JSONNode::set_Item(System.String,SimpleJSON.JSONNode)
extern void JSONNode_set_Item_m9F8D28C00CA46F8C0BD5E43500AEAFC3A1999E07 (void);
// 0x000001EB System.String SimpleJSON.JSONNode::get_Value()
extern void JSONNode_get_Value_m8F31142225AAE4DDEC7ACB74B7FFB30A6DD672F1 (void);
// 0x000001EC System.Void SimpleJSON.JSONNode::set_Value(System.String)
extern void JSONNode_set_Value_mA046548FC6B7DCBDA4964DB05303EBA0CF2A29CA (void);
// 0x000001ED System.Int32 SimpleJSON.JSONNode::get_Count()
extern void JSONNode_get_Count_m6FD5676FECC6B26A6384D8294C213C4B032EA269 (void);
// 0x000001EE System.Boolean SimpleJSON.JSONNode::get_IsNumber()
extern void JSONNode_get_IsNumber_m682272D381670DCFA66BD9EE3C0CE3CB161E3AAD (void);
// 0x000001EF System.Boolean SimpleJSON.JSONNode::get_IsString()
extern void JSONNode_get_IsString_mE0D6737C47977364E9F1620CEA98454EAFAEB196 (void);
// 0x000001F0 System.Boolean SimpleJSON.JSONNode::get_IsBoolean()
extern void JSONNode_get_IsBoolean_mA575C54AD05577A9F45E0F55355C0B40F60BFA24 (void);
// 0x000001F1 System.Boolean SimpleJSON.JSONNode::get_IsNull()
extern void JSONNode_get_IsNull_m85D1B73ABCCE95314768A4E690D941B82D08CD58 (void);
// 0x000001F2 System.Boolean SimpleJSON.JSONNode::get_IsArray()
extern void JSONNode_get_IsArray_m54719E46991451D5C8750D8265D0C6F91EEE97D7 (void);
// 0x000001F3 System.Boolean SimpleJSON.JSONNode::get_IsObject()
extern void JSONNode_get_IsObject_mC9E7B94CB90443629157D7C2AB5ED83510DD0BD1 (void);
// 0x000001F4 System.Boolean SimpleJSON.JSONNode::get_Inline()
extern void JSONNode_get_Inline_m6791C62A74ACCEA90470A7EC177FA3F12F744F09 (void);
// 0x000001F5 System.Void SimpleJSON.JSONNode::set_Inline(System.Boolean)
extern void JSONNode_set_Inline_m5C5C9E182CE521A831EBD2D708568297601AD835 (void);
// 0x000001F6 System.Void SimpleJSON.JSONNode::Add(System.String,SimpleJSON.JSONNode)
extern void JSONNode_Add_m10F03DC3FD017B22B831388D39FF735D5AFE96A4 (void);
// 0x000001F7 System.Void SimpleJSON.JSONNode::Add(SimpleJSON.JSONNode)
extern void JSONNode_Add_m7DCFC7266181DE46B90ECC7C5DD7D5B7618F9DE2 (void);
// 0x000001F8 SimpleJSON.JSONNode SimpleJSON.JSONNode::Remove(System.String)
extern void JSONNode_Remove_m49D627BAE273E902C8EB8345BDD7B33E945C2E47 (void);
// 0x000001F9 SimpleJSON.JSONNode SimpleJSON.JSONNode::Remove(System.Int32)
extern void JSONNode_Remove_m3B3259E69D33121E4974FE1ACFAEBED4B034C053 (void);
// 0x000001FA SimpleJSON.JSONNode SimpleJSON.JSONNode::Remove(SimpleJSON.JSONNode)
extern void JSONNode_Remove_mD1FE270ABF87BCD7D09AAA22286AB3C315BAA0EA (void);
// 0x000001FB System.Collections.Generic.IEnumerable`1<SimpleJSON.JSONNode> SimpleJSON.JSONNode::get_Children()
extern void JSONNode_get_Children_mB9A68101166B1C5176311D2EC1ABCC832248FDFF (void);
// 0x000001FC System.Collections.Generic.IEnumerable`1<SimpleJSON.JSONNode> SimpleJSON.JSONNode::get_DeepChildren()
extern void JSONNode_get_DeepChildren_m3E3CE542A4856D0BD557B7E121B4CAA999C8110C (void);
// 0x000001FD System.String SimpleJSON.JSONNode::ToString()
extern void JSONNode_ToString_m9E23A359C8B7209DEED6B81DA27C8D0619F9CE98 (void);
// 0x000001FE System.String SimpleJSON.JSONNode::ToString(System.Int32)
extern void JSONNode_ToString_m9B8D0351B9B14D65ABF1090E6D07E0C69DFB84C3 (void);
// 0x000001FF System.Void SimpleJSON.JSONNode::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
// 0x00000200 SimpleJSON.JSONNode/Enumerator SimpleJSON.JSONNode::GetEnumerator()
// 0x00000201 System.Collections.Generic.IEnumerable`1<System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode>> SimpleJSON.JSONNode::get_Linq()
extern void JSONNode_get_Linq_m568F8D242CB58C62C9D8230A0C1F684D70573268 (void);
// 0x00000202 SimpleJSON.JSONNode/KeyEnumerator SimpleJSON.JSONNode::get_Keys()
extern void JSONNode_get_Keys_m6C1FC7F6C35656362327B486F0426273234584E2 (void);
// 0x00000203 SimpleJSON.JSONNode/ValueEnumerator SimpleJSON.JSONNode::get_Values()
extern void JSONNode_get_Values_m1097622D87EFAC54681A9A9796E563DA142D7677 (void);
// 0x00000204 System.Double SimpleJSON.JSONNode::get_AsDouble()
extern void JSONNode_get_AsDouble_m0DE7D5E7712FB59B32C1C570EBBF85D597B98656 (void);
// 0x00000205 System.Void SimpleJSON.JSONNode::set_AsDouble(System.Double)
extern void JSONNode_set_AsDouble_mBA8F81DF080D29E60DDE8235663BAFFA688736D5 (void);
// 0x00000206 System.Int32 SimpleJSON.JSONNode::get_AsInt()
extern void JSONNode_get_AsInt_m743FC99FEF2DA80A8C6CB71137A5FFD28084E0FA (void);
// 0x00000207 System.Void SimpleJSON.JSONNode::set_AsInt(System.Int32)
extern void JSONNode_set_AsInt_m7CA6313AD31E9E08FEB0B6F8B92AD9A3882D9B75 (void);
// 0x00000208 System.Single SimpleJSON.JSONNode::get_AsFloat()
extern void JSONNode_get_AsFloat_m4B2A24C67F4FBF872DEA6360719D854AE1AD1FBB (void);
// 0x00000209 System.Void SimpleJSON.JSONNode::set_AsFloat(System.Single)
extern void JSONNode_set_AsFloat_m624BDD6CAF17D1709BACFDF3554F2AE11FDD22D1 (void);
// 0x0000020A System.Boolean SimpleJSON.JSONNode::get_AsBool()
extern void JSONNode_get_AsBool_m15160B79EBEA51E7A2C7C7A23B3540A60434B36D (void);
// 0x0000020B System.Void SimpleJSON.JSONNode::set_AsBool(System.Boolean)
extern void JSONNode_set_AsBool_m0878AF783E25077E17850DD1B4522B17FF08770F (void);
// 0x0000020C SimpleJSON.JSONArray SimpleJSON.JSONNode::get_AsArray()
extern void JSONNode_get_AsArray_m10801C5609C0C024480B49DFA03A4FB16A4E6829 (void);
// 0x0000020D SimpleJSON.JSONObject SimpleJSON.JSONNode::get_AsObject()
extern void JSONNode_get_AsObject_mDE74F42234B130BA44AD17DF9FFC64A2D8DFCD46 (void);
// 0x0000020E SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.String)
extern void JSONNode_op_Implicit_m3938223B519495895A5B4E53D60789BB2D4620F2 (void);
// 0x0000020F System.String SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m110AF33FB2CEF68FF905E93F656AF02222554668 (void);
// 0x00000210 SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.Double)
extern void JSONNode_op_Implicit_m66EA9B62BBFD5A306C2B95703443F1960338BA01 (void);
// 0x00000211 System.Double SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m98778303F0AD762A7C4FE928D82352A1F8276EBE (void);
// 0x00000212 SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.Single)
extern void JSONNode_op_Implicit_m3780006769998A253BE0F8068E68BD88AC692112 (void);
// 0x00000213 System.Single SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_mA90C42C0D957F5F284857AE91907D69477331166 (void);
// 0x00000214 SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.Int32)
extern void JSONNode_op_Implicit_mDAC018F58F31AB6333A9852BD0B038152063B3F2 (void);
// 0x00000215 System.Int32 SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m10A6B0627DE9BC247B0B39554FEA1CD37CA7988C (void);
// 0x00000216 SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.Boolean)
extern void JSONNode_op_Implicit_mAF077674B97BAF8FD4EB941A63B84D26F6E08EDC (void);
// 0x00000217 System.Boolean SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_mE4078D38F919268986EF017D4A9EA55FD8CBA826 (void);
// 0x00000218 SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode>)
extern void JSONNode_op_Implicit_m92A46C2D66615E97C21041238C09386EDCF0A266 (void);
// 0x00000219 System.Boolean SimpleJSON.JSONNode::op_Equality(SimpleJSON.JSONNode,System.Object)
extern void JSONNode_op_Equality_mA320F56360F44724C465167318C8FC38F0DCA38F (void);
// 0x0000021A System.Boolean SimpleJSON.JSONNode::op_Inequality(SimpleJSON.JSONNode,System.Object)
extern void JSONNode_op_Inequality_m00AE17CECA317ECC98958F027D0545C831DA24FE (void);
// 0x0000021B System.Boolean SimpleJSON.JSONNode::Equals(System.Object)
extern void JSONNode_Equals_mC1598F5265D88ECAA3040C537A28FC2DC878BBFF (void);
// 0x0000021C System.Int32 SimpleJSON.JSONNode::GetHashCode()
extern void JSONNode_GetHashCode_mF900BB7E50D965906CDBC7210B9097CDE6B89834 (void);
// 0x0000021D System.Text.StringBuilder SimpleJSON.JSONNode::get_EscapeBuilder()
extern void JSONNode_get_EscapeBuilder_m87ACD2EE75FD74E9E7B67BBB66FCDB0643749628 (void);
// 0x0000021E System.String SimpleJSON.JSONNode::Escape(System.String)
extern void JSONNode_Escape_m5C3A578A1281224A7BDF595470FA706CBC597A68 (void);
// 0x0000021F System.Void SimpleJSON.JSONNode::ParseElement(SimpleJSON.JSONNode,System.String,System.String,System.Boolean)
extern void JSONNode_ParseElement_mC27D1FD482D97FE1550AAFC625379A7BD8DB1895 (void);
// 0x00000220 SimpleJSON.JSONNode SimpleJSON.JSONNode::Parse(System.String)
extern void JSONNode_Parse_m747C7ACB3E8F42CB32651EAB1506047F02BF4076 (void);
// 0x00000221 System.Void SimpleJSON.JSONNode::SerializeBinary(System.IO.BinaryWriter)
// 0x00000222 System.Void SimpleJSON.JSONNode::SaveToBinaryStream(System.IO.Stream)
extern void JSONNode_SaveToBinaryStream_m5CD58A850F95A069843BC02BED9B7190D4CE6F73 (void);
// 0x00000223 System.Void SimpleJSON.JSONNode::SaveToCompressedStream(System.IO.Stream)
extern void JSONNode_SaveToCompressedStream_m939EB2E9267698F54AD64849D96E4ADFCCF5A00A (void);
// 0x00000224 System.Void SimpleJSON.JSONNode::SaveToCompressedFile(System.String)
extern void JSONNode_SaveToCompressedFile_m66A1234959DDCEE44D9000255F86A8FD32EC87BC (void);
// 0x00000225 System.String SimpleJSON.JSONNode::SaveToCompressedBase64()
extern void JSONNode_SaveToCompressedBase64_mF1BB008CE575D288A986676FD23806E62C98D539 (void);
// 0x00000226 System.Void SimpleJSON.JSONNode::SaveToBinaryFile(System.String)
extern void JSONNode_SaveToBinaryFile_m4C3B67B465C870818ED92E4FC14F9B9527734EC1 (void);
// 0x00000227 System.String SimpleJSON.JSONNode::SaveToBinaryBase64()
extern void JSONNode_SaveToBinaryBase64_m31110029F0029D7416604433C849EBF33A7F12B5 (void);
// 0x00000228 SimpleJSON.JSONNode SimpleJSON.JSONNode::DeserializeBinary(System.IO.BinaryReader)
extern void JSONNode_DeserializeBinary_mE398A9C5CB421A0CCC20A18486D83964E36DAE45 (void);
// 0x00000229 SimpleJSON.JSONNode SimpleJSON.JSONNode::LoadFromCompressedFile(System.String)
extern void JSONNode_LoadFromCompressedFile_m50BB9E1D951C7444EFE1FE570894E5A92BA35275 (void);
// 0x0000022A SimpleJSON.JSONNode SimpleJSON.JSONNode::LoadFromCompressedStream(System.IO.Stream)
extern void JSONNode_LoadFromCompressedStream_m6D59AB50712DF46BE7D37DB5EDB8EDD7C0B6D04D (void);
// 0x0000022B SimpleJSON.JSONNode SimpleJSON.JSONNode::LoadFromCompressedBase64(System.String)
extern void JSONNode_LoadFromCompressedBase64_mBC74C1DE9C65EF47BA738413E2140F1370A9F433 (void);
// 0x0000022C SimpleJSON.JSONNode SimpleJSON.JSONNode::LoadFromBinaryStream(System.IO.Stream)
extern void JSONNode_LoadFromBinaryStream_m540612C537938B1403281538E5B33BCFAFF29B46 (void);
// 0x0000022D SimpleJSON.JSONNode SimpleJSON.JSONNode::LoadFromBinaryFile(System.String)
extern void JSONNode_LoadFromBinaryFile_m47D1B956E15231FD6537A96C09B317F821374B1E (void);
// 0x0000022E SimpleJSON.JSONNode SimpleJSON.JSONNode::LoadFromBinaryBase64(System.String)
extern void JSONNode_LoadFromBinaryBase64_m91E331970C76EE0AE16B3FFFE8535321EADE65E0 (void);
// 0x0000022F SimpleJSON.JSONNode SimpleJSON.JSONNode::GetContainer(SimpleJSON.JSONContainerType)
extern void JSONNode_GetContainer_m8A2362DAE7D92FC4A7977415F5618EBE892DE819 (void);
// 0x00000230 SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(UnityEngine.Vector2)
extern void JSONNode_op_Implicit_m7F01CA65EDCA916BD0BFB9BFEC73444FB26D59FD (void);
// 0x00000231 SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(UnityEngine.Vector3)
extern void JSONNode_op_Implicit_mDD9DF895CC3317F2C6D8E325E6133C1FE0954864 (void);
// 0x00000232 SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(UnityEngine.Vector4)
extern void JSONNode_op_Implicit_mDF1EE24EF1759B2C134B0804A3497B2BFAF0A348 (void);
// 0x00000233 SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(UnityEngine.Quaternion)
extern void JSONNode_op_Implicit_m3B87FAAC3847B6EDAF525A9240A6AAD7489219B3 (void);
// 0x00000234 SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(UnityEngine.Rect)
extern void JSONNode_op_Implicit_m4B12356D2C135933B13ACB20AAA0BCA93221A308 (void);
// 0x00000235 SimpleJSON.JSONNode SimpleJSON.JSONNode::op_Implicit(UnityEngine.RectOffset)
extern void JSONNode_op_Implicit_m880C05AE8E8F125C0FEC37BE1C32F5D2A8D32B70 (void);
// 0x00000236 UnityEngine.Vector2 SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m50E48009BD81DB50555DADF99F5C424EAC0007FB (void);
// 0x00000237 UnityEngine.Vector3 SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m94957CEB32B45139355EE8C3D85B4C9DDA107484 (void);
// 0x00000238 UnityEngine.Vector4 SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m01E7C3C12602B1EFB9EDA7A4812536D44C1CC27D (void);
// 0x00000239 UnityEngine.Quaternion SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m39284C070FA8F77D4A75A67E44ECFDC033719CBA (void);
// 0x0000023A UnityEngine.Rect SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m1F0EDFBF473C04FAF2A76125C40D27AFBF427E56 (void);
// 0x0000023B UnityEngine.RectOffset SimpleJSON.JSONNode::op_Implicit(SimpleJSON.JSONNode)
extern void JSONNode_op_Implicit_m4F12BB544F8D9E6DC387FC276BAE60269715326C (void);
// 0x0000023C UnityEngine.Vector2 SimpleJSON.JSONNode::ReadVector2(UnityEngine.Vector2)
extern void JSONNode_ReadVector2_mB5168EB80BA8FE7D38324BB66580CDE962B5BE7B (void);
// 0x0000023D UnityEngine.Vector2 SimpleJSON.JSONNode::ReadVector2(System.String,System.String)
extern void JSONNode_ReadVector2_mE9045E685C7EA3CFE8546153F99152CCBE61A18A (void);
// 0x0000023E UnityEngine.Vector2 SimpleJSON.JSONNode::ReadVector2()
extern void JSONNode_ReadVector2_m80B259CF2BDAFA309EAF5B6B2C78AED2EB528A15 (void);
// 0x0000023F SimpleJSON.JSONNode SimpleJSON.JSONNode::WriteVector2(UnityEngine.Vector2,System.String,System.String)
extern void JSONNode_WriteVector2_m78FD594CD3963CDAC2B1BF2DF26220692FD54DAC (void);
// 0x00000240 UnityEngine.Vector3 SimpleJSON.JSONNode::ReadVector3(UnityEngine.Vector3)
extern void JSONNode_ReadVector3_mC7A091BF1F3DE71655260268551A9E449EDAA1B6 (void);
// 0x00000241 UnityEngine.Vector3 SimpleJSON.JSONNode::ReadVector3(System.String,System.String,System.String)
extern void JSONNode_ReadVector3_m1F04E19E177F89BB76CA022C70A975C08061F725 (void);
// 0x00000242 UnityEngine.Vector3 SimpleJSON.JSONNode::ReadVector3()
extern void JSONNode_ReadVector3_mF41BE2C41613AFEA70CAB539AFA7D0A6F573AB9C (void);
// 0x00000243 SimpleJSON.JSONNode SimpleJSON.JSONNode::WriteVector3(UnityEngine.Vector3,System.String,System.String,System.String)
extern void JSONNode_WriteVector3_mAE65146631E220583C90A007F05DE374A7E3D067 (void);
// 0x00000244 UnityEngine.Vector4 SimpleJSON.JSONNode::ReadVector4(UnityEngine.Vector4)
extern void JSONNode_ReadVector4_m2B894C5C5513E9454511DDAAFE7C3A416470907D (void);
// 0x00000245 UnityEngine.Vector4 SimpleJSON.JSONNode::ReadVector4()
extern void JSONNode_ReadVector4_m13FF00E78429AC6F1E1284539C759AB69BDF1CD6 (void);
// 0x00000246 SimpleJSON.JSONNode SimpleJSON.JSONNode::WriteVector4(UnityEngine.Vector4)
extern void JSONNode_WriteVector4_m1210A9104B4EFE99047D7AFE6AB10DD01A73D27B (void);
// 0x00000247 UnityEngine.Quaternion SimpleJSON.JSONNode::ReadQuaternion(UnityEngine.Quaternion)
extern void JSONNode_ReadQuaternion_m6EEE1B3ADE85662B1363029EE602CA1EC02F8DCF (void);
// 0x00000248 UnityEngine.Quaternion SimpleJSON.JSONNode::ReadQuaternion()
extern void JSONNode_ReadQuaternion_m6147D59E720DA900144A556FCB4365FCC2A71A32 (void);
// 0x00000249 SimpleJSON.JSONNode SimpleJSON.JSONNode::WriteQuaternion(UnityEngine.Quaternion)
extern void JSONNode_WriteQuaternion_m2A11E75564526030F3EF8D276E578F39AA18CCA1 (void);
// 0x0000024A UnityEngine.Rect SimpleJSON.JSONNode::ReadRect(UnityEngine.Rect)
extern void JSONNode_ReadRect_m831CAA59B68FD465C7739CE3DE1C297D8A23B023 (void);
// 0x0000024B UnityEngine.Rect SimpleJSON.JSONNode::ReadRect()
extern void JSONNode_ReadRect_m5DA61EE22CAA874BD2457B060F6286A7FB1DBFB4 (void);
// 0x0000024C SimpleJSON.JSONNode SimpleJSON.JSONNode::WriteRect(UnityEngine.Rect)
extern void JSONNode_WriteRect_m3614416241A91C0EDAB54E5645D7A5D9B7AADB55 (void);
// 0x0000024D UnityEngine.RectOffset SimpleJSON.JSONNode::ReadRectOffset(UnityEngine.RectOffset)
extern void JSONNode_ReadRectOffset_m830101CD2863A9DAA92A65983D5F7E44439522E0 (void);
// 0x0000024E UnityEngine.RectOffset SimpleJSON.JSONNode::ReadRectOffset()
extern void JSONNode_ReadRectOffset_mA8D64472E6F8F695EED27E33E13B204BD87194C8 (void);
// 0x0000024F SimpleJSON.JSONNode SimpleJSON.JSONNode::WriteRectOffset(UnityEngine.RectOffset)
extern void JSONNode_WriteRectOffset_m1146232B54BE25B8E9AC363C776E4B8A40C8A789 (void);
// 0x00000250 UnityEngine.Matrix4x4 SimpleJSON.JSONNode::ReadMatrix()
extern void JSONNode_ReadMatrix_mCE78E3CAC6BE0132EBF82990DB741A12E1B3030C (void);
// 0x00000251 SimpleJSON.JSONNode SimpleJSON.JSONNode::WriteMatrix(UnityEngine.Matrix4x4)
extern void JSONNode_WriteMatrix_m7F72D94C9EDC6B9E590231A27FFE6C52F200EA23 (void);
// 0x00000252 System.Void SimpleJSON.JSONNode::.ctor()
extern void JSONNode__ctor_mF0692814A0795056AE052B05EF63D6B216B5619E (void);
// 0x00000253 System.Void SimpleJSON.JSONNode::.cctor()
extern void JSONNode__cctor_m0F95CA9754AE155CEC873DEFB28A83E8AAC25990 (void);
// 0x00000254 System.Boolean SimpleJSON.JSONArray::get_Inline()
extern void JSONArray_get_Inline_mCCA0FAE052593C420DF7266D9030F7CBCFF5D43A (void);
// 0x00000255 System.Void SimpleJSON.JSONArray::set_Inline(System.Boolean)
extern void JSONArray_set_Inline_mCDD35E23A81DC4DF48DB54FA164B2D05CA8CDD5C (void);
// 0x00000256 SimpleJSON.JSONNodeType SimpleJSON.JSONArray::get_Tag()
extern void JSONArray_get_Tag_mA6475F4E3B27AE262DFF53A510E5DD9CA9A82212 (void);
// 0x00000257 System.Boolean SimpleJSON.JSONArray::get_IsArray()
extern void JSONArray_get_IsArray_m015BCD163D49042D8827D8B5880EAC8720B93DDD (void);
// 0x00000258 SimpleJSON.JSONNode/Enumerator SimpleJSON.JSONArray::GetEnumerator()
extern void JSONArray_GetEnumerator_m107028482740A363BC3AAC4348F729F49094B8D7 (void);
// 0x00000259 SimpleJSON.JSONNode SimpleJSON.JSONArray::get_Item(System.Int32)
extern void JSONArray_get_Item_mB56A68792D63083DAE88CF576C4E5C9C6D0E1AEC (void);
// 0x0000025A System.Void SimpleJSON.JSONArray::set_Item(System.Int32,SimpleJSON.JSONNode)
extern void JSONArray_set_Item_m8D70249BB8D717AD878AFE87F9F86345CD5DE9EF (void);
// 0x0000025B SimpleJSON.JSONNode SimpleJSON.JSONArray::get_Item(System.String)
extern void JSONArray_get_Item_m5E864AF954B22AD5841E8CEB58AF68DED45C4C8B (void);
// 0x0000025C System.Void SimpleJSON.JSONArray::set_Item(System.String,SimpleJSON.JSONNode)
extern void JSONArray_set_Item_mD6DB253A66DC2D34179D9773A1ADF25A11FE7DDE (void);
// 0x0000025D System.Int32 SimpleJSON.JSONArray::get_Count()
extern void JSONArray_get_Count_m8649900F00FBEDB004834FB9D814F848E6C7D72B (void);
// 0x0000025E System.Void SimpleJSON.JSONArray::Add(System.String,SimpleJSON.JSONNode)
extern void JSONArray_Add_m14182F062E171457670DB45BE811A46A78B2D685 (void);
// 0x0000025F SimpleJSON.JSONNode SimpleJSON.JSONArray::Remove(System.Int32)
extern void JSONArray_Remove_m65505E642765169F35A9F21760C7EDC39407B9F9 (void);
// 0x00000260 SimpleJSON.JSONNode SimpleJSON.JSONArray::Remove(SimpleJSON.JSONNode)
extern void JSONArray_Remove_mA79C09C43B22EE2B4965C1D8B3794BAE1957037E (void);
// 0x00000261 System.Collections.Generic.IEnumerable`1<SimpleJSON.JSONNode> SimpleJSON.JSONArray::get_Children()
extern void JSONArray_get_Children_m56D8A696E09F5EEFF1FF9F8BE06011CE5EAFA197 (void);
// 0x00000262 System.Void SimpleJSON.JSONArray::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONArray_WriteToStringBuilder_m64C54C1D36684A7CE5EAB86E3350AE4B811D40C9 (void);
// 0x00000263 System.Void SimpleJSON.JSONArray::SerializeBinary(System.IO.BinaryWriter)
extern void JSONArray_SerializeBinary_m8BB39836ADBAC72EB4DD67C37A3CFC44FFB8333C (void);
// 0x00000264 System.Void SimpleJSON.JSONArray::.ctor()
extern void JSONArray__ctor_m2B6B15254CC3578F4B78A9E5CA656C4D25119512 (void);
// 0x00000265 System.Boolean SimpleJSON.JSONObject::get_Inline()
extern void JSONObject_get_Inline_mEBB1EA4AB1EE32D80F2DB4C5C4240DD13593750A (void);
// 0x00000266 System.Void SimpleJSON.JSONObject::set_Inline(System.Boolean)
extern void JSONObject_set_Inline_mDDC84E63241E060414987E3B8301F7F1AB2967B4 (void);
// 0x00000267 SimpleJSON.JSONNodeType SimpleJSON.JSONObject::get_Tag()
extern void JSONObject_get_Tag_m0F8454A3B161D9954B2C1B49945DE0E9041907B6 (void);
// 0x00000268 System.Boolean SimpleJSON.JSONObject::get_IsObject()
extern void JSONObject_get_IsObject_m6E6B9273D50B23643A6DFBD17FBEA6BD6143E12C (void);
// 0x00000269 SimpleJSON.JSONNode/Enumerator SimpleJSON.JSONObject::GetEnumerator()
extern void JSONObject_GetEnumerator_mC0AAAB9C27211FED4A7DA97BB1D2DEA1777CABD6 (void);
// 0x0000026A SimpleJSON.JSONNode SimpleJSON.JSONObject::get_Item(System.String)
extern void JSONObject_get_Item_mF7EF352F2BA035D7EFC5902F2DEB12168E586CBD (void);
// 0x0000026B System.Void SimpleJSON.JSONObject::set_Item(System.String,SimpleJSON.JSONNode)
extern void JSONObject_set_Item_m84B17410261AD3D4C06D551BB169126C42331DD8 (void);
// 0x0000026C SimpleJSON.JSONNode SimpleJSON.JSONObject::get_Item(System.Int32)
extern void JSONObject_get_Item_m28A01147850D0D538204316C35A6E1A823602C0E (void);
// 0x0000026D System.Void SimpleJSON.JSONObject::set_Item(System.Int32,SimpleJSON.JSONNode)
extern void JSONObject_set_Item_m17A462CA68BCC6AB9A077BD2D5354C2CD9BAA448 (void);
// 0x0000026E System.Int32 SimpleJSON.JSONObject::get_Count()
extern void JSONObject_get_Count_m5271E2DB59F0A774A71E1313A43FC47D209DE4BD (void);
// 0x0000026F System.Void SimpleJSON.JSONObject::Add(System.String,SimpleJSON.JSONNode)
extern void JSONObject_Add_m89A24748B1E3A3BC627C355AD3F946A5541D0D47 (void);
// 0x00000270 SimpleJSON.JSONNode SimpleJSON.JSONObject::Remove(System.String)
extern void JSONObject_Remove_m20C910052474109A5D0B8C63DA98648296C1E846 (void);
// 0x00000271 SimpleJSON.JSONNode SimpleJSON.JSONObject::Remove(System.Int32)
extern void JSONObject_Remove_m6A66604EDD3CBED40F44D85BAD15342B133A2B21 (void);
// 0x00000272 SimpleJSON.JSONNode SimpleJSON.JSONObject::Remove(SimpleJSON.JSONNode)
extern void JSONObject_Remove_m2000AEFD68838D8F04D250DDFF4E5B2B52D2661A (void);
// 0x00000273 System.Collections.Generic.IEnumerable`1<SimpleJSON.JSONNode> SimpleJSON.JSONObject::get_Children()
extern void JSONObject_get_Children_mDC37B11CC04E072B9E1DFE56A3B176A1E22F8E6D (void);
// 0x00000274 System.Void SimpleJSON.JSONObject::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONObject_WriteToStringBuilder_m8B9F8C4C58F878A4992C39D21E75CF735DB65F61 (void);
// 0x00000275 System.Void SimpleJSON.JSONObject::SerializeBinary(System.IO.BinaryWriter)
extern void JSONObject_SerializeBinary_mD11FD4D432B7BA08AC16D504396247E871FBCDEA (void);
// 0x00000276 System.Void SimpleJSON.JSONObject::.ctor()
extern void JSONObject__ctor_m159A8A4A6AD38E7E4786F8EA8FFB9948DF520F92 (void);
// 0x00000277 SimpleJSON.JSONNodeType SimpleJSON.JSONString::get_Tag()
extern void JSONString_get_Tag_m8CD40E0F8FEA6E68B02D661527AAB26E581AD925 (void);
// 0x00000278 System.Boolean SimpleJSON.JSONString::get_IsString()
extern void JSONString_get_IsString_mE62F3299F2BDCFA5704FEFBB6D6174C7D18EFEA0 (void);
// 0x00000279 SimpleJSON.JSONNode/Enumerator SimpleJSON.JSONString::GetEnumerator()
extern void JSONString_GetEnumerator_m2DB5AD3DA3FA00B24522A9083E0EC92952D28C14 (void);
// 0x0000027A System.String SimpleJSON.JSONString::get_Value()
extern void JSONString_get_Value_m0A72F580BA1EBD98A7592810069BDF215BC737F0 (void);
// 0x0000027B System.Void SimpleJSON.JSONString::set_Value(System.String)
extern void JSONString_set_Value_m9B7FCFDF82ED801A434AC208F52582A2B0DF2164 (void);
// 0x0000027C System.Void SimpleJSON.JSONString::.ctor(System.String)
extern void JSONString__ctor_mE09DCD65C60DE37D1F0F79BD6902E464EE1960A3 (void);
// 0x0000027D System.Void SimpleJSON.JSONString::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONString_WriteToStringBuilder_mB338AAA2644A34B03F6DBE6620F8370C64D15627 (void);
// 0x0000027E System.Boolean SimpleJSON.JSONString::Equals(System.Object)
extern void JSONString_Equals_m9709B63863E6B7E1000EF9E0318B2C499D325B92 (void);
// 0x0000027F System.Int32 SimpleJSON.JSONString::GetHashCode()
extern void JSONString_GetHashCode_m573FB034DD33C5A300F358A318A0BCA761E35AB1 (void);
// 0x00000280 System.Void SimpleJSON.JSONString::SerializeBinary(System.IO.BinaryWriter)
extern void JSONString_SerializeBinary_m155A6E6D4045E9859823E7811A1434C0A0006654 (void);
// 0x00000281 SimpleJSON.JSONNodeType SimpleJSON.JSONNumber::get_Tag()
extern void JSONNumber_get_Tag_mC3886860EBAEBED98DF4B58A2E5D2DA1C86D0DF0 (void);
// 0x00000282 System.Boolean SimpleJSON.JSONNumber::get_IsNumber()
extern void JSONNumber_get_IsNumber_m55E3251DC2B157F6A06A78047C8B7F6A8710CFC1 (void);
// 0x00000283 SimpleJSON.JSONNode/Enumerator SimpleJSON.JSONNumber::GetEnumerator()
extern void JSONNumber_GetEnumerator_m48371CA028B151ED79247AB9B49B7B120C172F78 (void);
// 0x00000284 System.String SimpleJSON.JSONNumber::get_Value()
extern void JSONNumber_get_Value_m02C36CBFEEDCE75834055B25A6F0F9845B661B98 (void);
// 0x00000285 System.Void SimpleJSON.JSONNumber::set_Value(System.String)
extern void JSONNumber_set_Value_m437CA8FBE33C4A5C6CA4E2951767974987142A01 (void);
// 0x00000286 System.Double SimpleJSON.JSONNumber::get_AsDouble()
extern void JSONNumber_get_AsDouble_m181BFF83082F0A4A380023EDE5D1EA55B0EC6427 (void);
// 0x00000287 System.Void SimpleJSON.JSONNumber::set_AsDouble(System.Double)
extern void JSONNumber_set_AsDouble_m48E57B49311F9720098069A63A056AF5B44AECBE (void);
// 0x00000288 System.Void SimpleJSON.JSONNumber::.ctor(System.Double)
extern void JSONNumber__ctor_mD0AF1324557B9FC7B1D63842D2CA65EC2D1A643A (void);
// 0x00000289 System.Void SimpleJSON.JSONNumber::.ctor(System.String)
extern void JSONNumber__ctor_m03A803840EF273185C5C63642FAA70FAF9DFE40E (void);
// 0x0000028A System.Void SimpleJSON.JSONNumber::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONNumber_WriteToStringBuilder_m48A94BA46F195E37F78A7AA167655AE1E870231A (void);
// 0x0000028B System.Boolean SimpleJSON.JSONNumber::IsNumeric(System.Object)
extern void JSONNumber_IsNumeric_mEA4ED6C1587A4D94F5D6F30B12FA284AB56F11EB (void);
// 0x0000028C System.Boolean SimpleJSON.JSONNumber::Equals(System.Object)
extern void JSONNumber_Equals_m0D80E8BA86AA78FBB0482F9F053A0BACB65D8D97 (void);
// 0x0000028D System.Int32 SimpleJSON.JSONNumber::GetHashCode()
extern void JSONNumber_GetHashCode_mB10776EF12594532AB12FA617D1BF18FC53C790C (void);
// 0x0000028E System.Void SimpleJSON.JSONNumber::SerializeBinary(System.IO.BinaryWriter)
extern void JSONNumber_SerializeBinary_m0250E8C465049F13DF32A2553DC76191A2012C7A (void);
// 0x0000028F SimpleJSON.JSONNodeType SimpleJSON.JSONBool::get_Tag()
extern void JSONBool_get_Tag_m43F4216FE45F0043979093D7BE5FD16C0F462A62 (void);
// 0x00000290 System.Boolean SimpleJSON.JSONBool::get_IsBoolean()
extern void JSONBool_get_IsBoolean_mE70A4F82605CAB1E14A70413FC42EF66ADBAB727 (void);
// 0x00000291 SimpleJSON.JSONNode/Enumerator SimpleJSON.JSONBool::GetEnumerator()
extern void JSONBool_GetEnumerator_mA15666FFF18AADF12912689A521087C98B5B2D1C (void);
// 0x00000292 System.String SimpleJSON.JSONBool::get_Value()
extern void JSONBool_get_Value_m11D2863303B2003F337549581C8A583A6A3B4D74 (void);
// 0x00000293 System.Void SimpleJSON.JSONBool::set_Value(System.String)
extern void JSONBool_set_Value_mA7FB197B21177B947BC3C2940A63724FE532E373 (void);
// 0x00000294 System.Boolean SimpleJSON.JSONBool::get_AsBool()
extern void JSONBool_get_AsBool_mA5617A7BC781E111FBD24A7E53FB34C7DE6F62B6 (void);
// 0x00000295 System.Void SimpleJSON.JSONBool::set_AsBool(System.Boolean)
extern void JSONBool_set_AsBool_mDDD0D1CA7B06DAD37AA13987F46B95C6A1AEF489 (void);
// 0x00000296 System.Void SimpleJSON.JSONBool::.ctor(System.Boolean)
extern void JSONBool__ctor_m7593DD0B3E9628BA9BB4DE4A0E904BBA6BF151C1 (void);
// 0x00000297 System.Void SimpleJSON.JSONBool::.ctor(System.String)
extern void JSONBool__ctor_m8E5E6A23827EE2B5DB5E7C86377805ADD8A771B2 (void);
// 0x00000298 System.Void SimpleJSON.JSONBool::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONBool_WriteToStringBuilder_m7EA9750436CEE42357B6A109B83870F4FC2C403E (void);
// 0x00000299 System.Boolean SimpleJSON.JSONBool::Equals(System.Object)
extern void JSONBool_Equals_m57A524B4FEBF04285341E4FC42EDEDE7A524F4BB (void);
// 0x0000029A System.Int32 SimpleJSON.JSONBool::GetHashCode()
extern void JSONBool_GetHashCode_m49A9C417E05DFB795FAE0F38737C7629A3A69A22 (void);
// 0x0000029B System.Void SimpleJSON.JSONBool::SerializeBinary(System.IO.BinaryWriter)
extern void JSONBool_SerializeBinary_m58E3D67CAB665202AF9AF699A0E2C267BE4A778A (void);
// 0x0000029C SimpleJSON.JSONNull SimpleJSON.JSONNull::CreateOrGet()
extern void JSONNull_CreateOrGet_m96465EECF543329FFBBC26B552EDB6E43D8DBC90 (void);
// 0x0000029D System.Void SimpleJSON.JSONNull::.ctor()
extern void JSONNull__ctor_mA4FF0FB3001766DFB7A8D8C23063101AC377F277 (void);
// 0x0000029E SimpleJSON.JSONNodeType SimpleJSON.JSONNull::get_Tag()
extern void JSONNull_get_Tag_m2A705402C448944F6395AAC88DA5DE7ADF99C4D1 (void);
// 0x0000029F System.Boolean SimpleJSON.JSONNull::get_IsNull()
extern void JSONNull_get_IsNull_m1964A7B7E9C0FD17CDE2E4DFDF6345FE03EBA59B (void);
// 0x000002A0 SimpleJSON.JSONNode/Enumerator SimpleJSON.JSONNull::GetEnumerator()
extern void JSONNull_GetEnumerator_m7B4ADD53B56B6663A0832AC611D7B47FA98D1A35 (void);
// 0x000002A1 System.String SimpleJSON.JSONNull::get_Value()
extern void JSONNull_get_Value_m76B2E412BD13C31AD68C272CA9AF2E6063F165D3 (void);
// 0x000002A2 System.Void SimpleJSON.JSONNull::set_Value(System.String)
extern void JSONNull_set_Value_m713260989D4BA62C8B8CD716893585A1AA88ABE9 (void);
// 0x000002A3 System.Boolean SimpleJSON.JSONNull::get_AsBool()
extern void JSONNull_get_AsBool_m5E619608D71A63CBD234CA282B3BB2A7EFF385D5 (void);
// 0x000002A4 System.Void SimpleJSON.JSONNull::set_AsBool(System.Boolean)
extern void JSONNull_set_AsBool_m267D1AA7D6003F371DC10DB65EB4E80643BB99F1 (void);
// 0x000002A5 System.Boolean SimpleJSON.JSONNull::Equals(System.Object)
extern void JSONNull_Equals_m3063912429EF72D2F9AFAEDF0D0303EB5D60A893 (void);
// 0x000002A6 System.Int32 SimpleJSON.JSONNull::GetHashCode()
extern void JSONNull_GetHashCode_m899F43B83E13ABFDE999C3781C45C6C03CC21077 (void);
// 0x000002A7 System.Void SimpleJSON.JSONNull::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONNull_WriteToStringBuilder_m66FC8C85A593D6198406753B845BFECA59117B46 (void);
// 0x000002A8 System.Void SimpleJSON.JSONNull::SerializeBinary(System.IO.BinaryWriter)
extern void JSONNull_SerializeBinary_m0D96E78D3AC9372DFC0EEF6F260367E9FCB4D939 (void);
// 0x000002A9 System.Void SimpleJSON.JSONNull::.cctor()
extern void JSONNull__cctor_mF5CFD47108FE35E808A8BA6514AA8283BBFD3193 (void);
// 0x000002AA SimpleJSON.JSONNodeType SimpleJSON.JSONLazyCreator::get_Tag()
extern void JSONLazyCreator_get_Tag_m492C25FDFDC35D2EFB17711E1C259976A38BB46E (void);
// 0x000002AB SimpleJSON.JSONNode/Enumerator SimpleJSON.JSONLazyCreator::GetEnumerator()
extern void JSONLazyCreator_GetEnumerator_m6B2038B21158F725FC5B607544FB590A0E313AEC (void);
// 0x000002AC System.Void SimpleJSON.JSONLazyCreator::.ctor(SimpleJSON.JSONNode)
extern void JSONLazyCreator__ctor_m790C925B9AD99D9F574C556397F3C2D868D2F7EE (void);
// 0x000002AD System.Void SimpleJSON.JSONLazyCreator::.ctor(SimpleJSON.JSONNode,System.String)
extern void JSONLazyCreator__ctor_mE9B31AC731EAF8B853DA1671147CD5B9B5C6F10A (void);
// 0x000002AE System.Void SimpleJSON.JSONLazyCreator::Set(SimpleJSON.JSONNode)
extern void JSONLazyCreator_Set_m9C40ED5996DFD3D796BFAFBC25EEAB76F29F871F (void);
// 0x000002AF SimpleJSON.JSONNode SimpleJSON.JSONLazyCreator::get_Item(System.Int32)
extern void JSONLazyCreator_get_Item_m0A09379FDB2D2CB95B781D1E3A03855661A0F446 (void);
// 0x000002B0 System.Void SimpleJSON.JSONLazyCreator::set_Item(System.Int32,SimpleJSON.JSONNode)
extern void JSONLazyCreator_set_Item_mE77E182C7C150529FBA7BD013A03F95FDCA25E9E (void);
// 0x000002B1 SimpleJSON.JSONNode SimpleJSON.JSONLazyCreator::get_Item(System.String)
extern void JSONLazyCreator_get_Item_m55D3BD8AF905099931604EACC6A907C17AACA753 (void);
// 0x000002B2 System.Void SimpleJSON.JSONLazyCreator::set_Item(System.String,SimpleJSON.JSONNode)
extern void JSONLazyCreator_set_Item_m8E286CF72FF8622427E69587C30C91BE0D242FCF (void);
// 0x000002B3 System.Void SimpleJSON.JSONLazyCreator::Add(SimpleJSON.JSONNode)
extern void JSONLazyCreator_Add_m7393F681C0F1D9F798D3FEAE8C3A37C9F73E0E1B (void);
// 0x000002B4 System.Void SimpleJSON.JSONLazyCreator::Add(System.String,SimpleJSON.JSONNode)
extern void JSONLazyCreator_Add_m8762C532A8EBAFF5AE213C3B2A9AC983653C8D37 (void);
// 0x000002B5 System.Boolean SimpleJSON.JSONLazyCreator::op_Equality(SimpleJSON.JSONLazyCreator,System.Object)
extern void JSONLazyCreator_op_Equality_mF472937FE1ABFEBCEB8A717561AB198CCB0F75E8 (void);
// 0x000002B6 System.Boolean SimpleJSON.JSONLazyCreator::op_Inequality(SimpleJSON.JSONLazyCreator,System.Object)
extern void JSONLazyCreator_op_Inequality_m8C2EB320847B968D52F5E5D96599F734FEEE8ACA (void);
// 0x000002B7 System.Boolean SimpleJSON.JSONLazyCreator::Equals(System.Object)
extern void JSONLazyCreator_Equals_m94A17EF99F288D175F1A40DC7B6569643CA84DD9 (void);
// 0x000002B8 System.Int32 SimpleJSON.JSONLazyCreator::GetHashCode()
extern void JSONLazyCreator_GetHashCode_m85E38D69ED1F0970E4EE3583196A4E9CD37C9DAF (void);
// 0x000002B9 System.Int32 SimpleJSON.JSONLazyCreator::get_AsInt()
extern void JSONLazyCreator_get_AsInt_m8C8C09CC03C6E229A680D004C070734C424DA012 (void);
// 0x000002BA System.Void SimpleJSON.JSONLazyCreator::set_AsInt(System.Int32)
extern void JSONLazyCreator_set_AsInt_m9CEB176958FE16A9213B7828EEAF442F54E70587 (void);
// 0x000002BB System.Single SimpleJSON.JSONLazyCreator::get_AsFloat()
extern void JSONLazyCreator_get_AsFloat_m1EC025D56616385B163C548183783BA8E4FD67E4 (void);
// 0x000002BC System.Void SimpleJSON.JSONLazyCreator::set_AsFloat(System.Single)
extern void JSONLazyCreator_set_AsFloat_mAECCEF9C5598A5EA2204CDD1730D19B073050956 (void);
// 0x000002BD System.Double SimpleJSON.JSONLazyCreator::get_AsDouble()
extern void JSONLazyCreator_get_AsDouble_m019E3ACC054CCE18601A8731E772C86C62C09545 (void);
// 0x000002BE System.Void SimpleJSON.JSONLazyCreator::set_AsDouble(System.Double)
extern void JSONLazyCreator_set_AsDouble_mBFEC4A9E9A946716829F584BE00CFA35D8DBA1E2 (void);
// 0x000002BF System.Boolean SimpleJSON.JSONLazyCreator::get_AsBool()
extern void JSONLazyCreator_get_AsBool_mE7E32A21C51AFA46EC9BE4F78E96AE20986E7E00 (void);
// 0x000002C0 System.Void SimpleJSON.JSONLazyCreator::set_AsBool(System.Boolean)
extern void JSONLazyCreator_set_AsBool_m700796091E7C1940E21834CE4BB1A8FD80017094 (void);
// 0x000002C1 SimpleJSON.JSONArray SimpleJSON.JSONLazyCreator::get_AsArray()
extern void JSONLazyCreator_get_AsArray_mF737B0C7548A4C304278366931569BD173D4C2C6 (void);
// 0x000002C2 SimpleJSON.JSONObject SimpleJSON.JSONLazyCreator::get_AsObject()
extern void JSONLazyCreator_get_AsObject_m0678D4A1725598B36657E07D1626B8387D3B4C88 (void);
// 0x000002C3 System.Void SimpleJSON.JSONLazyCreator::WriteToStringBuilder(System.Text.StringBuilder,System.Int32,System.Int32,SimpleJSON.JSONTextMode)
extern void JSONLazyCreator_WriteToStringBuilder_m20163F20F4F24E7E82C30AAA1878AE7DE301D0E9 (void);
// 0x000002C4 System.Void SimpleJSON.JSONLazyCreator::SerializeBinary(System.IO.BinaryWriter)
extern void JSONLazyCreator_SerializeBinary_m2E7AFA0F831BF1CBB93F6BC72BEB67A531A31FD5 (void);
// 0x000002C5 SimpleJSON.JSONNode SimpleJSON.JSON::Parse(System.String)
extern void JSON_Parse_mEAB5874EF687F205543F8469A9FAE7DB3C134421 (void);
// 0x000002C6 System.Void Hero.Hero::Awake()
extern void Hero_Awake_m42F76DA5B80908CA13E6C2D5747DB2EC4D9E5935 (void);
// 0x000002C7 System.Void Hero.Hero::Start()
extern void Hero_Start_m7AC5D26F4F174B129C0FD0B55B343BB5561FABFC (void);
// 0x000002C8 System.Void Hero.Hero::Update()
extern void Hero_Update_mF216C64A6B2D1D38CF303333498F90FD63C1396F (void);
// 0x000002C9 System.Void Hero.Hero::CandyEffect()
extern void Hero_CandyEffect_mA519D257E5B628A01DD0532CF771D5E29EC70479 (void);
// 0x000002CA System.Void Hero.Hero::CookieEffect()
extern void Hero_CookieEffect_m2C00FF83BEF5F9A857F3AAF865C0CAAB46C62ACB (void);
// 0x000002CB System.Void Hero.Hero::LockOnTarget()
extern void Hero_LockOnTarget_mAC51B2745411A748812250162491069120EE2FC6 (void);
// 0x000002CC System.Void Hero.Hero::Laser()
extern void Hero_Laser_mB6341A7485EF37F581E23242688F1FE5176EAC9D (void);
// 0x000002CD System.Void Hero.Hero::GetEnemyElement()
extern void Hero_GetEnemyElement_m294B2143ACF0D11F576856B7EC8C0DF753795A30 (void);
// 0x000002CE System.Void Hero.Hero::Attack()
extern void Hero_Attack_m2FF65BCA3ED82FB5B05F7B8033F9EBDE5D6FE601 (void);
// 0x000002CF System.Void Hero.Hero::UpdateTarget()
extern void Hero_UpdateTarget_m9973558665291239C71450870D0344903E88384E (void);
// 0x000002D0 System.Void Hero.Hero::OnDrawGizmosSelected()
extern void Hero_OnDrawGizmosSelected_m46F9157D2FBA21EE460DDD1D8DA523544D234261 (void);
// 0x000002D1 System.Void Hero.Hero::.ctor()
extern void Hero__ctor_mF7CAE2C07B0FD7AD2A70363FA137B6A9992A1FAD (void);
// 0x000002D2 System.Single Enemy.BaseEnemy::get_EnemySpeed()
extern void BaseEnemy_get_EnemySpeed_m8E1469EAEFB76EE59CC6FEE9D45438D94EB0D703 (void);
// 0x000002D3 System.Void Enemy.BaseEnemy::set_EnemySpeed(System.Single)
extern void BaseEnemy_set_EnemySpeed_m17416B6AA011CE66A743C53E4D38FABABF30A19E (void);
// 0x000002D4 System.Single Enemy.BaseEnemy::get_EnemyHp()
extern void BaseEnemy_get_EnemyHp_m60279B2A0B27BCC826D13D21434E45C48CE5BE1B (void);
// 0x000002D5 System.Void Enemy.BaseEnemy::set_EnemyHp(System.Single)
extern void BaseEnemy_set_EnemyHp_mA8ABDB93079B24396F5D3BF6065D9E3EF53DC7DD (void);
// 0x000002D6 System.Int32 Enemy.BaseEnemy::get_GoldDrop()
extern void BaseEnemy_get_GoldDrop_m561D5F9F207855E6B2ACE07E9DDF833C3054E2D8 (void);
// 0x000002D7 System.Void Enemy.BaseEnemy::set_GoldDrop(System.Int32)
extern void BaseEnemy_set_GoldDrop_mA3990C857287CAE8CB85B7736238292CFBD872AC (void);
// 0x000002D8 Manager.Element Enemy.BaseEnemy::get_EnemyElement()
extern void BaseEnemy_get_EnemyElement_m7A2942567C4D07705628C3545EB72DE376C01239 (void);
// 0x000002D9 System.Void Enemy.BaseEnemy::set_EnemyElement(Manager.Element)
extern void BaseEnemy_set_EnemyElement_mC9B8C24FEA90E86264C0F771E4E51FD46BEFCF69 (void);
// 0x000002DA System.Void Enemy.BaseEnemy::Start()
extern void BaseEnemy_Start_mC883DB3A187EAB0FCBB645DB2F3193043077687D (void);
// 0x000002DB System.Void Enemy.BaseEnemy::TakeDamage(System.Single)
extern void BaseEnemy_TakeDamage_mACFBAD9CE871E1D06651395F0B8BA615F535A258 (void);
// 0x000002DC System.Collections.IEnumerator Enemy.BaseEnemy::DelayLaserPopUp()
extern void BaseEnemy_DelayLaserPopUp_m7060C6E807ED81FF861F30EB54208E527CC98D58 (void);
// 0x000002DD System.Void Enemy.BaseEnemy::Slow(System.Single)
extern void BaseEnemy_Slow_mD29992E09422BCAC5302B52C0B03A0D7610B1EC8 (void);
// 0x000002DE System.Void Enemy.BaseEnemy::EnemyDeath()
extern void BaseEnemy_EnemyDeath_m9F8223C370F09C3FDD5B9A56D6372FCE610C28B6 (void);
// 0x000002DF System.Void Enemy.BaseEnemy::Move()
extern void BaseEnemy_Move_mE311EEC0299B205996DB5FE37EFD9E57EE070FBF (void);
// 0x000002E0 System.Void Enemy.BaseEnemy::GoToNextWayPoint()
extern void BaseEnemy_GoToNextWayPoint_m74DFF5D5BE5B97F75C03A0C68610E025918431BF (void);
// 0x000002E1 System.Void Enemy.BaseEnemy::Init(System.Single,System.Single,System.Int32,Manager.Element)
extern void BaseEnemy_Init_m0B85C8E16C7D15051A7E001B24BBC4F9DDD1A16F (void);
// 0x000002E2 System.Void Enemy.BaseEnemy::.ctor()
extern void BaseEnemy__ctor_m8D06C165E9DD193548F0283A202D70FB90776645 (void);
// 0x000002E3 System.Void Enemy.Colubted::Init(System.Single,System.Single,System.Int32,Manager.Element)
extern void Colubted_Init_mB982329E42AC5AE5523E776123C0CBFD17575AC1 (void);
// 0x000002E4 System.Void Enemy.Colubted::Update()
extern void Colubted_Update_m257987F285384F076A7F3CFE4C1D7A6457C45572 (void);
// 0x000002E5 System.Void Enemy.Colubted::TakeDamage(System.Single)
extern void Colubted_TakeDamage_m9022A89B8A6E95195C8D9EA719C8A6E0254E7A61 (void);
// 0x000002E6 System.Void Enemy.Colubted::.ctor()
extern void Colubted__ctor_m608A2BFB8179AD9DF8C825EEE64994A2D8206857 (void);
// 0x000002E7 System.Void Bullet.BaseBullet::GetStatus()
extern void BaseBullet_GetStatus_mF2CEEF86F76177821CC531EF26145726DEF5FD63 (void);
// 0x000002E8 Manager.HeroBluePrint Bullet.BaseBullet::GetHeroBluePrint(Manager.HeroName)
extern void BaseBullet_GetHeroBluePrint_m8EA60934B0934FEF2BB39B00C12A5D326321A7BA (void);
// 0x000002E9 System.Void Bullet.BaseBullet::MoveToTarget()
extern void BaseBullet_MoveToTarget_m62CE9568DF3FBA1F24B252EB125927A158EC6CB8 (void);
// 0x000002EA System.Void Bullet.BaseBullet::FindEnemy(UnityEngine.Transform)
extern void BaseBullet_FindEnemy_m062595DD40AE03245F211D7C1C75D0A5A14A8665 (void);
// 0x000002EB System.Void Bullet.BaseBullet::EnemyHit()
extern void BaseBullet_EnemyHit_m2902EC924549ACE1E05C91C57B66BA3AB37FB9FD (void);
// 0x000002EC System.Void Bullet.BaseBullet::IsAoe()
extern void BaseBullet_IsAoe_m78227536AA3C587871BD1DDF101632FAFFEBAFEF (void);
// 0x000002ED System.Void Bullet.BaseBullet::Damage(UnityEngine.Component)
extern void BaseBullet_Damage_m0D2444A126AE9147E3C3C32091EB185E0E17EBD6 (void);
// 0x000002EE System.Void Bullet.BaseBullet::OnDrawGizmosSelected()
extern void BaseBullet_OnDrawGizmosSelected_mC283C2DD39626B23DBD1EFC3FA24674AEB68DD38 (void);
// 0x000002EF System.Void Bullet.BaseBullet::.ctor()
extern void BaseBullet__ctor_mC671AD70D46F5508C96B7A08D25BE639235AC504 (void);
// 0x000002F0 System.Void Bullet.Bullet_I168::Start()
extern void Bullet_I168_Start_m898C01071471405C08356AB455BE7991B68C75D8 (void);
// 0x000002F1 System.Void Bullet.Bullet_I168::Update()
extern void Bullet_I168_Update_m95C88867CC0E495429E2B47F06456771B7F52536 (void);
// 0x000002F2 System.Void Bullet.Bullet_I168::OnDrawGizmosSelected()
extern void Bullet_I168_OnDrawGizmosSelected_m1780721D391EEA31DDD4C931BFDB050916DE4B1F (void);
// 0x000002F3 System.Void Bullet.Bullet_I168::.ctor()
extern void Bullet_I168__ctor_m8247D6B82EBC1E1A6A65171A89C18E88D2FAEE4A (void);
// 0x000002F4 System.Void Bullet.Bullet_I26::Start()
extern void Bullet_I26_Start_m696E7487940249B2FCF1494ECF2A95556DE3A5BB (void);
// 0x000002F5 System.Void Bullet.Bullet_I26::Update()
extern void Bullet_I26_Update_m0BD786280E240600E4CBF5CA6CE95274604F5473 (void);
// 0x000002F6 System.Void Bullet.Bullet_I26::OnDrawGizmosSelected()
extern void Bullet_I26_OnDrawGizmosSelected_m51A767D91F44C8DCFA0DF3B738263E67D36DA55E (void);
// 0x000002F7 System.Void Bullet.Bullet_I26::.ctor()
extern void Bullet_I26__ctor_m1943C7D95D4777F01A960681DFB3A02E03613CD6 (void);
// 0x000002F8 System.Void Bullet.Bullet_I58::Start()
extern void Bullet_I58_Start_m03855CB5BF50C2D904ACEABE8B2BB7A1031225A5 (void);
// 0x000002F9 System.Void Bullet.Bullet_I58::Update()
extern void Bullet_I58_Update_mFF4416E50D5F13762729B8552375FE9D33B94EE5 (void);
// 0x000002FA System.Void Bullet.Bullet_I58::OnDrawGizmosSelected()
extern void Bullet_I58_OnDrawGizmosSelected_mB8ADFD1338408E2FB689705C76D4C5E49D59C8D3 (void);
// 0x000002FB System.Void Bullet.Bullet_I58::.ctor()
extern void Bullet_I58__ctor_m69FA5B604BB7BF45F1F41294BB8936C8047B2D6C (void);
// 0x000002FC System.Void Bullet.Bullet_U101::Start()
extern void Bullet_U101_Start_m9DDC98141F2F792FBF3BF02DAB0218A2682AB673 (void);
// 0x000002FD System.Void Bullet.Bullet_U101::Update()
extern void Bullet_U101_Update_m4E1D589AF81C04FA773544D5C6C638A9CA4307F0 (void);
// 0x000002FE System.Void Bullet.Bullet_U101::OnDrawGizmosSelected()
extern void Bullet_U101_OnDrawGizmosSelected_m0ECC7985907E622FDCBDC7E2D7FA68FAC5833315 (void);
// 0x000002FF System.Void Bullet.Bullet_U101::.ctor()
extern void Bullet_U101__ctor_mD02A90338507A8D4527E31AAD5218F4BA8E0AA4B (void);
// 0x00000300 System.Void Bullet.Bullet_U556::Start()
extern void Bullet_U556_Start_m4EE83A293FE1B121E4201F23D8D552C21E0DF363 (void);
// 0x00000301 System.Void Bullet.Bullet_U556::Update()
extern void Bullet_U556_Update_m15B15678E51B827DE8451DB3F97F42A81E025AA5 (void);
// 0x00000302 System.Void Bullet.Bullet_U556::OnDrawGizmosSelected()
extern void Bullet_U556_OnDrawGizmosSelected_m6040B0C030A8DD1985B1C7B37338F2D3C5908796 (void);
// 0x00000303 System.Void Bullet.Bullet_U556::.ctor()
extern void Bullet_U556__ctor_m989F31205ED9DF7D3AF7FA9A3F9625EEE23EE30B (void);
// 0x00000304 System.Void Bullet.Bullet_W75::Start()
extern void Bullet_W75_Start_m129179FDEDFD6676C1CCF2EDD48427B5A9874FEB (void);
// 0x00000305 System.Void Bullet.Bullet_W75::Update()
extern void Bullet_W75_Update_m25BD3D06E7918481F75CCE48DEEDA3D55CE5F33B (void);
// 0x00000306 System.Void Bullet.Bullet_W75::OnDrawGizmosSelected()
extern void Bullet_W75_OnDrawGizmosSelected_mEA94AD9A6B3D48AB8106AC5CD3B0C0D701DD5FF7 (void);
// 0x00000307 System.Void Bullet.Bullet_W75::.ctor()
extern void Bullet_W75__ctor_m0F83CD5E089EEAECCEF792F90022BDA27F00A241 (void);
// 0x00000308 System.Void Bullet.Bullet_Z46::Start()
extern void Bullet_Z46_Start_m8898CA2827A37183F452758B8694484FACAF4460 (void);
// 0x00000309 System.Void Bullet.Bullet_Z46::Update()
extern void Bullet_Z46_Update_mC9BBDC76D55FD5124ABAC4579715AA821B8D40C7 (void);
// 0x0000030A System.Void Bullet.Bullet_Z46::OnDrawGizmosSelected()
extern void Bullet_Z46_OnDrawGizmosSelected_m3B71F0181D1A8C233684BAA946D157372B170F05 (void);
// 0x0000030B System.Void Bullet.Bullet_Z46::.ctor()
extern void Bullet_Z46__ctor_mAEE5DB3049D824391AFA779965B6E8FC54A9A353 (void);
// 0x0000030C System.Int32 Manager.HeroBluePrint::GetSellAmount()
extern void HeroBluePrint_GetSellAmount_mDF12E5C55E45B410DCBD4F2A79DF7EAB57A44ABB (void);
// 0x0000030D System.Void Manager.HeroBluePrint::.ctor()
extern void HeroBluePrint__ctor_m55EDE4C35B443D976707CC7FE2C42D195C85A543 (void);
// 0x0000030E Manager.BuildManager Manager.BuildManager::get_Instance()
extern void BuildManager_get_Instance_m447C53DC52D9B8E9352F7CF07F13C1345CD8DA6F (void);
// 0x0000030F System.Void Manager.BuildManager::set_Instance(Manager.BuildManager)
extern void BuildManager_set_Instance_mA6204A4F0E4CA49361D88BE862C74472578890F3 (void);
// 0x00000310 System.Void Manager.BuildManager::Awake()
extern void BuildManager_Awake_m47DC36B5C3D606E054F41D2C5CDE67DEAB7290E9 (void);
// 0x00000311 System.Void Manager.BuildManager::Start()
extern void BuildManager_Start_m67CFEAABE2DB3AEC847746227732745BBB0B8CCF (void);
// 0x00000312 System.Boolean Manager.BuildManager::get_CanBuild()
extern void BuildManager_get_CanBuild_m0F6F9D491AF5F3924B8C5983FCC07FDBF446EA9C (void);
// 0x00000313 System.Boolean Manager.BuildManager::get_IsHeroBuilt()
extern void BuildManager_get_IsHeroBuilt_mADCDECD19EB9255AD1D6F7D1C608DCE36425C03F (void);
// 0x00000314 System.Void Manager.BuildManager::SelectHero(Map.Area)
extern void BuildManager_SelectHero_m45020078B6CDD905656790146B5C6CF1F364FB73 (void);
// 0x00000315 System.Void Manager.BuildManager::DeselectHero()
extern void BuildManager_DeselectHero_mBDBAF5B338497ACEC9011EAD3DFFE2CACBEC43CA (void);
// 0x00000316 System.Void Manager.BuildManager::SetHero(Manager.HeroBluePrint)
extern void BuildManager_SetHero_m05352D0582113E9F4355EB8FA228031EDF8E007D (void);
// 0x00000317 Manager.HeroBluePrint Manager.BuildManager::GetHeroToBuild()
extern void BuildManager_GetHeroToBuild_mE2E3B5CB59AD263F066BFD199B7FE45E9901DE8A (void);
// 0x00000318 System.Void Manager.BuildManager::.ctor()
extern void BuildManager__ctor_m31E3581B92CBEA74A1BFCA1EFD2C867C63EA8D26 (void);
// 0x00000319 Manager.GameManager Manager.GameManager::get_Instance()
extern void GameManager_get_Instance_m705AC8D9CAF114B59D3AF0563C19B5EE0B711074 (void);
// 0x0000031A System.Void Manager.GameManager::set_Instance(Manager.GameManager)
extern void GameManager_set_Instance_m9DB0F13876CD49066CC3A818D68686B3600F792E (void);
// 0x0000031B System.Void Manager.GameManager::add_GameOver(System.Action)
extern void GameManager_add_GameOver_mBED5EF8A33E9E9D30CCF2FF978C82C91AE824089 (void);
// 0x0000031C System.Void Manager.GameManager::remove_GameOver(System.Action)
extern void GameManager_remove_GameOver_m4A4097C770BD88B02898662A4165CDD35CD06677 (void);
// 0x0000031D System.Int32 Manager.GameManager::get_EnemyCount()
extern void GameManager_get_EnemyCount_mC5D539611B83ABD18DDE25362AAF39D6820E2588 (void);
// 0x0000031E System.Void Manager.GameManager::set_EnemyCount(System.Int32)
extern void GameManager_set_EnemyCount_mD5A07C5A7544A51C663A9921CC8F86E7041EA7D2 (void);
// 0x0000031F System.Int32 Manager.GameManager::get_GetCountdownOverTime()
extern void GameManager_get_GetCountdownOverTime_mE81FB3FF717EEDBE8457148BA95BE247E29B9CFD (void);
// 0x00000320 System.Void Manager.GameManager::Awake()
extern void GameManager_Awake_m821FE927548316EF82F1314392E190FC88778D52 (void);
// 0x00000321 System.Void Manager.GameManager::Start()
extern void GameManager_Start_mC69A683D4A04B4F90D765739ADF7D6C38C69F0AF (void);
// 0x00000322 System.Void Manager.GameManager::OnAdsReward()
extern void GameManager_OnAdsReward_mD63CE8A454A6C0770D504CB949646A1BABBB5762 (void);
// 0x00000323 System.Void Manager.GameManager::Update()
extern void GameManager_Update_m5431C66CACDF9E904C8E86AC7C164A4FF9E23DB9 (void);
// 0x00000324 System.Void Manager.GameManager::UpgradeEnemy()
extern void GameManager_UpgradeEnemy_mC6DF633917D6C46DE0F2B1732AE2CA2A01A6F334 (void);
// 0x00000325 System.Void Manager.GameManager::GetEnemyPicture(System.Int32)
extern void GameManager_GetEnemyPicture_m653D6E7B83289032F23720CBFD0DAE68C94DD0CF (void);
// 0x00000326 System.Void Manager.GameManager::UpdateEnemyInfo()
extern void GameManager_UpdateEnemyInfo_mB4E77AFDD16F194F614375BF6356D30181A40746 (void);
// 0x00000327 System.Void Manager.GameManager::GetHeroPicture(System.Int32)
extern void GameManager_GetHeroPicture_mD10D51FFB845B60B35976BE51A4C3710C5BA125B (void);
// 0x00000328 System.Void Manager.GameManager::UpdateHeroInfo()
extern void GameManager_UpdateHeroInfo_m4CC3E434C573C2C72341B4CDF3DEF11951A6926C (void);
// 0x00000329 System.Void Manager.GameManager::Spawn_Colubted()
extern void GameManager_Spawn_Colubted_mFC9D59E83643C0A48CD503F57D7A5F8E7B776A9B (void);
// 0x0000032A System.Void Manager.GameManager::Spawn_Embio()
extern void GameManager_Spawn_Embio_mE03625CC8573709C6B9836FEB2E33ED8BE37A22E (void);
// 0x0000032B System.Void Manager.GameManager::Spawn_Eclipseside()
extern void GameManager_Spawn_Eclipseside_m1B24FAB3354CEE5228F2EE64283EEDB697BF4B28 (void);
// 0x0000032C System.Void Manager.GameManager::Spawn_CrossDive()
extern void GameManager_Spawn_CrossDive_mD6E700E04C41B6793853EE7196DDA1ED5F03AA1A (void);
// 0x0000032D System.Void Manager.GameManager::Spawn_Shiro()
extern void GameManager_Spawn_Shiro_mA778293D823810DCBB656FE944A383CDFE1393B0 (void);
// 0x0000032E System.Void Manager.GameManager::Spawn_Kana()
extern void GameManager_Spawn_Kana_m19052171B0D10E0CED29F4D466930C76F79F4740 (void);
// 0x0000032F System.Void Manager.GameManager::Spawn_Chest()
extern void GameManager_Spawn_Chest_mE8574A07743D4BB7773BDEA1C9D86C85825A3374 (void);
// 0x00000330 System.Void Manager.GameManager::EnemyMoreThanLimit()
extern void GameManager_EnemyMoreThanLimit_m0C966A880C571B2BE28945BF1EA53188DD2409F9 (void);
// 0x00000331 System.Void Manager.GameManager::GameEnded()
extern void GameManager_GameEnded_m21DEA77AB67552E97AB9728609AEA8EB2645633E (void);
// 0x00000332 System.Void Manager.GameManager::Start_Colubted_Wave()
extern void GameManager_Start_Colubted_Wave_mA1BA1AB6B812755841331A5F6B13FB7B10F67875 (void);
// 0x00000333 System.Void Manager.GameManager::End_Wave()
extern void GameManager_End_Wave_m47AB0514552FCE3E7E4255AD29DEB332D42DEB09 (void);
// 0x00000334 System.Void Manager.GameManager::Start_Embio_Wave()
extern void GameManager_Start_Embio_Wave_m465D7B3AA05B4D82429D36AA1C6DF8884047A6A5 (void);
// 0x00000335 System.Void Manager.GameManager::Start_Eclipseside_Wave()
extern void GameManager_Start_Eclipseside_Wave_m12D5F86EC89C8BCA0709957EBB0E192CFEC3DAE1 (void);
// 0x00000336 System.Void Manager.GameManager::Start_CrossDive_Wave()
extern void GameManager_Start_CrossDive_Wave_m4CB5034D50DF2827DB652199D7C722E4515FA050 (void);
// 0x00000337 System.Void Manager.GameManager::Start_Shiro_Wave()
extern void GameManager_Start_Shiro_Wave_m98F53C844A27CB04DBA7F3E3C79432FFF8A38662 (void);
// 0x00000338 System.Void Manager.GameManager::Start_Kana_Wave()
extern void GameManager_Start_Kana_Wave_mEC9621F71D82363E5F9AE6EA64471461F0C72244 (void);
// 0x00000339 System.Void Manager.GameManager::Start_Chest_Wave()
extern void GameManager_Start_Chest_Wave_m66CA5948A7D2784F57F885071D3F42D98BFC4263 (void);
// 0x0000033A System.Void Manager.GameManager::GotoMainMenu()
extern void GameManager_GotoMainMenu_mDDCFA1F4A1EE0CF13795E0BA9FC057547950F765 (void);
// 0x0000033B System.Void Manager.GameManager::.ctor()
extern void GameManager__ctor_m4D37F4709FCE3942A010D8A64ECF2C2BA941D198 (void);
// 0x0000033C Manager.InventoryManager Manager.InventoryManager::get_Instance()
extern void InventoryManager_get_Instance_mD984E22ADAC6EE38F0CE613A6AD261064F62E121 (void);
// 0x0000033D System.Void Manager.InventoryManager::set_Instance(Manager.InventoryManager)
extern void InventoryManager_set_Instance_m290F386A14BBDD7A90E7A57D1F43D0CBE9680AF2 (void);
// 0x0000033E System.Void Manager.InventoryManager::Awake()
extern void InventoryManager_Awake_m86DF25A66E73FD10E9D077AAA95A6438D1C56697 (void);
// 0x0000033F System.Void Manager.InventoryManager::Start()
extern void InventoryManager_Start_m9E30E006307D2ADF875AA617511B37FF7789B661 (void);
// 0x00000340 System.Void Manager.InventoryManager::AddCardToInventory()
extern void InventoryManager_AddCardToInventory_m2BBB77157EF6D61EFC0451C8B8305CA3618C6B78 (void);
// 0x00000341 System.Void Manager.InventoryManager::TagCardId(UnityEngine.UI.Button)
extern void InventoryManager_TagCardId_m1BF827CB925D4B8E715E1E93EBA89AD75313C877 (void);
// 0x00000342 System.Void Manager.InventoryManager::AddCard(Manager.HeroName,CardBluePrint,UnityEngine.UI.Button)
extern void InventoryManager_AddCard_m5EC74258266C344C877249E8020229A0F91B34FE (void);
// 0x00000343 System.Void Manager.InventoryManager::GetCard(Manager.HeroName,CardBluePrint,UnityEngine.UI.Button)
extern void InventoryManager_GetCard_m7F701180E87E15385A0FB2DA9F83C74A1F1F0391 (void);
// 0x00000344 System.Void Manager.InventoryManager::CancelCardUsed(Manager.HeroName,UnityEngine.UI.Button,CardBluePrint)
extern void InventoryManager_CancelCardUsed_mFD94F2B274B3DE95D5B93C58FABE670CF8607D07 (void);
// 0x00000345 System.Void Manager.InventoryManager::GetHero(Manager.HeroName)
extern void InventoryManager_GetHero_m3C1013B04EE546101C53A637C635ED87E3C62623 (void);
// 0x00000346 System.Void Manager.InventoryManager::PickHero(Manager.HeroName)
extern void InventoryManager_PickHero_mE0B3D197BF30F7AF35FE0B1EED268236388BD86D (void);
// 0x00000347 System.Void Manager.InventoryManager::ShowInventoryUI()
extern void InventoryManager_ShowInventoryUI_mFED06A213DCF737DA14C5FB39F4CCA38F1768B78 (void);
// 0x00000348 System.Void Manager.InventoryManager::.ctor()
extern void InventoryManager__ctor_m7103387150C0ECB2B88D0C28D4F631C5F672D65B (void);
// 0x00000349 Manager.MoneyManager Manager.MoneyManager::get_Instance()
extern void MoneyManager_get_Instance_mD33F844A651475BDB3C97054EAAF8B8198A9AA5E (void);
// 0x0000034A System.Void Manager.MoneyManager::set_Instance(Manager.MoneyManager)
extern void MoneyManager_set_Instance_m9FDBDFA722BB515C6AB48DC35A5EF19C7BAB2798 (void);
// 0x0000034B System.Int32 Manager.MoneyManager::get_Money()
extern void MoneyManager_get_Money_m9F70B21FCD308184615E006F48FC5DDCB43BEA8D (void);
// 0x0000034C System.Void Manager.MoneyManager::set_Money(System.Int32)
extern void MoneyManager_set_Money_m39C6D08CC4093DAFBE32E4B86887FCB14882C0DB (void);
// 0x0000034D System.Void Manager.MoneyManager::AddMoney(System.Int32)
extern void MoneyManager_AddMoney_mDF0321F2FC4195904E9E61B32CE4360B655FB77D (void);
// 0x0000034E System.Void Manager.MoneyManager::DeleteMoney(System.Int32)
extern void MoneyManager_DeleteMoney_mD65C702DA90BB48C01CB79DD3D29383C7100F2D0 (void);
// 0x0000034F System.Void Manager.MoneyManager::Awake()
extern void MoneyManager_Awake_m009CA67919C27BFDF266292870B3373F3CEC84F4 (void);
// 0x00000350 System.Void Manager.MoneyManager::Start()
extern void MoneyManager_Start_mD77889A1713D67CC6C4AACE87D81A40362F7D188 (void);
// 0x00000351 System.Void Manager.MoneyManager::.ctor()
extern void MoneyManager__ctor_m02096BABFD8D485FFF48B7634D60D42BD8A8C189 (void);
// 0x00000352 Manager.RandomScrollManager Manager.RandomScrollManager::get_Instance()
extern void RandomScrollManager_get_Instance_m71192DB4EC184AB40057DDB9A1A58BF2F682D6DC (void);
// 0x00000353 System.Void Manager.RandomScrollManager::set_Instance(Manager.RandomScrollManager)
extern void RandomScrollManager_set_Instance_mC51D12544F256ABBFB0604230F7E9FBD14A34627 (void);
// 0x00000354 System.Boolean Manager.RandomScrollManager::get_NotEnoughMoneyScore()
extern void RandomScrollManager_get_NotEnoughMoneyScore_m2CEDCC4DD5A63ACB0FF0E4F56926E38713872F6E (void);
// 0x00000355 System.Boolean Manager.RandomScrollManager::get_NotEnoughMoneyScorePlus()
extern void RandomScrollManager_get_NotEnoughMoneyScorePlus_mAC187D9247AD10FDE2BCB62AD189A9C01E75DC56 (void);
// 0x00000356 System.Void Manager.RandomScrollManager::add_OnFirstRandomCard(System.Action)
extern void RandomScrollManager_add_OnFirstRandomCard_m55B9B6C95F99B39B0283EE6771C362967EEF59A3 (void);
// 0x00000357 System.Void Manager.RandomScrollManager::remove_OnFirstRandomCard(System.Action)
extern void RandomScrollManager_remove_OnFirstRandomCard_m75726C3DEC3C365DE5DFB8A44F5E0D99E658DD6B (void);
// 0x00000358 System.Void Manager.RandomScrollManager::add_AddCard(System.Action)
extern void RandomScrollManager_add_AddCard_m1DFB2FEF7AB61EE4C232D430BF6434FB96FDDC89 (void);
// 0x00000359 System.Void Manager.RandomScrollManager::remove_AddCard(System.Action)
extern void RandomScrollManager_remove_AddCard_m16A0C1743E0F9F8152FA98C089624F21376AD0AC (void);
// 0x0000035A System.Void Manager.RandomScrollManager::Awake()
extern void RandomScrollManager_Awake_m6B416832189E57E29BBE65E517A0537310DA2357 (void);
// 0x0000035B System.Void Manager.RandomScrollManager::Start()
extern void RandomScrollManager_Start_m737477F3C7A12F2A65635005B277D0A48501B775 (void);
// 0x0000035C System.Void Manager.RandomScrollManager::RandomCardOnStart()
extern void RandomScrollManager_RandomCardOnStart_m497D329AB662A001B77A72BC4A58201725D87993 (void);
// 0x0000035D System.Void Manager.RandomScrollManager::ClearCard()
extern void RandomScrollManager_ClearCard_mA3B65BB79544AFB43170291CF80C835780870A77 (void);
// 0x0000035E System.Void Manager.RandomScrollManager::RandomCard()
extern void RandomScrollManager_RandomCard_mE696C58712B9F9B3AD0847DEB0C8313620500421 (void);
// 0x0000035F System.Void Manager.RandomScrollManager::ShowCard()
extern void RandomScrollManager_ShowCard_m1F0446B2C6641AFB1578D9B16276B4D90B50054A (void);
// 0x00000360 System.Void Manager.RandomScrollManager::HideCard()
extern void RandomScrollManager_HideCard_m38066E4FCA046DBE7514AE5441CD761239BE0951 (void);
// 0x00000361 System.Void Manager.RandomScrollManager::ReRoll()
extern void RandomScrollManager_ReRoll_mF7D2EED2F23B12D0C8C956CF4022BF8BB8B4DD8F (void);
// 0x00000362 System.Void Manager.RandomScrollManager::AddCard1()
extern void RandomScrollManager_AddCard1_m5410E77622E9E6E587514EF5E3E73A6BEE97D2EA (void);
// 0x00000363 System.Void Manager.RandomScrollManager::AddCard2()
extern void RandomScrollManager_AddCard2_mC47A6EA099F8B44A780737196D6264B3B665451E (void);
// 0x00000364 System.Void Manager.RandomScrollManager::AddCard3()
extern void RandomScrollManager_AddCard3_m765C29E56DC213917B59BA874501C85944E4E2DA (void);
// 0x00000365 System.Void Manager.RandomScrollManager::.ctor()
extern void RandomScrollManager__ctor_m483B15CFC6395915D0B4066E753E66373E68D105 (void);
// 0x00000366 Manager.UiManager Manager.UiManager::get_Instance()
extern void UiManager_get_Instance_mD22B7B526318C512A940851D7C5F3605A3188F72 (void);
// 0x00000367 System.Void Manager.UiManager::set_Instance(Manager.UiManager)
extern void UiManager_set_Instance_mC55D7E31E56BDE146DC3270088C52E0D5C7CEBEA (void);
// 0x00000368 System.Void Manager.UiManager::Awake()
extern void UiManager_Awake_m5BEF6BE5A4FE6BE73E78D490AE5F9DC31291CEC6 (void);
// 0x00000369 System.Void Manager.UiManager::Start()
extern void UiManager_Start_mAEC8014428404D9EE8B303926D93E1F7784BE0F8 (void);
// 0x0000036A System.Void Manager.UiManager::FinishAdsReward()
extern void UiManager_FinishAdsReward_m2ABB56493CA19A845EE695023A6E1F3C0D90D4F2 (void);
// 0x0000036B System.Void Manager.UiManager::ShowRandomButton()
extern void UiManager_ShowRandomButton_m752FA56BE7282C6583C11E97EB05E6BE2FC99C55 (void);
// 0x0000036C System.Void Manager.UiManager::OnRestartButtonClicked()
extern void UiManager_OnRestartButtonClicked_mF11CC49EEBA6A78FC58A228249EE68F312B1F8F3 (void);
// 0x0000036D System.Void Manager.UiManager::Update()
extern void UiManager_Update_m602B413B5FCE514DD032305F5F43E32CC1FD1CF5 (void);
// 0x0000036E System.Void Manager.UiManager::UpdateCardInventory()
extern void UiManager_UpdateCardInventory_m300AF988DB9634A041B0AF02A6B2317637736DAC (void);
// 0x0000036F System.Void Manager.UiManager::ShowGameOver()
extern void UiManager_ShowGameOver_mEB30463AE683687EF3F9E272022336CA791B050B (void);
// 0x00000370 System.Void Manager.UiManager::SetWaveCount(System.String)
extern void UiManager_SetWaveCount_m9569D47246B4EFFC341D758E69D4F0E8A50FCD6E (void);
// 0x00000371 System.Void Manager.UiManager::GetHeroSubHeroAmount()
extern void UiManager_GetHeroSubHeroAmount_m2E643F76D41C24AA1B21B906096CCE6B494A0F7A (void);
// 0x00000372 System.Void Manager.UiManager::GetCurrentEnemyAmount()
extern void UiManager_GetCurrentEnemyAmount_m5626F482BD2851178F9E6E13F2B85F37921F8DF0 (void);
// 0x00000373 System.Void Manager.UiManager::GetCountDownGameOver()
extern void UiManager_GetCountDownGameOver_m268AE718B0955059E11990ADE18F7D1F1826CBE7 (void);
// 0x00000374 System.Void Manager.UiManager::HideTimerNotification()
extern void UiManager_HideTimerNotification_mDB178E930B8B3115266EC5A6029FA72176C8BCE7 (void);
// 0x00000375 System.Collections.IEnumerator Manager.UiManager::WaitForNoticeTimerOut()
extern void UiManager_WaitForNoticeTimerOut_mC2A0447A655995E085F1CF523E140BE180B9BBE9 (void);
// 0x00000376 System.Void Manager.UiManager::GetNotification()
extern void UiManager_GetNotification_m24AA4D5B27B37F62293A864A17E0BCE85DC92CEE (void);
// 0x00000377 System.Void Manager.UiManager::SetTextNotification(System.String)
extern void UiManager_SetTextNotification_mEE4E06CE14132D2C42882072AF7AFB3215075F24 (void);
// 0x00000378 System.Collections.IEnumerator Manager.UiManager::WaitForNoticeOut()
extern void UiManager_WaitForNoticeOut_m80849B33FF4B00687BDF23BDA6DE02123DEF6831 (void);
// 0x00000379 System.Void Manager.UiManager::NotEnoughMoneyUI()
extern void UiManager_NotEnoughMoneyUI_mADD8A2CE44C81C22D862B4293E947889FAC87D2E (void);
// 0x0000037A System.Void Manager.UiManager::OnBuyRandomScrollButtonClicked()
extern void UiManager_OnBuyRandomScrollButtonClicked_m4CA0969BE782DB98FE738FC605B1CA21E41DBD04 (void);
// 0x0000037B System.Void Manager.UiManager::OnReRandomButtonClicked()
extern void UiManager_OnReRandomButtonClicked_mAE4524AC7DD905AC578908F8D2FBED36C6F96A6E (void);
// 0x0000037C System.Void Manager.UiManager::OnBuyRandomScrollPlusButtonClicked()
extern void UiManager_OnBuyRandomScrollPlusButtonClicked_m9AF1F900786473075121B5BC68AAD1D4E123C104 (void);
// 0x0000037D System.Void Manager.UiManager::OnCloseRandomButtonClicked()
extern void UiManager_OnCloseRandomButtonClicked_mBBEDD95CA422C9D970D69E37C92D9DEC166D1223 (void);
// 0x0000037E System.Void Manager.UiManager::SpawnChainSkillText(System.String)
extern void UiManager_SpawnChainSkillText_mA6CC644F29891F8823C6AA865CC729B4F9AAB1D3 (void);
// 0x0000037F System.Void Manager.UiManager::RemoveChainSkillText(System.String)
extern void UiManager_RemoveChainSkillText_m0E99C5CB0FCA7728A8AC014D46183ADA3ED7D244 (void);
// 0x00000380 System.Void Manager.UiManager::SpawnChainElement(System.Int32)
extern void UiManager_SpawnChainElement_mECF976B3F7841CB95BFD926D6595AF74C19F2CE4 (void);
// 0x00000381 System.Void Manager.UiManager::RemoveChainElement(System.Int32)
extern void UiManager_RemoveChainElement_m3C31CE220D7A108557C70D54D8485F90C831E0F5 (void);
// 0x00000382 System.Void Manager.UiManager::SetTimeSurvived(System.String)
extern void UiManager_SetTimeSurvived_m7D4BF72F85D64604AD92163AE0A5C055F1FAFB36 (void);
// 0x00000383 System.Void Manager.UiManager::SetWaveSurvived(System.String)
extern void UiManager_SetWaveSurvived_m7CAD2C5729733F67DD6AC26D72DC93F476EC6452 (void);
// 0x00000384 System.Void Manager.UiManager::SetDamageDone()
extern void UiManager_SetDamageDone_m80EEC02ED196909AFF938A78B2A12964E3DBF897 (void);
// 0x00000385 System.Void Manager.UiManager::SetCompUsed(System.String)
extern void UiManager_SetCompUsed_mCBAE5653066F3E3885D6D078D01F485EBF077E75 (void);
// 0x00000386 System.Void Manager.UiManager::.ctor()
extern void UiManager__ctor_m70492F9645C447E80D2E401C45C7F19E15754406 (void);
// 0x00000387 Manager.WaveManager Manager.WaveManager::get_Instance()
extern void WaveManager_get_Instance_m245870AA93671235A9952030594169263629EA4F (void);
// 0x00000388 System.Void Manager.WaveManager::set_Instance(Manager.WaveManager)
extern void WaveManager_set_Instance_mEDF39195B0EDFB8F8661CA8614CAE135274FDA64 (void);
// 0x00000389 System.Single Manager.WaveManager::get_Countdown()
extern void WaveManager_get_Countdown_m13E9315BE0AF304560DBDD9D9D152CABC4878C86 (void);
// 0x0000038A System.Void Manager.WaveManager::set_Countdown(System.Single)
extern void WaveManager_set_Countdown_mB1EE76187468F945164393EE86AF16C2AC70FBB3 (void);
// 0x0000038B System.Void Manager.WaveManager::add_OnNextWave(System.Action)
extern void WaveManager_add_OnNextWave_mBFB31E8EE88D78103A44F33E610C07051A7A209E (void);
// 0x0000038C System.Void Manager.WaveManager::remove_OnNextWave(System.Action)
extern void WaveManager_remove_OnNextWave_mB3182F529CF54627875EBADF430F16687BD211F5 (void);
// 0x0000038D System.Void Manager.WaveManager::Awake()
extern void WaveManager_Awake_m47B112FB6D5462FDB30EEA5B91B082CEA1674D55 (void);
// 0x0000038E System.Void Manager.WaveManager::Start()
extern void WaveManager_Start_m066305636504F4AC3A870930DF9BF541ADDC90FA (void);
// 0x0000038F System.Void Manager.WaveManager::Update()
extern void WaveManager_Update_m9DC75D4D382EA7E4416CB8667925AE6AFFC17BC3 (void);
// 0x00000390 System.Void Manager.WaveManager::GetWaveAndTimeGameOver()
extern void WaveManager_GetWaveAndTimeGameOver_m1641FEC82072D469F8F67B027C283F4B28221FDB (void);
// 0x00000391 System.Void Manager.WaveManager::WaveManage()
extern void WaveManager_WaveManage_m291BF8DD3EE9E1CEBC120ABFAE68AFC53DAE6EB8 (void);
// 0x00000392 System.Void Manager.WaveManager::.ctor()
extern void WaveManager__ctor_m8125B6CA2FFD8462A76F2C92420B403A304CEA7C (void);
// 0x00000393 Manager.WorldUIManager Manager.WorldUIManager::get_Instance()
extern void WorldUIManager_get_Instance_m69CA555FDFB6797BFB2D7E2CBF758AD7CF1BE7EF (void);
// 0x00000394 System.Void Manager.WorldUIManager::set_Instance(Manager.WorldUIManager)
extern void WorldUIManager_set_Instance_m4F34BA28C67F98C9F4152355F3B13247D259AB0A (void);
// 0x00000395 System.Void Manager.WorldUIManager::Awake()
extern void WorldUIManager_Awake_mB4D6AAB48187C19552529C752164CF3B0BAF8304 (void);
// 0x00000396 System.Void Manager.WorldUIManager::UpdateEnemyInfoText(System.String)
extern void WorldUIManager_UpdateEnemyInfoText_mC99C90BF8DF880E21D5A1A24A2CC904329613D9F (void);
// 0x00000397 System.Void Manager.WorldUIManager::UpdateHeroInfoText(System.String)
extern void WorldUIManager_UpdateHeroInfoText_m8DE11CBD55CD640491F265E8FF873473DD5FDA86 (void);
// 0x00000398 System.Collections.IEnumerator Manager.WorldUIManager::NextEnemy()
extern void WorldUIManager_NextEnemy_m9B9022BB73E120504B17458013796E023FB5AE30 (void);
// 0x00000399 System.Collections.IEnumerator Manager.WorldUIManager::NextHero()
extern void WorldUIManager_NextHero_m31ABAD8168C9EAB5D6829B341F1E759AE2D8D137 (void);
// 0x0000039A System.Void Manager.WorldUIManager::ShowFireWorkUI()
extern void WorldUIManager_ShowFireWorkUI_m6F04947DA87AEF3EA3E69EA1C625073E0F07A677 (void);
// 0x0000039B System.Void Manager.WorldUIManager::ShowCookieUI()
extern void WorldUIManager_ShowCookieUI_m3E872B2A2C5B9227C5DFA42F370F574937CC1B77 (void);
// 0x0000039C System.Void Manager.WorldUIManager::ShowCandy()
extern void WorldUIManager_ShowCandy_m38D7F48BA705992258B229B6369AC45CA962FD20 (void);
// 0x0000039D System.Void Manager.WorldUIManager::ShowScreenShot()
extern void WorldUIManager_ShowScreenShot_mAF73F9B85D3C7F208C82B161F871BA4F0EADEA2C (void);
// 0x0000039E System.Void Manager.WorldUIManager::ShowLeaderBoard()
extern void WorldUIManager_ShowLeaderBoard_m49B1CAAD05F7EC9AC86D0DB79E4A095FC0F87752 (void);
// 0x0000039F System.Collections.IEnumerator Manager.WorldUIManager::DisableInfo()
extern void WorldUIManager_DisableInfo_m486646FD2278E1A68A2ACC89D9B5C195CB5D6D0C (void);
// 0x000003A0 System.Collections.IEnumerator Manager.WorldUIManager::FireWorkClicked()
extern void WorldUIManager_FireWorkClicked_m2E89B09A70BB788222B38B1CC8704857A97B43BE (void);
// 0x000003A1 System.Collections.IEnumerator Manager.WorldUIManager::CookieClicked()
extern void WorldUIManager_CookieClicked_mCB4A623F5D5D4231D2EC0C4BAD9FC47E9BF83F1E (void);
// 0x000003A2 System.Collections.IEnumerator Manager.WorldUIManager::CandyClicked()
extern void WorldUIManager_CandyClicked_m004176E7D20CC7412E7303ACA8CDF6ED8C81D6B3 (void);
// 0x000003A3 System.Collections.IEnumerator Manager.WorldUIManager::ScreenShotClicked()
extern void WorldUIManager_ScreenShotClicked_m957D2F14806168935EB078D5FCF9CCBA2EB21B5A (void);
// 0x000003A4 System.Collections.IEnumerator Manager.WorldUIManager::LeaderBoardClicked()
extern void WorldUIManager_LeaderBoardClicked_m7933E88951F99F4887431F5E90C732CDC4D74627 (void);
// 0x000003A5 System.Void Manager.WorldUIManager::OnScreenShotToggle()
extern void WorldUIManager_OnScreenShotToggle_m4167432ACB401A5C1A002F6FD9F37EA5E9C9FB6F (void);
// 0x000003A6 System.Void Manager.WorldUIManager::.ctor()
extern void WorldUIManager__ctor_m1FEB23E1D23BA593DB04C2CEC0CDDDBE983FA68C (void);
// 0x000003A7 System.Void KAID_Script.SafeAreaSetter::Awake()
extern void SafeAreaSetter_Awake_m8CDC989411F0A6B2970439146BACFB54940F8BC0 (void);
// 0x000003A8 System.Void KAID_Script.SafeAreaSetter::.ctor()
extern void SafeAreaSetter__ctor_mF3443520D4663615C2A80F465F9FE50461CD412A (void);
// 0x000003A9 System.Void Script.Floater::FixedUpdate()
extern void Floater_FixedUpdate_m65A78EC97F6BB17CF34859E6FDBA8467C7587937 (void);
// 0x000003AA System.Void Script.Floater::.ctor()
extern void Floater__ctor_m66BADD84264ADAB461E8A97E0D06E05C66AE07B5 (void);
// 0x000003AB System.UInt32 <PrivateImplementationDetails>::ComputeStringHash(System.String)
extern void U3CPrivateImplementationDetailsU3E_ComputeStringHash_mD94B0E22EF32AD3DFD277ED8E911B5DFA4CDB91E (void);
// 0x000003AC System.Void CinemachineSwitcher/<WaitToStart>d__14::.ctor(System.Int32)
extern void U3CWaitToStartU3Ed__14__ctor_m29B7C44F25045CA2AF686E43C9FC185788C40503 (void);
// 0x000003AD System.Void CinemachineSwitcher/<WaitToStart>d__14::System.IDisposable.Dispose()
extern void U3CWaitToStartU3Ed__14_System_IDisposable_Dispose_mD5DE83D793D38D79C4BFC2C7300A08CBA9A27C3E (void);
// 0x000003AE System.Boolean CinemachineSwitcher/<WaitToStart>d__14::MoveNext()
extern void U3CWaitToStartU3Ed__14_MoveNext_mFF0C85FF523AAE60E111BA772AD683F307BF84F3 (void);
// 0x000003AF System.Object CinemachineSwitcher/<WaitToStart>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitToStartU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9EC5D5E119B878E35802A8030693DCAEE73E1886 (void);
// 0x000003B0 System.Void CinemachineSwitcher/<WaitToStart>d__14::System.Collections.IEnumerator.Reset()
extern void U3CWaitToStartU3Ed__14_System_Collections_IEnumerator_Reset_m524B6AAAA453CEFB7943404F24E2BAB89E723FD3 (void);
// 0x000003B1 System.Object CinemachineSwitcher/<WaitToStart>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CWaitToStartU3Ed__14_System_Collections_IEnumerator_get_Current_m54B43C5EE89291B63D84359793AF6F746A48A102 (void);
// 0x000003B2 System.Void CinemachineSwitcher/<WaitForCam>d__18::.ctor(System.Int32)
extern void U3CWaitForCamU3Ed__18__ctor_m0F9FDC3F7885AD8CB9300502DAB0CF3BD03ADE43 (void);
// 0x000003B3 System.Void CinemachineSwitcher/<WaitForCam>d__18::System.IDisposable.Dispose()
extern void U3CWaitForCamU3Ed__18_System_IDisposable_Dispose_m60C82B3C406C8BD576F9B5C7C8A3E9399F2844F0 (void);
// 0x000003B4 System.Boolean CinemachineSwitcher/<WaitForCam>d__18::MoveNext()
extern void U3CWaitForCamU3Ed__18_MoveNext_m940216FF13F15E4A77F99F2E54389A1209A6B5B7 (void);
// 0x000003B5 System.Object CinemachineSwitcher/<WaitForCam>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForCamU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4D0F0E0221A015D42D448233811E1E3F1D18844A (void);
// 0x000003B6 System.Void CinemachineSwitcher/<WaitForCam>d__18::System.Collections.IEnumerator.Reset()
extern void U3CWaitForCamU3Ed__18_System_Collections_IEnumerator_Reset_m3A75E79EA70C002A7A8E6357119F039688749E5E (void);
// 0x000003B7 System.Object CinemachineSwitcher/<WaitForCam>d__18::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForCamU3Ed__18_System_Collections_IEnumerator_get_Current_mE2168EB7F89A76D61B57517E899160E4E598659C (void);
// 0x000003B8 System.Void CinemachineSwitcher/<WaitForShowCanvas>d__21::.ctor(System.Int32)
extern void U3CWaitForShowCanvasU3Ed__21__ctor_m35D4BF98BB8BA1BC8821E567536E8DFF2D90F18D (void);
// 0x000003B9 System.Void CinemachineSwitcher/<WaitForShowCanvas>d__21::System.IDisposable.Dispose()
extern void U3CWaitForShowCanvasU3Ed__21_System_IDisposable_Dispose_m82BACA53350D7E4E634FA212196B7DADBE60FEFA (void);
// 0x000003BA System.Boolean CinemachineSwitcher/<WaitForShowCanvas>d__21::MoveNext()
extern void U3CWaitForShowCanvasU3Ed__21_MoveNext_mA15A30F87CBD29EE0D184424116903578C50CD57 (void);
// 0x000003BB System.Object CinemachineSwitcher/<WaitForShowCanvas>d__21::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForShowCanvasU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4AC62F2A7C2D79CA96258278EB16915408AB0A0F (void);
// 0x000003BC System.Void CinemachineSwitcher/<WaitForShowCanvas>d__21::System.Collections.IEnumerator.Reset()
extern void U3CWaitForShowCanvasU3Ed__21_System_Collections_IEnumerator_Reset_m0919B8BEE473A81A79E5048EE957A3EF9DD9922B (void);
// 0x000003BD System.Object CinemachineSwitcher/<WaitForShowCanvas>d__21::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForShowCanvasU3Ed__21_System_Collections_IEnumerator_get_Current_m75D2DA098B2ECBB96569E0DF8A16E85DF389FBF2 (void);
// 0x000003BE System.Void InputManager/StartTouch::.ctor(System.Object,System.IntPtr)
extern void StartTouch__ctor_m5CEF3C2B02DD8AA5E09F2AB1B6157F2FD77F9615 (void);
// 0x000003BF System.Void InputManager/StartTouch::Invoke(UnityEngine.Vector2,System.Single)
extern void StartTouch_Invoke_mE16B1DEB5FD8F966D19ED393666A832869C30266 (void);
// 0x000003C0 System.IAsyncResult InputManager/StartTouch::BeginInvoke(UnityEngine.Vector2,System.Single,System.AsyncCallback,System.Object)
extern void StartTouch_BeginInvoke_mC4859EA8DF9A46F32FAA4392CD02717181887BD6 (void);
// 0x000003C1 System.Void InputManager/StartTouch::EndInvoke(System.IAsyncResult)
extern void StartTouch_EndInvoke_m11CCBBAA2F257BEAAAFA29C0F47DEAA3D1BD6346 (void);
// 0x000003C2 System.Void InputManager/EndTouch::.ctor(System.Object,System.IntPtr)
extern void EndTouch__ctor_m3741372E9F1D53D5AB4D702882317B5C2BEA97AC (void);
// 0x000003C3 System.Void InputManager/EndTouch::Invoke(UnityEngine.Vector2,System.Single)
extern void EndTouch_Invoke_mEDF30247BB87D5AFA917553C15CA410B00421F77 (void);
// 0x000003C4 System.IAsyncResult InputManager/EndTouch::BeginInvoke(UnityEngine.Vector2,System.Single,System.AsyncCallback,System.Object)
extern void EndTouch_BeginInvoke_mB67BD9B80F75086272A0F463615030133D1716DA (void);
// 0x000003C5 System.Void InputManager/EndTouch::EndInvoke(System.IAsyncResult)
extern void EndTouch_EndInvoke_mB4771BF3EF6C8CF0F41EC05C403C0C7E75B503A2 (void);
// 0x000003C6 System.Void MobileInput/TouchActions::.ctor(MobileInput)
extern void TouchActions__ctor_m5F79CEC71E38F8C83D916CB78192AC260AF83592 (void);
// 0x000003C7 UnityEngine.InputSystem.InputAction MobileInput/TouchActions::get_PrimaryContect()
extern void TouchActions_get_PrimaryContect_mB54C668A5C15D5F965ABE7CEB759E234361C7063 (void);
// 0x000003C8 UnityEngine.InputSystem.InputAction MobileInput/TouchActions::get_PrimaryPosition()
extern void TouchActions_get_PrimaryPosition_mAA3C5A043DD92BB973B901DBFE311D2B8A4112D8 (void);
// 0x000003C9 UnityEngine.InputSystem.InputActionMap MobileInput/TouchActions::Get()
extern void TouchActions_Get_mC8FC631CF4A79917E0F0D6303C9AFD669CA2F1C8 (void);
// 0x000003CA System.Void MobileInput/TouchActions::Enable()
extern void TouchActions_Enable_m9ABE56F3809AD02B52FEB58E0822CBFD13E15988 (void);
// 0x000003CB System.Void MobileInput/TouchActions::Disable()
extern void TouchActions_Disable_m237F8450B651D42320F5C745C07A4BD2EF0027C6 (void);
// 0x000003CC System.Boolean MobileInput/TouchActions::get_enabled()
extern void TouchActions_get_enabled_mCAA5D8F20D1CAB85BE56F95F033AEC090EC3A650 (void);
// 0x000003CD UnityEngine.InputSystem.InputActionMap MobileInput/TouchActions::op_Implicit(MobileInput/TouchActions)
extern void TouchActions_op_Implicit_mABA1D829548CA94240CB7CB92E0A81B334926630 (void);
// 0x000003CE System.Void MobileInput/TouchActions::SetCallbacks(MobileInput/ITouchActions)
extern void TouchActions_SetCallbacks_m948F215A5E7BF2316FF838E0BE421F42F6D81412 (void);
// 0x000003CF System.Void MobileInput/ITouchActions::OnPrimaryContect(UnityEngine.InputSystem.InputAction/CallbackContext)
// 0x000003D0 System.Void MobileInput/ITouchActions::OnPrimaryPosition(UnityEngine.InputSystem.InputAction/CallbackContext)
// 0x000003D1 System.Void ScreenCapture/<>c__DisplayClass15_0::.ctor()
extern void U3CU3Ec__DisplayClass15_0__ctor_mFAB3648391D7F7415C8099820E6687D05184B688 (void);
// 0x000003D2 System.Void ScreenCapture/<>c__DisplayClass15_0::<CaptureScreenshot>b__0()
extern void U3CU3Ec__DisplayClass15_0_U3CCaptureScreenshotU3Eb__0_m110EE270D4757DE3C34A53F2FA8CDC320BE4B987 (void);
// 0x000003D3 System.Void ScreenCapture/<ImageShowCase>d__17::.ctor(System.Int32)
extern void U3CImageShowCaseU3Ed__17__ctor_mCD4899C7ABD62E58107628266F40BCE1A71BD8C5 (void);
// 0x000003D4 System.Void ScreenCapture/<ImageShowCase>d__17::System.IDisposable.Dispose()
extern void U3CImageShowCaseU3Ed__17_System_IDisposable_Dispose_m0C3DEEFF1C66F08CCB7AFDC7309909C55D39EBBA (void);
// 0x000003D5 System.Boolean ScreenCapture/<ImageShowCase>d__17::MoveNext()
extern void U3CImageShowCaseU3Ed__17_MoveNext_mF87CD7D9E7883DB15BA089A85FA5EA5E98A4E49E (void);
// 0x000003D6 System.Object ScreenCapture/<ImageShowCase>d__17::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CImageShowCaseU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF252504AC2355F64820C71DF7B5DBCF6FF71F9B5 (void);
// 0x000003D7 System.Void ScreenCapture/<ImageShowCase>d__17::System.Collections.IEnumerator.Reset()
extern void U3CImageShowCaseU3Ed__17_System_Collections_IEnumerator_Reset_mCA401AA94E70446FFEED94191F1061F4E0FE5AD3 (void);
// 0x000003D8 System.Object ScreenCapture/<ImageShowCase>d__17::System.Collections.IEnumerator.get_Current()
extern void U3CImageShowCaseU3Ed__17_System_Collections_IEnumerator_get_Current_m470E7F0C70ED4B0BA7C5E8FA54FE620C60160B7C (void);
// 0x000003D9 System.Void LongClickButton/<DelayClick>d__8::.ctor(System.Int32)
extern void U3CDelayClickU3Ed__8__ctor_m482F03E4612F42F141D0E23529A7F47BD9D322D7 (void);
// 0x000003DA System.Void LongClickButton/<DelayClick>d__8::System.IDisposable.Dispose()
extern void U3CDelayClickU3Ed__8_System_IDisposable_Dispose_mD47B909BCEBDDC24A464483BCCF4456D5566180F (void);
// 0x000003DB System.Boolean LongClickButton/<DelayClick>d__8::MoveNext()
extern void U3CDelayClickU3Ed__8_MoveNext_m1A9448216F0C01605FE9D1DFDA7B3E1116B9DA2E (void);
// 0x000003DC System.Object LongClickButton/<DelayClick>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDelayClickU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m120379B849FCB4CEA022C33647A4ED839DA3B925 (void);
// 0x000003DD System.Void LongClickButton/<DelayClick>d__8::System.Collections.IEnumerator.Reset()
extern void U3CDelayClickU3Ed__8_System_Collections_IEnumerator_Reset_m227BC1F635FF1B103C8353698AE384738441998A (void);
// 0x000003DE System.Object LongClickButton/<DelayClick>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CDelayClickU3Ed__8_System_Collections_IEnumerator_get_Current_m4DB8DE888D03118595DF28332153EAE8662A0447 (void);
// 0x000003DF System.Void LongClickButton/<>c::.cctor()
extern void U3CU3Ec__cctor_m5CB640B2D2760E81EB392FD51B8C704144A1965A (void);
// 0x000003E0 System.Void LongClickButton/<>c::.ctor()
extern void U3CU3Ec__ctor_mAE2789A7F3C1325CEE49F6FD675C6018634C21DF (void);
// 0x000003E1 System.Boolean LongClickButton/<>c::<DestroyCard>b__10_0(Manager.HeroBluePrint)
extern void U3CU3Ec_U3CDestroyCardU3Eb__10_0_mD8F6FCF82CBB0DB29CFA3CB0CFD264D862CE7436 (void);
// 0x000003E2 System.Void FirebaseManager/<>c__DisplayClass50_0::.ctor()
extern void U3CU3Ec__DisplayClass50_0__ctor_m4B9795463D1DE5D979F62A55395EA7ED749AA3CB (void);
// 0x000003E3 System.Boolean FirebaseManager/<>c__DisplayClass50_0::<CheckAlreadyName>b__0()
extern void U3CU3Ec__DisplayClass50_0_U3CCheckAlreadyNameU3Eb__0_mE0C230F89F6F98C4E6E1994A6336A7B407431BFC (void);
// 0x000003E4 System.Void FirebaseManager/<CheckAlreadyName>d__50::.ctor(System.Int32)
extern void U3CCheckAlreadyNameU3Ed__50__ctor_m84595AAC374EC74BF9C5457425E54D84BEAB35AD (void);
// 0x000003E5 System.Void FirebaseManager/<CheckAlreadyName>d__50::System.IDisposable.Dispose()
extern void U3CCheckAlreadyNameU3Ed__50_System_IDisposable_Dispose_m35313E7C7FD768948368F6DA71DB5F5E08EE80AF (void);
// 0x000003E6 System.Boolean FirebaseManager/<CheckAlreadyName>d__50::MoveNext()
extern void U3CCheckAlreadyNameU3Ed__50_MoveNext_m2EAFDAECCDD584F2F31AB80092EFD67C60CE922B (void);
// 0x000003E7 System.Object FirebaseManager/<CheckAlreadyName>d__50::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCheckAlreadyNameU3Ed__50_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCE98E48779C30473658D59AC7A4E3708DB45F871 (void);
// 0x000003E8 System.Void FirebaseManager/<CheckAlreadyName>d__50::System.Collections.IEnumerator.Reset()
extern void U3CCheckAlreadyNameU3Ed__50_System_Collections_IEnumerator_Reset_mCD04C1434BC6EB6D7F385D7255F507715CE0413E (void);
// 0x000003E9 System.Object FirebaseManager/<CheckAlreadyName>d__50::System.Collections.IEnumerator.get_Current()
extern void U3CCheckAlreadyNameU3Ed__50_System_Collections_IEnumerator_get_Current_mB63916D8978C57C9AE01329190AFA4B9DB1C4251 (void);
// 0x000003EA System.Void FirebaseManager/<>c__DisplayClass52_0::.ctor()
extern void U3CU3Ec__DisplayClass52_0__ctor_m5990FC199363FCF73B7A8C182C5C1FAC8CE992AC (void);
// 0x000003EB System.Void FirebaseManager/<>c__DisplayClass52_0::<CreateUser>b__0(System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser>)
extern void U3CU3Ec__DisplayClass52_0_U3CCreateUserU3Eb__0_mB2C40C89DFDAA78C736F30AFF58BE28C2F9B64AB (void);
// 0x000003EC System.Void FirebaseManager/<>c__DisplayClass53_0::.ctor()
extern void U3CU3Ec__DisplayClass53_0__ctor_m2A5A68D914CF2AA0B26FE1FF8AADB2EFC0934702 (void);
// 0x000003ED System.Void FirebaseManager/<>c__DisplayClass53_0::<UpdateUserProfile>b__0(System.Threading.Tasks.Task)
extern void U3CU3Ec__DisplayClass53_0_U3CUpdateUserProfileU3Eb__0_m918B0A532747ACAA5363AFCFEA009FA10EC36B6C (void);
// 0x000003EE System.Void FirebaseManager/<CloseNotification>d__62::.ctor(System.Int32)
extern void U3CCloseNotificationU3Ed__62__ctor_m43778604560229E1A3384C14D2CB78F31BD3EC62 (void);
// 0x000003EF System.Void FirebaseManager/<CloseNotification>d__62::System.IDisposable.Dispose()
extern void U3CCloseNotificationU3Ed__62_System_IDisposable_Dispose_m8707E38A3EE8232B8D3B016AB054931AE47A8550 (void);
// 0x000003F0 System.Boolean FirebaseManager/<CloseNotification>d__62::MoveNext()
extern void U3CCloseNotificationU3Ed__62_MoveNext_m640115CD25650ED6F8F063F1DEFB72C15F8217D4 (void);
// 0x000003F1 System.Object FirebaseManager/<CloseNotification>d__62::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCloseNotificationU3Ed__62_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8EC69539785A0FFFA97B6B1E2BECD465CAA38899 (void);
// 0x000003F2 System.Void FirebaseManager/<CloseNotification>d__62::System.Collections.IEnumerator.Reset()
extern void U3CCloseNotificationU3Ed__62_System_Collections_IEnumerator_Reset_mDF06AABB22CECEE0C65B06A046FDC6A0B0B07F2E (void);
// 0x000003F3 System.Object FirebaseManager/<CloseNotification>d__62::System.Collections.IEnumerator.get_Current()
extern void U3CCloseNotificationU3Ed__62_System_Collections_IEnumerator_get_Current_m9ECE534C9D9315FD7B66D40424D4390992F60552 (void);
// 0x000003F4 System.Void FirebaseManager/<>c__DisplayClass73_0::.ctor()
extern void U3CU3Ec__DisplayClass73_0__ctor_mB26E4710C5E7502BB4462B6EF4E175A63BD5CC2F (void);
// 0x000003F5 System.Void FirebaseManager/<>c__DisplayClass73_0::<TransferData>b__0(System.Threading.Tasks.Task`1<Firebase.Auth.FirebaseUser>)
extern void U3CU3Ec__DisplayClass73_0_U3CTransferDataU3Eb__0_m5560C85FFFE2A541AD241196F19AFB90B0CC5C06 (void);
// 0x000003F6 System.Void FirebaseManager/<>c__DisplayClass74_0::.ctor()
extern void U3CU3Ec__DisplayClass74_0__ctor_mA6AA8A80AAC8605B455A7F9CDAB52F86A6CF5255 (void);
// 0x000003F7 System.Boolean FirebaseManager/<>c__DisplayClass74_0::<CheckAlreadyNameIdTransfer>b__0()
extern void U3CU3Ec__DisplayClass74_0_U3CCheckAlreadyNameIdTransferU3Eb__0_m6636A41227CD460A537009FAC82F75BE3B15D729 (void);
// 0x000003F8 System.Void FirebaseManager/<CheckAlreadyNameIdTransfer>d__74::.ctor(System.Int32)
extern void U3CCheckAlreadyNameIdTransferU3Ed__74__ctor_mF54BC3DA1886144F3C8CBFD0F06CBFB613D1250E (void);
// 0x000003F9 System.Void FirebaseManager/<CheckAlreadyNameIdTransfer>d__74::System.IDisposable.Dispose()
extern void U3CCheckAlreadyNameIdTransferU3Ed__74_System_IDisposable_Dispose_m6840128964E0260E22537A16FBC442377B5CB78B (void);
// 0x000003FA System.Boolean FirebaseManager/<CheckAlreadyNameIdTransfer>d__74::MoveNext()
extern void U3CCheckAlreadyNameIdTransferU3Ed__74_MoveNext_m3969539A58C080E36B3DB89816E37357673C9F1F (void);
// 0x000003FB System.Object FirebaseManager/<CheckAlreadyNameIdTransfer>d__74::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCheckAlreadyNameIdTransferU3Ed__74_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAC8343C114D017BB1AEE74AB38D9A4AC6D46E47D (void);
// 0x000003FC System.Void FirebaseManager/<CheckAlreadyNameIdTransfer>d__74::System.Collections.IEnumerator.Reset()
extern void U3CCheckAlreadyNameIdTransferU3Ed__74_System_Collections_IEnumerator_Reset_m58D65B1A3ABB708128F90AC7B13AC4C48862B114 (void);
// 0x000003FD System.Object FirebaseManager/<CheckAlreadyNameIdTransfer>d__74::System.Collections.IEnumerator.get_Current()
extern void U3CCheckAlreadyNameIdTransferU3Ed__74_System_Collections_IEnumerator_get_Current_mAECF9042F87AB9BBDA616C14ADAF015C8CABDC4A (void);
// 0x000003FE System.Void FirebaseManager/<>c__DisplayClass75_0::.ctor()
extern void U3CU3Ec__DisplayClass75_0__ctor_mA6CB96E2C3366F8AB03CBC71BE0415047B6C9CB7 (void);
// 0x000003FF System.Boolean FirebaseManager/<>c__DisplayClass75_0::<LoadUserScoreFromDB>b__0()
extern void U3CU3Ec__DisplayClass75_0_U3CLoadUserScoreFromDBU3Eb__0_m8C45A10644C451BD7ECDDE4FB621041427AFC392 (void);
// 0x00000400 System.Void FirebaseManager/<LoadUserScoreFromDB>d__75::.ctor(System.Int32)
extern void U3CLoadUserScoreFromDBU3Ed__75__ctor_m704CC19001BACD6E7621DBB5487CF8F23B19EB79 (void);
// 0x00000401 System.Void FirebaseManager/<LoadUserScoreFromDB>d__75::System.IDisposable.Dispose()
extern void U3CLoadUserScoreFromDBU3Ed__75_System_IDisposable_Dispose_mB57F1F147F4E74A61906CF48147B58D654B61655 (void);
// 0x00000402 System.Boolean FirebaseManager/<LoadUserScoreFromDB>d__75::MoveNext()
extern void U3CLoadUserScoreFromDBU3Ed__75_MoveNext_m1E98BB066DE2F7D4BF9F182D25FECF5EA2F2C226 (void);
// 0x00000403 System.Object FirebaseManager/<LoadUserScoreFromDB>d__75::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadUserScoreFromDBU3Ed__75_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7EC769D8AC3147416F9224A21B0C4DB92846B4F7 (void);
// 0x00000404 System.Void FirebaseManager/<LoadUserScoreFromDB>d__75::System.Collections.IEnumerator.Reset()
extern void U3CLoadUserScoreFromDBU3Ed__75_System_Collections_IEnumerator_Reset_m5DDB2E8C37B38A0BAEEB860654298AC7E64DD21A (void);
// 0x00000405 System.Object FirebaseManager/<LoadUserScoreFromDB>d__75::System.Collections.IEnumerator.get_Current()
extern void U3CLoadUserScoreFromDBU3Ed__75_System_Collections_IEnumerator_get_Current_m07AF984DCC7A697BD9F97B5D7FE103676B722BFB (void);
// 0x00000406 System.Void FirebaseManager/<DownloadImage>d__76::.ctor(System.Int32)
extern void U3CDownloadImageU3Ed__76__ctor_m5CB98433BF9CD23864038893E2E3B3A6A6DE27EF (void);
// 0x00000407 System.Void FirebaseManager/<DownloadImage>d__76::System.IDisposable.Dispose()
extern void U3CDownloadImageU3Ed__76_System_IDisposable_Dispose_mD04A6581A340602E2B175B8886A700423A8F9509 (void);
// 0x00000408 System.Boolean FirebaseManager/<DownloadImage>d__76::MoveNext()
extern void U3CDownloadImageU3Ed__76_MoveNext_m1D88425A1DCE51C0E629EA7D7D106032C75C4374 (void);
// 0x00000409 System.Object FirebaseManager/<DownloadImage>d__76::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDownloadImageU3Ed__76_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFBDB22DD4A78362581BFEC7DCBAC6DC48D18F23A (void);
// 0x0000040A System.Void FirebaseManager/<DownloadImage>d__76::System.Collections.IEnumerator.Reset()
extern void U3CDownloadImageU3Ed__76_System_Collections_IEnumerator_Reset_m88F7C5F894E29D5CB0C542490E7A8EDF17C01245 (void);
// 0x0000040B System.Object FirebaseManager/<DownloadImage>d__76::System.Collections.IEnumerator.get_Current()
extern void U3CDownloadImageU3Ed__76_System_Collections_IEnumerator_get_Current_m46B996CB3BECE15A4B97173AF9BB4449777E971C (void);
// 0x0000040C System.Void FirebaseManager/<>c__DisplayClass80_0::.ctor()
extern void U3CU3Ec__DisplayClass80_0__ctor_mFC8D9A1D51F266F9D284B66FB431CC8C102AA33E (void);
// 0x0000040D System.Boolean FirebaseManager/<>c__DisplayClass80_0::<LoadLeaderBoardData>b__0()
extern void U3CU3Ec__DisplayClass80_0_U3CLoadLeaderBoardDataU3Eb__0_mDF8AC42FF873A2AD9C51A2EAEFCCE864AFCD49F0 (void);
// 0x0000040E System.Void FirebaseManager/<LoadLeaderBoardData>d__80::.ctor(System.Int32)
extern void U3CLoadLeaderBoardDataU3Ed__80__ctor_mC00F58C654FE55829D55D858BBEAB5EBA0E804C1 (void);
// 0x0000040F System.Void FirebaseManager/<LoadLeaderBoardData>d__80::System.IDisposable.Dispose()
extern void U3CLoadLeaderBoardDataU3Ed__80_System_IDisposable_Dispose_m0C8C77ADD2592E7F7409CEE5C220CDADDA47B9BB (void);
// 0x00000410 System.Boolean FirebaseManager/<LoadLeaderBoardData>d__80::MoveNext()
extern void U3CLoadLeaderBoardDataU3Ed__80_MoveNext_mF9252997DBAF12FB8B5A0CF48B0E25EBC1DC7F90 (void);
// 0x00000411 System.Object FirebaseManager/<LoadLeaderBoardData>d__80::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadLeaderBoardDataU3Ed__80_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8F6B7887F6E6B15F168E5A764040545E91375F54 (void);
// 0x00000412 System.Void FirebaseManager/<LoadLeaderBoardData>d__80::System.Collections.IEnumerator.Reset()
extern void U3CLoadLeaderBoardDataU3Ed__80_System_Collections_IEnumerator_Reset_m9CB4781BA05AC6B10FD795F9C3DB8BC690E62CB0 (void);
// 0x00000413 System.Object FirebaseManager/<LoadLeaderBoardData>d__80::System.Collections.IEnumerator.get_Current()
extern void U3CLoadLeaderBoardDataU3Ed__80_System_Collections_IEnumerator_get_Current_m0E44B192105C8A4F010F1976F1E212B3B6287568 (void);
// 0x00000414 System.Void RankData/<DownloadImage>d__5::.ctor(System.Int32)
extern void U3CDownloadImageU3Ed__5__ctor_m56A893022DA599581E1A11DCF1D1BF961B1A0316 (void);
// 0x00000415 System.Void RankData/<DownloadImage>d__5::System.IDisposable.Dispose()
extern void U3CDownloadImageU3Ed__5_System_IDisposable_Dispose_m2398236E2D1FB99231363CFF876F7EB857FA7042 (void);
// 0x00000416 System.Boolean RankData/<DownloadImage>d__5::MoveNext()
extern void U3CDownloadImageU3Ed__5_MoveNext_m594786314C7795FD05A476C72AA9E7C2B63931E7 (void);
// 0x00000417 System.Object RankData/<DownloadImage>d__5::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDownloadImageU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m26063764B51701B2831DCC38CAB2DA3C0A819609 (void);
// 0x00000418 System.Void RankData/<DownloadImage>d__5::System.Collections.IEnumerator.Reset()
extern void U3CDownloadImageU3Ed__5_System_Collections_IEnumerator_Reset_m3EFD1AFEB3C0C56382735B8E26FA7B102D127CCB (void);
// 0x00000419 System.Object RankData/<DownloadImage>d__5::System.Collections.IEnumerator.get_Current()
extern void U3CDownloadImageU3Ed__5_System_Collections_IEnumerator_get_Current_mABB560831447A0159C1C41B02A70EEA0E3292041 (void);
// 0x0000041A System.Void RankManager/<>c__DisplayClass12_0::.ctor()
extern void U3CU3Ec__DisplayClass12_0__ctor_m0E9A98FEAD760AA673AA23EAF8E8DBCDB9BE9EED (void);
// 0x0000041B System.Boolean RankManager/<>c__DisplayClass12_0::<CheckDamageOverCurrentScore>b__0()
extern void U3CU3Ec__DisplayClass12_0_U3CCheckDamageOverCurrentScoreU3Eb__0_m14DB9E5AB7091133E8B3AE0DBC7D628DFDCD54F5 (void);
// 0x0000041C System.Void RankManager/<CheckDamageOverCurrentScore>d__12::.ctor(System.Int32)
extern void U3CCheckDamageOverCurrentScoreU3Ed__12__ctor_m9FBF500C44EB1E27C562C6EB65C62D9ABF486BB5 (void);
// 0x0000041D System.Void RankManager/<CheckDamageOverCurrentScore>d__12::System.IDisposable.Dispose()
extern void U3CCheckDamageOverCurrentScoreU3Ed__12_System_IDisposable_Dispose_m7D36BC8498D8CBDFF8D4E22E822E2573EE8A31A0 (void);
// 0x0000041E System.Boolean RankManager/<CheckDamageOverCurrentScore>d__12::MoveNext()
extern void U3CCheckDamageOverCurrentScoreU3Ed__12_MoveNext_m3F1F15548BE4B7A6EE72C4C66F41F956FFAD382A (void);
// 0x0000041F System.Object RankManager/<CheckDamageOverCurrentScore>d__12::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCheckDamageOverCurrentScoreU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m64A78464A601CF744FD9495D04B11CB0AB3B3332 (void);
// 0x00000420 System.Void RankManager/<CheckDamageOverCurrentScore>d__12::System.Collections.IEnumerator.Reset()
extern void U3CCheckDamageOverCurrentScoreU3Ed__12_System_Collections_IEnumerator_Reset_m0ED25AEE06E5E9ACAE43E03BE0DD6424A38EB4B1 (void);
// 0x00000421 System.Object RankManager/<CheckDamageOverCurrentScore>d__12::System.Collections.IEnumerator.get_Current()
extern void U3CCheckDamageOverCurrentScoreU3Ed__12_System_Collections_IEnumerator_get_Current_m5D792B1CE7190B73BC86974F72B0B35ACC2DCCC7 (void);
// 0x00000422 System.Void RankManager/<>c__DisplayClass13_0::.ctor()
extern void U3CU3Ec__DisplayClass13_0__ctor_mED4FFF10797AFE19F6B942DCE18B2AC86C34E20E (void);
// 0x00000423 System.Boolean RankManager/<>c__DisplayClass13_0::<CheckTimeSurvivedOverCurrentScore>b__0()
extern void U3CU3Ec__DisplayClass13_0_U3CCheckTimeSurvivedOverCurrentScoreU3Eb__0_m8DA272998DDA0EFC0A0FA233DE3E14D690CCABD9 (void);
// 0x00000424 System.Void RankManager/<CheckTimeSurvivedOverCurrentScore>d__13::.ctor(System.Int32)
extern void U3CCheckTimeSurvivedOverCurrentScoreU3Ed__13__ctor_mC125234B920EDD35FA13597070656207C0559A01 (void);
// 0x00000425 System.Void RankManager/<CheckTimeSurvivedOverCurrentScore>d__13::System.IDisposable.Dispose()
extern void U3CCheckTimeSurvivedOverCurrentScoreU3Ed__13_System_IDisposable_Dispose_m5C4AD400A1C79CDE7A58680CDD80589512DF527A (void);
// 0x00000426 System.Boolean RankManager/<CheckTimeSurvivedOverCurrentScore>d__13::MoveNext()
extern void U3CCheckTimeSurvivedOverCurrentScoreU3Ed__13_MoveNext_m8D8A608ADE7672C83E94BD5C88E1D3E83AF3BAF6 (void);
// 0x00000427 System.Object RankManager/<CheckTimeSurvivedOverCurrentScore>d__13::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCheckTimeSurvivedOverCurrentScoreU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m79CDF24809F8E9C369BC5D7C6C5D6E52F49464A0 (void);
// 0x00000428 System.Void RankManager/<CheckTimeSurvivedOverCurrentScore>d__13::System.Collections.IEnumerator.Reset()
extern void U3CCheckTimeSurvivedOverCurrentScoreU3Ed__13_System_Collections_IEnumerator_Reset_mA71171569E9A5B046E37F9679E3A6514B4AAFEAB (void);
// 0x00000429 System.Object RankManager/<CheckTimeSurvivedOverCurrentScore>d__13::System.Collections.IEnumerator.get_Current()
extern void U3CCheckTimeSurvivedOverCurrentScoreU3Ed__13_System_Collections_IEnumerator_get_Current_mED6D775E0EAD9B2B09E6410C85BFC9E7852E6D10 (void);
// 0x0000042A System.Void RankManager/<>c__DisplayClass14_0::.ctor()
extern void U3CU3Ec__DisplayClass14_0__ctor_m990D1CE5144DB1D372A8CF0EFB65541C44A677A5 (void);
// 0x0000042B System.Boolean RankManager/<>c__DisplayClass14_0::<UpdateDamageDone>b__0()
extern void U3CU3Ec__DisplayClass14_0_U3CUpdateDamageDoneU3Eb__0_m15ECF55814497519998C606A19B0F9B13E511292 (void);
// 0x0000042C System.Void RankManager/<UpdateDamageDone>d__14::.ctor(System.Int32)
extern void U3CUpdateDamageDoneU3Ed__14__ctor_mAB8EF0D4F0930E31C58ED12AFE14E283DDC7EE50 (void);
// 0x0000042D System.Void RankManager/<UpdateDamageDone>d__14::System.IDisposable.Dispose()
extern void U3CUpdateDamageDoneU3Ed__14_System_IDisposable_Dispose_mF8D5C1001B4D15B1CE1F7795A5753AC7EF9911EF (void);
// 0x0000042E System.Boolean RankManager/<UpdateDamageDone>d__14::MoveNext()
extern void U3CUpdateDamageDoneU3Ed__14_MoveNext_mAB7DE3DDFB1AAFBE4C7CD07B386DE3FDA46F5810 (void);
// 0x0000042F System.Object RankManager/<UpdateDamageDone>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CUpdateDamageDoneU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF5C127E539E04AB52223DBC2368D5924E4E03973 (void);
// 0x00000430 System.Void RankManager/<UpdateDamageDone>d__14::System.Collections.IEnumerator.Reset()
extern void U3CUpdateDamageDoneU3Ed__14_System_Collections_IEnumerator_Reset_m0D826E9EC709E9DA07E20F68E4D1B7D201161304 (void);
// 0x00000431 System.Object RankManager/<UpdateDamageDone>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CUpdateDamageDoneU3Ed__14_System_Collections_IEnumerator_get_Current_m28E02CEDC813B4CDF8898076064DC9FDA23E56E6 (void);
// 0x00000432 System.Void RankManager/<>c__DisplayClass15_0::.ctor()
extern void U3CU3Ec__DisplayClass15_0__ctor_m42332C025BABA3F1486E25869316ED2D83C6D9AF (void);
// 0x00000433 System.Boolean RankManager/<>c__DisplayClass15_0::<UpdateWaveSurvived>b__0()
extern void U3CU3Ec__DisplayClass15_0_U3CUpdateWaveSurvivedU3Eb__0_m5DCC9423DB65E6B5DE7D177D6361988F5C5792BF (void);
// 0x00000434 System.Void RankManager/<UpdateWaveSurvived>d__15::.ctor(System.Int32)
extern void U3CUpdateWaveSurvivedU3Ed__15__ctor_m32610F33973E8345603BD6EA5FA6009B3F3FBF79 (void);
// 0x00000435 System.Void RankManager/<UpdateWaveSurvived>d__15::System.IDisposable.Dispose()
extern void U3CUpdateWaveSurvivedU3Ed__15_System_IDisposable_Dispose_mD827AC0451DBDBDC047E281486D890B27354253D (void);
// 0x00000436 System.Boolean RankManager/<UpdateWaveSurvived>d__15::MoveNext()
extern void U3CUpdateWaveSurvivedU3Ed__15_MoveNext_mA53A84301D5743E88EB52DAE5B14A628778A03D1 (void);
// 0x00000437 System.Object RankManager/<UpdateWaveSurvived>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CUpdateWaveSurvivedU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m54EA5333230F4F5CE9F3E1A7E97BFCA71F16B538 (void);
// 0x00000438 System.Void RankManager/<UpdateWaveSurvived>d__15::System.Collections.IEnumerator.Reset()
extern void U3CUpdateWaveSurvivedU3Ed__15_System_Collections_IEnumerator_Reset_mAEF947D6C4ED3C809ECBEDFE1836E725B5D05A45 (void);
// 0x00000439 System.Object RankManager/<UpdateWaveSurvived>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CUpdateWaveSurvivedU3Ed__15_System_Collections_IEnumerator_get_Current_mDC7061BB2AF7F2A042B2C0E6D4F137EC8F5F1AD3 (void);
// 0x0000043A System.Void RankManager/<>c__DisplayClass16_0::.ctor()
extern void U3CU3Ec__DisplayClass16_0__ctor_m16D38AD81F83DE7807FB730AF9F40135877739B3 (void);
// 0x0000043B System.Boolean RankManager/<>c__DisplayClass16_0::<UpdateTimeSurvived>b__0()
extern void U3CU3Ec__DisplayClass16_0_U3CUpdateTimeSurvivedU3Eb__0_m2A19E6E6EFB3E011DC505633755842FAFF693599 (void);
// 0x0000043C System.Void RankManager/<UpdateTimeSurvived>d__16::.ctor(System.Int32)
extern void U3CUpdateTimeSurvivedU3Ed__16__ctor_mE7CA8949AA742F0CD2614EC3BF5905CEA3373323 (void);
// 0x0000043D System.Void RankManager/<UpdateTimeSurvived>d__16::System.IDisposable.Dispose()
extern void U3CUpdateTimeSurvivedU3Ed__16_System_IDisposable_Dispose_mB43ADA3CF19E76A5808A135D3E7E3E0D03793A8B (void);
// 0x0000043E System.Boolean RankManager/<UpdateTimeSurvived>d__16::MoveNext()
extern void U3CUpdateTimeSurvivedU3Ed__16_MoveNext_mADC6F67FD4D181C61C312E39A7A54BB6DC35D83A (void);
// 0x0000043F System.Object RankManager/<UpdateTimeSurvived>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CUpdateTimeSurvivedU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE11BE326E56200BFE14E5915BFEAB2B74C051FF2 (void);
// 0x00000440 System.Void RankManager/<UpdateTimeSurvived>d__16::System.Collections.IEnumerator.Reset()
extern void U3CUpdateTimeSurvivedU3Ed__16_System_Collections_IEnumerator_Reset_m9C672B2F72A298CB037B3B5753468644270E4BEF (void);
// 0x00000441 System.Object RankManager/<UpdateTimeSurvived>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CUpdateTimeSurvivedU3Ed__16_System_Collections_IEnumerator_get_Current_m3E8EBFA8DC883686D75A92E8E7E3EDF97EDD4DD7 (void);
// 0x00000442 System.Void RankManager/<>c__DisplayClass17_0::.ctor()
extern void U3CU3Ec__DisplayClass17_0__ctor_m9A8346676BB7E1F7668C1BFC2220B3990B6C9123 (void);
// 0x00000443 System.Boolean RankManager/<>c__DisplayClass17_0::<LoadUserScoreFromDB>b__0()
extern void U3CU3Ec__DisplayClass17_0_U3CLoadUserScoreFromDBU3Eb__0_mAF3BFC5497997AD1F5B95317443B86CF9537D016 (void);
// 0x00000444 System.Void RankManager/<LoadUserScoreFromDB>d__17::.ctor(System.Int32)
extern void U3CLoadUserScoreFromDBU3Ed__17__ctor_m8D1FA3BBE852BAE443A006877DE97924D717A842 (void);
// 0x00000445 System.Void RankManager/<LoadUserScoreFromDB>d__17::System.IDisposable.Dispose()
extern void U3CLoadUserScoreFromDBU3Ed__17_System_IDisposable_Dispose_m2D7F9A2A3A5BF15A09C5E667906D55A8F3A7243D (void);
// 0x00000446 System.Boolean RankManager/<LoadUserScoreFromDB>d__17::MoveNext()
extern void U3CLoadUserScoreFromDBU3Ed__17_MoveNext_m8634E0CB7D8E63023E80F2467790F4E7DFB11C80 (void);
// 0x00000447 System.Object RankManager/<LoadUserScoreFromDB>d__17::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadUserScoreFromDBU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m930F67A39ADE639B8D30C6DB54037E755C365308 (void);
// 0x00000448 System.Void RankManager/<LoadUserScoreFromDB>d__17::System.Collections.IEnumerator.Reset()
extern void U3CLoadUserScoreFromDBU3Ed__17_System_Collections_IEnumerator_Reset_m19BD1A5B2D3986337D43651A6346B78AFA0EAB93 (void);
// 0x00000449 System.Object RankManager/<LoadUserScoreFromDB>d__17::System.Collections.IEnumerator.get_Current()
extern void U3CLoadUserScoreFromDBU3Ed__17_System_Collections_IEnumerator_get_Current_m16A3FB960C8208C64ABA393EEA909C9C289F0481 (void);
// 0x0000044A System.Void RankManager/<>c__DisplayClass18_0::.ctor()
extern void U3CU3Ec__DisplayClass18_0__ctor_m66E344A88629D506AF23620ADF275531C3ED4683 (void);
// 0x0000044B System.Boolean RankManager/<>c__DisplayClass18_0::<LoadLeaderBoardData>b__0()
extern void U3CU3Ec__DisplayClass18_0_U3CLoadLeaderBoardDataU3Eb__0_mD5E2704551C12D12E5693B6415FF59CE623971D6 (void);
// 0x0000044C System.Void RankManager/<LoadLeaderBoardData>d__18::.ctor(System.Int32)
extern void U3CLoadLeaderBoardDataU3Ed__18__ctor_mC41A8A5403C2606BB13F4B461653F8E5D36C1F2F (void);
// 0x0000044D System.Void RankManager/<LoadLeaderBoardData>d__18::System.IDisposable.Dispose()
extern void U3CLoadLeaderBoardDataU3Ed__18_System_IDisposable_Dispose_m1618C92B5A0462FA30052FFC0F1FCBF0F97A6875 (void);
// 0x0000044E System.Boolean RankManager/<LoadLeaderBoardData>d__18::MoveNext()
extern void U3CLoadLeaderBoardDataU3Ed__18_MoveNext_m791539A2DB2C21BC5C2D55FD5A0C51BAE36B6596 (void);
// 0x0000044F System.Object RankManager/<LoadLeaderBoardData>d__18::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLoadLeaderBoardDataU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBFA6427E3891D37646831F9AFA8290094EFA9D71 (void);
// 0x00000450 System.Void RankManager/<LoadLeaderBoardData>d__18::System.Collections.IEnumerator.Reset()
extern void U3CLoadLeaderBoardDataU3Ed__18_System_Collections_IEnumerator_Reset_mEF596DF92F906E40A50D828F7AEA4E7897DE215C (void);
// 0x00000451 System.Object RankManager/<LoadLeaderBoardData>d__18::System.Collections.IEnumerator.get_Current()
extern void U3CLoadLeaderBoardDataU3Ed__18_System_Collections_IEnumerator_get_Current_m8CDA43B0A3DCCC9938B4E6E503DE3A784B6FB011 (void);
// 0x00000452 System.Void RankManager/<DownloadImage>d__19::.ctor(System.Int32)
extern void U3CDownloadImageU3Ed__19__ctor_mA337C682C0E9601A354AF20655032AEA526D026C (void);
// 0x00000453 System.Void RankManager/<DownloadImage>d__19::System.IDisposable.Dispose()
extern void U3CDownloadImageU3Ed__19_System_IDisposable_Dispose_mE416C30E9AE2D2DEE3C4D14332D65CF86208C5B5 (void);
// 0x00000454 System.Boolean RankManager/<DownloadImage>d__19::MoveNext()
extern void U3CDownloadImageU3Ed__19_MoveNext_m6554A92C83B0A5952DBC30A26581E82038DF0380 (void);
// 0x00000455 System.Object RankManager/<DownloadImage>d__19::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDownloadImageU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDB1F79F0F8367FFE5586098C13EB91E7547542EA (void);
// 0x00000456 System.Void RankManager/<DownloadImage>d__19::System.Collections.IEnumerator.Reset()
extern void U3CDownloadImageU3Ed__19_System_Collections_IEnumerator_Reset_mA266BA2E8E8DCC326775AC8272102BB1B324ED9A (void);
// 0x00000457 System.Object RankManager/<DownloadImage>d__19::System.Collections.IEnumerator.get_Current()
extern void U3CDownloadImageU3Ed__19_System_Collections_IEnumerator_get_Current_mFD392F88C8F541BEC86F9DC21B847FACC19DB873 (void);
// 0x00000458 System.Void HeroUI/<ChangeButtonColor>d__21::.ctor(System.Int32)
extern void U3CChangeButtonColorU3Ed__21__ctor_m022B1F3DDDC026D92DE234A1607B268DF00F2C97 (void);
// 0x00000459 System.Void HeroUI/<ChangeButtonColor>d__21::System.IDisposable.Dispose()
extern void U3CChangeButtonColorU3Ed__21_System_IDisposable_Dispose_m3517C42530E1127D15045D7C88535291F122D6B0 (void);
// 0x0000045A System.Boolean HeroUI/<ChangeButtonColor>d__21::MoveNext()
extern void U3CChangeButtonColorU3Ed__21_MoveNext_m7C06508FEE819A1040C3CDC0E2F39875A950467E (void);
// 0x0000045B System.Object HeroUI/<ChangeButtonColor>d__21::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CChangeButtonColorU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8821EF38042F4839506600E9F1669248B0928E96 (void);
// 0x0000045C System.Void HeroUI/<ChangeButtonColor>d__21::System.Collections.IEnumerator.Reset()
extern void U3CChangeButtonColorU3Ed__21_System_Collections_IEnumerator_Reset_m3A87C825847F51A6BB2A1DB026FF22945CA831D0 (void);
// 0x0000045D System.Object HeroUI/<ChangeButtonColor>d__21::System.Collections.IEnumerator.get_Current()
extern void U3CChangeButtonColorU3Ed__21_System_Collections_IEnumerator_get_Current_mE05A61B3BF11D846169720E6B7E6DAB5F7E79DB4 (void);
// 0x0000045E System.Void HeroUI/<WaitForUpdateUi>d__27::.ctor(System.Int32)
extern void U3CWaitForUpdateUiU3Ed__27__ctor_mEFF8FCAE0E22B88642092D496E94249D3B9F9FB5 (void);
// 0x0000045F System.Void HeroUI/<WaitForUpdateUi>d__27::System.IDisposable.Dispose()
extern void U3CWaitForUpdateUiU3Ed__27_System_IDisposable_Dispose_m19B749EC2C6F276D35A0BE03B51D69A659170E46 (void);
// 0x00000460 System.Boolean HeroUI/<WaitForUpdateUi>d__27::MoveNext()
extern void U3CWaitForUpdateUiU3Ed__27_MoveNext_m13DF403095CC52D7A94317E1A0D901A13CE89C8F (void);
// 0x00000461 System.Object HeroUI/<WaitForUpdateUi>d__27::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForUpdateUiU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m99553AC5B564ED4462C3587A1AE492105F8773D4 (void);
// 0x00000462 System.Void HeroUI/<WaitForUpdateUi>d__27::System.Collections.IEnumerator.Reset()
extern void U3CWaitForUpdateUiU3Ed__27_System_Collections_IEnumerator_Reset_m269F0236C1C5426233ECF692D38DB744748508EB (void);
// 0x00000463 System.Object HeroUI/<WaitForUpdateUi>d__27::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForUpdateUiU3Ed__27_System_Collections_IEnumerator_get_Current_mB1C2059DD75E7B11DFA1E993B2BBA333AF79F63D (void);
// 0x00000464 System.Void ItemManager/<ActiveCookie30s>d__19::.ctor(System.Int32)
extern void U3CActiveCookie30sU3Ed__19__ctor_m8FCF61703691001EFA7961AE2E1D787D16F7A5B5 (void);
// 0x00000465 System.Void ItemManager/<ActiveCookie30s>d__19::System.IDisposable.Dispose()
extern void U3CActiveCookie30sU3Ed__19_System_IDisposable_Dispose_m045AB7688AF359AF2B0E85847CB3ED26389AE618 (void);
// 0x00000466 System.Boolean ItemManager/<ActiveCookie30s>d__19::MoveNext()
extern void U3CActiveCookie30sU3Ed__19_MoveNext_m2AC7A061189E8FCCDFA69816B62AF60328971AE6 (void);
// 0x00000467 System.Object ItemManager/<ActiveCookie30s>d__19::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CActiveCookie30sU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEF460E178959DB2B321C7DB1AABCB6B86E2D6829 (void);
// 0x00000468 System.Void ItemManager/<ActiveCookie30s>d__19::System.Collections.IEnumerator.Reset()
extern void U3CActiveCookie30sU3Ed__19_System_Collections_IEnumerator_Reset_m8854B0851AC842D51089315669AFF02D7908C1EC (void);
// 0x00000469 System.Object ItemManager/<ActiveCookie30s>d__19::System.Collections.IEnumerator.get_Current()
extern void U3CActiveCookie30sU3Ed__19_System_Collections_IEnumerator_get_Current_m440F33539D6609523F55870DB875411892221539 (void);
// 0x0000046A System.Void ItemManager/<ActiveCandy30s>d__21::.ctor(System.Int32)
extern void U3CActiveCandy30sU3Ed__21__ctor_m6125A56FB311570E4C1DA52533D5BC1E8D963429 (void);
// 0x0000046B System.Void ItemManager/<ActiveCandy30s>d__21::System.IDisposable.Dispose()
extern void U3CActiveCandy30sU3Ed__21_System_IDisposable_Dispose_m398275D5D386B790D02DCE00C807A13C41F141D4 (void);
// 0x0000046C System.Boolean ItemManager/<ActiveCandy30s>d__21::MoveNext()
extern void U3CActiveCandy30sU3Ed__21_MoveNext_m72D01F843FD11DD442803FBE1F6D3C9C4545C667 (void);
// 0x0000046D System.Object ItemManager/<ActiveCandy30s>d__21::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CActiveCandy30sU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE97A411E62DD8FD2994CDA0381430C7B376D09FE (void);
// 0x0000046E System.Void ItemManager/<ActiveCandy30s>d__21::System.Collections.IEnumerator.Reset()
extern void U3CActiveCandy30sU3Ed__21_System_Collections_IEnumerator_Reset_m0D06ED95FB91F1B91B0A9BC2A0ACB2557BCD6729 (void);
// 0x0000046F System.Object ItemManager/<ActiveCandy30s>d__21::System.Collections.IEnumerator.get_Current()
extern void U3CActiveCandy30sU3Ed__21_System_Collections_IEnumerator_get_Current_mF71FC42683C0F4C24A8DD650B36D6B141CF43735 (void);
// 0x00000470 System.Void TimeManager/<WaitForPanel>d__14::.ctor(System.Int32)
extern void U3CWaitForPanelU3Ed__14__ctor_m59D042B5E4A18907C5B493929DE55BB2793A8A20 (void);
// 0x00000471 System.Void TimeManager/<WaitForPanel>d__14::System.IDisposable.Dispose()
extern void U3CWaitForPanelU3Ed__14_System_IDisposable_Dispose_m629FF274124309F78AEE0A0EE1E78820401E3659 (void);
// 0x00000472 System.Boolean TimeManager/<WaitForPanel>d__14::MoveNext()
extern void U3CWaitForPanelU3Ed__14_MoveNext_m3C201F956F57B5772EBD5AEB3BB0107EFEE4A65C (void);
// 0x00000473 System.Object TimeManager/<WaitForPanel>d__14::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForPanelU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1BA1E11BDC976FFDA71876728621CC3580B339C9 (void);
// 0x00000474 System.Void TimeManager/<WaitForPanel>d__14::System.Collections.IEnumerator.Reset()
extern void U3CWaitForPanelU3Ed__14_System_Collections_IEnumerator_Reset_mFDFA0E2253D02365F762B257186B1B188BA591BC (void);
// 0x00000475 System.Object TimeManager/<WaitForPanel>d__14::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForPanelU3Ed__14_System_Collections_IEnumerator_get_Current_m7FF353409E4AB340F005DEE86648F7D8B53A47D0 (void);
// 0x00000476 System.Void TimeManager/<WaitForUnPause>d__15::.ctor(System.Int32)
extern void U3CWaitForUnPauseU3Ed__15__ctor_m0FD75504981C15A71618791163C95DA6832B1558 (void);
// 0x00000477 System.Void TimeManager/<WaitForUnPause>d__15::System.IDisposable.Dispose()
extern void U3CWaitForUnPauseU3Ed__15_System_IDisposable_Dispose_m1967E6755E3C99E168315655FDBC938D996A7C80 (void);
// 0x00000478 System.Boolean TimeManager/<WaitForUnPause>d__15::MoveNext()
extern void U3CWaitForUnPauseU3Ed__15_MoveNext_mC7C5AE8C9F075859493834CBAB151845C565F63D (void);
// 0x00000479 System.Object TimeManager/<WaitForUnPause>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForUnPauseU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3ABE4E9FEA6847127D2EBA79A3BDCC495BF3A886 (void);
// 0x0000047A System.Void TimeManager/<WaitForUnPause>d__15::System.Collections.IEnumerator.Reset()
extern void U3CWaitForUnPauseU3Ed__15_System_Collections_IEnumerator_Reset_mF18699718458033B6427B047F0C6892055FFD52E (void);
// 0x0000047B System.Object TimeManager/<WaitForUnPause>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForUnPauseU3Ed__15_System_Collections_IEnumerator_get_Current_mA42AEF9003416FA995E1070577B3323FCD65A232 (void);
// 0x0000047C System.Void ChainSkill/<>c::.cctor()
extern void U3CU3Ec__cctor_m513A6A65465EE7880F12B2AF44B2717264E0604B (void);
// 0x0000047D System.Void ChainSkill/<>c::.ctor()
extern void U3CU3Ec__ctor_mA948058D00B7A0FFDA0329A96D24D9F074C5242B (void);
// 0x0000047E System.Boolean ChainSkill/<>c::<GetCompGameOver>b__15_0(SkillBluePrint)
extern void U3CU3Ec_U3CGetCompGameOverU3Eb__15_0_m20F15C994B7F75226F9CC655EC52E9FACF026B30 (void);
// 0x0000047F System.Boolean ChainSkill/<>c::<FindElementBuild>b__22_0(Manager.HeroBluePrint)
extern void U3CU3Ec_U3CFindElementBuildU3Eb__22_0_m547CFF3D9472E416349F847DE2E3F335A2B5340B (void);
// 0x00000480 System.Boolean ChainSkill/<>c::<FindElementBuild>b__22_1(Manager.HeroBluePrint)
extern void U3CU3Ec_U3CFindElementBuildU3Eb__22_1_m415FA8E80E3F5752623D6E492F9E7207C38FEC8C (void);
// 0x00000481 System.Boolean ChainSkill/<>c::<FindElementBuild>b__22_2(Manager.HeroBluePrint)
extern void U3CU3Ec_U3CFindElementBuildU3Eb__22_2_mD0A3F49E0F79D431F4F522441E0B91E8AC803081 (void);
// 0x00000482 System.Void ChainSkill/<>c__DisplayClass21_0::.ctor()
extern void U3CU3Ec__DisplayClass21_0__ctor_m9AC962536759F39B88CF5CB33D14D82B1984668E (void);
// 0x00000483 System.Boolean ChainSkill/<>c__DisplayClass21_0::<FindBuild>b__0(Manager.HeroBluePrint)
extern void U3CU3Ec__DisplayClass21_0_U3CFindBuildU3Eb__0_m19E7DCACB759BF62E58D0B92AD4974EEFB37BD41 (void);
// 0x00000484 System.Boolean ChainSkill/<>c__DisplayClass21_0::<FindBuild>b__1(Manager.HeroBluePrint)
extern void U3CU3Ec__DisplayClass21_0_U3CFindBuildU3Eb__1_mF2FC8C1952F1AAD93C7EA457C367E6570A5B83DE (void);
// 0x00000485 System.Boolean ChainSkill/<>c__DisplayClass21_0::<FindBuild>b__2(Manager.HeroBluePrint)
extern void U3CU3Ec__DisplayClass21_0_U3CFindBuildU3Eb__2_mBB8561A36050368B1E0A7E9CA6E747B98C89A62F (void);
// 0x00000486 System.Boolean ChainSkill/<>c__DisplayClass21_0::<FindBuild>b__3(Manager.HeroBluePrint)
extern void U3CU3Ec__DisplayClass21_0_U3CFindBuildU3Eb__3_m1D2543C1C5A011B6484F37A3828F7FD0A90C4C28 (void);
// 0x00000487 System.Boolean ChainSkill/<>c__DisplayClass21_0::<FindBuild>b__4(Manager.HeroBluePrint)
extern void U3CU3Ec__DisplayClass21_0_U3CFindBuildU3Eb__4_mA0EFAA92951ADF1D319C7F9181625543136C3000 (void);
// 0x00000488 System.Boolean ChainSkill/<>c__DisplayClass21_0::<FindBuild>b__5(Manager.HeroBluePrint)
extern void U3CU3Ec__DisplayClass21_0_U3CFindBuildU3Eb__5_mFABD8728047CE3F5A86651EE5437AAC16CF98781 (void);
// 0x00000489 System.Boolean ChainSkill/<>c__DisplayClass21_0::<FindBuild>b__6(Manager.HeroBluePrint)
extern void U3CU3Ec__DisplayClass21_0_U3CFindBuildU3Eb__6_m18D32D35D8B4D714907057563F1BC7963F0E856F (void);
// 0x0000048A System.Boolean ChainSkill/<>c__DisplayClass21_0::<FindBuild>b__7(Manager.HeroBluePrint)
extern void U3CU3Ec__DisplayClass21_0_U3CFindBuildU3Eb__7_m468398931098D61CBAC2A54DE09B5F53D232DE61 (void);
// 0x0000048B System.Boolean ChainSkill/<>c__DisplayClass21_0::<FindBuild>b__8(Manager.HeroBluePrint)
extern void U3CU3Ec__DisplayClass21_0_U3CFindBuildU3Eb__8_mA8542566E261C3F300D9FC82A91DF6AA59CE0DCD (void);
// 0x0000048C System.Void EnvMapAnimator/<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_m4D65F4FC2207AE4B6BE963AF9B5EDC55C7E29B23 (void);
// 0x0000048D System.Void EnvMapAnimator/<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_mFEE2ACED70A3D825988E28CC61FEF8DCD7660A5B (void);
// 0x0000048E System.Boolean EnvMapAnimator/<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_m2F1A8053A32AD86DA80F86391EC32EDC1C396AEE (void);
// 0x0000048F System.Object EnvMapAnimator/<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF03E2A57659B8F598AC450183F55D43F903C0A1E (void);
// 0x00000490 System.Void EnvMapAnimator/<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m268BA538CF6812C56EB281C0CE29D5AA2E9A2CAB (void);
// 0x00000491 System.Object EnvMapAnimator/<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mFE9CA9F52F9AFA1E4C9DBADA2064F854D6931CFF (void);
// 0x00000492 System.Void TMPro.TMP_TextEventHandler/CharacterSelectionEvent::.ctor()
extern void CharacterSelectionEvent__ctor_m06BC183AF31BA4A2055A44514BC3FF0539DD04C7 (void);
// 0x00000493 System.Void TMPro.TMP_TextEventHandler/SpriteSelectionEvent::.ctor()
extern void SpriteSelectionEvent__ctor_m0F760052E9A5AF44A7AF7AC006CB4B24809590F2 (void);
// 0x00000494 System.Void TMPro.TMP_TextEventHandler/WordSelectionEvent::.ctor()
extern void WordSelectionEvent__ctor_m106CDEB17C520C9D20CE7120DE6BBBDEDB48886C (void);
// 0x00000495 System.Void TMPro.TMP_TextEventHandler/LineSelectionEvent::.ctor()
extern void LineSelectionEvent__ctor_m5D735FDA9B71B9147C6F791B331498F145D75018 (void);
// 0x00000496 System.Void TMPro.TMP_TextEventHandler/LinkSelectionEvent::.ctor()
extern void LinkSelectionEvent__ctor_m7AB7977D0D0F8883C5A98DD6BB2D390BC3CAB8E0 (void);
// 0x00000497 System.Void TMPro.Examples.Benchmark01/<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_mBE5D8B4B98C372BD6DC3936437999DF3048DE3AB (void);
// 0x00000498 System.Void TMPro.Examples.Benchmark01/<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_m1F4180A7FDAE9AC5F11F30B67F813B3D7CD56D73 (void);
// 0x00000499 System.Boolean TMPro.Examples.Benchmark01/<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_mF3B1D38CD37FD5187C3192141DB380747C66AD3C (void);
// 0x0000049A System.Object TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m22B2975D42326E3DE437174BBE4F0E8790CB6591 (void);
// 0x0000049B System.Void TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m514DA545ECA10AA62BFC239BB582FF0017B91D2B (void);
// 0x0000049C System.Object TMPro.Examples.Benchmark01/<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m6904A738F04118BA24E473F1809B3D69E711627E (void);
// 0x0000049D System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::.ctor(System.Int32)
extern void U3CStartU3Ed__10__ctor_m653B757B49A674E239C804FFF4EDEF325B5DB651 (void);
// 0x0000049E System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.IDisposable.Dispose()
extern void U3CStartU3Ed__10_System_IDisposable_Dispose_mAD0BC985E1E01C8369A7A955A60D3E545B0B3561 (void);
// 0x0000049F System.Boolean TMPro.Examples.Benchmark01_UGUI/<Start>d__10::MoveNext()
extern void U3CStartU3Ed__10_MoveNext_mF9B2C8D290035BC9A79C4347772B5B7B9D404DE1 (void);
// 0x000004A0 System.Object TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF438D42D7C5A811FF9A283F8780F015FF55826CD (void);
// 0x000004A1 System.Void TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m65545EB5B6DDDDAB5ACF92600137C1C754B322BA (void);
// 0x000004A2 System.Object TMPro.Examples.Benchmark01_UGUI/<Start>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mD4523BE1D1014AAF1C0709EB767C0B0F47D739D2 (void);
// 0x000004A3 System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::.ctor(System.Int32)
extern void U3CAnimatePropertiesU3Ed__6__ctor_mFAC0F8A7368D9D35AD2780C118E13414DA79B56A (void);
// 0x000004A4 System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.IDisposable.Dispose()
extern void U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_m628EECBFCBC49087298185F17BC2AE7A73FC8A7B (void);
// 0x000004A5 System.Boolean TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::MoveNext()
extern void U3CAnimatePropertiesU3Ed__6_MoveNext_m8A58CCFDAE59F55AB1BB2C103800886E62C807CF (void);
// 0x000004A6 System.Object TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0742A0FA62C518EBD119ED5FEBF849F918E12390 (void);
// 0x000004A7 System.Void TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.IEnumerator.Reset()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_mC076F281DD17668D6CE42EB04EA974DE1FFB3F6B (void);
// 0x000004A8 System.Object TMPro.Examples.ShaderPropAnimator/<AnimateProperties>d__6::System.Collections.IEnumerator.get_Current()
extern void U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_mA585E786DA86D2589EBCA029AD9E64E8B5135362 (void);
// 0x000004A9 System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::.ctor(System.Int32)
extern void U3CWarpTextU3Ed__7__ctor_mEAD3C39209B75514446A44B6C2FA76F8097EBD6F (void);
// 0x000004AA System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::System.IDisposable.Dispose()
extern void U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m32AA7120BE15547799BDC515FA3486488952BDCF (void);
// 0x000004AB System.Boolean TMPro.Examples.SkewTextExample/<WarpText>d__7::MoveNext()
extern void U3CWarpTextU3Ed__7_MoveNext_mB832E4A3DFDDFECC460A510BBC664F218B3D7FCF (void);
// 0x000004AC System.Object TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3CF506B740C16FEDE58B6E1BE4556CAC2C6AEAD1 (void);
// 0x000004AD System.Void TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.IEnumerator.Reset()
extern void U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_mDDCDBB794F21DF70178A75747D8D8398F38A9C2F (void);
// 0x000004AE System.Object TMPro.Examples.SkewTextExample/<WarpText>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m6D89CF1910F76577E23C924B535E93FD1B01CF0A (void);
// 0x000004AF System.Void TMPro.Examples.TeleType/<Start>d__4::.ctor(System.Int32)
extern void U3CStartU3Ed__4__ctor_mF3575DBEBF4F153F6A899AF3362940298C11B629 (void);
// 0x000004B0 System.Void TMPro.Examples.TeleType/<Start>d__4::System.IDisposable.Dispose()
extern void U3CStartU3Ed__4_System_IDisposable_Dispose_m6DABFDBC2A313BF6DD90AA1C43B4EF9D6249CBE9 (void);
// 0x000004B1 System.Boolean TMPro.Examples.TeleType/<Start>d__4::MoveNext()
extern void U3CStartU3Ed__4_MoveNext_mEF7A3215376BDFB52C3DA9D26FF0459074E89715 (void);
// 0x000004B2 System.Object TMPro.Examples.TeleType/<Start>d__4::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m66DC06D52865D30DAA78DBD70B6554D13E2EA70B (void);
// 0x000004B3 System.Void TMPro.Examples.TeleType/<Start>d__4::System.Collections.IEnumerator.Reset()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_mE854A2CECA0E01F3C2D90DA4F720CF0F387994F8 (void);
// 0x000004B4 System.Object TMPro.Examples.TeleType/<Start>d__4::System.Collections.IEnumerator.get_Current()
extern void U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m77D3936B94684C817EE9F6D2C903238A457D5261 (void);
// 0x000004B5 System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::.ctor(System.Int32)
extern void U3CRevealCharactersU3Ed__7__ctor_mD45A85F5F50909F70C80AC8CE460F4FD261CDE9D (void);
// 0x000004B6 System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.IDisposable.Dispose()
extern void U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m85F270FDC11A4D79E9CF47AADC9FA1FBF032F86C (void);
// 0x000004B7 System.Boolean TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::MoveNext()
extern void U3CRevealCharactersU3Ed__7_MoveNext_mA9A6555E50889A7AA73F20749F25989165023DE3 (void);
// 0x000004B8 System.Object TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5BBAE6868EB7F1C11BE5DF001641E18C9D625F83 (void);
// 0x000004B9 System.Void TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.IEnumerator.Reset()
extern void U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_mA9F4893381AB24E01EAEBFC38A88AF363A9A0691 (void);
// 0x000004BA System.Object TMPro.Examples.TextConsoleSimulator/<RevealCharacters>d__7::System.Collections.IEnumerator.get_Current()
extern void U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_mFD0B7538B1A650FB389FFE9296B0E51AEA5B6B6F (void);
// 0x000004BB System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::.ctor(System.Int32)
extern void U3CRevealWordsU3Ed__8__ctor_m22FF9E770988107A928C5D1EA639F60239BFFEF0 (void);
// 0x000004BC System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.IDisposable.Dispose()
extern void U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m3EAF6EF4A8C99A71FEED251BF3F28A26BF6AD7F8 (void);
// 0x000004BD System.Boolean TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::MoveNext()
extern void U3CRevealWordsU3Ed__8_MoveNext_m3811753E2384D4CBBAD6BD712EBA4FAF00D73210 (void);
// 0x000004BE System.Object TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3F93E4AA532F15D52D68B7121805C014AB2D7FB (void);
// 0x000004BF System.Void TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.IEnumerator.Reset()
extern void U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_mCDC8982948ED5F7743567569CA1D4A354218714F (void);
// 0x000004C0 System.Object TMPro.Examples.TextConsoleSimulator/<RevealWords>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_mF39E492D576810F58DB9031556CFD6806FD32E27 (void);
// 0x000004C1 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::.ctor(System.Int32)
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15__ctor_m3F7F9561735FF8403090A93E708E781B3997B81B (void);
// 0x000004C2 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.IDisposable.Dispose()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_IDisposable_Dispose_m6F24E1DC5777D4682375D8742BC9272EE6D4EA2C (void);
// 0x000004C3 System.Boolean TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::MoveNext()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_MoveNext_m04B1BB9074CBA76BA7F63849721F4CAEA1045A28 (void);
// 0x000004C4 System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m14A7C9DEBD0FF0C7A6979AFEC78B0BEE9E4C3963 (void);
// 0x000004C5 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.Collections.IEnumerator.Reset()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_Reset_mC0D5548A92F57550F60EE76E17B58125B57B94A8 (void);
// 0x000004C6 System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshProFloatingText>d__15::System.Collections.IEnumerator.get_Current()
extern void U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_get_Current_mD3CBFD6F8558EF09F2B4A3D8D1B76BD7FDB3A98B (void);
// 0x000004C7 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::.ctor(System.Int32)
extern void U3CDisplayTextMeshFloatingTextU3Ed__16__ctor_m0466586E44A01852547C6CECCA91C14673B98C20 (void);
// 0x000004C8 System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.IDisposable.Dispose()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_IDisposable_Dispose_m8730A3A2EFBF759A51F0E5330C3BBF8EFC1736A3 (void);
// 0x000004C9 System.Boolean TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::MoveNext()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_MoveNext_m93E19CF96C4EBC57CC4E03E2D3D0B56B3E92A36A (void);
// 0x000004CA System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA4076BE73343A473EFE46E73ABA5DE66CC797598 (void);
// 0x000004CB System.Void TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.Collections.IEnumerator.Reset()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_Reset_mC33B355BB392C5A52DCA0AAF2588E1E78AF6D3A5 (void);
// 0x000004CC System.Object TMPro.Examples.TextMeshProFloatingText/<DisplayTextMeshFloatingText>d__16::System.Collections.IEnumerator.get_Current()
extern void U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_get_Current_m28CCE2E51295AD5F368C504BADDDFF008C2CC0A6 (void);
// 0x000004CD System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__3__ctor_m0038B1054BCC928D35F8C0021ED7D2E1C533E35F (void);
// 0x000004CE System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_m5BAD394A0B09B3E0FF19E91521E02C2B3ADD6007 (void);
// 0x000004CF System.Boolean TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__3_MoveNext_m2842AF5B12AFC17112D1AE75E46AB1B12776D2A6 (void);
// 0x000004D0 System.Object TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAE810D7968957A09C88E61C29DAAEC68E4AF1E51 (void);
// 0x000004D1 System.Void TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_m72ABAC50E9E4D972FB44CAFF387F3E23FEC5D932 (void);
// 0x000004D2 System.Object TMPro.Examples.VertexColorCycler/<AnimateVertexColors>d__3::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_m8DE595A1D01F3A507A356F8BCE020D0851412B52 (void);
// 0x000004D3 System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__11__ctor_m0222C3457F5ACA497FE3A8EC829DE4AD11A169F8 (void);
// 0x000004D4 System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m717E79A39A8161ADDA9E62F7CDFB67B8F2D65099 (void);
// 0x000004D5 System.Boolean TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__11_MoveNext_m169A75C7E2147372CB933520B670AF77907C1C6B (void);
// 0x000004D6 System.Object TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m920EBA43D59A1A88E27FED92CF0AC0DF90179479 (void);
// 0x000004D7 System.Void TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m6C045A6DD0B60F1512457448E615877EAB86D75D (void);
// 0x000004D8 System.Object TMPro.Examples.VertexJitter/<AnimateVertexColors>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m017345FE58B497EAFC9D0CB1FB733F76EB3449AF (void);
// 0x000004D9 System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__11__ctor_m92612416BEC0EBF5E9849FB603629C0F2F95FEF2 (void);
// 0x000004DA System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m77D966994D4717EAFD8EFE169F3E8A4EE8B05B81 (void);
// 0x000004DB System.Boolean TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__11_MoveNext_mCEEDE03667D329386BB4AE7B8252B7A9B54F443F (void);
// 0x000004DC System.Object TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDCCD3645ACF9B18D760B341C863F853996FA9BCE (void);
// 0x000004DD System.Void TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_mB9444B5E58B4E97105C447F547C0F74C51BCFBFA (void);
// 0x000004DE System.Object TMPro.Examples.VertexShakeA/<AnimateVertexColors>d__11::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m5CC464185D5C0251C6206E20AFFA681BA0525A7E (void);
// 0x000004DF System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__10__ctor_m3E3B1D286DEBED2BC028AD490308568B930C3760 (void);
// 0x000004E0 System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m0F9D4B2A6ED0500C2120DBA29932CC279E8908DC (void);
// 0x000004E1 System.Boolean TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__10_MoveNext_m0534E436514E663BF024364E524EE5716FF15C8E (void);
// 0x000004E2 System.Object TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF2A8BAFC0261ACBBE8EEA74DE4B498D30C68AE3D (void);
// 0x000004E3 System.Void TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m5F12D7C6F2523BEB6326FE49AE972116D6157CBB (void);
// 0x000004E4 System.Object TMPro.Examples.VertexShakeB/<AnimateVertexColors>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_mB9C12786011A3B78517AEFAE8D5A78B95A4219AD (void);
// 0x000004E5 System.Void TMPro.Examples.VertexZoom/<>c__DisplayClass10_0::.ctor()
extern void U3CU3Ec__DisplayClass10_0__ctor_m4EC5F8042B5AC645EA7D0913E50FD22144DBD348 (void);
// 0x000004E6 System.Int32 TMPro.Examples.VertexZoom/<>c__DisplayClass10_0::<AnimateVertexColors>b__0(System.Int32,System.Int32)
extern void U3CU3Ec__DisplayClass10_0_U3CAnimateVertexColorsU3Eb__0_m959A7E1D6162B5AAF28B953AFB04D7943BCAB107 (void);
// 0x000004E7 System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::.ctor(System.Int32)
extern void U3CAnimateVertexColorsU3Ed__10__ctor_m537CF0A5ADF5BBC2DF784BF526E3F32EB528E1B2 (void);
// 0x000004E8 System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.IDisposable.Dispose()
extern void U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_mF0CD944C393771101D2718A660E1DFF22385819F (void);
// 0x000004E9 System.Boolean TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::MoveNext()
extern void U3CAnimateVertexColorsU3Ed__10_MoveNext_m8340EFEB2B2CF6EA1545CFB6967968CA171FEE66 (void);
// 0x000004EA System.Object TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m093E922A5A046910B2A7EE826D804CA3064A7BD9 (void);
// 0x000004EB System.Void TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.IEnumerator.Reset()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_mDED63FD52741D58D8D5A3E53415F91B6560F680C (void);
// 0x000004EC System.Object TMPro.Examples.VertexZoom/<AnimateVertexColors>d__10::System.Collections.IEnumerator.get_Current()
extern void U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m933EBF7C4300C7C1E3EE60E4741110025877CA5F (void);
// 0x000004ED System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::.ctor(System.Int32)
extern void U3CWarpTextU3Ed__8__ctor_m9FADE04C27A0034C5A276232FCA187AECDC6BF49 (void);
// 0x000004EE System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::System.IDisposable.Dispose()
extern void U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m5AB0DCF4B3DE6C290487F3DBBC7BAE931130DE60 (void);
// 0x000004EF System.Boolean TMPro.Examples.WarpTextExample/<WarpText>d__8::MoveNext()
extern void U3CWarpTextU3Ed__8_MoveNext_m9C83C8455FF8ADFBAA8D05958C3048612A96EB84 (void);
// 0x000004F0 System.Object TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D5776AF80C4227401ADD7E13D98F2530CB9E7A1 (void);
// 0x000004F1 System.Void TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.IEnumerator.Reset()
extern void U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_m4F2FBBE7B375E6FC837736B4275A7B601A01F535 (void);
// 0x000004F2 System.Object TMPro.Examples.WarpTextExample/<WarpText>d__8::System.Collections.IEnumerator.get_Current()
extern void U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m01FA935D93D737C9DDD88A051F7DFC393DD7BD25 (void);
// 0x000004F3 System.Void Map.Area/<>c::.cctor()
extern void U3CU3Ec__cctor_m6CFB8B055EB055FD65E19623424D0978714EBAF5 (void);
// 0x000004F4 System.Void Map.Area/<>c::.ctor()
extern void U3CU3Ec__ctor_m5E2634FAC998262A2F28D52C5561A946470F7AFB (void);
// 0x000004F5 System.Boolean Map.Area/<>c::<SellHero>b__20_0(Manager.HeroBluePrint)
extern void U3CU3Ec_U3CSellHeroU3Eb__20_0_m60AC6C48E612C15ED96B4B421C09A3FD3A9B0A91 (void);
// 0x000004F6 System.Boolean SimpleJSON.JSONNode/Enumerator::get_IsValid()
extern void Enumerator_get_IsValid_m078841FA11B23FAA03C8C0F0F5CFE1C7A33CC267 (void);
// 0x000004F7 System.Void SimpleJSON.JSONNode/Enumerator::.ctor(System.Collections.Generic.List`1/Enumerator<SimpleJSON.JSONNode>)
extern void Enumerator__ctor_m0A9EDBF3F33AA5FAE87C204FA885D965D16E8A44 (void);
// 0x000004F8 System.Void SimpleJSON.JSONNode/Enumerator::.ctor(System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJSON.JSONNode>)
extern void Enumerator__ctor_mDE6D3B06DF10240F0C637131119C8B09CE9EC4DC (void);
// 0x000004F9 System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode> SimpleJSON.JSONNode/Enumerator::get_Current()
extern void Enumerator_get_Current_m6C700A8C86DAB32F9FE845C3BE1E35D17757C65C (void);
// 0x000004FA System.Boolean SimpleJSON.JSONNode/Enumerator::MoveNext()
extern void Enumerator_MoveNext_m095925BE3D881DF1E015997DBEAFE0851722E53E (void);
// 0x000004FB System.Void SimpleJSON.JSONNode/ValueEnumerator::.ctor(System.Collections.Generic.List`1/Enumerator<SimpleJSON.JSONNode>)
extern void ValueEnumerator__ctor_m1FF9C4191C706E48C3F9A191D0CE02F9BECDCE45 (void);
// 0x000004FC System.Void SimpleJSON.JSONNode/ValueEnumerator::.ctor(System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJSON.JSONNode>)
extern void ValueEnumerator__ctor_m8143FFB5AD9AB3936DF1A59E3492C2CF81793AF5 (void);
// 0x000004FD System.Void SimpleJSON.JSONNode/ValueEnumerator::.ctor(SimpleJSON.JSONNode/Enumerator)
extern void ValueEnumerator__ctor_mE92051B3948C6A45D4F9265EA6D8329D70649D06 (void);
// 0x000004FE SimpleJSON.JSONNode SimpleJSON.JSONNode/ValueEnumerator::get_Current()
extern void ValueEnumerator_get_Current_mAB38FFFD4C1293C61EE73C825D85B7258B61961C (void);
// 0x000004FF System.Boolean SimpleJSON.JSONNode/ValueEnumerator::MoveNext()
extern void ValueEnumerator_MoveNext_mE68E69AFB9D6FA2B41F55B066F3295490EED5589 (void);
// 0x00000500 SimpleJSON.JSONNode/ValueEnumerator SimpleJSON.JSONNode/ValueEnumerator::GetEnumerator()
extern void ValueEnumerator_GetEnumerator_mF841522782073D8D5F9782A37B2487BEA63E5E09 (void);
// 0x00000501 System.Void SimpleJSON.JSONNode/KeyEnumerator::.ctor(System.Collections.Generic.List`1/Enumerator<SimpleJSON.JSONNode>)
extern void KeyEnumerator__ctor_m6E59374D6FF2E994DFB9BFCA3759CC335CA48D23 (void);
// 0x00000502 System.Void SimpleJSON.JSONNode/KeyEnumerator::.ctor(System.Collections.Generic.Dictionary`2/Enumerator<System.String,SimpleJSON.JSONNode>)
extern void KeyEnumerator__ctor_mB67A74FB33429333A208FA269F011122C1AB6395 (void);
// 0x00000503 System.Void SimpleJSON.JSONNode/KeyEnumerator::.ctor(SimpleJSON.JSONNode/Enumerator)
extern void KeyEnumerator__ctor_m5F83F3F252DE00DB6AC2AE59FD501F921C56F224 (void);
// 0x00000504 SimpleJSON.JSONNode SimpleJSON.JSONNode/KeyEnumerator::get_Current()
extern void KeyEnumerator_get_Current_mAC153090E0D57451FE61CA3651CF32A4CE21477C (void);
// 0x00000505 System.Boolean SimpleJSON.JSONNode/KeyEnumerator::MoveNext()
extern void KeyEnumerator_MoveNext_mFF5FB4ECB623B58733E9B85846A296853462F2F4 (void);
// 0x00000506 SimpleJSON.JSONNode/KeyEnumerator SimpleJSON.JSONNode/KeyEnumerator::GetEnumerator()
extern void KeyEnumerator_GetEnumerator_m4534071ECBA3E7165BB0DC642EA9E827A29F5A87 (void);
// 0x00000507 System.Void SimpleJSON.JSONNode/LinqEnumerator::.ctor(SimpleJSON.JSONNode)
extern void LinqEnumerator__ctor_m711F6A5F82CC6093E5245A51BFEF95B0355E2FAC (void);
// 0x00000508 System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode> SimpleJSON.JSONNode/LinqEnumerator::get_Current()
extern void LinqEnumerator_get_Current_m7F293F41FED328A040E5BC71C7F4560C3B8C295C (void);
// 0x00000509 System.Object SimpleJSON.JSONNode/LinqEnumerator::System.Collections.IEnumerator.get_Current()
extern void LinqEnumerator_System_Collections_IEnumerator_get_Current_m36203F845AA95DEB756D145E03469EF077B79BF1 (void);
// 0x0000050A System.Boolean SimpleJSON.JSONNode/LinqEnumerator::MoveNext()
extern void LinqEnumerator_MoveNext_m547BA7FD679ED14C3E698763EBEC58333194B9EE (void);
// 0x0000050B System.Void SimpleJSON.JSONNode/LinqEnumerator::Dispose()
extern void LinqEnumerator_Dispose_mF5270BF1DEAB954E8FC65435DF6D6FD5DBE1C21F (void);
// 0x0000050C System.Collections.Generic.IEnumerator`1<System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode>> SimpleJSON.JSONNode/LinqEnumerator::GetEnumerator()
extern void LinqEnumerator_GetEnumerator_m356D1E4E1EC2D215962F677F3C6AE538E3CF9A20 (void);
// 0x0000050D System.Void SimpleJSON.JSONNode/LinqEnumerator::Reset()
extern void LinqEnumerator_Reset_mCC3E0E49D5343E81C63A2B4A28A7268F628A2522 (void);
// 0x0000050E System.Collections.IEnumerator SimpleJSON.JSONNode/LinqEnumerator::System.Collections.IEnumerable.GetEnumerator()
extern void LinqEnumerator_System_Collections_IEnumerable_GetEnumerator_m713D8655544E9FC2911762CD17D215B5763AE735 (void);
// 0x0000050F System.Void SimpleJSON.JSONNode/<get_Children>d__39::.ctor(System.Int32)
extern void U3Cget_ChildrenU3Ed__39__ctor_m0BF33D6A9ACF56C849BB64AB175B43D7692499A8 (void);
// 0x00000510 System.Void SimpleJSON.JSONNode/<get_Children>d__39::System.IDisposable.Dispose()
extern void U3Cget_ChildrenU3Ed__39_System_IDisposable_Dispose_mFD004DCB3358CE2D7764E776A031A077B21BEB29 (void);
// 0x00000511 System.Boolean SimpleJSON.JSONNode/<get_Children>d__39::MoveNext()
extern void U3Cget_ChildrenU3Ed__39_MoveNext_m858F052BAF7771FE605FCE68EC05F97FA5143538 (void);
// 0x00000512 SimpleJSON.JSONNode SimpleJSON.JSONNode/<get_Children>d__39::System.Collections.Generic.IEnumerator<SimpleJSON.JSONNode>.get_Current()
extern void U3Cget_ChildrenU3Ed__39_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_mB88A038112706EAD24B23A14267B794021C761C2 (void);
// 0x00000513 System.Void SimpleJSON.JSONNode/<get_Children>d__39::System.Collections.IEnumerator.Reset()
extern void U3Cget_ChildrenU3Ed__39_System_Collections_IEnumerator_Reset_m716ACA15194120CF8192937025FD3D4D7F8C710F (void);
// 0x00000514 System.Object SimpleJSON.JSONNode/<get_Children>d__39::System.Collections.IEnumerator.get_Current()
extern void U3Cget_ChildrenU3Ed__39_System_Collections_IEnumerator_get_Current_m831C58F860DBA03146F00BB7DC77DD1852943881 (void);
// 0x00000515 System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode> SimpleJSON.JSONNode/<get_Children>d__39::System.Collections.Generic.IEnumerable<SimpleJSON.JSONNode>.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__39_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_mC22210E7538A1A259E64227EEAC5440FE868ED60 (void);
// 0x00000516 System.Collections.IEnumerator SimpleJSON.JSONNode/<get_Children>d__39::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__39_System_Collections_IEnumerable_GetEnumerator_mDB57DC3D1B30B06919F6F3985E3B3564974C1CAA (void);
// 0x00000517 System.Void SimpleJSON.JSONNode/<get_DeepChildren>d__41::.ctor(System.Int32)
extern void U3Cget_DeepChildrenU3Ed__41__ctor_mAF92DE35182876CDDB7F9E2831AA0078B111A5F9 (void);
// 0x00000518 System.Void SimpleJSON.JSONNode/<get_DeepChildren>d__41::System.IDisposable.Dispose()
extern void U3Cget_DeepChildrenU3Ed__41_System_IDisposable_Dispose_m0C1243483A3118059E0FB871E404B606DD55CBBD (void);
// 0x00000519 System.Boolean SimpleJSON.JSONNode/<get_DeepChildren>d__41::MoveNext()
extern void U3Cget_DeepChildrenU3Ed__41_MoveNext_mBC10790834A0F6E35D8D274754EF1C77A320E391 (void);
// 0x0000051A System.Void SimpleJSON.JSONNode/<get_DeepChildren>d__41::<>m__Finally1()
extern void U3Cget_DeepChildrenU3Ed__41_U3CU3Em__Finally1_m90DA0BA712B5459B1157239B9995E01820163928 (void);
// 0x0000051B System.Void SimpleJSON.JSONNode/<get_DeepChildren>d__41::<>m__Finally2()
extern void U3Cget_DeepChildrenU3Ed__41_U3CU3Em__Finally2_mB8A69D17B39DA453A0811F66B86B5426EF3C74DA (void);
// 0x0000051C SimpleJSON.JSONNode SimpleJSON.JSONNode/<get_DeepChildren>d__41::System.Collections.Generic.IEnumerator<SimpleJSON.JSONNode>.get_Current()
extern void U3Cget_DeepChildrenU3Ed__41_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_mF12A4534E7D4122F67B420801B2522BC9F839868 (void);
// 0x0000051D System.Void SimpleJSON.JSONNode/<get_DeepChildren>d__41::System.Collections.IEnumerator.Reset()
extern void U3Cget_DeepChildrenU3Ed__41_System_Collections_IEnumerator_Reset_m3F120F44D1BEB26137AF14E11165003EF8E18190 (void);
// 0x0000051E System.Object SimpleJSON.JSONNode/<get_DeepChildren>d__41::System.Collections.IEnumerator.get_Current()
extern void U3Cget_DeepChildrenU3Ed__41_System_Collections_IEnumerator_get_Current_mF43CD05D3BF0CD3E76A265F0A684D43EFA4F22EC (void);
// 0x0000051F System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode> SimpleJSON.JSONNode/<get_DeepChildren>d__41::System.Collections.Generic.IEnumerable<SimpleJSON.JSONNode>.GetEnumerator()
extern void U3Cget_DeepChildrenU3Ed__41_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_mAD435B3462420F0485567562B0FF3F1DB60B6AF8 (void);
// 0x00000520 System.Collections.IEnumerator SimpleJSON.JSONNode/<get_DeepChildren>d__41::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_DeepChildrenU3Ed__41_System_Collections_IEnumerable_GetEnumerator_m554A68924D4655591E09B01C0EFF3A9204E5E117 (void);
// 0x00000521 System.Void SimpleJSON.JSONArray/<get_Children>d__22::.ctor(System.Int32)
extern void U3Cget_ChildrenU3Ed__22__ctor_m95EBCC8BC9794B6D9F4C6B5885704BF181FC9DBF (void);
// 0x00000522 System.Void SimpleJSON.JSONArray/<get_Children>d__22::System.IDisposable.Dispose()
extern void U3Cget_ChildrenU3Ed__22_System_IDisposable_Dispose_mCBFE04EF85659884667F66453D36323A90F4A37E (void);
// 0x00000523 System.Boolean SimpleJSON.JSONArray/<get_Children>d__22::MoveNext()
extern void U3Cget_ChildrenU3Ed__22_MoveNext_m465E0B8D3164E27BE47D02A0ACC56B5B795DBB95 (void);
// 0x00000524 System.Void SimpleJSON.JSONArray/<get_Children>d__22::<>m__Finally1()
extern void U3Cget_ChildrenU3Ed__22_U3CU3Em__Finally1_mCA65F3342A396F12C190A1654CC8019248A0BAB4 (void);
// 0x00000525 SimpleJSON.JSONNode SimpleJSON.JSONArray/<get_Children>d__22::System.Collections.Generic.IEnumerator<SimpleJSON.JSONNode>.get_Current()
extern void U3Cget_ChildrenU3Ed__22_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_mA7F43EFC69B364F48AC29BF1C37A5001990511FE (void);
// 0x00000526 System.Void SimpleJSON.JSONArray/<get_Children>d__22::System.Collections.IEnumerator.Reset()
extern void U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerator_Reset_m531EF5FB0BD16A29769CBDE11FDF26AEC2D55F5E (void);
// 0x00000527 System.Object SimpleJSON.JSONArray/<get_Children>d__22::System.Collections.IEnumerator.get_Current()
extern void U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerator_get_Current_m9C20E2FAE0940D92F51C5E1C9D804E406BF2F34F (void);
// 0x00000528 System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode> SimpleJSON.JSONArray/<get_Children>d__22::System.Collections.Generic.IEnumerable<SimpleJSON.JSONNode>.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__22_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m7F40779C5E4EF43C9898EFDECF0FA816A7E5DCFA (void);
// 0x00000529 System.Collections.IEnumerator SimpleJSON.JSONArray/<get_Children>d__22::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerable_GetEnumerator_mCC77091552DD207B63E4B7C2B33693B4467ED68D (void);
// 0x0000052A System.Void SimpleJSON.JSONObject/<>c__DisplayClass21_0::.ctor()
extern void U3CU3Ec__DisplayClass21_0__ctor_mC0EC999DE1ACB8335F31D6D652C0379FF2D065A6 (void);
// 0x0000052B System.Boolean SimpleJSON.JSONObject/<>c__DisplayClass21_0::<Remove>b__0(System.Collections.Generic.KeyValuePair`2<System.String,SimpleJSON.JSONNode>)
extern void U3CU3Ec__DisplayClass21_0_U3CRemoveU3Eb__0_m4DAF41E69ADC8210F10A0E9353204C4C9BB593F0 (void);
// 0x0000052C System.Void SimpleJSON.JSONObject/<get_Children>d__23::.ctor(System.Int32)
extern void U3Cget_ChildrenU3Ed__23__ctor_m24E00E4270F8E59C9ECF5B85CDB1A34683FC1141 (void);
// 0x0000052D System.Void SimpleJSON.JSONObject/<get_Children>d__23::System.IDisposable.Dispose()
extern void U3Cget_ChildrenU3Ed__23_System_IDisposable_Dispose_m413B24FB7D7B0156DFC7E240DA8391C859440847 (void);
// 0x0000052E System.Boolean SimpleJSON.JSONObject/<get_Children>d__23::MoveNext()
extern void U3Cget_ChildrenU3Ed__23_MoveNext_m3AD86370936CBDF4689019E0369B72B54B4B4BE0 (void);
// 0x0000052F System.Void SimpleJSON.JSONObject/<get_Children>d__23::<>m__Finally1()
extern void U3Cget_ChildrenU3Ed__23_U3CU3Em__Finally1_m86405C8AACDCB84C101AB4D4812392161C83D82C (void);
// 0x00000530 SimpleJSON.JSONNode SimpleJSON.JSONObject/<get_Children>d__23::System.Collections.Generic.IEnumerator<SimpleJSON.JSONNode>.get_Current()
extern void U3Cget_ChildrenU3Ed__23_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_m226D4CC6A995605411240005C904F701A7E7D224 (void);
// 0x00000531 System.Void SimpleJSON.JSONObject/<get_Children>d__23::System.Collections.IEnumerator.Reset()
extern void U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerator_Reset_m60B145CE869947C90DB4665A23EDDF48AE63AC25 (void);
// 0x00000532 System.Object SimpleJSON.JSONObject/<get_Children>d__23::System.Collections.IEnumerator.get_Current()
extern void U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerator_get_Current_m51D1A00926BFE5BDA287FE9A878C6E949F9D0DBB (void);
// 0x00000533 System.Collections.Generic.IEnumerator`1<SimpleJSON.JSONNode> SimpleJSON.JSONObject/<get_Children>d__23::System.Collections.Generic.IEnumerable<SimpleJSON.JSONNode>.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__23_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_mBE8B0928518FD2517C5F344791F32EA64F13C45C (void);
// 0x00000534 System.Collections.IEnumerator SimpleJSON.JSONObject/<get_Children>d__23::System.Collections.IEnumerable.GetEnumerator()
extern void U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerable_GetEnumerator_m19CBB01715DEE09E679303048771E8ABC2A2D4C0 (void);
// 0x00000535 System.Void Enemy.BaseEnemy/<DelayLaserPopUp>d__28::.ctor(System.Int32)
extern void U3CDelayLaserPopUpU3Ed__28__ctor_m1DD030E25497BC7EAFFF48B12878BC3F6E42E867 (void);
// 0x00000536 System.Void Enemy.BaseEnemy/<DelayLaserPopUp>d__28::System.IDisposable.Dispose()
extern void U3CDelayLaserPopUpU3Ed__28_System_IDisposable_Dispose_m4ACA82D241FAA199208AD68B89AB6A6D558B6F7F (void);
// 0x00000537 System.Boolean Enemy.BaseEnemy/<DelayLaserPopUp>d__28::MoveNext()
extern void U3CDelayLaserPopUpU3Ed__28_MoveNext_mDE7E5B884ECE5D232BFB54FCFD9034013DD43E7C (void);
// 0x00000538 System.Object Enemy.BaseEnemy/<DelayLaserPopUp>d__28::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDelayLaserPopUpU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2836850B7B9251D61A015650525BA37CFEE489AC (void);
// 0x00000539 System.Void Enemy.BaseEnemy/<DelayLaserPopUp>d__28::System.Collections.IEnumerator.Reset()
extern void U3CDelayLaserPopUpU3Ed__28_System_Collections_IEnumerator_Reset_m418F0BB7A31FD5BEAFECE3CA6971390F3E76542F (void);
// 0x0000053A System.Object Enemy.BaseEnemy/<DelayLaserPopUp>d__28::System.Collections.IEnumerator.get_Current()
extern void U3CDelayLaserPopUpU3Ed__28_System_Collections_IEnumerator_get_Current_m1E4F5A5594B28D5034EFE6A5E55EEC44FC0C490B (void);
// 0x0000053B System.Void Bullet.BaseBullet/<>c__DisplayClass7_0::.ctor()
extern void U3CU3Ec__DisplayClass7_0__ctor_mF8395969F602712B1940D9A5208E54CCC007EAD1 (void);
// 0x0000053C System.Boolean Bullet.BaseBullet/<>c__DisplayClass7_0::<GetHeroBluePrint>b__0(Manager.HeroBluePrint)
extern void U3CU3Ec__DisplayClass7_0_U3CGetHeroBluePrintU3Eb__0_m3EB2F62C4685E7DEDE93C2248CD6B69CAB4EB29C (void);
// 0x0000053D System.Void Manager.InventoryManager/<>c::.cctor()
extern void U3CU3Ec__cctor_m4491C4A23EA840B8C61AFA8375CB12AEF6F638C6 (void);
// 0x0000053E System.Void Manager.InventoryManager/<>c::.ctor()
extern void U3CU3Ec__ctor_mF4208387AD3EAEC1BB7C3B22944DE7E6A1BE4ED2 (void);
// 0x0000053F System.Boolean Manager.InventoryManager/<>c::<AddCardToInventory>b__21_0(UnityEngine.UI.Button)
extern void U3CU3Ec_U3CAddCardToInventoryU3Eb__21_0_m0657414F89D8AB6E5CF7563BE7FCE7C201EF2CC0 (void);
// 0x00000540 System.Boolean Manager.InventoryManager/<>c::<CancelCardUsed>b__25_1(Manager.HeroBluePrint)
extern void U3CU3Ec_U3CCancelCardUsedU3Eb__25_1_m70E89BBD35DAF3418A612AF26236FECA85C3D167 (void);
// 0x00000541 System.Void Manager.InventoryManager/<>c__DisplayClass23_0::.ctor()
extern void U3CU3Ec__DisplayClass23_0__ctor_mC9B305238A92678949F2044FCE9213C7F988D0C6 (void);
// 0x00000542 System.Void Manager.InventoryManager/<>c__DisplayClass23_0::<AddCard>b__0()
extern void U3CU3Ec__DisplayClass23_0_U3CAddCardU3Eb__0_m1AC7B81E60BC03D0551F39C31ACC0FB73E233A7A (void);
// 0x00000543 System.Void Manager.InventoryManager/<>c__DisplayClass24_0::.ctor()
extern void U3CU3Ec__DisplayClass24_0__ctor_m3E6E520D7E39B9BB8356F800C5F52FB83B9903A1 (void);
// 0x00000544 System.Void Manager.InventoryManager/<>c__DisplayClass24_0::<GetCard>b__0()
extern void U3CU3Ec__DisplayClass24_0_U3CGetCardU3Eb__0_m3FB036C259055D0782F68F34328B4F22F6023617 (void);
// 0x00000545 System.Void Manager.InventoryManager/<>c__DisplayClass25_0::.ctor()
extern void U3CU3Ec__DisplayClass25_0__ctor_m793F6F8EBF9F8306C210BA7FED94F50D49B2F481 (void);
// 0x00000546 System.Void Manager.InventoryManager/<>c__DisplayClass25_0::<CancelCardUsed>b__0()
extern void U3CU3Ec__DisplayClass25_0_U3CCancelCardUsedU3Eb__0_m999307189E9BA8992DAE87D569137A2177822AA9 (void);
// 0x00000547 System.Void Manager.InventoryManager/<>c__DisplayClass26_0::.ctor()
extern void U3CU3Ec__DisplayClass26_0__ctor_mC9EF1121F9954DC8B7BF172EFDFDCB5369489FED (void);
// 0x00000548 System.Boolean Manager.InventoryManager/<>c__DisplayClass26_0::<GetHero>b__0(Manager.HeroBluePrint)
extern void U3CU3Ec__DisplayClass26_0_U3CGetHeroU3Eb__0_mBB95FF796E6F6186B374AE81CB8E8DF6D504AC3E (void);
// 0x00000549 System.Void Manager.UiManager/<WaitForNoticeTimerOut>d__43::.ctor(System.Int32)
extern void U3CWaitForNoticeTimerOutU3Ed__43__ctor_mC64694756F43EEF9DCC1F4101737D83C1DC8BB93 (void);
// 0x0000054A System.Void Manager.UiManager/<WaitForNoticeTimerOut>d__43::System.IDisposable.Dispose()
extern void U3CWaitForNoticeTimerOutU3Ed__43_System_IDisposable_Dispose_m01791A5F038C1BCF58742292247713EFA6C770CC (void);
// 0x0000054B System.Boolean Manager.UiManager/<WaitForNoticeTimerOut>d__43::MoveNext()
extern void U3CWaitForNoticeTimerOutU3Ed__43_MoveNext_m37CBB672866BB1D4AC5A1DDD2B4FED3D9512F30D (void);
// 0x0000054C System.Object Manager.UiManager/<WaitForNoticeTimerOut>d__43::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForNoticeTimerOutU3Ed__43_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m03D682641A449C18CD1C00484E7509D3C8D843EC (void);
// 0x0000054D System.Void Manager.UiManager/<WaitForNoticeTimerOut>d__43::System.Collections.IEnumerator.Reset()
extern void U3CWaitForNoticeTimerOutU3Ed__43_System_Collections_IEnumerator_Reset_mDDA5A5A3E227742BEFD8545DFC4EDA5B33430563 (void);
// 0x0000054E System.Object Manager.UiManager/<WaitForNoticeTimerOut>d__43::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForNoticeTimerOutU3Ed__43_System_Collections_IEnumerator_get_Current_mB7841FEE52274FEA64951C4EF021BC1825F42031 (void);
// 0x0000054F System.Void Manager.UiManager/<WaitForNoticeOut>d__46::.ctor(System.Int32)
extern void U3CWaitForNoticeOutU3Ed__46__ctor_m6F5A034947277CE289E43F8D368B15C565DA34DA (void);
// 0x00000550 System.Void Manager.UiManager/<WaitForNoticeOut>d__46::System.IDisposable.Dispose()
extern void U3CWaitForNoticeOutU3Ed__46_System_IDisposable_Dispose_m5E3269113CF4303B8B8D4081E875B335EB3FEE1C (void);
// 0x00000551 System.Boolean Manager.UiManager/<WaitForNoticeOut>d__46::MoveNext()
extern void U3CWaitForNoticeOutU3Ed__46_MoveNext_m45475A48061520D331EF00054E79795D11B0B591 (void);
// 0x00000552 System.Object Manager.UiManager/<WaitForNoticeOut>d__46::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CWaitForNoticeOutU3Ed__46_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m389C85183678A455CB9EFF03218F4F9459269270 (void);
// 0x00000553 System.Void Manager.UiManager/<WaitForNoticeOut>d__46::System.Collections.IEnumerator.Reset()
extern void U3CWaitForNoticeOutU3Ed__46_System_Collections_IEnumerator_Reset_m84D84F466AE4D9EC09F0827B66E1C4085133898A (void);
// 0x00000554 System.Object Manager.UiManager/<WaitForNoticeOut>d__46::System.Collections.IEnumerator.get_Current()
extern void U3CWaitForNoticeOutU3Ed__46_System_Collections_IEnumerator_get_Current_mE2FCDD664F3D5710AF71599273562A7143A1B9E9 (void);
// 0x00000555 System.Void Manager.UiManager/<>c__DisplayClass53_0::.ctor()
extern void U3CU3Ec__DisplayClass53_0__ctor_mCB48B14B6E4EB303A05D36AE14EB70BE8D1D03B0 (void);
// 0x00000556 System.Boolean Manager.UiManager/<>c__DisplayClass53_0::<RemoveChainSkillText>b__0(TMPro.TextMeshProUGUI)
extern void U3CU3Ec__DisplayClass53_0_U3CRemoveChainSkillTextU3Eb__0_mC29500B91D3208446E347FEF32EFF8E8E215F397 (void);
// 0x00000557 System.Void Manager.WorldUIManager/<NextEnemy>d__26::.ctor(System.Int32)
extern void U3CNextEnemyU3Ed__26__ctor_mF7F906DB2AE8D88AD0A7E5231306EFF4CD401EE0 (void);
// 0x00000558 System.Void Manager.WorldUIManager/<NextEnemy>d__26::System.IDisposable.Dispose()
extern void U3CNextEnemyU3Ed__26_System_IDisposable_Dispose_m51376AE841CDA01FEDF72193251CD600592192CA (void);
// 0x00000559 System.Boolean Manager.WorldUIManager/<NextEnemy>d__26::MoveNext()
extern void U3CNextEnemyU3Ed__26_MoveNext_mAFF9FB7DE510E6F97E8346DCF1859BA14FF15E78 (void);
// 0x0000055A System.Object Manager.WorldUIManager/<NextEnemy>d__26::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CNextEnemyU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0644F6F37C2E4EB196F8C3EE1EAE560375A86B61 (void);
// 0x0000055B System.Void Manager.WorldUIManager/<NextEnemy>d__26::System.Collections.IEnumerator.Reset()
extern void U3CNextEnemyU3Ed__26_System_Collections_IEnumerator_Reset_mC1135533A36ACAF17E80743D6142CD29C926476B (void);
// 0x0000055C System.Object Manager.WorldUIManager/<NextEnemy>d__26::System.Collections.IEnumerator.get_Current()
extern void U3CNextEnemyU3Ed__26_System_Collections_IEnumerator_get_Current_m2FF04E7D947B7C1A1ABCB509822D255ADEE518EC (void);
// 0x0000055D System.Void Manager.WorldUIManager/<NextHero>d__27::.ctor(System.Int32)
extern void U3CNextHeroU3Ed__27__ctor_m223862A8FD430B9194B60EB590B188E6761543A9 (void);
// 0x0000055E System.Void Manager.WorldUIManager/<NextHero>d__27::System.IDisposable.Dispose()
extern void U3CNextHeroU3Ed__27_System_IDisposable_Dispose_m5E5704CA63BBBD3107B59C4A610ABEFBE5175BEC (void);
// 0x0000055F System.Boolean Manager.WorldUIManager/<NextHero>d__27::MoveNext()
extern void U3CNextHeroU3Ed__27_MoveNext_m5A5751DC419DBB8D53891D47F442C31D73549C95 (void);
// 0x00000560 System.Object Manager.WorldUIManager/<NextHero>d__27::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CNextHeroU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0AF8B7EC64336EA9C996CD13B146749D3EA9816C (void);
// 0x00000561 System.Void Manager.WorldUIManager/<NextHero>d__27::System.Collections.IEnumerator.Reset()
extern void U3CNextHeroU3Ed__27_System_Collections_IEnumerator_Reset_mADB82B690752F5485FD704C02A3BBFF637267083 (void);
// 0x00000562 System.Object Manager.WorldUIManager/<NextHero>d__27::System.Collections.IEnumerator.get_Current()
extern void U3CNextHeroU3Ed__27_System_Collections_IEnumerator_get_Current_m94202AE088D04C101BF4F74AB5F76AD0286EA924 (void);
// 0x00000563 System.Void Manager.WorldUIManager/<DisableInfo>d__33::.ctor(System.Int32)
extern void U3CDisableInfoU3Ed__33__ctor_m60DCAC3ED8F8B0CC7BCBE7E03BABF63357A6FA6D (void);
// 0x00000564 System.Void Manager.WorldUIManager/<DisableInfo>d__33::System.IDisposable.Dispose()
extern void U3CDisableInfoU3Ed__33_System_IDisposable_Dispose_m932AC83042484FA097910F368C56EC1CC724865F (void);
// 0x00000565 System.Boolean Manager.WorldUIManager/<DisableInfo>d__33::MoveNext()
extern void U3CDisableInfoU3Ed__33_MoveNext_m953A4EE1471D47663CB0077A7CA42BD3BD106C8D (void);
// 0x00000566 System.Object Manager.WorldUIManager/<DisableInfo>d__33::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CDisableInfoU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m370655C1AD58BE840BC850118005790511801227 (void);
// 0x00000567 System.Void Manager.WorldUIManager/<DisableInfo>d__33::System.Collections.IEnumerator.Reset()
extern void U3CDisableInfoU3Ed__33_System_Collections_IEnumerator_Reset_m028A5C860CB4B798DDF150CFF80F4E64A1F419E9 (void);
// 0x00000568 System.Object Manager.WorldUIManager/<DisableInfo>d__33::System.Collections.IEnumerator.get_Current()
extern void U3CDisableInfoU3Ed__33_System_Collections_IEnumerator_get_Current_mC802088660CAAE6D1999E1F89950C8372657FF82 (void);
// 0x00000569 System.Void Manager.WorldUIManager/<FireWorkClicked>d__34::.ctor(System.Int32)
extern void U3CFireWorkClickedU3Ed__34__ctor_m7A1B53D72F6553C8AEF3C2D73B1DC7C5E9E56B91 (void);
// 0x0000056A System.Void Manager.WorldUIManager/<FireWorkClicked>d__34::System.IDisposable.Dispose()
extern void U3CFireWorkClickedU3Ed__34_System_IDisposable_Dispose_m46CCAE3CD88625D4F9544A098BCD88E0AB58DD9A (void);
// 0x0000056B System.Boolean Manager.WorldUIManager/<FireWorkClicked>d__34::MoveNext()
extern void U3CFireWorkClickedU3Ed__34_MoveNext_mB9F9F4B2B8DA0F6EF84CD1C751D1567ADFC11C96 (void);
// 0x0000056C System.Object Manager.WorldUIManager/<FireWorkClicked>d__34::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CFireWorkClickedU3Ed__34_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF91C50E982052F7A84C122811F02FB459B0E63E1 (void);
// 0x0000056D System.Void Manager.WorldUIManager/<FireWorkClicked>d__34::System.Collections.IEnumerator.Reset()
extern void U3CFireWorkClickedU3Ed__34_System_Collections_IEnumerator_Reset_mFCCA5DE1786D2F6C82570827018FC03D4E1773F8 (void);
// 0x0000056E System.Object Manager.WorldUIManager/<FireWorkClicked>d__34::System.Collections.IEnumerator.get_Current()
extern void U3CFireWorkClickedU3Ed__34_System_Collections_IEnumerator_get_Current_mD0C5FEE89BAAAEFDD74726E93B4C290812B0764C (void);
// 0x0000056F System.Void Manager.WorldUIManager/<CookieClicked>d__35::.ctor(System.Int32)
extern void U3CCookieClickedU3Ed__35__ctor_m0B575DE0D60FB548252B06CE74082E1EBDF42D57 (void);
// 0x00000570 System.Void Manager.WorldUIManager/<CookieClicked>d__35::System.IDisposable.Dispose()
extern void U3CCookieClickedU3Ed__35_System_IDisposable_Dispose_mA29E952E03BC1A952D9B75AF867842C8C285DADA (void);
// 0x00000571 System.Boolean Manager.WorldUIManager/<CookieClicked>d__35::MoveNext()
extern void U3CCookieClickedU3Ed__35_MoveNext_mA7BA31A2952F719DC9FB167CCC07036F2B55B5D0 (void);
// 0x00000572 System.Object Manager.WorldUIManager/<CookieClicked>d__35::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCookieClickedU3Ed__35_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB2959A148D9B6ECFE069BB4DA16D939690F29580 (void);
// 0x00000573 System.Void Manager.WorldUIManager/<CookieClicked>d__35::System.Collections.IEnumerator.Reset()
extern void U3CCookieClickedU3Ed__35_System_Collections_IEnumerator_Reset_m26D0EA370FA27109A9812968AF7E2C4D9C53C38E (void);
// 0x00000574 System.Object Manager.WorldUIManager/<CookieClicked>d__35::System.Collections.IEnumerator.get_Current()
extern void U3CCookieClickedU3Ed__35_System_Collections_IEnumerator_get_Current_mD356438C0477B5CAC2CE60DCE85ACC6FAFA10FEC (void);
// 0x00000575 System.Void Manager.WorldUIManager/<CandyClicked>d__36::.ctor(System.Int32)
extern void U3CCandyClickedU3Ed__36__ctor_m4A8D08E3E4BCF74EC885E69C8CC1CF1F0F4AACD4 (void);
// 0x00000576 System.Void Manager.WorldUIManager/<CandyClicked>d__36::System.IDisposable.Dispose()
extern void U3CCandyClickedU3Ed__36_System_IDisposable_Dispose_m5A18D55FE5C43EE1DD366017D649C02BB71BEA7B (void);
// 0x00000577 System.Boolean Manager.WorldUIManager/<CandyClicked>d__36::MoveNext()
extern void U3CCandyClickedU3Ed__36_MoveNext_m48818AF3002C9EF3AE68ADE7DACCD5565D78AA3D (void);
// 0x00000578 System.Object Manager.WorldUIManager/<CandyClicked>d__36::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CCandyClickedU3Ed__36_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m35E28353074F1165B229CE56346CB372606A7FBC (void);
// 0x00000579 System.Void Manager.WorldUIManager/<CandyClicked>d__36::System.Collections.IEnumerator.Reset()
extern void U3CCandyClickedU3Ed__36_System_Collections_IEnumerator_Reset_m85D7FF94205CA8A2640EB611D6CECFC4FFBF9DB7 (void);
// 0x0000057A System.Object Manager.WorldUIManager/<CandyClicked>d__36::System.Collections.IEnumerator.get_Current()
extern void U3CCandyClickedU3Ed__36_System_Collections_IEnumerator_get_Current_m8AE9C3DD85D994C5A0EACACEC1365F49104F17AC (void);
// 0x0000057B System.Void Manager.WorldUIManager/<ScreenShotClicked>d__37::.ctor(System.Int32)
extern void U3CScreenShotClickedU3Ed__37__ctor_m4DF76FA6E783CE5C95EC9174F493FE6E32391324 (void);
// 0x0000057C System.Void Manager.WorldUIManager/<ScreenShotClicked>d__37::System.IDisposable.Dispose()
extern void U3CScreenShotClickedU3Ed__37_System_IDisposable_Dispose_mF247A4A440E9BCC6723DBD6F4829A6A85901EC04 (void);
// 0x0000057D System.Boolean Manager.WorldUIManager/<ScreenShotClicked>d__37::MoveNext()
extern void U3CScreenShotClickedU3Ed__37_MoveNext_m3EC6F91524753A029FFA3B53C3888E051F281D5F (void);
// 0x0000057E System.Object Manager.WorldUIManager/<ScreenShotClicked>d__37::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CScreenShotClickedU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m672E78976F63D8246C55DFA344FBD7F0B5E7062B (void);
// 0x0000057F System.Void Manager.WorldUIManager/<ScreenShotClicked>d__37::System.Collections.IEnumerator.Reset()
extern void U3CScreenShotClickedU3Ed__37_System_Collections_IEnumerator_Reset_m4926A46FCFF998299A791CD8D19835398F2D0851 (void);
// 0x00000580 System.Object Manager.WorldUIManager/<ScreenShotClicked>d__37::System.Collections.IEnumerator.get_Current()
extern void U3CScreenShotClickedU3Ed__37_System_Collections_IEnumerator_get_Current_m2A8F29B42E02863B6334D1099147F59F611D35AA (void);
// 0x00000581 System.Void Manager.WorldUIManager/<LeaderBoardClicked>d__38::.ctor(System.Int32)
extern void U3CLeaderBoardClickedU3Ed__38__ctor_mE7E26DB8380A3579B64BC586E0253B6F18867F6F (void);
// 0x00000582 System.Void Manager.WorldUIManager/<LeaderBoardClicked>d__38::System.IDisposable.Dispose()
extern void U3CLeaderBoardClickedU3Ed__38_System_IDisposable_Dispose_m7CB383587DF40BD43652C5FC78C26B604462A95A (void);
// 0x00000583 System.Boolean Manager.WorldUIManager/<LeaderBoardClicked>d__38::MoveNext()
extern void U3CLeaderBoardClickedU3Ed__38_MoveNext_mB9232F2C56612D985777768BA921D9D2B7EC8696 (void);
// 0x00000584 System.Object Manager.WorldUIManager/<LeaderBoardClicked>d__38::System.Collections.Generic.IEnumerator<System.Object>.get_Current()
extern void U3CLeaderBoardClickedU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9EC830344B8F7C475D00C335A2411922FFD4E25C (void);
// 0x00000585 System.Void Manager.WorldUIManager/<LeaderBoardClicked>d__38::System.Collections.IEnumerator.Reset()
extern void U3CLeaderBoardClickedU3Ed__38_System_Collections_IEnumerator_Reset_m576E4DED7EC5D84BDEEFDDB2EE94C963C2915406 (void);
// 0x00000586 System.Object Manager.WorldUIManager/<LeaderBoardClicked>d__38::System.Collections.IEnumerator.get_Current()
extern void U3CLeaderBoardClickedU3Ed__38_System_Collections_IEnumerator_get_Current_m5C0AFE5F19E6B3B18AF2CAB6BA66B991D14EF762 (void);
static Il2CppMethodPointer s_methodPointers[1414] = 
{
	DayNightCycle_Update_m0C25023C3F2BE55D8394DFAD4D5BCCE163317571,
	DayNightCycle_ChangeTime_m7EB3321F875846760487296086DF3621DA714889,
	DayNightCycle__ctor_m5EDE9A8D722F9FD94C7D511C707D3CFDB3ABD6C8,
	WaterManager_Awake_m9E6149DE853EF8F04B99CEBF9621926BD93684E4,
	WaterManager_Update_m2845FC186C1A7C567F74799E81314268852F6B95,
	WaterManager__ctor_m3329A005F2509CFB87BA80071501999B51095416,
	WaveManager_Awake_m62ED0E86417DC404A2CB4301389740B6124EFD78,
	WaveManager_Update_m179CA7CE42049BFC0291C7EC3AF0D77D7FB9B301,
	WaveManager_GetWaveHeight_m08B1C320B5B3F4106C5912C6865B08488CC183C9,
	WaveManager__ctor_mBDC5BC3BFE97218B2ADA2160C31B34682498DCFF,
	WaterReflection_OnWillRenderObject_mA8126C59F0AA196FB9356E67B1E3EC2A8C8CEE74,
	WaterReflection_OnDisable_m0FBE16036955394DE08D1D84231C9978CC58D70F,
	WaterReflection_UpdateCameraModes_mA407B437EFF18CFEE10A5B5AB080446350D257ED,
	WaterReflection_CreateMirrorObjects_mF9AA9BBC3E767667BA6BF1AFE390CDC9537C63D8,
	WaterReflection_sgn_m95E5C8AE0595A89E79BBFEF34070081D829A23A9,
	WaterReflection_CameraSpacePlane_m175AAD5A32459E659B994B1DD97DD87AA79F614E,
	WaterReflection_CalculateObliqueMatrix_m357CED7D00638688AECE4AF9514B088F6887ABAE,
	WaterReflection_CalculateReflectionMatrix_mF66BFCC859AC9FEDA112C41643F6ABA335787CAD,
	WaterReflection__ctor_m7B8DE8B3F86141802475B009C5E3AC8342BE312F,
	WaterReflection__cctor_mE97A05A6742F123C183BC939586B59393F869B24,
	CardBluePrint__ctor_m6F2F9B910B7B2AE9DFD18F5E89D6D5750CE2A3D5,
	ElementBluePrint__ctor_m84BBC6026AB88DDBC06918F594967E444C2764B9,
	SkillBluePrint__ctor_m581922088833BC48E4F1CBDE2F003D76A0959897,
	Bullet_F13_Start_mD127A0DD8AF669A549D1A3C42D26ED1DD2052AB0,
	Bullet_F13_Update_mA5E5F41024D7CD6D7C13FF93E001B23E3338581D,
	Bullet_F13_OnDrawGizmosSelected_mB208EE1054DB3FA44817CB8542D4760BCF67CA88,
	Bullet_F13__ctor_mC1FBE3B05B7BC50C9BCE337370EBBCF9F2B44FFD,
	Bullet_F81_Start_mD1E95E1CF0D3FEAF51E17214B6357136C170E699,
	Bullet_F81_Update_mF17E98C0F357AE9EF1F88B0777472E9BF5487CC0,
	Bullet_F81_OnDrawGizmosSelected_m85176FD1CC2BDA897D53DB2CF5899A244C4542E3,
	Bullet_F81__ctor_m57FE37CC0A00C7811FEEE929715B386D1C1E8B22,
	Bullet_P32_Start_m09D1FE8339CB537CCB371D12A4A72EB317C0A12C,
	Bullet_P32_Update_mB39B26D371938503D61AE471F1F6EAC9C8EEE63A,
	Bullet_P32_OnDrawGizmosSelected_m413006E0ABB213202A26E5A825914742A7DCFFEE,
	Bullet_P32__ctor_m9A4A6D8F64D0E9493FB97235FD3F2A59DB8FD8A6,
	Bullet_Z1_Start_m80B201D494B0FC5DFF158E99C92C31E983F6762C,
	Bullet_Z1_Update_m85CF42F158B46630C326885EB2E4D2718CB0F078,
	Bullet_Z1_OnDrawGizmosSelected_m232511525BE5F218A0F62A8E25CB6185C4875995,
	Bullet_Z1__ctor_mB915962289836878CF61A1A72E3C3EE22293552E,
	Bullet_Z23_Start_m6FC25F184D891B1BF89B9EC84C4AC71F4412CD59,
	Bullet_Z23_Update_mEFE8EFC6F10825767E2792060EB2AACD9B95E7E1,
	Bullet_Z23_OnDrawGizmosSelected_mD2D81A888B8F331D6AB0BE8E8279B7D9F2F5E32B,
	Bullet_Z23__ctor_m73738B84EC10C746F476ACCC3A07F002DD981F2B,
	CinemachineSwitcher_get_Instance_mF6ABBBE7B4E38636BEB09D7ADF3940BA80DC808B,
	CinemachineSwitcher_set_Instance_mC4980BCB3320D1E7B59956E65C60DCA174B641EA,
	CinemachineSwitcher_Awake_m25CB21057BF4834495973CB7BBA4E68865BBB801,
	CinemachineSwitcher_Start_m5DE5EDDABC85812FDA214459EBD9F0FAD2658BDA,
	CinemachineSwitcher_AdsReward_mBD399E9DD2A6B31B5B4B95D95357F2C7D0F4FA27,
	CinemachineSwitcher_GameOverCam_mDF1566D5ED0F4F43F71DEE6EA6F9597763E3D35E,
	CinemachineSwitcher_WaitToStart_mFB3F4280F74BD988595B53EE4F855CCDBA6708F1,
	CinemachineSwitcher_HideCanvas_m1EB69E3F766DC9792A4F54EF2C22F257335AC661,
	CinemachineSwitcher_SwitchLeftCamera_mE27C16222721F42BD5DF4AE7E7E89A2B479A04C0,
	CinemachineSwitcher_SwitchFontCamera_m59A0CFCD4B5BE80C3B763F614675AE06D616A81C,
	CinemachineSwitcher_WaitForCam_m20F16C627FA98D38C064729772CE31961D3735AE,
	CinemachineSwitcher_SwitchTopCamera_mC4B609132602A7E361F6BD11F5AC626A872BD7AA,
	CinemachineSwitcher_SwitchRightCamera_mB37954856A657DD5B2AB594446F941C46509E46B,
	CinemachineSwitcher_WaitForShowCanvas_m66CF0EE7E5A38923F80DA4040DD48571364BE4AD,
	CinemachineSwitcher__ctor_m125076B67A450514B9B0DEF65A389DFB41FB2D46,
	InputManager_get_Instance_mC5B4EA7BB8A909DB0FDD5D9CBF67B3A1CC40181D,
	InputManager_set_Instance_mB94B3A652E1A869CBB1FFBBE8CA25842E26A3C34,
	InputManager_add_OnStartTouch_mCF106264B78FB07537E71524C99EE140805FF25D,
	InputManager_remove_OnStartTouch_m24C6AC368EC9AA24B40A5D1F3EF7B07A72766C7D,
	InputManager_add_OnEndTouch_m50D940EFE21F790A04BCF54CE88FDF55F953CDFF,
	InputManager_remove_OnEndTouch_m3B6F0D7B26BC509080CA8F30E8D80E0FCA5423D2,
	InputManager_Awake_m56AAFE728DF63B4B922FC31449C82EC6CFB0D946,
	InputManager_OnEnable_m584432543637338E8BBECE286DF8167F10309018,
	InputManager_OnDisable_m93B2282C567FD565E189E8F08A5BBF08EED61478,
	InputManager_Start_m99C6C0D9277906D13076BCD14E7BE2DDAF88FF08,
	InputManager_StartTouchPrimary_m8AABC7B12D09F2ED23825E1CCAD1A40F15ADCE47,
	InputManager_EndTouchPrimary_m6532C782DC9F442C8272E685D963D478C255AA35,
	InputManager_PrimaryPosition_m6F82DF759B187A46A0A049D5F7B15C3E106438DD,
	InputManager__ctor_mB533F16325A793C9274F6CA3804EBCE27AD700A7,
	InputManager_U3CStartU3Eb__17_0_m6D32B313D99F02F691422CF3763619364E7A8936,
	InputManager_U3CStartU3Eb__17_1_m3F85443FB5E717AC691727FFF4C91444705B47AD,
	MobileInput_get_asset_m8065F2CC02A1444F16ADA2794E8071D867C46888,
	MobileInput__ctor_m4DDB04139A6D7296830473E18185DA65099FAB86,
	MobileInput_Dispose_m0CB0FBDC49ECFF0B6A98A66419805962C27B68A2,
	MobileInput_get_bindingMask_m0F596245A82CAB9AAAB735DC0D28E5B4BD4A8BD5,
	MobileInput_set_bindingMask_mD9523881398BCAFA7CB7B3A66A0CAE5787705CCA,
	MobileInput_get_devices_m74DB85C25EB1629E6EDEB33FD0D3D2A362307EB4,
	MobileInput_set_devices_m39041648627016BD8A9B579D906D7ECB268B2F8B,
	MobileInput_get_controlSchemes_mB1AB9A0279747DE3E5B20C983485A3AEF64FC462,
	MobileInput_Contains_m0F8F2B0431E6E73175F69C173DD2F445316CEA98,
	MobileInput_GetEnumerator_m2E60ADD04AAD89BB1E230C5190708B4C669D0D26,
	MobileInput_System_Collections_IEnumerable_GetEnumerator_m718E93A97714E3C66F70E97FB2E5140212B1D4AA,
	MobileInput_Enable_mB764A4D6375832398D0A79024AB57088FE3854F0,
	MobileInput_Disable_mD3641EE7154939C47A0691630D2B97E83878C5BF,
	MobileInput_get_Touch_mE6235E514104B07DAB37A8ADEA5DA6D0B4DD660F,
	SwipeDetection_Awake_m1F16EE2EC922E7C6087AB6C76AB37166B978AE50,
	SwipeDetection_OnEnable_m8F56324CB51CEA6AB5AE215FF93D6354D59EDB74,
	SwipeDetection_OnDisable_m55567A728958F6000E8FA0CBE12600CC0CE90D23,
	SwipeDetection_SwipeStart_mF76804A19BA6BEADB9F5F86288D6E263CBEF6CB7,
	SwipeDetection_SwipeEnd_m58C2324A595B121023B3F2CEA1E8126DB4DA5C60,
	SwipeDetection_DetectSwipe_m3F29BC8BF7CC981CB377FF27B55FFB479FDF41C7,
	SwipeDetection_SwipeDirection_m860B495376A9B2D25B0520C9E8F2D706A8887692,
	SwipeDetection__ctor_m2C4513A493B17DBB06F395DCEA18547D30EC2D58,
	Utils_ScreenToWorld_m0DC4BED80C5AD99D0B2950C6BF74F1AE186539E8,
	Utils__ctor_mC41E39AE5559F8129A8C15EFD3E22EF8ACEDCCAE,
	ScreenCapture_Start_mE754AB7DB07B1B52F9528011C8394B374ACF6CE5,
	ScreenCapture_CreateFileName_m70DCE68DAC386836200A8F1BEC91721F20210989,
	ScreenCapture_CaptureScreenshot_mE2E816A00BCB0CDC06E3650079C466729D0B0E4D,
	ScreenCapture_TakeScreenShot_mAE70B259C4E62A028B502F320DAAF09D79ACCEEB,
	ScreenCapture_ImageShowCase_m7C1802B6832D044746834F97B1D7D55505D707AA,
	ScreenCapture_ShareImage_m9227EEB4098F09988F9995027C2422525349B905,
	ScreenCapture__ctor_mF1A1D405C141990DEFD5166FA5370FD91EE8F6D4,
	CubeButton_OnMouseDown_m7B567FA80595EF0E1614FEA98BDC304BF13D5320,
	CubeButton__ctor_mCBB82476431D9EC9DAEBDD69FC1DE33C4CCE5D7B,
	LongClickButton_OnPointerDown_m396A66D12FFF14E5AC982CA62ADC8A75DE29821A,
	LongClickButton_OnPointerUp_m55F255A996E879C45277323F539735150523F911,
	LongClickButton_Update_m62D430D348D82E24ECD02DC67D58BEE330F77237,
	LongClickButton_DelayClick_m57566FE8544386D752B832895C7A3C424E60F152,
	LongClickButton_Reset_mDA906B766DB3C337E865915B83EB4FCA0E1223C6,
	LongClickButton_DestroyCard_mCFD62BA1BC8B1C5EBF53BE5E56C428E2AE058FD6,
	LongClickButton__ctor_m62A293FBCE550D5C9975FD1BCED8445F9D5CCBD3,
	EnemyWayPoint_get_WayPoints_mA9B0EC26719F8C9A85CE11979FA70ACAA767FC8D,
	EnemyWayPoint_set_WayPoints_m29C19EC458381493C0C989548754E56303BB6B6F,
	EnemyWayPoint_get_Instance_m9E8A4D26FF4B696112549EDA6B14E2A419990393,
	EnemyWayPoint_set_Instance_m7FCF1F5029861DE0A65F2271EB287F6CC3D0C4F6,
	EnemyWayPoint_Awake_m56E825FF651D08C2088FD8E9A804B95F603689B9,
	EnemyWayPoint__ctor_m3490E4C651E5A136BF1702E2A9E75642B9A65AFE,
	Chest_Init_mE065D87A18D700C18CA776CBAD844B41E792C41A,
	Chest_Update_m4FA8556D3034321A01625325CE1F0612CB333866,
	Chest_TakeDamage_m32038BCB9444386B50AE02229FA8B9028DBEDE36,
	Chest__ctor_m107B91B4F8435DE9E7C45BC7C0707B14BE3632D2,
	CrossDive_Init_m4814A2EF01947AB112E05F3F198160FB857476C1,
	CrossDive_Update_m23A7D345248DBA6A2E84C48C2BA4D2B26787CDC4,
	CrossDive_TakeDamage_mD319E8B592E1C76C2ED73341E85F530A1CD23F4A,
	CrossDive__ctor_mA643E63EF6E24F3330AE6441506D650376DDB96B,
	Eclipseside_Init_m9DE15A51131D42E6A01C72CE8BF80EF401100358,
	Eclipseside_Update_mBF4E50D9FC1476C35795D9AF6E37684D488B691D,
	Eclipseside_TakeDamage_m818F719D07F247CE231CAFCA1D2FF75EAB984A76,
	Eclipseside__ctor_m7CA9E4526EAF88914CED49C54D8F749479DF83C6,
	Embio_Init_m22EE30C4B7BEBF452ADABFB88BAF3D7820F20C9A,
	Embio_Update_m79A2048FC2AD6AE31D1EF609F75CE77FCA177B96,
	Embio_TakeDamage_mDF3BE2419C8EB9673B8AC5B6A0BE84507A43A105,
	Embio__ctor_m24F040E5B257FBF6A4839D473D345D35FF2E5318,
	Kana_Init_mC88B141257F094D7A4D528CC491DFB1D9BD3160F,
	Kana_Update_mB003DAF267B2758BBC811A04420512A022EF9882,
	Kana_TakeDamage_mF5BFD7BF789316E305D1D4B759CC4ECA5979E78D,
	Kana__ctor_mA72390CC4E4F7C29C8EEEB5DF61A31ED0FE76A7E,
	Shiro_Init_m5DB57855BC3CA04029B5F227B84895D4221F8055,
	Shiro_Update_mFD63634086788B962AE85CF60C820134C64121A6,
	Shiro_TakeDamage_mCC810C83FFBF232F847D07DD1F910983B9B8E6A9,
	Shiro__ctor_mFCE0E57D1815B837736164675C8247246E3C80C9,
	PopUpDamage_Start_m961A2CFD842EB20B0136E7AA58D4097AA385F977,
	PopUpDamage_Update_m267C109471D594CCFC3AEDF5F24583AE032E0480,
	PopUpDamage_SetDamageText_m20DE6CDDA6356C942C236CCD17F926C0E26EBFB7,
	PopUpDamage__ctor_m4903615A6D2049D6E46B34F6A425ABB4E393FE00,
	NULL,
	NULL,
	FirebaseManager_ClearUserData_mD89AD374A9CDD4D89E8EE4B096BFB071F205A206,
	FirebaseManager_OpenLoginPanel_m03F016EB02757294F66B14D5B05A3AC5ED36870C,
	FirebaseManager_OpenSignUpPanel_mDEB14871C0EBFFD03CE8CFDE9467FBB43D6D73B5,
	FirebaseManager_OpenMenuPanel_mB431E4ABBC4285FA5B93B850A42FA27C816E950D,
	FirebaseManager_OpenForgetPasswordPanel_m7D006EACB2B60F75E06E6D089969F894F2AEB1FF,
	FirebaseManager_Start_mC613ACB1AF5C9327DFD21634676C02D3991E5571,
	FirebaseManager_SaveStateRememberMe_m510B158BB8236151DBB6417C8F5ED16D908AFB02,
	FirebaseManager_CheckAlreadyName_m25B27E898AA4C9F8DA81920ED46EE6B052C2DDE5,
	FirebaseManager_SignUpUser_m108DBA3676562F47C9FD9C5CD17E9039BD23FA19,
	FirebaseManager_CreateUser_mA84786334174BCC8FE2DF2DDD0357E89029138D7,
	FirebaseManager_UpdateUserProfile_mD09EC38F532181435E4ED5151CC52367E43962B5,
	FirebaseManager_LoginUser_m2B5D47B80CDE60E55E793786177DDC35E5FD0F93,
	FirebaseManager_SignInUser_mB8701CF8750D17E6CA49F57E7D1456E2119BAB6E,
	FirebaseManager_WriteNewUser_m1C4B00BD9B783B7A5E4F608A0F247CFD07B0F66C,
	FirebaseManager_WriteOldUser_m488ADB3E281DD8F23C56F7BC9CC87857CEAFECFD,
	FirebaseManager_ForgetPass_m953784A0B10809BE2D5A2F27BD65951302492CE8,
	FirebaseManager_SendPasswordResetEmail_m6B355D137B2B60EF488CD2AC200035966C4E6CC1,
	FirebaseManager_GetErrorMessage_m6C1AA969EE15A97C868DC7757BB10BEBEDBEF318,
	FirebaseManager_ShowNotification_mF62AC4770A509F9C26420EECE955311813C5C3FD,
	FirebaseManager_CloseNotification_m909268520C1AA67BBB1CC394D2A6BC6E63520F43,
	FirebaseManager_InitializeFirebase_m0AB60DA55A5299AEFEEB03BAA7AAAA2CEDB11E90,
	FirebaseManager_AuthStateChanged_m918C661027778087914DAF004BDD83A730CD7F3B,
	FirebaseManager_OnDestroy_mA9EBED6EECE1364720C35962CDD7567AF2B71B0E,
	FirebaseManager_Logout_m2A7271F20B8490BB33CD79E54CFF27C4024F5F09,
	FirebaseManager_DeleteUser_m4EBA59F2A09549EE164E845BFA7C0F565501B833,
	FirebaseManager_CheckID_m403FAB3FD33179843578B306FFC2B6FEECC0E3B2,
	FirebaseManager_LateUpdate_m80D016FED347E580CBEBB8A79EDA3A63195727D7,
	FirebaseManager_ExitGame_m1184C846093F039C598268587F880B6B7FCE43D0,
	FirebaseManager_PlayAsGuest_m853322836EADD190A1F745585BEC9D31342659C3,
	FirebaseManager_TransferID_m1447677FF66D860B182C84D1187A9AB735141C75,
	FirebaseManager_TransferData_mBD424AE5151A98C7FF2E62FCA22D275306FA3AF3,
	FirebaseManager_CheckAlreadyNameIdTransfer_m5BFE3B6936231D494314BE845F42A5AF4AC949FB,
	FirebaseManager_LoadUserScoreFromDB_mD894FC790D4E61DCE6BA00C7CD7FDDAA39CD1E36,
	FirebaseManager_DownloadImage_m7181991C7853AADCE4D73A9D254FD97D898B378A,
	FirebaseManager_UploadPhotoUrl_m0B34CAE805E4FFBC822E118875EF7A0E9E5B10CC,
	FirebaseManager_RefreshPhoto_m2AC1AC744922F23EBAD47CE7F82252FCFE3568E8,
	FirebaseManager_OpenLeaderBoard_mFFE6757D86E27742C1685C757A3FBBB6FA395646,
	FirebaseManager_LoadLeaderBoardData_m06C184117B561767D286D1B7BD89173A9A4AB3CD,
	FirebaseManager__ctor_mB894832C008369C3C25D3B0DF9CDF84481C89506,
	FirebaseManager_U3CStartU3Eb__48_0_m36581C2AEC12E1C90BB72B87048D131734928B3C,
	FirebaseManager_U3CSignInUserU3Eb__55_0_mC0D0D74A9B0AC51CEA0C4944D148B7D608C0EDA1,
	FirebaseManager_U3CSendPasswordResetEmailU3Eb__59_0_m91D2331DFDE02DA0307FB03795621C211246F4A2,
	FirebaseManager_U3CDeleteUserU3Eb__67_0_mBFE0C01FAAF9354F30490CB854BF25624839A5D9,
	FirebaseManager_U3CPlayAsGuestU3Eb__71_0_m0D1455F6948BC1B608F5C3150AEEFD920DE04982,
	RankData_NewRankData_m90E3025AD61654DA9BB167577B1EAC595EE311D9,
	RankData_DownloadImage_mB3634704E17275CFFC430EA174C96A48CDFCA196,
	RankData__ctor_mCE49986E1CB806794090039EB62946C655982EF4,
	RankManager_Start_m1879556A68EA7C24A1E7F166E654327CE0A08184,
	RankManager_UpdateToDB_mA569F162B13FDE577289EDF4C6D20F0792B09F07,
	RankManager_CheckDamageOverCurrentScore_mCB8486642C343A640FA9437DE98B3B4D87616034,
	RankManager_CheckTimeSurvivedOverCurrentScore_m04A388299D839D31AAD14DF4E736989BDB46EFA0,
	RankManager_UpdateDamageDone_mD0D78C65C914F189377894AE8F83E0788D6B2683,
	RankManager_UpdateWaveSurvived_mF9400B91A4798D5BBE0AFCF5267A32977115979C,
	RankManager_UpdateTimeSurvived_m6B3EFE193E94C4FBCCA5FA99EF20AE3D7417235F,
	RankManager_LoadUserScoreFromDB_m0C1F32AE08783D1EFE74247850F9423A856FA627,
	RankManager_LoadLeaderBoardData_mEA3E6CB77B24A2D858BF2423FCB7DCD52FC032B6,
	RankManager_DownloadImage_m9D8910B3E34BE8AEB8BDC0DD2E91135FF024FED7,
	RankManager_OpenLeaderBoard_mE438584CE2BB3B1561E81F68893F7EA87AC4D54E,
	RankManager_InitializeFirebase_m93EA5CB6A0753B8C0476A92EAAC9E1F4ADFE3433,
	RankManager_AuthStateChanged_mFD4C507AB0443DD59C44AD5D2C47E795383DA6E6,
	RankManager_OnDestroy_mE0A63330921A2CD1B8992EFE5CB30DD984DA6E4C,
	RankManager__ctor_mABC316C9D683BDCF250052EF5C2FAD05EAD53377,
	AdsManager_Start_m561A6D94B810C2CBBAD6235533FEF81D3FCB93BA,
	AdsManager_ShowBannerAd_mEE21D50E8CF7E7E4C29DDDCBE56409C7B5B79783,
	AdsManager_HideBanner_m649BED280175777ACA3807ACB1B332B37E2236D7,
	AdsManager_ShowAds_m8F51286E3D51611E15459A5635394278B8DB192D,
	AdsManager_ShowMenuAds_m1210026A28F80B54E21D1D89209CB54FDE13F4AE,
	AdsManager_OnUnityAdsReady_mA76941ECD72FD15E03D0B7DBF10365BF5AD72764,
	AdsManager_OnUnityAdsDidError_m1908BC190CA0D84743BF3480F0D10B4202F9E207,
	AdsManager_OnUnityAdsDidStart_mF0AB782E7E55745E4D5A5A1EDFDB13CF64F73B15,
	AdsManager_OnUnityAdsDidFinish_mC9DD4B662622209033D8314AD626FC82ACA736F0,
	AdsManager__ctor_m1324EE3D9841045940C44D5E0379750DBE0C0A53,
	AnimationManager_get_Instance_m6006600F80794DD76BB6AC3235F42B08C76DFC21,
	AnimationManager_set_Instance_m129B4A95F6E1A05D296C65F5FF4654A34E191C1A,
	AnimationManager_Awake_m627DE07938651E02F0A44463872756F99CEBE1A0,
	AnimationManager_MapUp_m0671E4730102F8A1A16189D0519CD1DECF12FB83,
	AnimationManager_MapDown_m888E8697FF4886EF0F124BE1DBF6E60FF2B3018E,
	AnimationManager_FireworkItemClick_m9D08BDDF76F297BA01437D256E0A8AFC77C53037,
	AnimationManager_FireworkItemClicked_m2F47BFF9156FAECAF864BC25CBF9E85195419EC9,
	AnimationManager_CookieItemClick_mA61DD11F51A285F81B5B49CDC4873B7FEC43AA68,
	AnimationManager_CookieItemClicked_m784832DFA3744E37046384144043421A354A969E,
	AnimationManager_CandyItemClick_mC5637379CA1202E055174E2C0B3EF094E02E6BEF,
	AnimationManager_CandyItemClicked_m2E8C80CC584738CC7C27DE953214E38E2B34B38C,
	AnimationManager_ScreenShotClick_m3898AC476C591FB1745516AFDC8EB22502279752,
	AnimationManager_ScreenShotClicked_m930B9D597CC234711BD297210DA32E7F26E23698,
	AnimationManager_LeaderBoardClick_mEFE537E4AE47C191A838A194165A325D1D3CCD64,
	AnimationManager_LeaderBoardClicked_mBEFB624ED531785691AB117629A43766714FBBD9,
	AnimationManager_NoticeTimerIn_mC8DC954F73922F7206D8A90992002523045F427C,
	AnimationManager_NoticeTimerOut_m6221B0A1F27D0377F1B16A1DEE3443E91F444ADA,
	AnimationManager_NoticeIn_m6CBA2BFB6C0BF4D1A111DAECD8101CC088255948,
	AnimationManager_NoticeOut_m21DB4E8741E8B7A9467F17B6057AD243692D324B,
	AnimationManager__ctor_m0C4622326DC614221E503860BBCC73F91BFB9F20,
	HeroUI_Awake_m346912E3285BAE99C967A6AFCD65747A22734273,
	HeroUI_SetTarget_mE184F5ACE462C0BEC4348DA2AC654926CFFC401F,
	HeroUI_HideHeroUI_mCD64F46AC83B9BF1FEBBB53D306002EA79703119,
	HeroUI_ChangeButtonColor_m57C3A34E9EFD62ECB84BB942B1369D742BD003B3,
	HeroUI_Upgrade_m2200F32D9546740ACE0947DB0A1273DC4E6178FB,
	HeroUI_UpdateSlider_m6BC29E2EA6BE802981C4FDC3C4ABB6FD32CF254A,
	HeroUI_UpgradeHero_m27EE9516C460948699817C75002BAAAAFB525DFE,
	HeroUI_UpdateLevel_mF08433D72311D0D92ED645915129D17A643C67EB,
	HeroUI_EvoHero_mE05637FF195C442F64ADC9FADE12043944EA0BDF,
	HeroUI_WaitForUpdateUi_mB163302A7AC7999CDB49AD6C3F1E8D1891C6F1A4,
	HeroUI_SellHero_mD120DEC2BCD2425BF46A97E768B4B161389296CB,
	HeroUI_CheckHeroType_mD7BB6E270BA8AEEFCE513EF1792E95B5A2703ABF,
	HeroUI_ShowAttackRange_m8D2084F32ACD2685C52EAE3C749F3AE3D9F945E7,
	HeroUI__ctor_m6EAC57B737C83863D29DF818694B7AA43AF62F94,
	HeroUI_U3CSetTargetU3Eb__19_0_m1F49EFF4785EA6875D6F430195C5647A22F490D4,
	HeroUI_U3CEvoHeroU3Eb__26_0_m890A8A5597D9EAAC6DAF8ED08FE6C8F3DF37798B,
	ItemManager_get_Instance_mC07F08CD08726E948EF32F708CA18982076190B6,
	ItemManager_set_Instance_m91F8EBA1EDB2F3C0C38B96B94567147A357531FC,
	ItemManager_add_OnCandyUsed_mE11A67279CF917C592370DC3E5F19F43624F4B82,
	ItemManager_remove_OnCandyUsed_m095FF8D6FEB3DE6137B118DB77A0E92606FFEF72,
	ItemManager_add_OnCookieUsed_m28C6881C57300A2FB437BFF027FF42500973E98E,
	ItemManager_remove_OnCookieUsed_mAC2B81471A8D57878164BE8A6905EB88B9AC9CF3,
	ItemManager_Awake_m38A9CB7617956210243C27F71C25FE547791586C,
	ItemManager_FireWork_m3633E8176747DF6245F055249A1C8F6E0C0684AC,
	ItemManager_Cookie_m016B249CE01E73A1E38AEA6E590D24485BFBE547,
	ItemManager_ActiveCookie30s_m2159094A265F281AD920E8F646A10FF9F2B429A3,
	ItemManager_Candy_mC7F7ECAB66A3949773BD0ECEC85C9199572FF69C,
	ItemManager_ActiveCandy30s_mD5662C359943A8F22218D041056106C2FAFB2AF6,
	ItemManager__ctor_mB4E542587417D4CED86516E00930C40F85219460,
	MainMenuManager_StartGame_m2211BA6F6E02EDDCD05A6319868508E6E1F8C96B,
	MainMenuManager_OpenUploadPhoto_mAB911DA65BD02BFF796FC696813B2EDDE1269E6C,
	MainMenuManager__ctor_m39846598ADB8889025A11166F08497A1E99D13BF,
	TimeManager_get_Instance_m19259A61D1B020A332EC31181A9E667EF907151A,
	TimeManager_set_Instance_mB699CF404EFA8C02A254633270C30C6E0EA0EFF6,
	TimeManager_Awake_mFBAAE1A728FE924382001A6378AE45F9F592999D,
	TimeManager_Start_m266A4C8445E0E6B1356A27AFF828649C364A62D3,
	TimeManager_Update_m2FE11D93837B4183FDA835BBBACACE91FEFC9B3A,
	TimeManager_Pause_m9479102F27E8F107AFEF234A8FE989E5BE6F96D0,
	TimeManager_Resume_mFA56BDC4CEE7EBBE2A7812CB150FED8606D18D95,
	TimeManager_GameOver_m2C2A1E99AE84267A588678B82ED69C806A6D8B94,
	TimeManager_WaitForPanel_m68280F25263044850C9A68C113FDEC174C3D7625,
	TimeManager_WaitForUnPause_mB05F71F812DAD1687CFF92931741779EA69EB465,
	TimeManager_AdsReward_mDC1E38660B356F05B0FCFD1A4C329B5675F59E93,
	TimeManager__ctor_m29682988877F236DB723B6DA48DA89855B9934A7,
	MapGenerate_Start_m87A416E3BD8D9823C663C02B505EBEAB634B5BAB,
	MapGenerate_getTopEdgeAreas_m99A213FEAACEAB2E074D44484E1EC3B47CD5DE22,
	MapGenerate_getBottomEdgeAreas_m4275F34434287FC2E2BD013A524AC33258712FBE,
	MapGenerate_GenerateMap_mD2373E341C842CE69C9F57746801194E8BC0718A,
	MapGenerate__ctor_m8EF35BCBED274A480CD54BF60D4B67CC3BE770DD,
	SafeAreaSetter_Start_m83780A11588277507500E26788A2EA69E09FF392,
	SafeAreaSetter_ApplySafeArea_m4F4F38103E2C3689B92117BF87F0E7559B48CB25,
	SafeAreaSetter_Update_mF64C75E3889BDD924F5886A3AFF934BC395A5E01,
	SafeAreaSetter__ctor_mA93E55DED3B5E17188E4D3CC093573F5E1BC696B,
	ChainSkill_get_Instance_mFF57A1E3160BADBC15485C65C3459453C8388C8E,
	ChainSkill_set_Instance_m463EFBE4C3064E87C11ECFB2FBD865ADF6E335A5,
	ChainSkill_Awake_mC5E9C708E5F6FCA441E0790222686142EC016586,
	ChainSkill_Start_mC8744523813A83A5A94B9B7699588D8798CE64B5,
	ChainSkill_GetCompGameOver_mEB67BD090A0FA99296477DE170E104A3B1E95C99,
	ChainSkill_CheckSkillChainActive_m123F7C619409D75D13F8E7BA55EA763832AFC328,
	ChainSkill_AddHeroToField_mC4E2F4D3A707D7483A35840CCEA93D7A95AE5C42,
	ChainSkill_RemoveHeroInField_mE4877B0CA855F3D4E2F8B7E9160A06D40EF9B317,
	ChainSkill_SetBuff_m3D524A1E8D7D21AC8C6BA9C2D4BD93AFC53B6B6E,
	ChainSkill_RemoveBuff_m86021B3D4839A440243460AAA9362C7FD1B1C480,
	ChainSkill_FindBuild_m413B5AD731302D27C5D979F21B8B6EC6FE0313F6,
	ChainSkill_FindElementBuild_m2348A7F0B91B94352F188F89AEB72CFA296CAD76,
	ChainSkill__ctor_m956A177A4F7FECBCB2AB5FD95BC7D286B636CACF,
	ChatController_OnEnable_mBC3BFC05A1D47069DFA58A4F0E39CFF8FAD44FF2,
	ChatController_OnDisable_m268A7488A3FDEB850FC3BC91A0BBDA767D42876B,
	ChatController_AddToChatOutput_m43856EEA133E04C24701A2616E7F10D7FFA68371,
	ChatController__ctor_m3B66A5F749B457D865E8BDA1DE481C8CF1158026,
	DropdownSample_OnButtonClick_m97641451A2EAC4FC6682897A948A37FDD6BF9EA6,
	DropdownSample__ctor_m87664E3DC75A1A9AA19FEE6ED6B993AB1E50E2B9,
	EnvMapAnimator_Awake_mFFC04BC5320F83CA8C45FF36A11BF43AC17C6B93,
	EnvMapAnimator_Start_mC0D348CAB0F0DC920EF3D3A008688B533F66D1BE,
	EnvMapAnimator__ctor_mC151D673A394E2E7CEA8774C4004985028C5C3EC,
	TMP_DigitValidator_Validate_m5D303EB8CD6E9E7D526D633BA1021884051FE226,
	TMP_DigitValidator__ctor_m1E838567EF38170662F1BAF52A847CC2C258333E,
	TMP_PhoneNumberValidator_Validate_mF0E90A277E9E91BC213DD02DC60088D03C9436B1,
	TMP_PhoneNumberValidator__ctor_mB3C36CAAE3B52554C44A1D19194F0176B5A8EED3,
	TMP_TextEventHandler_get_onCharacterSelection_m90C39320C726E8E542D91F4BBD690697349F5385,
	TMP_TextEventHandler_set_onCharacterSelection_m5ED7658EB101C6740A921FA150DE18C443BDA0C4,
	TMP_TextEventHandler_get_onSpriteSelection_m0E645AE1DFE19B011A3319474D0CF0DA612C8B7B,
	TMP_TextEventHandler_set_onSpriteSelection_m4AFC6772C3357218956A5D33B3CD19F3AAF39788,
	TMP_TextEventHandler_get_onWordSelection_mA42B89A37810FB659FCFA8539339A3BB8037203A,
	TMP_TextEventHandler_set_onWordSelection_m4A839FAFFABAFECD073B82BA8826E1CD033C0076,
	TMP_TextEventHandler_get_onLineSelection_mB701B6C713AD4EFC61E1B30A564EE54ADE31F58D,
	TMP_TextEventHandler_set_onLineSelection_m0B2337598350E51D0A17B8FCB3AAA533F312F3DA,
	TMP_TextEventHandler_get_onLinkSelection_mF5C3875D661F5B1E3712566FE16D332EA37D15BB,
	TMP_TextEventHandler_set_onLinkSelection_m200566EDEB2C9299647F3EEAC588B51818D360A5,
	TMP_TextEventHandler_Awake_m43EB03A4A6776A624F79457EC49E78E7B5BA1C70,
	TMP_TextEventHandler_LateUpdate_mE1D989C40DA8E54E116E3C60217DFCAADD6FDE11,
	TMP_TextEventHandler_OnPointerEnter_m8FC88F25858B24CE68BE80C727A3F0227A8EE5AC,
	TMP_TextEventHandler_OnPointerExit_mBB2A74D55741F631A678A5D6997D40247FE75D44,
	TMP_TextEventHandler_SendOnCharacterSelection_m78983D3590F1B0C242BEAB0A11FDBDABDD1814EC,
	TMP_TextEventHandler_SendOnSpriteSelection_m10C257A74F121B95E7077F7E488FBC52380A6C53,
	TMP_TextEventHandler_SendOnWordSelection_m95A4E5A00E339E5B8BA9AA63B98DB3E81C8B8F66,
	TMP_TextEventHandler_SendOnLineSelection_mE85BA6ECE1188B666FD36839B8970C3E0130BC43,
	TMP_TextEventHandler_SendOnLinkSelection_m299E6620DFD835C8258A3F48B4EA076C304E9B77,
	TMP_TextEventHandler__ctor_m6345DC1CEA2E4209E928AD1E61C3ACA4227DD9B8,
	Benchmark01_Start_mE7E5146B0D8D926CC410CAA48F3B617A0A7D955C,
	Benchmark01__ctor_mB92568AA7A9E13B92315B6270FCA23584A7D0F7F,
	Benchmark01_UGUI_Start_m2F8F9E1798943DA8086A7C7E73BA9D7C67482BD4,
	Benchmark01_UGUI__ctor_mACFE8D997EAB50FDBD7F671C1A25A892D9F78376,
	Benchmark02_Start_m315C0DF3A9AC66B4F5802FDAED056E7E5F3D2046,
	Benchmark02__ctor_m526B38D3B7E75905208E4E57B168D8AC1900F4E5,
	Benchmark03_Awake_mD23429C1EE428EEF4ED9A753385BF194BAA41A77,
	Benchmark03_Start_m19E638DED29CB7F96362F3C346DC47217C4AF516,
	Benchmark03__ctor_m89F9102259BE1694EC418E9023DC563F3999B0BE,
	Benchmark04_Start_mEABD467C12547066E33359FDC433E8B5BAD43DB9,
	Benchmark04__ctor_m5015375E0C8D19060CB5DE14CBF4AC02DDD70E7C,
	CameraController_Awake_m420393892377B9703EC97764B34E32890FC5283E,
	CameraController_Start_m32D90EE7232BE6DE08D224F824F8EB9571655610,
	CameraController_LateUpdate_m6D81DEBA4E8B443CF2AD8288F0E76E3A6B3B5373,
	CameraController_GetPlayerInput_m6695FE20CFC691585A6AC279EDB338EC9DD13FE3,
	CameraController__ctor_m09187FB27B590118043D4DC7B89B93164124C124,
	ObjectSpin_Awake_mC1EB9630B6D3BAE645D3DD79C264F71F7B18A1AA,
	ObjectSpin_Update_mD39DCBA0789DC0116037C442F0BA1EE6752E36D3,
	ObjectSpin__ctor_m1827B9648659746252026432DFED907AEC6007FC,
	ShaderPropAnimator_Awake_mE04C66A41CA53AB73733E7D2CCD21B30A360C5A8,
	ShaderPropAnimator_Start_mC5AC59C59AF2F71E0CF65F11ACD787488140EDD2,
	ShaderPropAnimator_AnimateProperties_mAF49CD157AD41377CE00AA10F0C06C8BF5AA0469,
	ShaderPropAnimator__ctor_mAA626BC8AEEB00C5AE362FE8690D3F2200CE6E64,
	SimpleScript_Start_m75CD9CEDCAFA9A991753478D7C28407C7329FB8F,
	SimpleScript_Update_mE8C3930DCC1767C2DF59B622FABD188E6FB91BE5,
	SimpleScript__ctor_mEB370A69544227EF04C48C604A28502ADAA73697,
	SkewTextExample_Awake_m6FBA7E7DC9AFDD3099F0D0B9CDE91574551105DB,
	SkewTextExample_Start_m536F82F3A5D229332694688C59F396E07F88153F,
	SkewTextExample_CopyAnimationCurve_m555177255F5828DBC7E677ED06F7EFFC052886B3,
	SkewTextExample_WarpText_m0E0C46988600673F0E5DFA3133534DC6CA5950D3,
	SkewTextExample__ctor_m945059906734DD38CF860CD32B3E07635D0E1F86,
	TMP_ExampleScript_01_Awake_m381EF5B50E1D012B8CA1883DEFF604EEAE6D95F4,
	TMP_ExampleScript_01_Update_m31611F22A608784F164A24444195B33B714567FB,
	TMP_ExampleScript_01__ctor_m3641F2E0D25B6666CE77773A4A493C4C4D3D3A12,
	TMP_FrameRateCounter_Awake_m958B668086DB4A38D9C56E3F8C2DCCB6FF11FAA3,
	TMP_FrameRateCounter_Start_mC642CA714D0FB8482D2AC6192D976DA179EE3720,
	TMP_FrameRateCounter_Update_m5E41B55573D86C3D345BFE11C489B00229BCEE21,
	TMP_FrameRateCounter_Set_FrameCounter_Position_mA72ECDE464E3290E4F533491FC8113B8D4068BE1,
	TMP_FrameRateCounter__ctor_mC79D5BF3FAE2BB7D22D9955A75E3483725BA619C,
	TMP_TextEventCheck_OnEnable_mE6D5125EEE0720E6F414978A496066E7CF1592DF,
	TMP_TextEventCheck_OnDisable_m94F087C259890C48D4F5464ABA4CD1D0E5863A9A,
	TMP_TextEventCheck_OnCharacterSelection_mDA044F0808D61A99CDA374075943CEB1C92C253D,
	TMP_TextEventCheck_OnSpriteSelection_mE8FFA550F38F5447CA37205698A30A41ADE901E0,
	TMP_TextEventCheck_OnWordSelection_m6037166D18A938678A2B06F28A5DCB3E09FAC61B,
	TMP_TextEventCheck_OnLineSelection_mD2BAA6C8ABD61F0442A40C5448FA7774D782C363,
	TMP_TextEventCheck_OnLinkSelection_m184B11D5E35AC4EA7CDCE3340AB9FEE3C14BF41C,
	TMP_TextEventCheck__ctor_mA5E82EE7CE8F8836FC6CF3823CBC7356005B898B,
	TMP_TextInfoDebugTool__ctor_mF6AA30660FBD4CE708B6147833853498993CB9EE,
	TMP_TextSelector_A_Awake_mD7252A5075E30E3BF9BEF4353F7AA2A9203FC943,
	TMP_TextSelector_A_LateUpdate_mDBBC09726332EDDEAF7C30AB6C08FB33261F79FD,
	TMP_TextSelector_A_OnPointerEnter_mC19B85FB5B4E6EB47832C39F0115A513B85060D0,
	TMP_TextSelector_A_OnPointerExit_mE51C850F54B18A5C96E3BE015DFBB0F079E5AE66,
	TMP_TextSelector_A__ctor_mEB83D3952B32CEE9A871EC60E3AE9B79048302BF,
	TMP_TextSelector_B_Awake_m7E413A43C54DF9E0FE70C4E69FC682391B47205A,
	TMP_TextSelector_B_OnEnable_m0828D13E2D407B90038442228D54FB0D7B3D29FB,
	TMP_TextSelector_B_OnDisable_m2B559A85B52C8CAFC7350CC7B4F8E5BC773EF781,
	TMP_TextSelector_B_ON_TEXT_CHANGED_m1597DBE7C7EBE7CFA4DC395897A4779387B59910,
	TMP_TextSelector_B_LateUpdate_m8BA10E368C7F3483E9EC05589BBE45D7EFC75691,
	TMP_TextSelector_B_OnPointerEnter_m0A0064632E4C0E0ADCD4358AD5BC168BFB74AC4D,
	TMP_TextSelector_B_OnPointerExit_m667659102B5B17FBAF56593B7034E5EC1C48D43F,
	TMP_TextSelector_B_OnPointerClick_m8DB2D22EE2F4D3965115791C81F985D82021469F,
	TMP_TextSelector_B_OnPointerUp_m303500BE309B56F3ADCE8B461CEC50C8D5ED70BC,
	TMP_TextSelector_B_RestoreCachedVertexAttributes_m3D637CF2C6CA663922EBE56FFD672BE582E9F1F2,
	TMP_TextSelector_B__ctor_mE654F9F1570570C4BACDA79640B8DB3033D91C33,
	TMP_UiFrameRateCounter_Awake_m83DBE22B6CC551BE5E385048B92067679716179E,
	TMP_UiFrameRateCounter_Start_m59DA342A492C9EF8AD5C4512753211BF3796C944,
	TMP_UiFrameRateCounter_Update_m28FC233C475AA15A3BE399BF9345977089997749,
	TMP_UiFrameRateCounter_Set_FrameCounter_Position_m2025933902F312C0EC4B6A864196A8BA8D545647,
	TMP_UiFrameRateCounter__ctor_m5774BF4B9389770FC34991095557B24417600F84,
	TMPro_InstructionOverlay_Awake_m2444EA8749D72BCEA4DC30A328E3745AB19062EC,
	TMPro_InstructionOverlay_Set_FrameCounter_Position_m62B897E4DB2D6B1936833EC54D9C246D68D50EEB,
	TMPro_InstructionOverlay__ctor_m7AB5B851B4BFB07547E460D6B7B9D969DEB4A7CF,
	TeleType_Awake_m099FCB202F2A8299B133DC56ECB9D01A16C08DFE,
	TeleType_Start_m72DE9DE597F4FA0B1CA02115CBC1E74EB53F63F7,
	TeleType__ctor_m12A79B34F66CBFDCA8F582F0689D1731586B90AF,
	TextConsoleSimulator_Awake_mA51EE182A528CCA5CAEA8DBE8AD30E43FDFAE7B0,
	TextConsoleSimulator_Start_m429701BE4C9A52AA6B257172A4D85D58E091E7DA,
	TextConsoleSimulator_OnEnable_m3397FBCDA8D9DA264D2149288BE4DCF63368AB50,
	TextConsoleSimulator_OnDisable_m43DF869E864789E215873192ECFCDC51ABA79712,
	TextConsoleSimulator_ON_TEXT_CHANGED_m51F06EE5DD9B32FEFB529743F209C6E6C41BE3B8,
	TextConsoleSimulator_RevealCharacters_mE8E415644F7BD2056D0809F7BB0FA9A2F9FE8534,
	TextConsoleSimulator_RevealWords_mFDBC863D30BC63ADCF2860F303AF252E27D0F4F4,
	TextConsoleSimulator__ctor_m4719FB9D4D89F37234757D93876DF0193E8E2848,
	TextMeshProFloatingText_Awake_m679E597FF18E192C1FBD263D0E5ECEA392412046,
	TextMeshProFloatingText_Start_m68E511DEEDA883FE0F0999B329451F3A8A7269B1,
	TextMeshProFloatingText_DisplayTextMeshProFloatingText_m3C381B8A53C58CF001D7A9212B8BDA6F368CC2C7,
	TextMeshProFloatingText_DisplayTextMeshFloatingText_m22691E6EA41B7FCF782B03954865D8E3B890E4DF,
	TextMeshProFloatingText__ctor_m968E2691E21A93C010CF8205BC3666ADF712457E,
	TextMeshProFloatingText__cctor_m5FF3949FBDB0FD13908FB7349F64DDB3012512B8,
	TextMeshSpawner_Awake_mDF8CCC9C6B7380D6FA027263CDC3BC00EF75AEFB,
	TextMeshSpawner_Start_m7CC21883CA786A846A44921D2E37C201C25439BA,
	TextMeshSpawner__ctor_m1255777673BE591F62B4FC55EB999DBD7A7CB5A1,
	VertexColorCycler_Awake_m84B4548078500DA811F3ADFF66186373BE8EDABC,
	VertexColorCycler_Start_mC3FF90808EDD6A02F08375E909254778D5268B66,
	VertexColorCycler_AnimateVertexColors_m6960F777E4876DFCA726BCAE7A8163850D58FA42,
	VertexColorCycler__ctor_m09990A8066C8A957A96CDBEBDA980399283B45E9,
	VertexJitter_Awake_m9C5586A35BD9C928455D6479C93C1CE095447D8F,
	VertexJitter_OnEnable_m97ED60DBD350C72D1436ADFB8009A6F33A78C825,
	VertexJitter_OnDisable_mF8D32D6E02E41A73C3E958FB6EE5D1D659D2A846,
	VertexJitter_Start_m8A0FED7ED16F90DBF287E09BFCBD7B26E07DBF97,
	VertexJitter_ON_TEXT_CHANGED_mC7AB0113F6823D4FF415A096314B55D9C1BE9549,
	VertexJitter_AnimateVertexColors_mECAE037FC0CBA52CAC71C0B61E88829FF18BCC16,
	VertexJitter__ctor_m550C9169D6FCD6F60D6AABCB8B4955DF58A12DCE,
	VertexShakeA_Awake_m005C6F9EE8A8FD816CD3A736D6AF199CD2B615A2,
	VertexShakeA_OnEnable_m9F60F3951A7D0DF4201A0409F0ADC05243D324EA,
	VertexShakeA_OnDisable_m59B86895C03B278B2E634A7C68CC942344A8D2D2,
	VertexShakeA_Start_m3E623C28F873F54F4AC7579433C7C50B2A3319DE,
	VertexShakeA_ON_TEXT_CHANGED_m7AD31F3239351E0916E4D02148F46A80DC148D7E,
	VertexShakeA_AnimateVertexColors_mC451E2732A4E3E90E2553811AD75903EEF974599,
	VertexShakeA__ctor_m7E6FD1700BD08616532AF22B3B489A18B88DFB62,
	VertexShakeB_Awake_m99C9A0474DBFE462DC88C898F958C1805376F9D5,
	VertexShakeB_OnEnable_m64299258797D745CDFE7F3CBEC4708BDBC7C3971,
	VertexShakeB_OnDisable_m07D520A8D7BCD8D188CE9F5CC7845F47D5AD6EF4,
	VertexShakeB_Start_m9642281210FA5F701A324B78850331E4638B2DD1,
	VertexShakeB_ON_TEXT_CHANGED_m5650D69D258D0EEF35AF390D6D5086B327B308A5,
	VertexShakeB_AnimateVertexColors_mDF7A7E7028361D288DF588D9301541E4FA1EFA87,
	VertexShakeB__ctor_m605A778C36B506B5763A1AE17B01F8DCCBCD51EC,
	VertexZoom_Awake_m1B5D386B98CF2EB05A8155B238D6F6E8275D181C,
	VertexZoom_OnEnable_m7F980FC038FC2534C428A5FD33E3E13AEAEB4EEC,
	VertexZoom_OnDisable_m880C38B62B4BB05AF49F12F75B83FFA2F0519884,
	VertexZoom_Start_m10A182DCEF8D430DADAFBFFA3F04171F8E60C84C,
	VertexZoom_ON_TEXT_CHANGED_mB696E443C61D2212AC93A7615AA58106DD25250F,
	VertexZoom_AnimateVertexColors_mEFBA6C940DFECB485236C247358A32011A7963ED,
	VertexZoom__ctor_mE7B36F2D3EC8EF397AB0AD7A19E988C06927A3B2,
	WarpTextExample_Awake_mEDB072A485F3F37270FC736E1A201624D696B4B0,
	WarpTextExample_Start_m36448AEA494658D7C129C3B71BF3A64CC4DFEDC4,
	WarpTextExample_CopyAnimationCurve_m744026E638662DA48AC81ED21E0589E3E4E05FAB,
	WarpTextExample_WarpText_mA29C98CF3B92F253C2DD2BADFC76618F8324BEF2,
	WarpTextExample__ctor_m62299DFDB704027267321007E16DB87D93D0F099,
	Area_get_CurrentHeroAmount_mF7EE46F068C4AF7A0C0EE036CF6853C75337B957,
	Area_set_CurrentHeroAmount_m7D64716F3C5D20FC426581D30EED068869264294,
	Area_get_CurrentSubHeroAmount_mB1AE1AEACA136C7E29DD9855DCF10ACC8B7C3D66,
	Area_set_CurrentSubHeroAmount_m3E01B5026F9398B8D8B3012ED50B3BAE83B620E6,
	Area_get_moreThanHeroLimit_mDB9A2215AD1ABF9BB60CCBFC30DC3816CDF736D2,
	Area_get_moreThanSubHeroLimit_mAD4ED3CE6380F3E01555DC4CFF6A36DBF27CCF3E,
	Area_Start_m737D962CA274329A6A24C6E4EC0A41CCE87DDC31,
	Area_GetPosition_m0DCF0740581DF32E8F70FD8F873EA94E4574A48D,
	Area_GetEvoPosition_mD3B273FE99D8AC70F52BC0A05B3651D5658577C2,
	Area_BuildHero_m114CE898FD89E055F3BDB6B11402915B15E252F3,
	Area_UpgradeHero_m4BA42283AF51359CCEF2D273974E66FDAA111196,
	Area_EvoHero_m0D5410A9FF26B54AF75E8B2912EC4F436CED9C41,
	Area_SellHero_m9900D32E6F213DA261F2236921ABC3D4A2D53CA9,
	Area_OnMouseDown_m46157C8A055549E5354058828A86CA41D798B875,
	Area__ctor_mD2D681B0DD7E6A0FE5C9A5D587C11DE5A12DFB8F,
	NULL,
	JSONNode_get_Item_m9B4DBE85D7742250EF918F4450E3CE020AF7C807,
	JSONNode_set_Item_m62DFE58028E55800BF80EFE6E0CE4CDE6DF00CA7,
	JSONNode_get_Item_mB39E17FF10DD6C0D91FCF1EF485A73F434B34F1D,
	JSONNode_set_Item_m9F8D28C00CA46F8C0BD5E43500AEAFC3A1999E07,
	JSONNode_get_Value_m8F31142225AAE4DDEC7ACB74B7FFB30A6DD672F1,
	JSONNode_set_Value_mA046548FC6B7DCBDA4964DB05303EBA0CF2A29CA,
	JSONNode_get_Count_m6FD5676FECC6B26A6384D8294C213C4B032EA269,
	JSONNode_get_IsNumber_m682272D381670DCFA66BD9EE3C0CE3CB161E3AAD,
	JSONNode_get_IsString_mE0D6737C47977364E9F1620CEA98454EAFAEB196,
	JSONNode_get_IsBoolean_mA575C54AD05577A9F45E0F55355C0B40F60BFA24,
	JSONNode_get_IsNull_m85D1B73ABCCE95314768A4E690D941B82D08CD58,
	JSONNode_get_IsArray_m54719E46991451D5C8750D8265D0C6F91EEE97D7,
	JSONNode_get_IsObject_mC9E7B94CB90443629157D7C2AB5ED83510DD0BD1,
	JSONNode_get_Inline_m6791C62A74ACCEA90470A7EC177FA3F12F744F09,
	JSONNode_set_Inline_m5C5C9E182CE521A831EBD2D708568297601AD835,
	JSONNode_Add_m10F03DC3FD017B22B831388D39FF735D5AFE96A4,
	JSONNode_Add_m7DCFC7266181DE46B90ECC7C5DD7D5B7618F9DE2,
	JSONNode_Remove_m49D627BAE273E902C8EB8345BDD7B33E945C2E47,
	JSONNode_Remove_m3B3259E69D33121E4974FE1ACFAEBED4B034C053,
	JSONNode_Remove_mD1FE270ABF87BCD7D09AAA22286AB3C315BAA0EA,
	JSONNode_get_Children_mB9A68101166B1C5176311D2EC1ABCC832248FDFF,
	JSONNode_get_DeepChildren_m3E3CE542A4856D0BD557B7E121B4CAA999C8110C,
	JSONNode_ToString_m9E23A359C8B7209DEED6B81DA27C8D0619F9CE98,
	JSONNode_ToString_m9B8D0351B9B14D65ABF1090E6D07E0C69DFB84C3,
	NULL,
	NULL,
	JSONNode_get_Linq_m568F8D242CB58C62C9D8230A0C1F684D70573268,
	JSONNode_get_Keys_m6C1FC7F6C35656362327B486F0426273234584E2,
	JSONNode_get_Values_m1097622D87EFAC54681A9A9796E563DA142D7677,
	JSONNode_get_AsDouble_m0DE7D5E7712FB59B32C1C570EBBF85D597B98656,
	JSONNode_set_AsDouble_mBA8F81DF080D29E60DDE8235663BAFFA688736D5,
	JSONNode_get_AsInt_m743FC99FEF2DA80A8C6CB71137A5FFD28084E0FA,
	JSONNode_set_AsInt_m7CA6313AD31E9E08FEB0B6F8B92AD9A3882D9B75,
	JSONNode_get_AsFloat_m4B2A24C67F4FBF872DEA6360719D854AE1AD1FBB,
	JSONNode_set_AsFloat_m624BDD6CAF17D1709BACFDF3554F2AE11FDD22D1,
	JSONNode_get_AsBool_m15160B79EBEA51E7A2C7C7A23B3540A60434B36D,
	JSONNode_set_AsBool_m0878AF783E25077E17850DD1B4522B17FF08770F,
	JSONNode_get_AsArray_m10801C5609C0C024480B49DFA03A4FB16A4E6829,
	JSONNode_get_AsObject_mDE74F42234B130BA44AD17DF9FFC64A2D8DFCD46,
	JSONNode_op_Implicit_m3938223B519495895A5B4E53D60789BB2D4620F2,
	JSONNode_op_Implicit_m110AF33FB2CEF68FF905E93F656AF02222554668,
	JSONNode_op_Implicit_m66EA9B62BBFD5A306C2B95703443F1960338BA01,
	JSONNode_op_Implicit_m98778303F0AD762A7C4FE928D82352A1F8276EBE,
	JSONNode_op_Implicit_m3780006769998A253BE0F8068E68BD88AC692112,
	JSONNode_op_Implicit_mA90C42C0D957F5F284857AE91907D69477331166,
	JSONNode_op_Implicit_mDAC018F58F31AB6333A9852BD0B038152063B3F2,
	JSONNode_op_Implicit_m10A6B0627DE9BC247B0B39554FEA1CD37CA7988C,
	JSONNode_op_Implicit_mAF077674B97BAF8FD4EB941A63B84D26F6E08EDC,
	JSONNode_op_Implicit_mE4078D38F919268986EF017D4A9EA55FD8CBA826,
	JSONNode_op_Implicit_m92A46C2D66615E97C21041238C09386EDCF0A266,
	JSONNode_op_Equality_mA320F56360F44724C465167318C8FC38F0DCA38F,
	JSONNode_op_Inequality_m00AE17CECA317ECC98958F027D0545C831DA24FE,
	JSONNode_Equals_mC1598F5265D88ECAA3040C537A28FC2DC878BBFF,
	JSONNode_GetHashCode_mF900BB7E50D965906CDBC7210B9097CDE6B89834,
	JSONNode_get_EscapeBuilder_m87ACD2EE75FD74E9E7B67BBB66FCDB0643749628,
	JSONNode_Escape_m5C3A578A1281224A7BDF595470FA706CBC597A68,
	JSONNode_ParseElement_mC27D1FD482D97FE1550AAFC625379A7BD8DB1895,
	JSONNode_Parse_m747C7ACB3E8F42CB32651EAB1506047F02BF4076,
	NULL,
	JSONNode_SaveToBinaryStream_m5CD58A850F95A069843BC02BED9B7190D4CE6F73,
	JSONNode_SaveToCompressedStream_m939EB2E9267698F54AD64849D96E4ADFCCF5A00A,
	JSONNode_SaveToCompressedFile_m66A1234959DDCEE44D9000255F86A8FD32EC87BC,
	JSONNode_SaveToCompressedBase64_mF1BB008CE575D288A986676FD23806E62C98D539,
	JSONNode_SaveToBinaryFile_m4C3B67B465C870818ED92E4FC14F9B9527734EC1,
	JSONNode_SaveToBinaryBase64_m31110029F0029D7416604433C849EBF33A7F12B5,
	JSONNode_DeserializeBinary_mE398A9C5CB421A0CCC20A18486D83964E36DAE45,
	JSONNode_LoadFromCompressedFile_m50BB9E1D951C7444EFE1FE570894E5A92BA35275,
	JSONNode_LoadFromCompressedStream_m6D59AB50712DF46BE7D37DB5EDB8EDD7C0B6D04D,
	JSONNode_LoadFromCompressedBase64_mBC74C1DE9C65EF47BA738413E2140F1370A9F433,
	JSONNode_LoadFromBinaryStream_m540612C537938B1403281538E5B33BCFAFF29B46,
	JSONNode_LoadFromBinaryFile_m47D1B956E15231FD6537A96C09B317F821374B1E,
	JSONNode_LoadFromBinaryBase64_m91E331970C76EE0AE16B3FFFE8535321EADE65E0,
	JSONNode_GetContainer_m8A2362DAE7D92FC4A7977415F5618EBE892DE819,
	JSONNode_op_Implicit_m7F01CA65EDCA916BD0BFB9BFEC73444FB26D59FD,
	JSONNode_op_Implicit_mDD9DF895CC3317F2C6D8E325E6133C1FE0954864,
	JSONNode_op_Implicit_mDF1EE24EF1759B2C134B0804A3497B2BFAF0A348,
	JSONNode_op_Implicit_m3B87FAAC3847B6EDAF525A9240A6AAD7489219B3,
	JSONNode_op_Implicit_m4B12356D2C135933B13ACB20AAA0BCA93221A308,
	JSONNode_op_Implicit_m880C05AE8E8F125C0FEC37BE1C32F5D2A8D32B70,
	JSONNode_op_Implicit_m50E48009BD81DB50555DADF99F5C424EAC0007FB,
	JSONNode_op_Implicit_m94957CEB32B45139355EE8C3D85B4C9DDA107484,
	JSONNode_op_Implicit_m01E7C3C12602B1EFB9EDA7A4812536D44C1CC27D,
	JSONNode_op_Implicit_m39284C070FA8F77D4A75A67E44ECFDC033719CBA,
	JSONNode_op_Implicit_m1F0EDFBF473C04FAF2A76125C40D27AFBF427E56,
	JSONNode_op_Implicit_m4F12BB544F8D9E6DC387FC276BAE60269715326C,
	JSONNode_ReadVector2_mB5168EB80BA8FE7D38324BB66580CDE962B5BE7B,
	JSONNode_ReadVector2_mE9045E685C7EA3CFE8546153F99152CCBE61A18A,
	JSONNode_ReadVector2_m80B259CF2BDAFA309EAF5B6B2C78AED2EB528A15,
	JSONNode_WriteVector2_m78FD594CD3963CDAC2B1BF2DF26220692FD54DAC,
	JSONNode_ReadVector3_mC7A091BF1F3DE71655260268551A9E449EDAA1B6,
	JSONNode_ReadVector3_m1F04E19E177F89BB76CA022C70A975C08061F725,
	JSONNode_ReadVector3_mF41BE2C41613AFEA70CAB539AFA7D0A6F573AB9C,
	JSONNode_WriteVector3_mAE65146631E220583C90A007F05DE374A7E3D067,
	JSONNode_ReadVector4_m2B894C5C5513E9454511DDAAFE7C3A416470907D,
	JSONNode_ReadVector4_m13FF00E78429AC6F1E1284539C759AB69BDF1CD6,
	JSONNode_WriteVector4_m1210A9104B4EFE99047D7AFE6AB10DD01A73D27B,
	JSONNode_ReadQuaternion_m6EEE1B3ADE85662B1363029EE602CA1EC02F8DCF,
	JSONNode_ReadQuaternion_m6147D59E720DA900144A556FCB4365FCC2A71A32,
	JSONNode_WriteQuaternion_m2A11E75564526030F3EF8D276E578F39AA18CCA1,
	JSONNode_ReadRect_m831CAA59B68FD465C7739CE3DE1C297D8A23B023,
	JSONNode_ReadRect_m5DA61EE22CAA874BD2457B060F6286A7FB1DBFB4,
	JSONNode_WriteRect_m3614416241A91C0EDAB54E5645D7A5D9B7AADB55,
	JSONNode_ReadRectOffset_m830101CD2863A9DAA92A65983D5F7E44439522E0,
	JSONNode_ReadRectOffset_mA8D64472E6F8F695EED27E33E13B204BD87194C8,
	JSONNode_WriteRectOffset_m1146232B54BE25B8E9AC363C776E4B8A40C8A789,
	JSONNode_ReadMatrix_mCE78E3CAC6BE0132EBF82990DB741A12E1B3030C,
	JSONNode_WriteMatrix_m7F72D94C9EDC6B9E590231A27FFE6C52F200EA23,
	JSONNode__ctor_mF0692814A0795056AE052B05EF63D6B216B5619E,
	JSONNode__cctor_m0F95CA9754AE155CEC873DEFB28A83E8AAC25990,
	JSONArray_get_Inline_mCCA0FAE052593C420DF7266D9030F7CBCFF5D43A,
	JSONArray_set_Inline_mCDD35E23A81DC4DF48DB54FA164B2D05CA8CDD5C,
	JSONArray_get_Tag_mA6475F4E3B27AE262DFF53A510E5DD9CA9A82212,
	JSONArray_get_IsArray_m015BCD163D49042D8827D8B5880EAC8720B93DDD,
	JSONArray_GetEnumerator_m107028482740A363BC3AAC4348F729F49094B8D7,
	JSONArray_get_Item_mB56A68792D63083DAE88CF576C4E5C9C6D0E1AEC,
	JSONArray_set_Item_m8D70249BB8D717AD878AFE87F9F86345CD5DE9EF,
	JSONArray_get_Item_m5E864AF954B22AD5841E8CEB58AF68DED45C4C8B,
	JSONArray_set_Item_mD6DB253A66DC2D34179D9773A1ADF25A11FE7DDE,
	JSONArray_get_Count_m8649900F00FBEDB004834FB9D814F848E6C7D72B,
	JSONArray_Add_m14182F062E171457670DB45BE811A46A78B2D685,
	JSONArray_Remove_m65505E642765169F35A9F21760C7EDC39407B9F9,
	JSONArray_Remove_mA79C09C43B22EE2B4965C1D8B3794BAE1957037E,
	JSONArray_get_Children_m56D8A696E09F5EEFF1FF9F8BE06011CE5EAFA197,
	JSONArray_WriteToStringBuilder_m64C54C1D36684A7CE5EAB86E3350AE4B811D40C9,
	JSONArray_SerializeBinary_m8BB39836ADBAC72EB4DD67C37A3CFC44FFB8333C,
	JSONArray__ctor_m2B6B15254CC3578F4B78A9E5CA656C4D25119512,
	JSONObject_get_Inline_mEBB1EA4AB1EE32D80F2DB4C5C4240DD13593750A,
	JSONObject_set_Inline_mDDC84E63241E060414987E3B8301F7F1AB2967B4,
	JSONObject_get_Tag_m0F8454A3B161D9954B2C1B49945DE0E9041907B6,
	JSONObject_get_IsObject_m6E6B9273D50B23643A6DFBD17FBEA6BD6143E12C,
	JSONObject_GetEnumerator_mC0AAAB9C27211FED4A7DA97BB1D2DEA1777CABD6,
	JSONObject_get_Item_mF7EF352F2BA035D7EFC5902F2DEB12168E586CBD,
	JSONObject_set_Item_m84B17410261AD3D4C06D551BB169126C42331DD8,
	JSONObject_get_Item_m28A01147850D0D538204316C35A6E1A823602C0E,
	JSONObject_set_Item_m17A462CA68BCC6AB9A077BD2D5354C2CD9BAA448,
	JSONObject_get_Count_m5271E2DB59F0A774A71E1313A43FC47D209DE4BD,
	JSONObject_Add_m89A24748B1E3A3BC627C355AD3F946A5541D0D47,
	JSONObject_Remove_m20C910052474109A5D0B8C63DA98648296C1E846,
	JSONObject_Remove_m6A66604EDD3CBED40F44D85BAD15342B133A2B21,
	JSONObject_Remove_m2000AEFD68838D8F04D250DDFF4E5B2B52D2661A,
	JSONObject_get_Children_mDC37B11CC04E072B9E1DFE56A3B176A1E22F8E6D,
	JSONObject_WriteToStringBuilder_m8B9F8C4C58F878A4992C39D21E75CF735DB65F61,
	JSONObject_SerializeBinary_mD11FD4D432B7BA08AC16D504396247E871FBCDEA,
	JSONObject__ctor_m159A8A4A6AD38E7E4786F8EA8FFB9948DF520F92,
	JSONString_get_Tag_m8CD40E0F8FEA6E68B02D661527AAB26E581AD925,
	JSONString_get_IsString_mE62F3299F2BDCFA5704FEFBB6D6174C7D18EFEA0,
	JSONString_GetEnumerator_m2DB5AD3DA3FA00B24522A9083E0EC92952D28C14,
	JSONString_get_Value_m0A72F580BA1EBD98A7592810069BDF215BC737F0,
	JSONString_set_Value_m9B7FCFDF82ED801A434AC208F52582A2B0DF2164,
	JSONString__ctor_mE09DCD65C60DE37D1F0F79BD6902E464EE1960A3,
	JSONString_WriteToStringBuilder_mB338AAA2644A34B03F6DBE6620F8370C64D15627,
	JSONString_Equals_m9709B63863E6B7E1000EF9E0318B2C499D325B92,
	JSONString_GetHashCode_m573FB034DD33C5A300F358A318A0BCA761E35AB1,
	JSONString_SerializeBinary_m155A6E6D4045E9859823E7811A1434C0A0006654,
	JSONNumber_get_Tag_mC3886860EBAEBED98DF4B58A2E5D2DA1C86D0DF0,
	JSONNumber_get_IsNumber_m55E3251DC2B157F6A06A78047C8B7F6A8710CFC1,
	JSONNumber_GetEnumerator_m48371CA028B151ED79247AB9B49B7B120C172F78,
	JSONNumber_get_Value_m02C36CBFEEDCE75834055B25A6F0F9845B661B98,
	JSONNumber_set_Value_m437CA8FBE33C4A5C6CA4E2951767974987142A01,
	JSONNumber_get_AsDouble_m181BFF83082F0A4A380023EDE5D1EA55B0EC6427,
	JSONNumber_set_AsDouble_m48E57B49311F9720098069A63A056AF5B44AECBE,
	JSONNumber__ctor_mD0AF1324557B9FC7B1D63842D2CA65EC2D1A643A,
	JSONNumber__ctor_m03A803840EF273185C5C63642FAA70FAF9DFE40E,
	JSONNumber_WriteToStringBuilder_m48A94BA46F195E37F78A7AA167655AE1E870231A,
	JSONNumber_IsNumeric_mEA4ED6C1587A4D94F5D6F30B12FA284AB56F11EB,
	JSONNumber_Equals_m0D80E8BA86AA78FBB0482F9F053A0BACB65D8D97,
	JSONNumber_GetHashCode_mB10776EF12594532AB12FA617D1BF18FC53C790C,
	JSONNumber_SerializeBinary_m0250E8C465049F13DF32A2553DC76191A2012C7A,
	JSONBool_get_Tag_m43F4216FE45F0043979093D7BE5FD16C0F462A62,
	JSONBool_get_IsBoolean_mE70A4F82605CAB1E14A70413FC42EF66ADBAB727,
	JSONBool_GetEnumerator_mA15666FFF18AADF12912689A521087C98B5B2D1C,
	JSONBool_get_Value_m11D2863303B2003F337549581C8A583A6A3B4D74,
	JSONBool_set_Value_mA7FB197B21177B947BC3C2940A63724FE532E373,
	JSONBool_get_AsBool_mA5617A7BC781E111FBD24A7E53FB34C7DE6F62B6,
	JSONBool_set_AsBool_mDDD0D1CA7B06DAD37AA13987F46B95C6A1AEF489,
	JSONBool__ctor_m7593DD0B3E9628BA9BB4DE4A0E904BBA6BF151C1,
	JSONBool__ctor_m8E5E6A23827EE2B5DB5E7C86377805ADD8A771B2,
	JSONBool_WriteToStringBuilder_m7EA9750436CEE42357B6A109B83870F4FC2C403E,
	JSONBool_Equals_m57A524B4FEBF04285341E4FC42EDEDE7A524F4BB,
	JSONBool_GetHashCode_m49A9C417E05DFB795FAE0F38737C7629A3A69A22,
	JSONBool_SerializeBinary_m58E3D67CAB665202AF9AF699A0E2C267BE4A778A,
	JSONNull_CreateOrGet_m96465EECF543329FFBBC26B552EDB6E43D8DBC90,
	JSONNull__ctor_mA4FF0FB3001766DFB7A8D8C23063101AC377F277,
	JSONNull_get_Tag_m2A705402C448944F6395AAC88DA5DE7ADF99C4D1,
	JSONNull_get_IsNull_m1964A7B7E9C0FD17CDE2E4DFDF6345FE03EBA59B,
	JSONNull_GetEnumerator_m7B4ADD53B56B6663A0832AC611D7B47FA98D1A35,
	JSONNull_get_Value_m76B2E412BD13C31AD68C272CA9AF2E6063F165D3,
	JSONNull_set_Value_m713260989D4BA62C8B8CD716893585A1AA88ABE9,
	JSONNull_get_AsBool_m5E619608D71A63CBD234CA282B3BB2A7EFF385D5,
	JSONNull_set_AsBool_m267D1AA7D6003F371DC10DB65EB4E80643BB99F1,
	JSONNull_Equals_m3063912429EF72D2F9AFAEDF0D0303EB5D60A893,
	JSONNull_GetHashCode_m899F43B83E13ABFDE999C3781C45C6C03CC21077,
	JSONNull_WriteToStringBuilder_m66FC8C85A593D6198406753B845BFECA59117B46,
	JSONNull_SerializeBinary_m0D96E78D3AC9372DFC0EEF6F260367E9FCB4D939,
	JSONNull__cctor_mF5CFD47108FE35E808A8BA6514AA8283BBFD3193,
	JSONLazyCreator_get_Tag_m492C25FDFDC35D2EFB17711E1C259976A38BB46E,
	JSONLazyCreator_GetEnumerator_m6B2038B21158F725FC5B607544FB590A0E313AEC,
	JSONLazyCreator__ctor_m790C925B9AD99D9F574C556397F3C2D868D2F7EE,
	JSONLazyCreator__ctor_mE9B31AC731EAF8B853DA1671147CD5B9B5C6F10A,
	JSONLazyCreator_Set_m9C40ED5996DFD3D796BFAFBC25EEAB76F29F871F,
	JSONLazyCreator_get_Item_m0A09379FDB2D2CB95B781D1E3A03855661A0F446,
	JSONLazyCreator_set_Item_mE77E182C7C150529FBA7BD013A03F95FDCA25E9E,
	JSONLazyCreator_get_Item_m55D3BD8AF905099931604EACC6A907C17AACA753,
	JSONLazyCreator_set_Item_m8E286CF72FF8622427E69587C30C91BE0D242FCF,
	JSONLazyCreator_Add_m7393F681C0F1D9F798D3FEAE8C3A37C9F73E0E1B,
	JSONLazyCreator_Add_m8762C532A8EBAFF5AE213C3B2A9AC983653C8D37,
	JSONLazyCreator_op_Equality_mF472937FE1ABFEBCEB8A717561AB198CCB0F75E8,
	JSONLazyCreator_op_Inequality_m8C2EB320847B968D52F5E5D96599F734FEEE8ACA,
	JSONLazyCreator_Equals_m94A17EF99F288D175F1A40DC7B6569643CA84DD9,
	JSONLazyCreator_GetHashCode_m85E38D69ED1F0970E4EE3583196A4E9CD37C9DAF,
	JSONLazyCreator_get_AsInt_m8C8C09CC03C6E229A680D004C070734C424DA012,
	JSONLazyCreator_set_AsInt_m9CEB176958FE16A9213B7828EEAF442F54E70587,
	JSONLazyCreator_get_AsFloat_m1EC025D56616385B163C548183783BA8E4FD67E4,
	JSONLazyCreator_set_AsFloat_mAECCEF9C5598A5EA2204CDD1730D19B073050956,
	JSONLazyCreator_get_AsDouble_m019E3ACC054CCE18601A8731E772C86C62C09545,
	JSONLazyCreator_set_AsDouble_mBFEC4A9E9A946716829F584BE00CFA35D8DBA1E2,
	JSONLazyCreator_get_AsBool_mE7E32A21C51AFA46EC9BE4F78E96AE20986E7E00,
	JSONLazyCreator_set_AsBool_m700796091E7C1940E21834CE4BB1A8FD80017094,
	JSONLazyCreator_get_AsArray_mF737B0C7548A4C304278366931569BD173D4C2C6,
	JSONLazyCreator_get_AsObject_m0678D4A1725598B36657E07D1626B8387D3B4C88,
	JSONLazyCreator_WriteToStringBuilder_m20163F20F4F24E7E82C30AAA1878AE7DE301D0E9,
	JSONLazyCreator_SerializeBinary_m2E7AFA0F831BF1CBB93F6BC72BEB67A531A31FD5,
	JSON_Parse_mEAB5874EF687F205543F8469A9FAE7DB3C134421,
	Hero_Awake_m42F76DA5B80908CA13E6C2D5747DB2EC4D9E5935,
	Hero_Start_m7AC5D26F4F174B129C0FD0B55B343BB5561FABFC,
	Hero_Update_mF216C64A6B2D1D38CF303333498F90FD63C1396F,
	Hero_CandyEffect_mA519D257E5B628A01DD0532CF771D5E29EC70479,
	Hero_CookieEffect_m2C00FF83BEF5F9A857F3AAF865C0CAAB46C62ACB,
	Hero_LockOnTarget_mAC51B2745411A748812250162491069120EE2FC6,
	Hero_Laser_mB6341A7485EF37F581E23242688F1FE5176EAC9D,
	Hero_GetEnemyElement_m294B2143ACF0D11F576856B7EC8C0DF753795A30,
	Hero_Attack_m2FF65BCA3ED82FB5B05F7B8033F9EBDE5D6FE601,
	Hero_UpdateTarget_m9973558665291239C71450870D0344903E88384E,
	Hero_OnDrawGizmosSelected_m46F9157D2FBA21EE460DDD1D8DA523544D234261,
	Hero__ctor_mF7CAE2C07B0FD7AD2A70363FA137B6A9992A1FAD,
	BaseEnemy_get_EnemySpeed_m8E1469EAEFB76EE59CC6FEE9D45438D94EB0D703,
	BaseEnemy_set_EnemySpeed_m17416B6AA011CE66A743C53E4D38FABABF30A19E,
	BaseEnemy_get_EnemyHp_m60279B2A0B27BCC826D13D21434E45C48CE5BE1B,
	BaseEnemy_set_EnemyHp_mA8ABDB93079B24396F5D3BF6065D9E3EF53DC7DD,
	BaseEnemy_get_GoldDrop_m561D5F9F207855E6B2ACE07E9DDF833C3054E2D8,
	BaseEnemy_set_GoldDrop_mA3990C857287CAE8CB85B7736238292CFBD872AC,
	BaseEnemy_get_EnemyElement_m7A2942567C4D07705628C3545EB72DE376C01239,
	BaseEnemy_set_EnemyElement_mC9B8C24FEA90E86264C0F771E4E51FD46BEFCF69,
	BaseEnemy_Start_mC883DB3A187EAB0FCBB645DB2F3193043077687D,
	BaseEnemy_TakeDamage_mACFBAD9CE871E1D06651395F0B8BA615F535A258,
	BaseEnemy_DelayLaserPopUp_m7060C6E807ED81FF861F30EB54208E527CC98D58,
	BaseEnemy_Slow_mD29992E09422BCAC5302B52C0B03A0D7610B1EC8,
	BaseEnemy_EnemyDeath_m9F8223C370F09C3FDD5B9A56D6372FCE610C28B6,
	BaseEnemy_Move_mE311EEC0299B205996DB5FE37EFD9E57EE070FBF,
	BaseEnemy_GoToNextWayPoint_m74DFF5D5BE5B97F75C03A0C68610E025918431BF,
	BaseEnemy_Init_m0B85C8E16C7D15051A7E001B24BBC4F9DDD1A16F,
	BaseEnemy__ctor_m8D06C165E9DD193548F0283A202D70FB90776645,
	Colubted_Init_mB982329E42AC5AE5523E776123C0CBFD17575AC1,
	Colubted_Update_m257987F285384F076A7F3CFE4C1D7A6457C45572,
	Colubted_TakeDamage_m9022A89B8A6E95195C8D9EA719C8A6E0254E7A61,
	Colubted__ctor_m608A2BFB8179AD9DF8C825EEE64994A2D8206857,
	BaseBullet_GetStatus_mF2CEEF86F76177821CC531EF26145726DEF5FD63,
	BaseBullet_GetHeroBluePrint_m8EA60934B0934FEF2BB39B00C12A5D326321A7BA,
	BaseBullet_MoveToTarget_m62CE9568DF3FBA1F24B252EB125927A158EC6CB8,
	BaseBullet_FindEnemy_m062595DD40AE03245F211D7C1C75D0A5A14A8665,
	BaseBullet_EnemyHit_m2902EC924549ACE1E05C91C57B66BA3AB37FB9FD,
	BaseBullet_IsAoe_m78227536AA3C587871BD1DDF101632FAFFEBAFEF,
	BaseBullet_Damage_m0D2444A126AE9147E3C3C32091EB185E0E17EBD6,
	BaseBullet_OnDrawGizmosSelected_mC283C2DD39626B23DBD1EFC3FA24674AEB68DD38,
	BaseBullet__ctor_mC671AD70D46F5508C96B7A08D25BE639235AC504,
	Bullet_I168_Start_m898C01071471405C08356AB455BE7991B68C75D8,
	Bullet_I168_Update_m95C88867CC0E495429E2B47F06456771B7F52536,
	Bullet_I168_OnDrawGizmosSelected_m1780721D391EEA31DDD4C931BFDB050916DE4B1F,
	Bullet_I168__ctor_m8247D6B82EBC1E1A6A65171A89C18E88D2FAEE4A,
	Bullet_I26_Start_m696E7487940249B2FCF1494ECF2A95556DE3A5BB,
	Bullet_I26_Update_m0BD786280E240600E4CBF5CA6CE95274604F5473,
	Bullet_I26_OnDrawGizmosSelected_m51A767D91F44C8DCFA0DF3B738263E67D36DA55E,
	Bullet_I26__ctor_m1943C7D95D4777F01A960681DFB3A02E03613CD6,
	Bullet_I58_Start_m03855CB5BF50C2D904ACEABE8B2BB7A1031225A5,
	Bullet_I58_Update_mFF4416E50D5F13762729B8552375FE9D33B94EE5,
	Bullet_I58_OnDrawGizmosSelected_mB8ADFD1338408E2FB689705C76D4C5E49D59C8D3,
	Bullet_I58__ctor_m69FA5B604BB7BF45F1F41294BB8936C8047B2D6C,
	Bullet_U101_Start_m9DDC98141F2F792FBF3BF02DAB0218A2682AB673,
	Bullet_U101_Update_m4E1D589AF81C04FA773544D5C6C638A9CA4307F0,
	Bullet_U101_OnDrawGizmosSelected_m0ECC7985907E622FDCBDC7E2D7FA68FAC5833315,
	Bullet_U101__ctor_mD02A90338507A8D4527E31AAD5218F4BA8E0AA4B,
	Bullet_U556_Start_m4EE83A293FE1B121E4201F23D8D552C21E0DF363,
	Bullet_U556_Update_m15B15678E51B827DE8451DB3F97F42A81E025AA5,
	Bullet_U556_OnDrawGizmosSelected_m6040B0C030A8DD1985B1C7B37338F2D3C5908796,
	Bullet_U556__ctor_m989F31205ED9DF7D3AF7FA9A3F9625EEE23EE30B,
	Bullet_W75_Start_m129179FDEDFD6676C1CCF2EDD48427B5A9874FEB,
	Bullet_W75_Update_m25BD3D06E7918481F75CCE48DEEDA3D55CE5F33B,
	Bullet_W75_OnDrawGizmosSelected_mEA94AD9A6B3D48AB8106AC5CD3B0C0D701DD5FF7,
	Bullet_W75__ctor_m0F83CD5E089EEAECCEF792F90022BDA27F00A241,
	Bullet_Z46_Start_m8898CA2827A37183F452758B8694484FACAF4460,
	Bullet_Z46_Update_mC9BBDC76D55FD5124ABAC4579715AA821B8D40C7,
	Bullet_Z46_OnDrawGizmosSelected_m3B71F0181D1A8C233684BAA946D157372B170F05,
	Bullet_Z46__ctor_mAEE5DB3049D824391AFA779965B6E8FC54A9A353,
	HeroBluePrint_GetSellAmount_mDF12E5C55E45B410DCBD4F2A79DF7EAB57A44ABB,
	HeroBluePrint__ctor_m55EDE4C35B443D976707CC7FE2C42D195C85A543,
	BuildManager_get_Instance_m447C53DC52D9B8E9352F7CF07F13C1345CD8DA6F,
	BuildManager_set_Instance_mA6204A4F0E4CA49361D88BE862C74472578890F3,
	BuildManager_Awake_m47DC36B5C3D606E054F41D2C5CDE67DEAB7290E9,
	BuildManager_Start_m67CFEAABE2DB3AEC847746227732745BBB0B8CCF,
	BuildManager_get_CanBuild_m0F6F9D491AF5F3924B8C5983FCC07FDBF446EA9C,
	BuildManager_get_IsHeroBuilt_mADCDECD19EB9255AD1D6F7D1C608DCE36425C03F,
	BuildManager_SelectHero_m45020078B6CDD905656790146B5C6CF1F364FB73,
	BuildManager_DeselectHero_mBDBAF5B338497ACEC9011EAD3DFFE2CACBEC43CA,
	BuildManager_SetHero_m05352D0582113E9F4355EB8FA228031EDF8E007D,
	BuildManager_GetHeroToBuild_mE2E3B5CB59AD263F066BFD199B7FE45E9901DE8A,
	BuildManager__ctor_m31E3581B92CBEA74A1BFCA1EFD2C867C63EA8D26,
	GameManager_get_Instance_m705AC8D9CAF114B59D3AF0563C19B5EE0B711074,
	GameManager_set_Instance_m9DB0F13876CD49066CC3A818D68686B3600F792E,
	GameManager_add_GameOver_mBED5EF8A33E9E9D30CCF2FF978C82C91AE824089,
	GameManager_remove_GameOver_m4A4097C770BD88B02898662A4165CDD35CD06677,
	GameManager_get_EnemyCount_mC5D539611B83ABD18DDE25362AAF39D6820E2588,
	GameManager_set_EnemyCount_mD5A07C5A7544A51C663A9921CC8F86E7041EA7D2,
	GameManager_get_GetCountdownOverTime_mE81FB3FF717EEDBE8457148BA95BE247E29B9CFD,
	GameManager_Awake_m821FE927548316EF82F1314392E190FC88778D52,
	GameManager_Start_mC69A683D4A04B4F90D765739ADF7D6C38C69F0AF,
	GameManager_OnAdsReward_mD63CE8A454A6C0770D504CB949646A1BABBB5762,
	GameManager_Update_m5431C66CACDF9E904C8E86AC7C164A4FF9E23DB9,
	GameManager_UpgradeEnemy_mC6DF633917D6C46DE0F2B1732AE2CA2A01A6F334,
	GameManager_GetEnemyPicture_m653D6E7B83289032F23720CBFD0DAE68C94DD0CF,
	GameManager_UpdateEnemyInfo_mB4E77AFDD16F194F614375BF6356D30181A40746,
	GameManager_GetHeroPicture_mD10D51FFB845B60B35976BE51A4C3710C5BA125B,
	GameManager_UpdateHeroInfo_m4CC3E434C573C2C72341B4CDF3DEF11951A6926C,
	GameManager_Spawn_Colubted_mFC9D59E83643C0A48CD503F57D7A5F8E7B776A9B,
	GameManager_Spawn_Embio_mE03625CC8573709C6B9836FEB2E33ED8BE37A22E,
	GameManager_Spawn_Eclipseside_m1B24FAB3354CEE5228F2EE64283EEDB697BF4B28,
	GameManager_Spawn_CrossDive_mD6E700E04C41B6793853EE7196DDA1ED5F03AA1A,
	GameManager_Spawn_Shiro_mA778293D823810DCBB656FE944A383CDFE1393B0,
	GameManager_Spawn_Kana_m19052171B0D10E0CED29F4D466930C76F79F4740,
	GameManager_Spawn_Chest_mE8574A07743D4BB7773BDEA1C9D86C85825A3374,
	GameManager_EnemyMoreThanLimit_m0C966A880C571B2BE28945BF1EA53188DD2409F9,
	GameManager_GameEnded_m21DEA77AB67552E97AB9728609AEA8EB2645633E,
	GameManager_Start_Colubted_Wave_mA1BA1AB6B812755841331A5F6B13FB7B10F67875,
	GameManager_End_Wave_m47AB0514552FCE3E7E4255AD29DEB332D42DEB09,
	GameManager_Start_Embio_Wave_m465D7B3AA05B4D82429D36AA1C6DF8884047A6A5,
	GameManager_Start_Eclipseside_Wave_m12D5F86EC89C8BCA0709957EBB0E192CFEC3DAE1,
	GameManager_Start_CrossDive_Wave_m4CB5034D50DF2827DB652199D7C722E4515FA050,
	GameManager_Start_Shiro_Wave_m98F53C844A27CB04DBA7F3E3C79432FFF8A38662,
	GameManager_Start_Kana_Wave_mEC9621F71D82363E5F9AE6EA64471461F0C72244,
	GameManager_Start_Chest_Wave_m66CA5948A7D2784F57F885071D3F42D98BFC4263,
	GameManager_GotoMainMenu_mDDCFA1F4A1EE0CF13795E0BA9FC057547950F765,
	GameManager__ctor_m4D37F4709FCE3942A010D8A64ECF2C2BA941D198,
	InventoryManager_get_Instance_mD984E22ADAC6EE38F0CE613A6AD261064F62E121,
	InventoryManager_set_Instance_m290F386A14BBDD7A90E7A57D1F43D0CBE9680AF2,
	InventoryManager_Awake_m86DF25A66E73FD10E9D077AAA95A6438D1C56697,
	InventoryManager_Start_m9E30E006307D2ADF875AA617511B37FF7789B661,
	InventoryManager_AddCardToInventory_m2BBB77157EF6D61EFC0451C8B8305CA3618C6B78,
	InventoryManager_TagCardId_m1BF827CB925D4B8E715E1E93EBA89AD75313C877,
	InventoryManager_AddCard_m5EC74258266C344C877249E8020229A0F91B34FE,
	InventoryManager_GetCard_m7F701180E87E15385A0FB2DA9F83C74A1F1F0391,
	InventoryManager_CancelCardUsed_mFD94F2B274B3DE95D5B93C58FABE670CF8607D07,
	InventoryManager_GetHero_m3C1013B04EE546101C53A637C635ED87E3C62623,
	InventoryManager_PickHero_mE0B3D197BF30F7AF35FE0B1EED268236388BD86D,
	InventoryManager_ShowInventoryUI_mFED06A213DCF737DA14C5FB39F4CCA38F1768B78,
	InventoryManager__ctor_m7103387150C0ECB2B88D0C28D4F631C5F672D65B,
	MoneyManager_get_Instance_mD33F844A651475BDB3C97054EAAF8B8198A9AA5E,
	MoneyManager_set_Instance_m9FDBDFA722BB515C6AB48DC35A5EF19C7BAB2798,
	MoneyManager_get_Money_m9F70B21FCD308184615E006F48FC5DDCB43BEA8D,
	MoneyManager_set_Money_m39C6D08CC4093DAFBE32E4B86887FCB14882C0DB,
	MoneyManager_AddMoney_mDF0321F2FC4195904E9E61B32CE4360B655FB77D,
	MoneyManager_DeleteMoney_mD65C702DA90BB48C01CB79DD3D29383C7100F2D0,
	MoneyManager_Awake_m009CA67919C27BFDF266292870B3373F3CEC84F4,
	MoneyManager_Start_mD77889A1713D67CC6C4AACE87D81A40362F7D188,
	MoneyManager__ctor_m02096BABFD8D485FFF48B7634D60D42BD8A8C189,
	RandomScrollManager_get_Instance_m71192DB4EC184AB40057DDB9A1A58BF2F682D6DC,
	RandomScrollManager_set_Instance_mC51D12544F256ABBFB0604230F7E9FBD14A34627,
	RandomScrollManager_get_NotEnoughMoneyScore_m2CEDCC4DD5A63ACB0FF0E4F56926E38713872F6E,
	RandomScrollManager_get_NotEnoughMoneyScorePlus_mAC187D9247AD10FDE2BCB62AD189A9C01E75DC56,
	RandomScrollManager_add_OnFirstRandomCard_m55B9B6C95F99B39B0283EE6771C362967EEF59A3,
	RandomScrollManager_remove_OnFirstRandomCard_m75726C3DEC3C365DE5DFB8A44F5E0D99E658DD6B,
	RandomScrollManager_add_AddCard_m1DFB2FEF7AB61EE4C232D430BF6434FB96FDDC89,
	RandomScrollManager_remove_AddCard_m16A0C1743E0F9F8152FA98C089624F21376AD0AC,
	RandomScrollManager_Awake_m6B416832189E57E29BBE65E517A0537310DA2357,
	RandomScrollManager_Start_m737477F3C7A12F2A65635005B277D0A48501B775,
	RandomScrollManager_RandomCardOnStart_m497D329AB662A001B77A72BC4A58201725D87993,
	RandomScrollManager_ClearCard_mA3B65BB79544AFB43170291CF80C835780870A77,
	RandomScrollManager_RandomCard_mE696C58712B9F9B3AD0847DEB0C8313620500421,
	RandomScrollManager_ShowCard_m1F0446B2C6641AFB1578D9B16276B4D90B50054A,
	RandomScrollManager_HideCard_m38066E4FCA046DBE7514AE5441CD761239BE0951,
	RandomScrollManager_ReRoll_mF7D2EED2F23B12D0C8C956CF4022BF8BB8B4DD8F,
	RandomScrollManager_AddCard1_m5410E77622E9E6E587514EF5E3E73A6BEE97D2EA,
	RandomScrollManager_AddCard2_mC47A6EA099F8B44A780737196D6264B3B665451E,
	RandomScrollManager_AddCard3_m765C29E56DC213917B59BA874501C85944E4E2DA,
	RandomScrollManager__ctor_m483B15CFC6395915D0B4066E753E66373E68D105,
	UiManager_get_Instance_mD22B7B526318C512A940851D7C5F3605A3188F72,
	UiManager_set_Instance_mC55D7E31E56BDE146DC3270088C52E0D5C7CEBEA,
	UiManager_Awake_m5BEF6BE5A4FE6BE73E78D490AE5F9DC31291CEC6,
	UiManager_Start_mAEC8014428404D9EE8B303926D93E1F7784BE0F8,
	UiManager_FinishAdsReward_m2ABB56493CA19A845EE695023A6E1F3C0D90D4F2,
	UiManager_ShowRandomButton_m752FA56BE7282C6583C11E97EB05E6BE2FC99C55,
	UiManager_OnRestartButtonClicked_mF11CC49EEBA6A78FC58A228249EE68F312B1F8F3,
	UiManager_Update_m602B413B5FCE514DD032305F5F43E32CC1FD1CF5,
	UiManager_UpdateCardInventory_m300AF988DB9634A041B0AF02A6B2317637736DAC,
	UiManager_ShowGameOver_mEB30463AE683687EF3F9E272022336CA791B050B,
	UiManager_SetWaveCount_m9569D47246B4EFFC341D758E69D4F0E8A50FCD6E,
	UiManager_GetHeroSubHeroAmount_m2E643F76D41C24AA1B21B906096CCE6B494A0F7A,
	UiManager_GetCurrentEnemyAmount_m5626F482BD2851178F9E6E13F2B85F37921F8DF0,
	UiManager_GetCountDownGameOver_m268AE718B0955059E11990ADE18F7D1F1826CBE7,
	UiManager_HideTimerNotification_mDB178E930B8B3115266EC5A6029FA72176C8BCE7,
	UiManager_WaitForNoticeTimerOut_mC2A0447A655995E085F1CF523E140BE180B9BBE9,
	UiManager_GetNotification_m24AA4D5B27B37F62293A864A17E0BCE85DC92CEE,
	UiManager_SetTextNotification_mEE4E06CE14132D2C42882072AF7AFB3215075F24,
	UiManager_WaitForNoticeOut_m80849B33FF4B00687BDF23BDA6DE02123DEF6831,
	UiManager_NotEnoughMoneyUI_mADD8A2CE44C81C22D862B4293E947889FAC87D2E,
	UiManager_OnBuyRandomScrollButtonClicked_m4CA0969BE782DB98FE738FC605B1CA21E41DBD04,
	UiManager_OnReRandomButtonClicked_mAE4524AC7DD905AC578908F8D2FBED36C6F96A6E,
	UiManager_OnBuyRandomScrollPlusButtonClicked_m9AF1F900786473075121B5BC68AAD1D4E123C104,
	UiManager_OnCloseRandomButtonClicked_mBBEDD95CA422C9D970D69E37C92D9DEC166D1223,
	UiManager_SpawnChainSkillText_mA6CC644F29891F8823C6AA865CC729B4F9AAB1D3,
	UiManager_RemoveChainSkillText_m0E99C5CB0FCA7728A8AC014D46183ADA3ED7D244,
	UiManager_SpawnChainElement_mECF976B3F7841CB95BFD926D6595AF74C19F2CE4,
	UiManager_RemoveChainElement_m3C31CE220D7A108557C70D54D8485F90C831E0F5,
	UiManager_SetTimeSurvived_m7D4BF72F85D64604AD92163AE0A5C055F1FAFB36,
	UiManager_SetWaveSurvived_m7CAD2C5729733F67DD6AC26D72DC93F476EC6452,
	UiManager_SetDamageDone_m80EEC02ED196909AFF938A78B2A12964E3DBF897,
	UiManager_SetCompUsed_mCBAE5653066F3E3885D6D078D01F485EBF077E75,
	UiManager__ctor_m70492F9645C447E80D2E401C45C7F19E15754406,
	WaveManager_get_Instance_m245870AA93671235A9952030594169263629EA4F,
	WaveManager_set_Instance_mEDF39195B0EDFB8F8661CA8614CAE135274FDA64,
	WaveManager_get_Countdown_m13E9315BE0AF304560DBDD9D9D152CABC4878C86,
	WaveManager_set_Countdown_mB1EE76187468F945164393EE86AF16C2AC70FBB3,
	WaveManager_add_OnNextWave_mBFB31E8EE88D78103A44F33E610C07051A7A209E,
	WaveManager_remove_OnNextWave_mB3182F529CF54627875EBADF430F16687BD211F5,
	WaveManager_Awake_m47B112FB6D5462FDB30EEA5B91B082CEA1674D55,
	WaveManager_Start_m066305636504F4AC3A870930DF9BF541ADDC90FA,
	WaveManager_Update_m9DC75D4D382EA7E4416CB8667925AE6AFFC17BC3,
	WaveManager_GetWaveAndTimeGameOver_m1641FEC82072D469F8F67B027C283F4B28221FDB,
	WaveManager_WaveManage_m291BF8DD3EE9E1CEBC120ABFAE68AFC53DAE6EB8,
	WaveManager__ctor_m8125B6CA2FFD8462A76F2C92420B403A304CEA7C,
	WorldUIManager_get_Instance_m69CA555FDFB6797BFB2D7E2CBF758AD7CF1BE7EF,
	WorldUIManager_set_Instance_m4F34BA28C67F98C9F4152355F3B13247D259AB0A,
	WorldUIManager_Awake_mB4D6AAB48187C19552529C752164CF3B0BAF8304,
	WorldUIManager_UpdateEnemyInfoText_mC99C90BF8DF880E21D5A1A24A2CC904329613D9F,
	WorldUIManager_UpdateHeroInfoText_m8DE11CBD55CD640491F265E8FF873473DD5FDA86,
	WorldUIManager_NextEnemy_m9B9022BB73E120504B17458013796E023FB5AE30,
	WorldUIManager_NextHero_m31ABAD8168C9EAB5D6829B341F1E759AE2D8D137,
	WorldUIManager_ShowFireWorkUI_m6F04947DA87AEF3EA3E69EA1C625073E0F07A677,
	WorldUIManager_ShowCookieUI_m3E872B2A2C5B9227C5DFA42F370F574937CC1B77,
	WorldUIManager_ShowCandy_m38D7F48BA705992258B229B6369AC45CA962FD20,
	WorldUIManager_ShowScreenShot_mAF73F9B85D3C7F208C82B161F871BA4F0EADEA2C,
	WorldUIManager_ShowLeaderBoard_m49B1CAAD05F7EC9AC86D0DB79E4A095FC0F87752,
	WorldUIManager_DisableInfo_m486646FD2278E1A68A2ACC89D9B5C195CB5D6D0C,
	WorldUIManager_FireWorkClicked_m2E89B09A70BB788222B38B1CC8704857A97B43BE,
	WorldUIManager_CookieClicked_mCB4A623F5D5D4231D2EC0C4BAD9FC47E9BF83F1E,
	WorldUIManager_CandyClicked_m004176E7D20CC7412E7303ACA8CDF6ED8C81D6B3,
	WorldUIManager_ScreenShotClicked_m957D2F14806168935EB078D5FCF9CCBA2EB21B5A,
	WorldUIManager_LeaderBoardClicked_m7933E88951F99F4887431F5E90C732CDC4D74627,
	WorldUIManager_OnScreenShotToggle_m4167432ACB401A5C1A002F6FD9F37EA5E9C9FB6F,
	WorldUIManager__ctor_m1FEB23E1D23BA593DB04C2CEC0CDDDBE983FA68C,
	SafeAreaSetter_Awake_m8CDC989411F0A6B2970439146BACFB54940F8BC0,
	SafeAreaSetter__ctor_mF3443520D4663615C2A80F465F9FE50461CD412A,
	Floater_FixedUpdate_m65A78EC97F6BB17CF34859E6FDBA8467C7587937,
	Floater__ctor_m66BADD84264ADAB461E8A97E0D06E05C66AE07B5,
	U3CPrivateImplementationDetailsU3E_ComputeStringHash_mD94B0E22EF32AD3DFD277ED8E911B5DFA4CDB91E,
	U3CWaitToStartU3Ed__14__ctor_m29B7C44F25045CA2AF686E43C9FC185788C40503,
	U3CWaitToStartU3Ed__14_System_IDisposable_Dispose_mD5DE83D793D38D79C4BFC2C7300A08CBA9A27C3E,
	U3CWaitToStartU3Ed__14_MoveNext_mFF0C85FF523AAE60E111BA772AD683F307BF84F3,
	U3CWaitToStartU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9EC5D5E119B878E35802A8030693DCAEE73E1886,
	U3CWaitToStartU3Ed__14_System_Collections_IEnumerator_Reset_m524B6AAAA453CEFB7943404F24E2BAB89E723FD3,
	U3CWaitToStartU3Ed__14_System_Collections_IEnumerator_get_Current_m54B43C5EE89291B63D84359793AF6F746A48A102,
	U3CWaitForCamU3Ed__18__ctor_m0F9FDC3F7885AD8CB9300502DAB0CF3BD03ADE43,
	U3CWaitForCamU3Ed__18_System_IDisposable_Dispose_m60C82B3C406C8BD576F9B5C7C8A3E9399F2844F0,
	U3CWaitForCamU3Ed__18_MoveNext_m940216FF13F15E4A77F99F2E54389A1209A6B5B7,
	U3CWaitForCamU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4D0F0E0221A015D42D448233811E1E3F1D18844A,
	U3CWaitForCamU3Ed__18_System_Collections_IEnumerator_Reset_m3A75E79EA70C002A7A8E6357119F039688749E5E,
	U3CWaitForCamU3Ed__18_System_Collections_IEnumerator_get_Current_mE2168EB7F89A76D61B57517E899160E4E598659C,
	U3CWaitForShowCanvasU3Ed__21__ctor_m35D4BF98BB8BA1BC8821E567536E8DFF2D90F18D,
	U3CWaitForShowCanvasU3Ed__21_System_IDisposable_Dispose_m82BACA53350D7E4E634FA212196B7DADBE60FEFA,
	U3CWaitForShowCanvasU3Ed__21_MoveNext_mA15A30F87CBD29EE0D184424116903578C50CD57,
	U3CWaitForShowCanvasU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4AC62F2A7C2D79CA96258278EB16915408AB0A0F,
	U3CWaitForShowCanvasU3Ed__21_System_Collections_IEnumerator_Reset_m0919B8BEE473A81A79E5048EE957A3EF9DD9922B,
	U3CWaitForShowCanvasU3Ed__21_System_Collections_IEnumerator_get_Current_m75D2DA098B2ECBB96569E0DF8A16E85DF389FBF2,
	StartTouch__ctor_m5CEF3C2B02DD8AA5E09F2AB1B6157F2FD77F9615,
	StartTouch_Invoke_mE16B1DEB5FD8F966D19ED393666A832869C30266,
	StartTouch_BeginInvoke_mC4859EA8DF9A46F32FAA4392CD02717181887BD6,
	StartTouch_EndInvoke_m11CCBBAA2F257BEAAAFA29C0F47DEAA3D1BD6346,
	EndTouch__ctor_m3741372E9F1D53D5AB4D702882317B5C2BEA97AC,
	EndTouch_Invoke_mEDF30247BB87D5AFA917553C15CA410B00421F77,
	EndTouch_BeginInvoke_mB67BD9B80F75086272A0F463615030133D1716DA,
	EndTouch_EndInvoke_mB4771BF3EF6C8CF0F41EC05C403C0C7E75B503A2,
	TouchActions__ctor_m5F79CEC71E38F8C83D916CB78192AC260AF83592,
	TouchActions_get_PrimaryContect_mB54C668A5C15D5F965ABE7CEB759E234361C7063,
	TouchActions_get_PrimaryPosition_mAA3C5A043DD92BB973B901DBFE311D2B8A4112D8,
	TouchActions_Get_mC8FC631CF4A79917E0F0D6303C9AFD669CA2F1C8,
	TouchActions_Enable_m9ABE56F3809AD02B52FEB58E0822CBFD13E15988,
	TouchActions_Disable_m237F8450B651D42320F5C745C07A4BD2EF0027C6,
	TouchActions_get_enabled_mCAA5D8F20D1CAB85BE56F95F033AEC090EC3A650,
	TouchActions_op_Implicit_mABA1D829548CA94240CB7CB92E0A81B334926630,
	TouchActions_SetCallbacks_m948F215A5E7BF2316FF838E0BE421F42F6D81412,
	NULL,
	NULL,
	U3CU3Ec__DisplayClass15_0__ctor_mFAB3648391D7F7415C8099820E6687D05184B688,
	U3CU3Ec__DisplayClass15_0_U3CCaptureScreenshotU3Eb__0_m110EE270D4757DE3C34A53F2FA8CDC320BE4B987,
	U3CImageShowCaseU3Ed__17__ctor_mCD4899C7ABD62E58107628266F40BCE1A71BD8C5,
	U3CImageShowCaseU3Ed__17_System_IDisposable_Dispose_m0C3DEEFF1C66F08CCB7AFDC7309909C55D39EBBA,
	U3CImageShowCaseU3Ed__17_MoveNext_mF87CD7D9E7883DB15BA089A85FA5EA5E98A4E49E,
	U3CImageShowCaseU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF252504AC2355F64820C71DF7B5DBCF6FF71F9B5,
	U3CImageShowCaseU3Ed__17_System_Collections_IEnumerator_Reset_mCA401AA94E70446FFEED94191F1061F4E0FE5AD3,
	U3CImageShowCaseU3Ed__17_System_Collections_IEnumerator_get_Current_m470E7F0C70ED4B0BA7C5E8FA54FE620C60160B7C,
	U3CDelayClickU3Ed__8__ctor_m482F03E4612F42F141D0E23529A7F47BD9D322D7,
	U3CDelayClickU3Ed__8_System_IDisposable_Dispose_mD47B909BCEBDDC24A464483BCCF4456D5566180F,
	U3CDelayClickU3Ed__8_MoveNext_m1A9448216F0C01605FE9D1DFDA7B3E1116B9DA2E,
	U3CDelayClickU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m120379B849FCB4CEA022C33647A4ED839DA3B925,
	U3CDelayClickU3Ed__8_System_Collections_IEnumerator_Reset_m227BC1F635FF1B103C8353698AE384738441998A,
	U3CDelayClickU3Ed__8_System_Collections_IEnumerator_get_Current_m4DB8DE888D03118595DF28332153EAE8662A0447,
	U3CU3Ec__cctor_m5CB640B2D2760E81EB392FD51B8C704144A1965A,
	U3CU3Ec__ctor_mAE2789A7F3C1325CEE49F6FD675C6018634C21DF,
	U3CU3Ec_U3CDestroyCardU3Eb__10_0_mD8F6FCF82CBB0DB29CFA3CB0CFD264D862CE7436,
	U3CU3Ec__DisplayClass50_0__ctor_m4B9795463D1DE5D979F62A55395EA7ED749AA3CB,
	U3CU3Ec__DisplayClass50_0_U3CCheckAlreadyNameU3Eb__0_mE0C230F89F6F98C4E6E1994A6336A7B407431BFC,
	U3CCheckAlreadyNameU3Ed__50__ctor_m84595AAC374EC74BF9C5457425E54D84BEAB35AD,
	U3CCheckAlreadyNameU3Ed__50_System_IDisposable_Dispose_m35313E7C7FD768948368F6DA71DB5F5E08EE80AF,
	U3CCheckAlreadyNameU3Ed__50_MoveNext_m2EAFDAECCDD584F2F31AB80092EFD67C60CE922B,
	U3CCheckAlreadyNameU3Ed__50_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCE98E48779C30473658D59AC7A4E3708DB45F871,
	U3CCheckAlreadyNameU3Ed__50_System_Collections_IEnumerator_Reset_mCD04C1434BC6EB6D7F385D7255F507715CE0413E,
	U3CCheckAlreadyNameU3Ed__50_System_Collections_IEnumerator_get_Current_mB63916D8978C57C9AE01329190AFA4B9DB1C4251,
	U3CU3Ec__DisplayClass52_0__ctor_m5990FC199363FCF73B7A8C182C5C1FAC8CE992AC,
	U3CU3Ec__DisplayClass52_0_U3CCreateUserU3Eb__0_mB2C40C89DFDAA78C736F30AFF58BE28C2F9B64AB,
	U3CU3Ec__DisplayClass53_0__ctor_m2A5A68D914CF2AA0B26FE1FF8AADB2EFC0934702,
	U3CU3Ec__DisplayClass53_0_U3CUpdateUserProfileU3Eb__0_m918B0A532747ACAA5363AFCFEA009FA10EC36B6C,
	U3CCloseNotificationU3Ed__62__ctor_m43778604560229E1A3384C14D2CB78F31BD3EC62,
	U3CCloseNotificationU3Ed__62_System_IDisposable_Dispose_m8707E38A3EE8232B8D3B016AB054931AE47A8550,
	U3CCloseNotificationU3Ed__62_MoveNext_m640115CD25650ED6F8F063F1DEFB72C15F8217D4,
	U3CCloseNotificationU3Ed__62_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8EC69539785A0FFFA97B6B1E2BECD465CAA38899,
	U3CCloseNotificationU3Ed__62_System_Collections_IEnumerator_Reset_mDF06AABB22CECEE0C65B06A046FDC6A0B0B07F2E,
	U3CCloseNotificationU3Ed__62_System_Collections_IEnumerator_get_Current_m9ECE534C9D9315FD7B66D40424D4390992F60552,
	U3CU3Ec__DisplayClass73_0__ctor_mB26E4710C5E7502BB4462B6EF4E175A63BD5CC2F,
	U3CU3Ec__DisplayClass73_0_U3CTransferDataU3Eb__0_m5560C85FFFE2A541AD241196F19AFB90B0CC5C06,
	U3CU3Ec__DisplayClass74_0__ctor_mA6AA8A80AAC8605B455A7F9CDAB52F86A6CF5255,
	U3CU3Ec__DisplayClass74_0_U3CCheckAlreadyNameIdTransferU3Eb__0_m6636A41227CD460A537009FAC82F75BE3B15D729,
	U3CCheckAlreadyNameIdTransferU3Ed__74__ctor_mF54BC3DA1886144F3C8CBFD0F06CBFB613D1250E,
	U3CCheckAlreadyNameIdTransferU3Ed__74_System_IDisposable_Dispose_m6840128964E0260E22537A16FBC442377B5CB78B,
	U3CCheckAlreadyNameIdTransferU3Ed__74_MoveNext_m3969539A58C080E36B3DB89816E37357673C9F1F,
	U3CCheckAlreadyNameIdTransferU3Ed__74_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAC8343C114D017BB1AEE74AB38D9A4AC6D46E47D,
	U3CCheckAlreadyNameIdTransferU3Ed__74_System_Collections_IEnumerator_Reset_m58D65B1A3ABB708128F90AC7B13AC4C48862B114,
	U3CCheckAlreadyNameIdTransferU3Ed__74_System_Collections_IEnumerator_get_Current_mAECF9042F87AB9BBDA616C14ADAF015C8CABDC4A,
	U3CU3Ec__DisplayClass75_0__ctor_mA6CB96E2C3366F8AB03CBC71BE0415047B6C9CB7,
	U3CU3Ec__DisplayClass75_0_U3CLoadUserScoreFromDBU3Eb__0_m8C45A10644C451BD7ECDDE4FB621041427AFC392,
	U3CLoadUserScoreFromDBU3Ed__75__ctor_m704CC19001BACD6E7621DBB5487CF8F23B19EB79,
	U3CLoadUserScoreFromDBU3Ed__75_System_IDisposable_Dispose_mB57F1F147F4E74A61906CF48147B58D654B61655,
	U3CLoadUserScoreFromDBU3Ed__75_MoveNext_m1E98BB066DE2F7D4BF9F182D25FECF5EA2F2C226,
	U3CLoadUserScoreFromDBU3Ed__75_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7EC769D8AC3147416F9224A21B0C4DB92846B4F7,
	U3CLoadUserScoreFromDBU3Ed__75_System_Collections_IEnumerator_Reset_m5DDB2E8C37B38A0BAEEB860654298AC7E64DD21A,
	U3CLoadUserScoreFromDBU3Ed__75_System_Collections_IEnumerator_get_Current_m07AF984DCC7A697BD9F97B5D7FE103676B722BFB,
	U3CDownloadImageU3Ed__76__ctor_m5CB98433BF9CD23864038893E2E3B3A6A6DE27EF,
	U3CDownloadImageU3Ed__76_System_IDisposable_Dispose_mD04A6581A340602E2B175B8886A700423A8F9509,
	U3CDownloadImageU3Ed__76_MoveNext_m1D88425A1DCE51C0E629EA7D7D106032C75C4374,
	U3CDownloadImageU3Ed__76_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFBDB22DD4A78362581BFEC7DCBAC6DC48D18F23A,
	U3CDownloadImageU3Ed__76_System_Collections_IEnumerator_Reset_m88F7C5F894E29D5CB0C542490E7A8EDF17C01245,
	U3CDownloadImageU3Ed__76_System_Collections_IEnumerator_get_Current_m46B996CB3BECE15A4B97173AF9BB4449777E971C,
	U3CU3Ec__DisplayClass80_0__ctor_mFC8D9A1D51F266F9D284B66FB431CC8C102AA33E,
	U3CU3Ec__DisplayClass80_0_U3CLoadLeaderBoardDataU3Eb__0_mDF8AC42FF873A2AD9C51A2EAEFCCE864AFCD49F0,
	U3CLoadLeaderBoardDataU3Ed__80__ctor_mC00F58C654FE55829D55D858BBEAB5EBA0E804C1,
	U3CLoadLeaderBoardDataU3Ed__80_System_IDisposable_Dispose_m0C8C77ADD2592E7F7409CEE5C220CDADDA47B9BB,
	U3CLoadLeaderBoardDataU3Ed__80_MoveNext_mF9252997DBAF12FB8B5A0CF48B0E25EBC1DC7F90,
	U3CLoadLeaderBoardDataU3Ed__80_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8F6B7887F6E6B15F168E5A764040545E91375F54,
	U3CLoadLeaderBoardDataU3Ed__80_System_Collections_IEnumerator_Reset_m9CB4781BA05AC6B10FD795F9C3DB8BC690E62CB0,
	U3CLoadLeaderBoardDataU3Ed__80_System_Collections_IEnumerator_get_Current_m0E44B192105C8A4F010F1976F1E212B3B6287568,
	U3CDownloadImageU3Ed__5__ctor_m56A893022DA599581E1A11DCF1D1BF961B1A0316,
	U3CDownloadImageU3Ed__5_System_IDisposable_Dispose_m2398236E2D1FB99231363CFF876F7EB857FA7042,
	U3CDownloadImageU3Ed__5_MoveNext_m594786314C7795FD05A476C72AA9E7C2B63931E7,
	U3CDownloadImageU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m26063764B51701B2831DCC38CAB2DA3C0A819609,
	U3CDownloadImageU3Ed__5_System_Collections_IEnumerator_Reset_m3EFD1AFEB3C0C56382735B8E26FA7B102D127CCB,
	U3CDownloadImageU3Ed__5_System_Collections_IEnumerator_get_Current_mABB560831447A0159C1C41B02A70EEA0E3292041,
	U3CU3Ec__DisplayClass12_0__ctor_m0E9A98FEAD760AA673AA23EAF8E8DBCDB9BE9EED,
	U3CU3Ec__DisplayClass12_0_U3CCheckDamageOverCurrentScoreU3Eb__0_m14DB9E5AB7091133E8B3AE0DBC7D628DFDCD54F5,
	U3CCheckDamageOverCurrentScoreU3Ed__12__ctor_m9FBF500C44EB1E27C562C6EB65C62D9ABF486BB5,
	U3CCheckDamageOverCurrentScoreU3Ed__12_System_IDisposable_Dispose_m7D36BC8498D8CBDFF8D4E22E822E2573EE8A31A0,
	U3CCheckDamageOverCurrentScoreU3Ed__12_MoveNext_m3F1F15548BE4B7A6EE72C4C66F41F956FFAD382A,
	U3CCheckDamageOverCurrentScoreU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m64A78464A601CF744FD9495D04B11CB0AB3B3332,
	U3CCheckDamageOverCurrentScoreU3Ed__12_System_Collections_IEnumerator_Reset_m0ED25AEE06E5E9ACAE43E03BE0DD6424A38EB4B1,
	U3CCheckDamageOverCurrentScoreU3Ed__12_System_Collections_IEnumerator_get_Current_m5D792B1CE7190B73BC86974F72B0B35ACC2DCCC7,
	U3CU3Ec__DisplayClass13_0__ctor_mED4FFF10797AFE19F6B942DCE18B2AC86C34E20E,
	U3CU3Ec__DisplayClass13_0_U3CCheckTimeSurvivedOverCurrentScoreU3Eb__0_m8DA272998DDA0EFC0A0FA233DE3E14D690CCABD9,
	U3CCheckTimeSurvivedOverCurrentScoreU3Ed__13__ctor_mC125234B920EDD35FA13597070656207C0559A01,
	U3CCheckTimeSurvivedOverCurrentScoreU3Ed__13_System_IDisposable_Dispose_m5C4AD400A1C79CDE7A58680CDD80589512DF527A,
	U3CCheckTimeSurvivedOverCurrentScoreU3Ed__13_MoveNext_m8D8A608ADE7672C83E94BD5C88E1D3E83AF3BAF6,
	U3CCheckTimeSurvivedOverCurrentScoreU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m79CDF24809F8E9C369BC5D7C6C5D6E52F49464A0,
	U3CCheckTimeSurvivedOverCurrentScoreU3Ed__13_System_Collections_IEnumerator_Reset_mA71171569E9A5B046E37F9679E3A6514B4AAFEAB,
	U3CCheckTimeSurvivedOverCurrentScoreU3Ed__13_System_Collections_IEnumerator_get_Current_mED6D775E0EAD9B2B09E6410C85BFC9E7852E6D10,
	U3CU3Ec__DisplayClass14_0__ctor_m990D1CE5144DB1D372A8CF0EFB65541C44A677A5,
	U3CU3Ec__DisplayClass14_0_U3CUpdateDamageDoneU3Eb__0_m15ECF55814497519998C606A19B0F9B13E511292,
	U3CUpdateDamageDoneU3Ed__14__ctor_mAB8EF0D4F0930E31C58ED12AFE14E283DDC7EE50,
	U3CUpdateDamageDoneU3Ed__14_System_IDisposable_Dispose_mF8D5C1001B4D15B1CE1F7795A5753AC7EF9911EF,
	U3CUpdateDamageDoneU3Ed__14_MoveNext_mAB7DE3DDFB1AAFBE4C7CD07B386DE3FDA46F5810,
	U3CUpdateDamageDoneU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF5C127E539E04AB52223DBC2368D5924E4E03973,
	U3CUpdateDamageDoneU3Ed__14_System_Collections_IEnumerator_Reset_m0D826E9EC709E9DA07E20F68E4D1B7D201161304,
	U3CUpdateDamageDoneU3Ed__14_System_Collections_IEnumerator_get_Current_m28E02CEDC813B4CDF8898076064DC9FDA23E56E6,
	U3CU3Ec__DisplayClass15_0__ctor_m42332C025BABA3F1486E25869316ED2D83C6D9AF,
	U3CU3Ec__DisplayClass15_0_U3CUpdateWaveSurvivedU3Eb__0_m5DCC9423DB65E6B5DE7D177D6361988F5C5792BF,
	U3CUpdateWaveSurvivedU3Ed__15__ctor_m32610F33973E8345603BD6EA5FA6009B3F3FBF79,
	U3CUpdateWaveSurvivedU3Ed__15_System_IDisposable_Dispose_mD827AC0451DBDBDC047E281486D890B27354253D,
	U3CUpdateWaveSurvivedU3Ed__15_MoveNext_mA53A84301D5743E88EB52DAE5B14A628778A03D1,
	U3CUpdateWaveSurvivedU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m54EA5333230F4F5CE9F3E1A7E97BFCA71F16B538,
	U3CUpdateWaveSurvivedU3Ed__15_System_Collections_IEnumerator_Reset_mAEF947D6C4ED3C809ECBEDFE1836E725B5D05A45,
	U3CUpdateWaveSurvivedU3Ed__15_System_Collections_IEnumerator_get_Current_mDC7061BB2AF7F2A042B2C0E6D4F137EC8F5F1AD3,
	U3CU3Ec__DisplayClass16_0__ctor_m16D38AD81F83DE7807FB730AF9F40135877739B3,
	U3CU3Ec__DisplayClass16_0_U3CUpdateTimeSurvivedU3Eb__0_m2A19E6E6EFB3E011DC505633755842FAFF693599,
	U3CUpdateTimeSurvivedU3Ed__16__ctor_mE7CA8949AA742F0CD2614EC3BF5905CEA3373323,
	U3CUpdateTimeSurvivedU3Ed__16_System_IDisposable_Dispose_mB43ADA3CF19E76A5808A135D3E7E3E0D03793A8B,
	U3CUpdateTimeSurvivedU3Ed__16_MoveNext_mADC6F67FD4D181C61C312E39A7A54BB6DC35D83A,
	U3CUpdateTimeSurvivedU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE11BE326E56200BFE14E5915BFEAB2B74C051FF2,
	U3CUpdateTimeSurvivedU3Ed__16_System_Collections_IEnumerator_Reset_m9C672B2F72A298CB037B3B5753468644270E4BEF,
	U3CUpdateTimeSurvivedU3Ed__16_System_Collections_IEnumerator_get_Current_m3E8EBFA8DC883686D75A92E8E7E3EDF97EDD4DD7,
	U3CU3Ec__DisplayClass17_0__ctor_m9A8346676BB7E1F7668C1BFC2220B3990B6C9123,
	U3CU3Ec__DisplayClass17_0_U3CLoadUserScoreFromDBU3Eb__0_mAF3BFC5497997AD1F5B95317443B86CF9537D016,
	U3CLoadUserScoreFromDBU3Ed__17__ctor_m8D1FA3BBE852BAE443A006877DE97924D717A842,
	U3CLoadUserScoreFromDBU3Ed__17_System_IDisposable_Dispose_m2D7F9A2A3A5BF15A09C5E667906D55A8F3A7243D,
	U3CLoadUserScoreFromDBU3Ed__17_MoveNext_m8634E0CB7D8E63023E80F2467790F4E7DFB11C80,
	U3CLoadUserScoreFromDBU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m930F67A39ADE639B8D30C6DB54037E755C365308,
	U3CLoadUserScoreFromDBU3Ed__17_System_Collections_IEnumerator_Reset_m19BD1A5B2D3986337D43651A6346B78AFA0EAB93,
	U3CLoadUserScoreFromDBU3Ed__17_System_Collections_IEnumerator_get_Current_m16A3FB960C8208C64ABA393EEA909C9C289F0481,
	U3CU3Ec__DisplayClass18_0__ctor_m66E344A88629D506AF23620ADF275531C3ED4683,
	U3CU3Ec__DisplayClass18_0_U3CLoadLeaderBoardDataU3Eb__0_mD5E2704551C12D12E5693B6415FF59CE623971D6,
	U3CLoadLeaderBoardDataU3Ed__18__ctor_mC41A8A5403C2606BB13F4B461653F8E5D36C1F2F,
	U3CLoadLeaderBoardDataU3Ed__18_System_IDisposable_Dispose_m1618C92B5A0462FA30052FFC0F1FCBF0F97A6875,
	U3CLoadLeaderBoardDataU3Ed__18_MoveNext_m791539A2DB2C21BC5C2D55FD5A0C51BAE36B6596,
	U3CLoadLeaderBoardDataU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBFA6427E3891D37646831F9AFA8290094EFA9D71,
	U3CLoadLeaderBoardDataU3Ed__18_System_Collections_IEnumerator_Reset_mEF596DF92F906E40A50D828F7AEA4E7897DE215C,
	U3CLoadLeaderBoardDataU3Ed__18_System_Collections_IEnumerator_get_Current_m8CDA43B0A3DCCC9938B4E6E503DE3A784B6FB011,
	U3CDownloadImageU3Ed__19__ctor_mA337C682C0E9601A354AF20655032AEA526D026C,
	U3CDownloadImageU3Ed__19_System_IDisposable_Dispose_mE416C30E9AE2D2DEE3C4D14332D65CF86208C5B5,
	U3CDownloadImageU3Ed__19_MoveNext_m6554A92C83B0A5952DBC30A26581E82038DF0380,
	U3CDownloadImageU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDB1F79F0F8367FFE5586098C13EB91E7547542EA,
	U3CDownloadImageU3Ed__19_System_Collections_IEnumerator_Reset_mA266BA2E8E8DCC326775AC8272102BB1B324ED9A,
	U3CDownloadImageU3Ed__19_System_Collections_IEnumerator_get_Current_mFD392F88C8F541BEC86F9DC21B847FACC19DB873,
	U3CChangeButtonColorU3Ed__21__ctor_m022B1F3DDDC026D92DE234A1607B268DF00F2C97,
	U3CChangeButtonColorU3Ed__21_System_IDisposable_Dispose_m3517C42530E1127D15045D7C88535291F122D6B0,
	U3CChangeButtonColorU3Ed__21_MoveNext_m7C06508FEE819A1040C3CDC0E2F39875A950467E,
	U3CChangeButtonColorU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8821EF38042F4839506600E9F1669248B0928E96,
	U3CChangeButtonColorU3Ed__21_System_Collections_IEnumerator_Reset_m3A87C825847F51A6BB2A1DB026FF22945CA831D0,
	U3CChangeButtonColorU3Ed__21_System_Collections_IEnumerator_get_Current_mE05A61B3BF11D846169720E6B7E6DAB5F7E79DB4,
	U3CWaitForUpdateUiU3Ed__27__ctor_mEFF8FCAE0E22B88642092D496E94249D3B9F9FB5,
	U3CWaitForUpdateUiU3Ed__27_System_IDisposable_Dispose_m19B749EC2C6F276D35A0BE03B51D69A659170E46,
	U3CWaitForUpdateUiU3Ed__27_MoveNext_m13DF403095CC52D7A94317E1A0D901A13CE89C8F,
	U3CWaitForUpdateUiU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m99553AC5B564ED4462C3587A1AE492105F8773D4,
	U3CWaitForUpdateUiU3Ed__27_System_Collections_IEnumerator_Reset_m269F0236C1C5426233ECF692D38DB744748508EB,
	U3CWaitForUpdateUiU3Ed__27_System_Collections_IEnumerator_get_Current_mB1C2059DD75E7B11DFA1E993B2BBA333AF79F63D,
	U3CActiveCookie30sU3Ed__19__ctor_m8FCF61703691001EFA7961AE2E1D787D16F7A5B5,
	U3CActiveCookie30sU3Ed__19_System_IDisposable_Dispose_m045AB7688AF359AF2B0E85847CB3ED26389AE618,
	U3CActiveCookie30sU3Ed__19_MoveNext_m2AC7A061189E8FCCDFA69816B62AF60328971AE6,
	U3CActiveCookie30sU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEF460E178959DB2B321C7DB1AABCB6B86E2D6829,
	U3CActiveCookie30sU3Ed__19_System_Collections_IEnumerator_Reset_m8854B0851AC842D51089315669AFF02D7908C1EC,
	U3CActiveCookie30sU3Ed__19_System_Collections_IEnumerator_get_Current_m440F33539D6609523F55870DB875411892221539,
	U3CActiveCandy30sU3Ed__21__ctor_m6125A56FB311570E4C1DA52533D5BC1E8D963429,
	U3CActiveCandy30sU3Ed__21_System_IDisposable_Dispose_m398275D5D386B790D02DCE00C807A13C41F141D4,
	U3CActiveCandy30sU3Ed__21_MoveNext_m72D01F843FD11DD442803FBE1F6D3C9C4545C667,
	U3CActiveCandy30sU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE97A411E62DD8FD2994CDA0381430C7B376D09FE,
	U3CActiveCandy30sU3Ed__21_System_Collections_IEnumerator_Reset_m0D06ED95FB91F1B91B0A9BC2A0ACB2557BCD6729,
	U3CActiveCandy30sU3Ed__21_System_Collections_IEnumerator_get_Current_mF71FC42683C0F4C24A8DD650B36D6B141CF43735,
	U3CWaitForPanelU3Ed__14__ctor_m59D042B5E4A18907C5B493929DE55BB2793A8A20,
	U3CWaitForPanelU3Ed__14_System_IDisposable_Dispose_m629FF274124309F78AEE0A0EE1E78820401E3659,
	U3CWaitForPanelU3Ed__14_MoveNext_m3C201F956F57B5772EBD5AEB3BB0107EFEE4A65C,
	U3CWaitForPanelU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1BA1E11BDC976FFDA71876728621CC3580B339C9,
	U3CWaitForPanelU3Ed__14_System_Collections_IEnumerator_Reset_mFDFA0E2253D02365F762B257186B1B188BA591BC,
	U3CWaitForPanelU3Ed__14_System_Collections_IEnumerator_get_Current_m7FF353409E4AB340F005DEE86648F7D8B53A47D0,
	U3CWaitForUnPauseU3Ed__15__ctor_m0FD75504981C15A71618791163C95DA6832B1558,
	U3CWaitForUnPauseU3Ed__15_System_IDisposable_Dispose_m1967E6755E3C99E168315655FDBC938D996A7C80,
	U3CWaitForUnPauseU3Ed__15_MoveNext_mC7C5AE8C9F075859493834CBAB151845C565F63D,
	U3CWaitForUnPauseU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3ABE4E9FEA6847127D2EBA79A3BDCC495BF3A886,
	U3CWaitForUnPauseU3Ed__15_System_Collections_IEnumerator_Reset_mF18699718458033B6427B047F0C6892055FFD52E,
	U3CWaitForUnPauseU3Ed__15_System_Collections_IEnumerator_get_Current_mA42AEF9003416FA995E1070577B3323FCD65A232,
	U3CU3Ec__cctor_m513A6A65465EE7880F12B2AF44B2717264E0604B,
	U3CU3Ec__ctor_mA948058D00B7A0FFDA0329A96D24D9F074C5242B,
	U3CU3Ec_U3CGetCompGameOverU3Eb__15_0_m20F15C994B7F75226F9CC655EC52E9FACF026B30,
	U3CU3Ec_U3CFindElementBuildU3Eb__22_0_m547CFF3D9472E416349F847DE2E3F335A2B5340B,
	U3CU3Ec_U3CFindElementBuildU3Eb__22_1_m415FA8E80E3F5752623D6E492F9E7207C38FEC8C,
	U3CU3Ec_U3CFindElementBuildU3Eb__22_2_mD0A3F49E0F79D431F4F522441E0B91E8AC803081,
	U3CU3Ec__DisplayClass21_0__ctor_m9AC962536759F39B88CF5CB33D14D82B1984668E,
	U3CU3Ec__DisplayClass21_0_U3CFindBuildU3Eb__0_m19E7DCACB759BF62E58D0B92AD4974EEFB37BD41,
	U3CU3Ec__DisplayClass21_0_U3CFindBuildU3Eb__1_mF2FC8C1952F1AAD93C7EA457C367E6570A5B83DE,
	U3CU3Ec__DisplayClass21_0_U3CFindBuildU3Eb__2_mBB8561A36050368B1E0A7E9CA6E747B98C89A62F,
	U3CU3Ec__DisplayClass21_0_U3CFindBuildU3Eb__3_m1D2543C1C5A011B6484F37A3828F7FD0A90C4C28,
	U3CU3Ec__DisplayClass21_0_U3CFindBuildU3Eb__4_mA0EFAA92951ADF1D319C7F9181625543136C3000,
	U3CU3Ec__DisplayClass21_0_U3CFindBuildU3Eb__5_mFABD8728047CE3F5A86651EE5437AAC16CF98781,
	U3CU3Ec__DisplayClass21_0_U3CFindBuildU3Eb__6_m18D32D35D8B4D714907057563F1BC7963F0E856F,
	U3CU3Ec__DisplayClass21_0_U3CFindBuildU3Eb__7_m468398931098D61CBAC2A54DE09B5F53D232DE61,
	U3CU3Ec__DisplayClass21_0_U3CFindBuildU3Eb__8_mA8542566E261C3F300D9FC82A91DF6AA59CE0DCD,
	U3CStartU3Ed__4__ctor_m4D65F4FC2207AE4B6BE963AF9B5EDC55C7E29B23,
	U3CStartU3Ed__4_System_IDisposable_Dispose_mFEE2ACED70A3D825988E28CC61FEF8DCD7660A5B,
	U3CStartU3Ed__4_MoveNext_m2F1A8053A32AD86DA80F86391EC32EDC1C396AEE,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF03E2A57659B8F598AC450183F55D43F903C0A1E,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m268BA538CF6812C56EB281C0CE29D5AA2E9A2CAB,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mFE9CA9F52F9AFA1E4C9DBADA2064F854D6931CFF,
	CharacterSelectionEvent__ctor_m06BC183AF31BA4A2055A44514BC3FF0539DD04C7,
	SpriteSelectionEvent__ctor_m0F760052E9A5AF44A7AF7AC006CB4B24809590F2,
	WordSelectionEvent__ctor_m106CDEB17C520C9D20CE7120DE6BBBDEDB48886C,
	LineSelectionEvent__ctor_m5D735FDA9B71B9147C6F791B331498F145D75018,
	LinkSelectionEvent__ctor_m7AB7977D0D0F8883C5A98DD6BB2D390BC3CAB8E0,
	U3CStartU3Ed__10__ctor_mBE5D8B4B98C372BD6DC3936437999DF3048DE3AB,
	U3CStartU3Ed__10_System_IDisposable_Dispose_m1F4180A7FDAE9AC5F11F30B67F813B3D7CD56D73,
	U3CStartU3Ed__10_MoveNext_mF3B1D38CD37FD5187C3192141DB380747C66AD3C,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m22B2975D42326E3DE437174BBE4F0E8790CB6591,
	U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m514DA545ECA10AA62BFC239BB582FF0017B91D2B,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m6904A738F04118BA24E473F1809B3D69E711627E,
	U3CStartU3Ed__10__ctor_m653B757B49A674E239C804FFF4EDEF325B5DB651,
	U3CStartU3Ed__10_System_IDisposable_Dispose_mAD0BC985E1E01C8369A7A955A60D3E545B0B3561,
	U3CStartU3Ed__10_MoveNext_mF9B2C8D290035BC9A79C4347772B5B7B9D404DE1,
	U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF438D42D7C5A811FF9A283F8780F015FF55826CD,
	U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m65545EB5B6DDDDAB5ACF92600137C1C754B322BA,
	U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mD4523BE1D1014AAF1C0709EB767C0B0F47D739D2,
	U3CAnimatePropertiesU3Ed__6__ctor_mFAC0F8A7368D9D35AD2780C118E13414DA79B56A,
	U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_m628EECBFCBC49087298185F17BC2AE7A73FC8A7B,
	U3CAnimatePropertiesU3Ed__6_MoveNext_m8A58CCFDAE59F55AB1BB2C103800886E62C807CF,
	U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0742A0FA62C518EBD119ED5FEBF849F918E12390,
	U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_mC076F281DD17668D6CE42EB04EA974DE1FFB3F6B,
	U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_mA585E786DA86D2589EBCA029AD9E64E8B5135362,
	U3CWarpTextU3Ed__7__ctor_mEAD3C39209B75514446A44B6C2FA76F8097EBD6F,
	U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m32AA7120BE15547799BDC515FA3486488952BDCF,
	U3CWarpTextU3Ed__7_MoveNext_mB832E4A3DFDDFECC460A510BBC664F218B3D7FCF,
	U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3CF506B740C16FEDE58B6E1BE4556CAC2C6AEAD1,
	U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_mDDCDBB794F21DF70178A75747D8D8398F38A9C2F,
	U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m6D89CF1910F76577E23C924B535E93FD1B01CF0A,
	U3CStartU3Ed__4__ctor_mF3575DBEBF4F153F6A899AF3362940298C11B629,
	U3CStartU3Ed__4_System_IDisposable_Dispose_m6DABFDBC2A313BF6DD90AA1C43B4EF9D6249CBE9,
	U3CStartU3Ed__4_MoveNext_mEF7A3215376BDFB52C3DA9D26FF0459074E89715,
	U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m66DC06D52865D30DAA78DBD70B6554D13E2EA70B,
	U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_mE854A2CECA0E01F3C2D90DA4F720CF0F387994F8,
	U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m77D3936B94684C817EE9F6D2C903238A457D5261,
	U3CRevealCharactersU3Ed__7__ctor_mD45A85F5F50909F70C80AC8CE460F4FD261CDE9D,
	U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m85F270FDC11A4D79E9CF47AADC9FA1FBF032F86C,
	U3CRevealCharactersU3Ed__7_MoveNext_mA9A6555E50889A7AA73F20749F25989165023DE3,
	U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5BBAE6868EB7F1C11BE5DF001641E18C9D625F83,
	U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_mA9F4893381AB24E01EAEBFC38A88AF363A9A0691,
	U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_mFD0B7538B1A650FB389FFE9296B0E51AEA5B6B6F,
	U3CRevealWordsU3Ed__8__ctor_m22FF9E770988107A928C5D1EA639F60239BFFEF0,
	U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m3EAF6EF4A8C99A71FEED251BF3F28A26BF6AD7F8,
	U3CRevealWordsU3Ed__8_MoveNext_m3811753E2384D4CBBAD6BD712EBA4FAF00D73210,
	U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3F93E4AA532F15D52D68B7121805C014AB2D7FB,
	U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_mCDC8982948ED5F7743567569CA1D4A354218714F,
	U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_mF39E492D576810F58DB9031556CFD6806FD32E27,
	U3CDisplayTextMeshProFloatingTextU3Ed__15__ctor_m3F7F9561735FF8403090A93E708E781B3997B81B,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_IDisposable_Dispose_m6F24E1DC5777D4682375D8742BC9272EE6D4EA2C,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_MoveNext_m04B1BB9074CBA76BA7F63849721F4CAEA1045A28,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m14A7C9DEBD0FF0C7A6979AFEC78B0BEE9E4C3963,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_Reset_mC0D5548A92F57550F60EE76E17B58125B57B94A8,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_get_Current_mD3CBFD6F8558EF09F2B4A3D8D1B76BD7FDB3A98B,
	U3CDisplayTextMeshFloatingTextU3Ed__16__ctor_m0466586E44A01852547C6CECCA91C14673B98C20,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_IDisposable_Dispose_m8730A3A2EFBF759A51F0E5330C3BBF8EFC1736A3,
	U3CDisplayTextMeshFloatingTextU3Ed__16_MoveNext_m93E19CF96C4EBC57CC4E03E2D3D0B56B3E92A36A,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA4076BE73343A473EFE46E73ABA5DE66CC797598,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_Reset_mC33B355BB392C5A52DCA0AAF2588E1E78AF6D3A5,
	U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_get_Current_m28CCE2E51295AD5F368C504BADDDFF008C2CC0A6,
	U3CAnimateVertexColorsU3Ed__3__ctor_m0038B1054BCC928D35F8C0021ED7D2E1C533E35F,
	U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_m5BAD394A0B09B3E0FF19E91521E02C2B3ADD6007,
	U3CAnimateVertexColorsU3Ed__3_MoveNext_m2842AF5B12AFC17112D1AE75E46AB1B12776D2A6,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAE810D7968957A09C88E61C29DAAEC68E4AF1E51,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_m72ABAC50E9E4D972FB44CAFF387F3E23FEC5D932,
	U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_m8DE595A1D01F3A507A356F8BCE020D0851412B52,
	U3CAnimateVertexColorsU3Ed__11__ctor_m0222C3457F5ACA497FE3A8EC829DE4AD11A169F8,
	U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m717E79A39A8161ADDA9E62F7CDFB67B8F2D65099,
	U3CAnimateVertexColorsU3Ed__11_MoveNext_m169A75C7E2147372CB933520B670AF77907C1C6B,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m920EBA43D59A1A88E27FED92CF0AC0DF90179479,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m6C045A6DD0B60F1512457448E615877EAB86D75D,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m017345FE58B497EAFC9D0CB1FB733F76EB3449AF,
	U3CAnimateVertexColorsU3Ed__11__ctor_m92612416BEC0EBF5E9849FB603629C0F2F95FEF2,
	U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m77D966994D4717EAFD8EFE169F3E8A4EE8B05B81,
	U3CAnimateVertexColorsU3Ed__11_MoveNext_mCEEDE03667D329386BB4AE7B8252B7A9B54F443F,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDCCD3645ACF9B18D760B341C863F853996FA9BCE,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_mB9444B5E58B4E97105C447F547C0F74C51BCFBFA,
	U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m5CC464185D5C0251C6206E20AFFA681BA0525A7E,
	U3CAnimateVertexColorsU3Ed__10__ctor_m3E3B1D286DEBED2BC028AD490308568B930C3760,
	U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m0F9D4B2A6ED0500C2120DBA29932CC279E8908DC,
	U3CAnimateVertexColorsU3Ed__10_MoveNext_m0534E436514E663BF024364E524EE5716FF15C8E,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF2A8BAFC0261ACBBE8EEA74DE4B498D30C68AE3D,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m5F12D7C6F2523BEB6326FE49AE972116D6157CBB,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_mB9C12786011A3B78517AEFAE8D5A78B95A4219AD,
	U3CU3Ec__DisplayClass10_0__ctor_m4EC5F8042B5AC645EA7D0913E50FD22144DBD348,
	U3CU3Ec__DisplayClass10_0_U3CAnimateVertexColorsU3Eb__0_m959A7E1D6162B5AAF28B953AFB04D7943BCAB107,
	U3CAnimateVertexColorsU3Ed__10__ctor_m537CF0A5ADF5BBC2DF784BF526E3F32EB528E1B2,
	U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_mF0CD944C393771101D2718A660E1DFF22385819F,
	U3CAnimateVertexColorsU3Ed__10_MoveNext_m8340EFEB2B2CF6EA1545CFB6967968CA171FEE66,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m093E922A5A046910B2A7EE826D804CA3064A7BD9,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_mDED63FD52741D58D8D5A3E53415F91B6560F680C,
	U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m933EBF7C4300C7C1E3EE60E4741110025877CA5F,
	U3CWarpTextU3Ed__8__ctor_m9FADE04C27A0034C5A276232FCA187AECDC6BF49,
	U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m5AB0DCF4B3DE6C290487F3DBBC7BAE931130DE60,
	U3CWarpTextU3Ed__8_MoveNext_m9C83C8455FF8ADFBAA8D05958C3048612A96EB84,
	U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D5776AF80C4227401ADD7E13D98F2530CB9E7A1,
	U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_m4F2FBBE7B375E6FC837736B4275A7B601A01F535,
	U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m01FA935D93D737C9DDD88A051F7DFC393DD7BD25,
	U3CU3Ec__cctor_m6CFB8B055EB055FD65E19623424D0978714EBAF5,
	U3CU3Ec__ctor_m5E2634FAC998262A2F28D52C5561A946470F7AFB,
	U3CU3Ec_U3CSellHeroU3Eb__20_0_m60AC6C48E612C15ED96B4B421C09A3FD3A9B0A91,
	Enumerator_get_IsValid_m078841FA11B23FAA03C8C0F0F5CFE1C7A33CC267,
	Enumerator__ctor_m0A9EDBF3F33AA5FAE87C204FA885D965D16E8A44,
	Enumerator__ctor_mDE6D3B06DF10240F0C637131119C8B09CE9EC4DC,
	Enumerator_get_Current_m6C700A8C86DAB32F9FE845C3BE1E35D17757C65C,
	Enumerator_MoveNext_m095925BE3D881DF1E015997DBEAFE0851722E53E,
	ValueEnumerator__ctor_m1FF9C4191C706E48C3F9A191D0CE02F9BECDCE45,
	ValueEnumerator__ctor_m8143FFB5AD9AB3936DF1A59E3492C2CF81793AF5,
	ValueEnumerator__ctor_mE92051B3948C6A45D4F9265EA6D8329D70649D06,
	ValueEnumerator_get_Current_mAB38FFFD4C1293C61EE73C825D85B7258B61961C,
	ValueEnumerator_MoveNext_mE68E69AFB9D6FA2B41F55B066F3295490EED5589,
	ValueEnumerator_GetEnumerator_mF841522782073D8D5F9782A37B2487BEA63E5E09,
	KeyEnumerator__ctor_m6E59374D6FF2E994DFB9BFCA3759CC335CA48D23,
	KeyEnumerator__ctor_mB67A74FB33429333A208FA269F011122C1AB6395,
	KeyEnumerator__ctor_m5F83F3F252DE00DB6AC2AE59FD501F921C56F224,
	KeyEnumerator_get_Current_mAC153090E0D57451FE61CA3651CF32A4CE21477C,
	KeyEnumerator_MoveNext_mFF5FB4ECB623B58733E9B85846A296853462F2F4,
	KeyEnumerator_GetEnumerator_m4534071ECBA3E7165BB0DC642EA9E827A29F5A87,
	LinqEnumerator__ctor_m711F6A5F82CC6093E5245A51BFEF95B0355E2FAC,
	LinqEnumerator_get_Current_m7F293F41FED328A040E5BC71C7F4560C3B8C295C,
	LinqEnumerator_System_Collections_IEnumerator_get_Current_m36203F845AA95DEB756D145E03469EF077B79BF1,
	LinqEnumerator_MoveNext_m547BA7FD679ED14C3E698763EBEC58333194B9EE,
	LinqEnumerator_Dispose_mF5270BF1DEAB954E8FC65435DF6D6FD5DBE1C21F,
	LinqEnumerator_GetEnumerator_m356D1E4E1EC2D215962F677F3C6AE538E3CF9A20,
	LinqEnumerator_Reset_mCC3E0E49D5343E81C63A2B4A28A7268F628A2522,
	LinqEnumerator_System_Collections_IEnumerable_GetEnumerator_m713D8655544E9FC2911762CD17D215B5763AE735,
	U3Cget_ChildrenU3Ed__39__ctor_m0BF33D6A9ACF56C849BB64AB175B43D7692499A8,
	U3Cget_ChildrenU3Ed__39_System_IDisposable_Dispose_mFD004DCB3358CE2D7764E776A031A077B21BEB29,
	U3Cget_ChildrenU3Ed__39_MoveNext_m858F052BAF7771FE605FCE68EC05F97FA5143538,
	U3Cget_ChildrenU3Ed__39_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_mB88A038112706EAD24B23A14267B794021C761C2,
	U3Cget_ChildrenU3Ed__39_System_Collections_IEnumerator_Reset_m716ACA15194120CF8192937025FD3D4D7F8C710F,
	U3Cget_ChildrenU3Ed__39_System_Collections_IEnumerator_get_Current_m831C58F860DBA03146F00BB7DC77DD1852943881,
	U3Cget_ChildrenU3Ed__39_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_mC22210E7538A1A259E64227EEAC5440FE868ED60,
	U3Cget_ChildrenU3Ed__39_System_Collections_IEnumerable_GetEnumerator_mDB57DC3D1B30B06919F6F3985E3B3564974C1CAA,
	U3Cget_DeepChildrenU3Ed__41__ctor_mAF92DE35182876CDDB7F9E2831AA0078B111A5F9,
	U3Cget_DeepChildrenU3Ed__41_System_IDisposable_Dispose_m0C1243483A3118059E0FB871E404B606DD55CBBD,
	U3Cget_DeepChildrenU3Ed__41_MoveNext_mBC10790834A0F6E35D8D274754EF1C77A320E391,
	U3Cget_DeepChildrenU3Ed__41_U3CU3Em__Finally1_m90DA0BA712B5459B1157239B9995E01820163928,
	U3Cget_DeepChildrenU3Ed__41_U3CU3Em__Finally2_mB8A69D17B39DA453A0811F66B86B5426EF3C74DA,
	U3Cget_DeepChildrenU3Ed__41_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_mF12A4534E7D4122F67B420801B2522BC9F839868,
	U3Cget_DeepChildrenU3Ed__41_System_Collections_IEnumerator_Reset_m3F120F44D1BEB26137AF14E11165003EF8E18190,
	U3Cget_DeepChildrenU3Ed__41_System_Collections_IEnumerator_get_Current_mF43CD05D3BF0CD3E76A265F0A684D43EFA4F22EC,
	U3Cget_DeepChildrenU3Ed__41_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_mAD435B3462420F0485567562B0FF3F1DB60B6AF8,
	U3Cget_DeepChildrenU3Ed__41_System_Collections_IEnumerable_GetEnumerator_m554A68924D4655591E09B01C0EFF3A9204E5E117,
	U3Cget_ChildrenU3Ed__22__ctor_m95EBCC8BC9794B6D9F4C6B5885704BF181FC9DBF,
	U3Cget_ChildrenU3Ed__22_System_IDisposable_Dispose_mCBFE04EF85659884667F66453D36323A90F4A37E,
	U3Cget_ChildrenU3Ed__22_MoveNext_m465E0B8D3164E27BE47D02A0ACC56B5B795DBB95,
	U3Cget_ChildrenU3Ed__22_U3CU3Em__Finally1_mCA65F3342A396F12C190A1654CC8019248A0BAB4,
	U3Cget_ChildrenU3Ed__22_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_mA7F43EFC69B364F48AC29BF1C37A5001990511FE,
	U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerator_Reset_m531EF5FB0BD16A29769CBDE11FDF26AEC2D55F5E,
	U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerator_get_Current_m9C20E2FAE0940D92F51C5E1C9D804E406BF2F34F,
	U3Cget_ChildrenU3Ed__22_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m7F40779C5E4EF43C9898EFDECF0FA816A7E5DCFA,
	U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerable_GetEnumerator_mCC77091552DD207B63E4B7C2B33693B4467ED68D,
	U3CU3Ec__DisplayClass21_0__ctor_mC0EC999DE1ACB8335F31D6D652C0379FF2D065A6,
	U3CU3Ec__DisplayClass21_0_U3CRemoveU3Eb__0_m4DAF41E69ADC8210F10A0E9353204C4C9BB593F0,
	U3Cget_ChildrenU3Ed__23__ctor_m24E00E4270F8E59C9ECF5B85CDB1A34683FC1141,
	U3Cget_ChildrenU3Ed__23_System_IDisposable_Dispose_m413B24FB7D7B0156DFC7E240DA8391C859440847,
	U3Cget_ChildrenU3Ed__23_MoveNext_m3AD86370936CBDF4689019E0369B72B54B4B4BE0,
	U3Cget_ChildrenU3Ed__23_U3CU3Em__Finally1_m86405C8AACDCB84C101AB4D4812392161C83D82C,
	U3Cget_ChildrenU3Ed__23_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_m226D4CC6A995605411240005C904F701A7E7D224,
	U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerator_Reset_m60B145CE869947C90DB4665A23EDDF48AE63AC25,
	U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerator_get_Current_m51D1A00926BFE5BDA287FE9A878C6E949F9D0DBB,
	U3Cget_ChildrenU3Ed__23_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_mBE8B0928518FD2517C5F344791F32EA64F13C45C,
	U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerable_GetEnumerator_m19CBB01715DEE09E679303048771E8ABC2A2D4C0,
	U3CDelayLaserPopUpU3Ed__28__ctor_m1DD030E25497BC7EAFFF48B12878BC3F6E42E867,
	U3CDelayLaserPopUpU3Ed__28_System_IDisposable_Dispose_m4ACA82D241FAA199208AD68B89AB6A6D558B6F7F,
	U3CDelayLaserPopUpU3Ed__28_MoveNext_mDE7E5B884ECE5D232BFB54FCFD9034013DD43E7C,
	U3CDelayLaserPopUpU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2836850B7B9251D61A015650525BA37CFEE489AC,
	U3CDelayLaserPopUpU3Ed__28_System_Collections_IEnumerator_Reset_m418F0BB7A31FD5BEAFECE3CA6971390F3E76542F,
	U3CDelayLaserPopUpU3Ed__28_System_Collections_IEnumerator_get_Current_m1E4F5A5594B28D5034EFE6A5E55EEC44FC0C490B,
	U3CU3Ec__DisplayClass7_0__ctor_mF8395969F602712B1940D9A5208E54CCC007EAD1,
	U3CU3Ec__DisplayClass7_0_U3CGetHeroBluePrintU3Eb__0_m3EB2F62C4685E7DEDE93C2248CD6B69CAB4EB29C,
	U3CU3Ec__cctor_m4491C4A23EA840B8C61AFA8375CB12AEF6F638C6,
	U3CU3Ec__ctor_mF4208387AD3EAEC1BB7C3B22944DE7E6A1BE4ED2,
	U3CU3Ec_U3CAddCardToInventoryU3Eb__21_0_m0657414F89D8AB6E5CF7563BE7FCE7C201EF2CC0,
	U3CU3Ec_U3CCancelCardUsedU3Eb__25_1_m70E89BBD35DAF3418A612AF26236FECA85C3D167,
	U3CU3Ec__DisplayClass23_0__ctor_mC9B305238A92678949F2044FCE9213C7F988D0C6,
	U3CU3Ec__DisplayClass23_0_U3CAddCardU3Eb__0_m1AC7B81E60BC03D0551F39C31ACC0FB73E233A7A,
	U3CU3Ec__DisplayClass24_0__ctor_m3E6E520D7E39B9BB8356F800C5F52FB83B9903A1,
	U3CU3Ec__DisplayClass24_0_U3CGetCardU3Eb__0_m3FB036C259055D0782F68F34328B4F22F6023617,
	U3CU3Ec__DisplayClass25_0__ctor_m793F6F8EBF9F8306C210BA7FED94F50D49B2F481,
	U3CU3Ec__DisplayClass25_0_U3CCancelCardUsedU3Eb__0_m999307189E9BA8992DAE87D569137A2177822AA9,
	U3CU3Ec__DisplayClass26_0__ctor_mC9EF1121F9954DC8B7BF172EFDFDCB5369489FED,
	U3CU3Ec__DisplayClass26_0_U3CGetHeroU3Eb__0_mBB95FF796E6F6186B374AE81CB8E8DF6D504AC3E,
	U3CWaitForNoticeTimerOutU3Ed__43__ctor_mC64694756F43EEF9DCC1F4101737D83C1DC8BB93,
	U3CWaitForNoticeTimerOutU3Ed__43_System_IDisposable_Dispose_m01791A5F038C1BCF58742292247713EFA6C770CC,
	U3CWaitForNoticeTimerOutU3Ed__43_MoveNext_m37CBB672866BB1D4AC5A1DDD2B4FED3D9512F30D,
	U3CWaitForNoticeTimerOutU3Ed__43_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m03D682641A449C18CD1C00484E7509D3C8D843EC,
	U3CWaitForNoticeTimerOutU3Ed__43_System_Collections_IEnumerator_Reset_mDDA5A5A3E227742BEFD8545DFC4EDA5B33430563,
	U3CWaitForNoticeTimerOutU3Ed__43_System_Collections_IEnumerator_get_Current_mB7841FEE52274FEA64951C4EF021BC1825F42031,
	U3CWaitForNoticeOutU3Ed__46__ctor_m6F5A034947277CE289E43F8D368B15C565DA34DA,
	U3CWaitForNoticeOutU3Ed__46_System_IDisposable_Dispose_m5E3269113CF4303B8B8D4081E875B335EB3FEE1C,
	U3CWaitForNoticeOutU3Ed__46_MoveNext_m45475A48061520D331EF00054E79795D11B0B591,
	U3CWaitForNoticeOutU3Ed__46_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m389C85183678A455CB9EFF03218F4F9459269270,
	U3CWaitForNoticeOutU3Ed__46_System_Collections_IEnumerator_Reset_m84D84F466AE4D9EC09F0827B66E1C4085133898A,
	U3CWaitForNoticeOutU3Ed__46_System_Collections_IEnumerator_get_Current_mE2FCDD664F3D5710AF71599273562A7143A1B9E9,
	U3CU3Ec__DisplayClass53_0__ctor_mCB48B14B6E4EB303A05D36AE14EB70BE8D1D03B0,
	U3CU3Ec__DisplayClass53_0_U3CRemoveChainSkillTextU3Eb__0_mC29500B91D3208446E347FEF32EFF8E8E215F397,
	U3CNextEnemyU3Ed__26__ctor_mF7F906DB2AE8D88AD0A7E5231306EFF4CD401EE0,
	U3CNextEnemyU3Ed__26_System_IDisposable_Dispose_m51376AE841CDA01FEDF72193251CD600592192CA,
	U3CNextEnemyU3Ed__26_MoveNext_mAFF9FB7DE510E6F97E8346DCF1859BA14FF15E78,
	U3CNextEnemyU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0644F6F37C2E4EB196F8C3EE1EAE560375A86B61,
	U3CNextEnemyU3Ed__26_System_Collections_IEnumerator_Reset_mC1135533A36ACAF17E80743D6142CD29C926476B,
	U3CNextEnemyU3Ed__26_System_Collections_IEnumerator_get_Current_m2FF04E7D947B7C1A1ABCB509822D255ADEE518EC,
	U3CNextHeroU3Ed__27__ctor_m223862A8FD430B9194B60EB590B188E6761543A9,
	U3CNextHeroU3Ed__27_System_IDisposable_Dispose_m5E5704CA63BBBD3107B59C4A610ABEFBE5175BEC,
	U3CNextHeroU3Ed__27_MoveNext_m5A5751DC419DBB8D53891D47F442C31D73549C95,
	U3CNextHeroU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0AF8B7EC64336EA9C996CD13B146749D3EA9816C,
	U3CNextHeroU3Ed__27_System_Collections_IEnumerator_Reset_mADB82B690752F5485FD704C02A3BBFF637267083,
	U3CNextHeroU3Ed__27_System_Collections_IEnumerator_get_Current_m94202AE088D04C101BF4F74AB5F76AD0286EA924,
	U3CDisableInfoU3Ed__33__ctor_m60DCAC3ED8F8B0CC7BCBE7E03BABF63357A6FA6D,
	U3CDisableInfoU3Ed__33_System_IDisposable_Dispose_m932AC83042484FA097910F368C56EC1CC724865F,
	U3CDisableInfoU3Ed__33_MoveNext_m953A4EE1471D47663CB0077A7CA42BD3BD106C8D,
	U3CDisableInfoU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m370655C1AD58BE840BC850118005790511801227,
	U3CDisableInfoU3Ed__33_System_Collections_IEnumerator_Reset_m028A5C860CB4B798DDF150CFF80F4E64A1F419E9,
	U3CDisableInfoU3Ed__33_System_Collections_IEnumerator_get_Current_mC802088660CAAE6D1999E1F89950C8372657FF82,
	U3CFireWorkClickedU3Ed__34__ctor_m7A1B53D72F6553C8AEF3C2D73B1DC7C5E9E56B91,
	U3CFireWorkClickedU3Ed__34_System_IDisposable_Dispose_m46CCAE3CD88625D4F9544A098BCD88E0AB58DD9A,
	U3CFireWorkClickedU3Ed__34_MoveNext_mB9F9F4B2B8DA0F6EF84CD1C751D1567ADFC11C96,
	U3CFireWorkClickedU3Ed__34_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF91C50E982052F7A84C122811F02FB459B0E63E1,
	U3CFireWorkClickedU3Ed__34_System_Collections_IEnumerator_Reset_mFCCA5DE1786D2F6C82570827018FC03D4E1773F8,
	U3CFireWorkClickedU3Ed__34_System_Collections_IEnumerator_get_Current_mD0C5FEE89BAAAEFDD74726E93B4C290812B0764C,
	U3CCookieClickedU3Ed__35__ctor_m0B575DE0D60FB548252B06CE74082E1EBDF42D57,
	U3CCookieClickedU3Ed__35_System_IDisposable_Dispose_mA29E952E03BC1A952D9B75AF867842C8C285DADA,
	U3CCookieClickedU3Ed__35_MoveNext_mA7BA31A2952F719DC9FB167CCC07036F2B55B5D0,
	U3CCookieClickedU3Ed__35_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB2959A148D9B6ECFE069BB4DA16D939690F29580,
	U3CCookieClickedU3Ed__35_System_Collections_IEnumerator_Reset_m26D0EA370FA27109A9812968AF7E2C4D9C53C38E,
	U3CCookieClickedU3Ed__35_System_Collections_IEnumerator_get_Current_mD356438C0477B5CAC2CE60DCE85ACC6FAFA10FEC,
	U3CCandyClickedU3Ed__36__ctor_m4A8D08E3E4BCF74EC885E69C8CC1CF1F0F4AACD4,
	U3CCandyClickedU3Ed__36_System_IDisposable_Dispose_m5A18D55FE5C43EE1DD366017D649C02BB71BEA7B,
	U3CCandyClickedU3Ed__36_MoveNext_m48818AF3002C9EF3AE68ADE7DACCD5565D78AA3D,
	U3CCandyClickedU3Ed__36_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m35E28353074F1165B229CE56346CB372606A7FBC,
	U3CCandyClickedU3Ed__36_System_Collections_IEnumerator_Reset_m85D7FF94205CA8A2640EB611D6CECFC4FFBF9DB7,
	U3CCandyClickedU3Ed__36_System_Collections_IEnumerator_get_Current_m8AE9C3DD85D994C5A0EACACEC1365F49104F17AC,
	U3CScreenShotClickedU3Ed__37__ctor_m4DF76FA6E783CE5C95EC9174F493FE6E32391324,
	U3CScreenShotClickedU3Ed__37_System_IDisposable_Dispose_mF247A4A440E9BCC6723DBD6F4829A6A85901EC04,
	U3CScreenShotClickedU3Ed__37_MoveNext_m3EC6F91524753A029FFA3B53C3888E051F281D5F,
	U3CScreenShotClickedU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m672E78976F63D8246C55DFA344FBD7F0B5E7062B,
	U3CScreenShotClickedU3Ed__37_System_Collections_IEnumerator_Reset_m4926A46FCFF998299A791CD8D19835398F2D0851,
	U3CScreenShotClickedU3Ed__37_System_Collections_IEnumerator_get_Current_m2A8F29B42E02863B6334D1099147F59F611D35AA,
	U3CLeaderBoardClickedU3Ed__38__ctor_mE7E26DB8380A3579B64BC586E0253B6F18867F6F,
	U3CLeaderBoardClickedU3Ed__38_System_IDisposable_Dispose_m7CB383587DF40BD43652C5FC78C26B604462A95A,
	U3CLeaderBoardClickedU3Ed__38_MoveNext_mB9232F2C56612D985777768BA921D9D2B7EC8696,
	U3CLeaderBoardClickedU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9EC830344B8F7C475D00C335A2411922FFD4E25C,
	U3CLeaderBoardClickedU3Ed__38_System_Collections_IEnumerator_Reset_m576E4DED7EC5D84BDEEFDDB2EE94C963C2915406,
	U3CLeaderBoardClickedU3Ed__38_System_Collections_IEnumerator_get_Current_m5C0AFE5F19E6B3B18AF2CAB6BA66B991D14EF762,
};
extern void TouchActions__ctor_m5F79CEC71E38F8C83D916CB78192AC260AF83592_AdjustorThunk (void);
extern void TouchActions_get_PrimaryContect_mB54C668A5C15D5F965ABE7CEB759E234361C7063_AdjustorThunk (void);
extern void TouchActions_get_PrimaryPosition_mAA3C5A043DD92BB973B901DBFE311D2B8A4112D8_AdjustorThunk (void);
extern void TouchActions_Get_mC8FC631CF4A79917E0F0D6303C9AFD669CA2F1C8_AdjustorThunk (void);
extern void TouchActions_Enable_m9ABE56F3809AD02B52FEB58E0822CBFD13E15988_AdjustorThunk (void);
extern void TouchActions_Disable_m237F8450B651D42320F5C745C07A4BD2EF0027C6_AdjustorThunk (void);
extern void TouchActions_get_enabled_mCAA5D8F20D1CAB85BE56F95F033AEC090EC3A650_AdjustorThunk (void);
extern void TouchActions_SetCallbacks_m948F215A5E7BF2316FF838E0BE421F42F6D81412_AdjustorThunk (void);
extern void Enumerator_get_IsValid_m078841FA11B23FAA03C8C0F0F5CFE1C7A33CC267_AdjustorThunk (void);
extern void Enumerator__ctor_m0A9EDBF3F33AA5FAE87C204FA885D965D16E8A44_AdjustorThunk (void);
extern void Enumerator__ctor_mDE6D3B06DF10240F0C637131119C8B09CE9EC4DC_AdjustorThunk (void);
extern void Enumerator_get_Current_m6C700A8C86DAB32F9FE845C3BE1E35D17757C65C_AdjustorThunk (void);
extern void Enumerator_MoveNext_m095925BE3D881DF1E015997DBEAFE0851722E53E_AdjustorThunk (void);
extern void ValueEnumerator__ctor_m1FF9C4191C706E48C3F9A191D0CE02F9BECDCE45_AdjustorThunk (void);
extern void ValueEnumerator__ctor_m8143FFB5AD9AB3936DF1A59E3492C2CF81793AF5_AdjustorThunk (void);
extern void ValueEnumerator__ctor_mE92051B3948C6A45D4F9265EA6D8329D70649D06_AdjustorThunk (void);
extern void ValueEnumerator_get_Current_mAB38FFFD4C1293C61EE73C825D85B7258B61961C_AdjustorThunk (void);
extern void ValueEnumerator_MoveNext_mE68E69AFB9D6FA2B41F55B066F3295490EED5589_AdjustorThunk (void);
extern void ValueEnumerator_GetEnumerator_mF841522782073D8D5F9782A37B2487BEA63E5E09_AdjustorThunk (void);
extern void KeyEnumerator__ctor_m6E59374D6FF2E994DFB9BFCA3759CC335CA48D23_AdjustorThunk (void);
extern void KeyEnumerator__ctor_mB67A74FB33429333A208FA269F011122C1AB6395_AdjustorThunk (void);
extern void KeyEnumerator__ctor_m5F83F3F252DE00DB6AC2AE59FD501F921C56F224_AdjustorThunk (void);
extern void KeyEnumerator_get_Current_mAC153090E0D57451FE61CA3651CF32A4CE21477C_AdjustorThunk (void);
extern void KeyEnumerator_MoveNext_mFF5FB4ECB623B58733E9B85846A296853462F2F4_AdjustorThunk (void);
extern void KeyEnumerator_GetEnumerator_m4534071ECBA3E7165BB0DC642EA9E827A29F5A87_AdjustorThunk (void);
static Il2CppTokenAdjustorThunkPair s_adjustorThunks[25] = 
{
	{ 0x060003C6, TouchActions__ctor_m5F79CEC71E38F8C83D916CB78192AC260AF83592_AdjustorThunk },
	{ 0x060003C7, TouchActions_get_PrimaryContect_mB54C668A5C15D5F965ABE7CEB759E234361C7063_AdjustorThunk },
	{ 0x060003C8, TouchActions_get_PrimaryPosition_mAA3C5A043DD92BB973B901DBFE311D2B8A4112D8_AdjustorThunk },
	{ 0x060003C9, TouchActions_Get_mC8FC631CF4A79917E0F0D6303C9AFD669CA2F1C8_AdjustorThunk },
	{ 0x060003CA, TouchActions_Enable_m9ABE56F3809AD02B52FEB58E0822CBFD13E15988_AdjustorThunk },
	{ 0x060003CB, TouchActions_Disable_m237F8450B651D42320F5C745C07A4BD2EF0027C6_AdjustorThunk },
	{ 0x060003CC, TouchActions_get_enabled_mCAA5D8F20D1CAB85BE56F95F033AEC090EC3A650_AdjustorThunk },
	{ 0x060003CE, TouchActions_SetCallbacks_m948F215A5E7BF2316FF838E0BE421F42F6D81412_AdjustorThunk },
	{ 0x060004F6, Enumerator_get_IsValid_m078841FA11B23FAA03C8C0F0F5CFE1C7A33CC267_AdjustorThunk },
	{ 0x060004F7, Enumerator__ctor_m0A9EDBF3F33AA5FAE87C204FA885D965D16E8A44_AdjustorThunk },
	{ 0x060004F8, Enumerator__ctor_mDE6D3B06DF10240F0C637131119C8B09CE9EC4DC_AdjustorThunk },
	{ 0x060004F9, Enumerator_get_Current_m6C700A8C86DAB32F9FE845C3BE1E35D17757C65C_AdjustorThunk },
	{ 0x060004FA, Enumerator_MoveNext_m095925BE3D881DF1E015997DBEAFE0851722E53E_AdjustorThunk },
	{ 0x060004FB, ValueEnumerator__ctor_m1FF9C4191C706E48C3F9A191D0CE02F9BECDCE45_AdjustorThunk },
	{ 0x060004FC, ValueEnumerator__ctor_m8143FFB5AD9AB3936DF1A59E3492C2CF81793AF5_AdjustorThunk },
	{ 0x060004FD, ValueEnumerator__ctor_mE92051B3948C6A45D4F9265EA6D8329D70649D06_AdjustorThunk },
	{ 0x060004FE, ValueEnumerator_get_Current_mAB38FFFD4C1293C61EE73C825D85B7258B61961C_AdjustorThunk },
	{ 0x060004FF, ValueEnumerator_MoveNext_mE68E69AFB9D6FA2B41F55B066F3295490EED5589_AdjustorThunk },
	{ 0x06000500, ValueEnumerator_GetEnumerator_mF841522782073D8D5F9782A37B2487BEA63E5E09_AdjustorThunk },
	{ 0x06000501, KeyEnumerator__ctor_m6E59374D6FF2E994DFB9BFCA3759CC335CA48D23_AdjustorThunk },
	{ 0x06000502, KeyEnumerator__ctor_mB67A74FB33429333A208FA269F011122C1AB6395_AdjustorThunk },
	{ 0x06000503, KeyEnumerator__ctor_m5F83F3F252DE00DB6AC2AE59FD501F921C56F224_AdjustorThunk },
	{ 0x06000504, KeyEnumerator_get_Current_mAC153090E0D57451FE61CA3651CF32A4CE21477C_AdjustorThunk },
	{ 0x06000505, KeyEnumerator_MoveNext_mFF5FB4ECB623B58733E9B85846A296853462F2F4_AdjustorThunk },
	{ 0x06000506, KeyEnumerator_GetEnumerator_m4534071ECBA3E7165BB0DC642EA9E827A29F5A87_AdjustorThunk },
};
static const int32_t s_InvokerIndices[1414] = 
{
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2055,
	2822,
	2822,
	2822,
	1326,
	1313,
	4359,
	426,
	4024,
	4024,
	2822,
	4494,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	4467,
	4391,
	2822,
	2822,
	2822,
	2822,
	2768,
	2822,
	2822,
	2822,
	2768,
	2822,
	2822,
	2768,
	2822,
	4467,
	4391,
	2257,
	2257,
	2257,
	2257,
	2822,
	2822,
	2822,
	2822,
	2331,
	2331,
	2817,
	2822,
	2331,
	2331,
	2768,
	2822,
	2822,
	2656,
	2162,
	2654,
	2160,
	2665,
	1933,
	2768,
	2768,
	2822,
	2822,
	2884,
	2822,
	2822,
	2822,
	1357,
	1357,
	2822,
	2300,
	2822,
	4000,
	2822,
	2822,
	921,
	2822,
	2822,
	2768,
	2822,
	2822,
	2822,
	2822,
	2257,
	2257,
	2822,
	2768,
	2822,
	2822,
	2822,
	2768,
	2257,
	4467,
	4391,
	2822,
	2822,
	516,
	2822,
	2278,
	2822,
	516,
	2822,
	2278,
	2822,
	516,
	2822,
	2278,
	2822,
	516,
	2822,
	2278,
	2822,
	516,
	2822,
	2278,
	2822,
	516,
	2822,
	2278,
	2822,
	2822,
	2822,
	2278,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	1744,
	2822,
	763,
	1326,
	2822,
	1326,
	763,
	763,
	2822,
	2257,
	4277,
	1326,
	2768,
	2822,
	1326,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	763,
	1744,
	2768,
	1744,
	2822,
	2822,
	2822,
	2768,
	2822,
	2257,
	2257,
	2257,
	2257,
	2257,
	461,
	1744,
	2822,
	2822,
	2822,
	1737,
	1748,
	1737,
	1737,
	1748,
	2768,
	2768,
	1744,
	2822,
	2822,
	1326,
	2822,
	2822,
	2822,
	2822,
	2822,
	2257,
	2822,
	2257,
	2257,
	2257,
	1321,
	2822,
	4467,
	4391,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2257,
	2822,
	2768,
	2822,
	2822,
	2822,
	2822,
	2822,
	2768,
	2822,
	2822,
	2822,
	2822,
	1933,
	1933,
	4467,
	4391,
	2257,
	2257,
	2257,
	2257,
	2822,
	2822,
	2822,
	2768,
	2822,
	2768,
	2822,
	2822,
	2822,
	2822,
	4467,
	4391,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2768,
	2768,
	2822,
	2822,
	2822,
	2768,
	2768,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	4467,
	4391,
	2822,
	2822,
	2822,
	2822,
	2257,
	2257,
	2257,
	2257,
	763,
	2822,
	2822,
	2822,
	2822,
	2257,
	2822,
	2822,
	2822,
	2822,
	2768,
	2822,
	529,
	2822,
	529,
	2822,
	2768,
	2257,
	2768,
	2257,
	2768,
	2257,
	2768,
	2257,
	2768,
	2257,
	2822,
	2822,
	2257,
	2257,
	1107,
	1107,
	752,
	752,
	761,
	2822,
	2768,
	2822,
	2768,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2768,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	1744,
	2768,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2240,
	2822,
	2822,
	2822,
	1107,
	1107,
	752,
	752,
	761,
	2822,
	2822,
	2822,
	2822,
	2257,
	2257,
	2822,
	2822,
	2822,
	2822,
	2257,
	2822,
	2257,
	2257,
	2257,
	2257,
	2240,
	2822,
	2822,
	2822,
	2822,
	2240,
	2822,
	2822,
	2240,
	2822,
	2822,
	2768,
	2822,
	2822,
	2822,
	2822,
	2822,
	2257,
	1744,
	1744,
	2822,
	2822,
	2822,
	2768,
	2768,
	2822,
	4494,
	2822,
	2822,
	2822,
	2822,
	2822,
	2768,
	2822,
	2822,
	2822,
	2822,
	2822,
	2257,
	2768,
	2822,
	2822,
	2822,
	2822,
	2822,
	2257,
	2768,
	2822,
	2822,
	2822,
	2822,
	2822,
	2257,
	2768,
	2822,
	2822,
	2822,
	2822,
	2822,
	2257,
	2768,
	2822,
	2822,
	2822,
	1744,
	2768,
	2822,
	4487,
	4397,
	4487,
	4397,
	2792,
	2792,
	2822,
	2819,
	2819,
	2257,
	2822,
	2822,
	2822,
	2822,
	2822,
	2750,
	1737,
	1201,
	1744,
	1326,
	2768,
	2257,
	2750,
	2792,
	2792,
	2792,
	2792,
	2792,
	2792,
	2792,
	2276,
	1326,
	2257,
	1744,
	1737,
	1744,
	2768,
	2768,
	2768,
	1737,
	476,
	2878,
	2768,
	2879,
	2880,
	2718,
	2208,
	2750,
	2240,
	2794,
	2278,
	2792,
	2276,
	2768,
	2768,
	4281,
	4281,
	4271,
	4179,
	4290,
	4356,
	4277,
	4216,
	4289,
	4337,
	4251,
	3938,
	3938,
	1933,
	2750,
	4467,
	4281,
	3409,
	4281,
	2257,
	2257,
	2257,
	2257,
	2768,
	2257,
	2768,
	4281,
	4281,
	4281,
	4281,
	4281,
	4281,
	4281,
	4277,
	4292,
	4293,
	4294,
	4282,
	4284,
	4281,
	4368,
	4372,
	4378,
	4318,
	4324,
	4281,
	2092,
	1069,
	2817,
	610,
	2097,
	699,
	2819,
	363,
	2101,
	2820,
	1752,
	1770,
	2779,
	1745,
	1779,
	2784,
	1746,
	1744,
	2768,
	1744,
	2763,
	1741,
	2822,
	4494,
	2792,
	2276,
	2750,
	2792,
	2878,
	1737,
	1201,
	1744,
	1326,
	2750,
	1326,
	1737,
	1744,
	2768,
	476,
	2257,
	2822,
	2792,
	2276,
	2750,
	2792,
	2878,
	1744,
	1326,
	1737,
	1201,
	2750,
	1326,
	1744,
	1737,
	1744,
	2768,
	476,
	2257,
	2822,
	2750,
	2792,
	2878,
	2768,
	2257,
	2257,
	476,
	1933,
	2750,
	2257,
	2750,
	2792,
	2878,
	2768,
	2257,
	2718,
	2208,
	2208,
	2257,
	476,
	4337,
	1933,
	2750,
	2257,
	2750,
	2792,
	2878,
	2768,
	2257,
	2792,
	2276,
	2276,
	2257,
	476,
	1933,
	2750,
	2257,
	4467,
	2822,
	2750,
	2792,
	2878,
	2768,
	2257,
	2792,
	2276,
	1933,
	2750,
	476,
	2257,
	4494,
	2750,
	2878,
	2257,
	1326,
	2257,
	1737,
	1201,
	1744,
	1326,
	2257,
	1326,
	3938,
	3938,
	1933,
	2750,
	2750,
	2240,
	2794,
	2278,
	2718,
	2208,
	2792,
	2276,
	2768,
	2768,
	476,
	2257,
	4281,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2794,
	2278,
	2794,
	2278,
	2750,
	2240,
	2750,
	2240,
	2822,
	2278,
	2768,
	2278,
	2822,
	2822,
	2822,
	516,
	2822,
	516,
	2822,
	2278,
	2822,
	2822,
	1737,
	2822,
	2257,
	2822,
	2822,
	2257,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2750,
	2822,
	4467,
	4391,
	2822,
	2822,
	2792,
	2792,
	2257,
	2822,
	2257,
	2768,
	2822,
	4467,
	4391,
	2257,
	2257,
	2750,
	2240,
	2750,
	2822,
	2822,
	2822,
	2822,
	2822,
	4389,
	2822,
	4389,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	4467,
	4391,
	2822,
	2822,
	2822,
	2257,
	731,
	731,
	731,
	2240,
	2240,
	2822,
	2822,
	4467,
	4391,
	2750,
	2240,
	2240,
	2240,
	2822,
	2822,
	2822,
	4467,
	4391,
	2792,
	2792,
	2257,
	2257,
	2257,
	2257,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	4467,
	4391,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2257,
	2822,
	2822,
	2822,
	2822,
	2768,
	2822,
	2257,
	2768,
	2822,
	2822,
	2822,
	2822,
	2822,
	2257,
	2257,
	2240,
	2240,
	2257,
	2257,
	2822,
	2257,
	2822,
	4467,
	4391,
	2794,
	2278,
	2257,
	2257,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	4467,
	4391,
	2822,
	2257,
	2257,
	2768,
	2768,
	2822,
	2822,
	2822,
	2822,
	2822,
	2768,
	2768,
	2768,
	2768,
	2768,
	2768,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	4216,
	2240,
	2822,
	2792,
	2768,
	2822,
	2768,
	2240,
	2822,
	2792,
	2768,
	2822,
	2768,
	2240,
	2822,
	2792,
	2768,
	2822,
	2768,
	1323,
	1357,
	361,
	2257,
	1323,
	1357,
	361,
	2257,
	2257,
	2768,
	2768,
	2768,
	2822,
	2822,
	2792,
	4296,
	2257,
	2331,
	2331,
	2822,
	2822,
	2240,
	2822,
	2792,
	2768,
	2822,
	2768,
	2240,
	2822,
	2792,
	2768,
	2822,
	2768,
	4494,
	2822,
	1933,
	2822,
	2792,
	2240,
	2822,
	2792,
	2768,
	2822,
	2768,
	2822,
	2257,
	2822,
	2257,
	2240,
	2822,
	2792,
	2768,
	2822,
	2768,
	2822,
	2257,
	2822,
	2792,
	2240,
	2822,
	2792,
	2768,
	2822,
	2768,
	2822,
	2792,
	2240,
	2822,
	2792,
	2768,
	2822,
	2768,
	2240,
	2822,
	2792,
	2768,
	2822,
	2768,
	2822,
	2792,
	2240,
	2822,
	2792,
	2768,
	2822,
	2768,
	2240,
	2822,
	2792,
	2768,
	2822,
	2768,
	2822,
	2792,
	2240,
	2822,
	2792,
	2768,
	2822,
	2768,
	2822,
	2792,
	2240,
	2822,
	2792,
	2768,
	2822,
	2768,
	2822,
	2792,
	2240,
	2822,
	2792,
	2768,
	2822,
	2768,
	2822,
	2792,
	2240,
	2822,
	2792,
	2768,
	2822,
	2768,
	2822,
	2792,
	2240,
	2822,
	2792,
	2768,
	2822,
	2768,
	2822,
	2792,
	2240,
	2822,
	2792,
	2768,
	2822,
	2768,
	2822,
	2792,
	2240,
	2822,
	2792,
	2768,
	2822,
	2768,
	2240,
	2822,
	2792,
	2768,
	2822,
	2768,
	2240,
	2822,
	2792,
	2768,
	2822,
	2768,
	2240,
	2822,
	2792,
	2768,
	2822,
	2768,
	2240,
	2822,
	2792,
	2768,
	2822,
	2768,
	2240,
	2822,
	2792,
	2768,
	2822,
	2768,
	2240,
	2822,
	2792,
	2768,
	2822,
	2768,
	2240,
	2822,
	2792,
	2768,
	2822,
	2768,
	4494,
	2822,
	1933,
	1933,
	1933,
	1933,
	2822,
	1933,
	1933,
	1933,
	1933,
	1933,
	1933,
	1933,
	1933,
	1933,
	2240,
	2822,
	2792,
	2768,
	2822,
	2768,
	2822,
	2822,
	2822,
	2822,
	2822,
	2240,
	2822,
	2792,
	2768,
	2822,
	2768,
	2240,
	2822,
	2792,
	2768,
	2822,
	2768,
	2240,
	2822,
	2792,
	2768,
	2822,
	2768,
	2240,
	2822,
	2792,
	2768,
	2822,
	2768,
	2240,
	2822,
	2792,
	2768,
	2822,
	2768,
	2240,
	2822,
	2792,
	2768,
	2822,
	2768,
	2240,
	2822,
	2792,
	2768,
	2822,
	2768,
	2240,
	2822,
	2792,
	2768,
	2822,
	2768,
	2240,
	2822,
	2792,
	2768,
	2822,
	2768,
	2240,
	2822,
	2792,
	2768,
	2822,
	2768,
	2240,
	2822,
	2792,
	2768,
	2822,
	2768,
	2240,
	2822,
	2792,
	2768,
	2822,
	2768,
	2240,
	2822,
	2792,
	2768,
	2822,
	2768,
	2822,
	846,
	2240,
	2822,
	2792,
	2768,
	2822,
	2768,
	2240,
	2822,
	2792,
	2768,
	2822,
	2768,
	4494,
	2822,
	1933,
	2792,
	2121,
	2122,
	2648,
	2792,
	2121,
	2122,
	2354,
	2768,
	2792,
	2880,
	2121,
	2122,
	2354,
	2768,
	2792,
	2879,
	2257,
	2648,
	2768,
	2792,
	2822,
	2768,
	2822,
	2768,
	2240,
	2822,
	2792,
	2768,
	2822,
	2768,
	2768,
	2768,
	2240,
	2822,
	2792,
	2822,
	2822,
	2768,
	2822,
	2768,
	2768,
	2768,
	2240,
	2822,
	2792,
	2822,
	2768,
	2822,
	2768,
	2768,
	2768,
	2822,
	1819,
	2240,
	2822,
	2792,
	2822,
	2768,
	2822,
	2768,
	2768,
	2768,
	2240,
	2822,
	2792,
	2768,
	2822,
	2768,
	2822,
	1933,
	4494,
	2822,
	1933,
	1933,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	1933,
	2240,
	2822,
	2792,
	2768,
	2822,
	2768,
	2240,
	2822,
	2792,
	2768,
	2822,
	2768,
	2822,
	1933,
	2240,
	2822,
	2792,
	2768,
	2822,
	2768,
	2240,
	2822,
	2792,
	2768,
	2822,
	2768,
	2240,
	2822,
	2792,
	2768,
	2822,
	2768,
	2240,
	2822,
	2792,
	2768,
	2822,
	2768,
	2240,
	2822,
	2792,
	2768,
	2822,
	2768,
	2240,
	2822,
	2792,
	2768,
	2822,
	2768,
	2240,
	2822,
	2792,
	2768,
	2822,
	2768,
	2240,
	2822,
	2792,
	2768,
	2822,
	2768,
};
extern const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule;
const Il2CppCodeGenModule g_AssemblyU2DCSharp_CodeGenModule = 
{
	"Assembly-CSharp.dll",
	1414,
	s_methodPointers,
	25,
	s_adjustorThunks,
	s_InvokerIndices,
	0,
	NULL,
	0,
	NULL,
	0,
	NULL,
	NULL,
	g_AssemblyU2DCSharp_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
