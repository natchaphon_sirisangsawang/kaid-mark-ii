﻿#include "pch-cpp.hpp"

#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include <limits>
#include <stdint.h>



// System.Char[]
struct CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34;
// System.Type[]
struct TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755;
// System.Reflection.Binder
struct Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30;
// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF;
// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C;
// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B;
// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88;
// UnityEngine.DefaultExecutionOrder
struct DefaultExecutionOrder_t8495D3D4ECDFC3590621D31C3677D234D8A9BB1F;
// System.Reflection.DefaultMemberAttribute
struct DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5;
// UnityEngine.ExecuteInEditMode
struct ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173;
// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB;
// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA;
// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830;
// System.Reflection.MemberFilter
struct MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81;
// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5;
// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91;
// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80;
// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25;
// System.String
struct String_t;
// System.ThreadStaticAttribute
struct ThreadStaticAttribute_tD3A8F4870EC5B163383AB888C364217A38134F14;
// System.Type
struct Type_t;
// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5;

IL2CPP_EXTERN_C const RuntimeType* MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CActiveCandy30sU3Ed__21_t679D864AD14DE99063DD90DAE48F757780A913F7_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CActiveCookie30sU3Ed__19_t0119DB09C6FEC13C8307690325CEE99855E3C782_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CAnimatePropertiesU3Ed__6_t6EF9435006F3F335B99084BECD0ABFEFFEF4A32D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCandyClickedU3Ed__36_tCA313AA216D873516B3C1A392A0302BBB97B86B0_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CChangeButtonColorU3Ed__21_tD9B8A0568D190D5F2BD60EEA784E0B908A3AE624_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCheckAlreadyNameIdTransferU3Ed__74_t7451AB99B91CA94A2D6E4F546CA0FD8CC686ED92_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCheckAlreadyNameU3Ed__50_t81299FEF47EDC5385754950A82C9FE4E5176462D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCheckDamageOverCurrentScoreU3Ed__12_t07A41FEF6FA87A2ECE1E7E0FF28AADDFA79B4546_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCheckTimeSurvivedOverCurrentScoreU3Ed__13_t361F8E558207AE97EA78DAC6651DDAA2FB989EE1_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCloseNotificationU3Ed__62_t50067E67A5466C3585675771E09DE822592CB4D0_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CCookieClickedU3Ed__35_tDA1263DFFDD0773D381B1D787122AEEABE86AF9C_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDelayClickU3Ed__8_tBF346C93C4D6EAB42D93211E5296F33032B7BC72_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDelayLaserPopUpU3Ed__28_tF0267396241BEFE79B23A545D65A2D3DFAE18C0D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDisableInfoU3Ed__33_t6EB1180E7648EE0413BB51BA7BFB27F30B17C577_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDisplayTextMeshFloatingTextU3Ed__16_tACCF2F4E02674FBB7D0A61F908094C4EC0DAD2A2_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDisplayTextMeshProFloatingTextU3Ed__15_tF7B0AC090E8E1BC19444DC46DEA61B2D608F7E20_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDownloadImageU3Ed__19_tC075483F4C0D4B7D7065D99C2135C5A8A0BCA1D7_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDownloadImageU3Ed__5_t2794312741EA00E7B69D55B73B3136C5C9F354C0_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CDownloadImageU3Ed__76_t723888129E1A8E7C4732F71C5E435C1DEC50CE12_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CFireWorkClickedU3Ed__34_t0A3C43FA8003F5D86FE232DD5D91E4F1EF7CFE24_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CImageShowCaseU3Ed__17_tB869F8E282B1CD6D682B87084152A7B70D6A3589_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLeaderBoardClickedU3Ed__38_tBAE695B5560CCB49D1653EAD612C1CE08B42F361_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLoadLeaderBoardDataU3Ed__18_t85C0AE47A469B364C930201232E48E31F00B91D1_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLoadLeaderBoardDataU3Ed__80_t53D5F08F28E7939D79D6BB3754783EF0607ABF3D_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLoadUserScoreFromDBU3Ed__17_tA72B0A24BFD2A694425FDB145D618C9F8A7C6AA8_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CLoadUserScoreFromDBU3Ed__75_tB538B410886D8F26EB89210EE854B5F7389C75DB_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CNextEnemyU3Ed__26_t4F6CB5EE30FB1BC45025C47422CA0BA6F4CFBA05_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CNextHeroU3Ed__27_tA3FA225A9F245C060D7D1285EC0ED7F01B031AA0_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CScreenShotClickedU3Ed__37_t982CA263942B0C8A08EC834CA778F5356084D169_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__10_t3D69371599D2A84DED6689D4FA6EA1720611EAD3_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__10_t4C97F7E5F1F2D654FF76C297F92CF423F472A640_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__4_t70319341791F5D1BC6F878503A507E845E98C44E_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CStartU3Ed__4_t85DC3CED72C53244AC5D2467B8CB12EF944D3266_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CUpdateDamageDoneU3Ed__14_t1D42A96E93CA3C068716D61E6E4C4A8CE4087D21_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CUpdateTimeSurvivedU3Ed__16_tD6AF10B6C261DD6230297852068466DCA37C64DA_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CUpdateWaveSurvivedU3Ed__15_tF7DE1FBC2C2CC828B58C7F6228A9912502633D6F_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitForCamU3Ed__18_t189EA3BB11BFA6AC795F202CDE0699E1C9029277_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitForNoticeOutU3Ed__46_tB40B7C9E84E36CF0E86D5063901513398B490775_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitForNoticeTimerOutU3Ed__43_t872B5C580D4D7A86DDD89BA935867637E1EDF564_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitForPanelU3Ed__14_t21DF4D5D04488D4635896E34F3A4930EBF6331AA_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitForShowCanvasU3Ed__21_t6623746B1B2635840CC2F08A9B9C83127716121E_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitForUnPauseU3Ed__15_t7C2E4C470D88899BE03226E81A5D36543A1ACF77_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitForUpdateUiU3Ed__27_t36E7FB8307E80FED7AB34985D4632506BC952711_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWaitToStartU3Ed__14_tAD265766599EEE9474750E00FA4939AEAABB823A_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3Cget_ChildrenU3Ed__22_t411188099DCF9174894F420F6390B7E43C48B481_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3Cget_ChildrenU3Ed__23_t213CCC2BA3B684AD1871D45A4278367199EFBCA8_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3Cget_ChildrenU3Ed__39_tB70A86AF5415D0A3C8B93D07712335F4FE14F44B_0_0_0_var;
IL2CPP_EXTERN_C const RuntimeType* U3Cget_DeepChildrenU3Ed__41_t1EFE91681671B35682051A9DF5D99D8012D7DDD7_0_0_0_var;


IL2CPP_EXTERN_C_BEGIN
IL2CPP_EXTERN_C_END

#ifdef __clang__
#pragma clang diagnostic push
#pragma clang diagnostic ignored "-Winvalid-offsetof"
#pragma clang diagnostic ignored "-Wunused-variable"
#endif

// System.Object


// System.Attribute
struct Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71  : public RuntimeObject
{
public:

public:
};


// System.Reflection.MemberInfo
struct MemberInfo_t  : public RuntimeObject
{
public:

public:
};


// System.String
struct String_t  : public RuntimeObject
{
public:
	// System.Int32 System.String::m_stringLength
	int32_t ___m_stringLength_0;
	// System.Char System.String::m_firstChar
	Il2CppChar ___m_firstChar_1;

public:
	inline static int32_t get_offset_of_m_stringLength_0() { return static_cast<int32_t>(offsetof(String_t, ___m_stringLength_0)); }
	inline int32_t get_m_stringLength_0() const { return ___m_stringLength_0; }
	inline int32_t* get_address_of_m_stringLength_0() { return &___m_stringLength_0; }
	inline void set_m_stringLength_0(int32_t value)
	{
		___m_stringLength_0 = value;
	}

	inline static int32_t get_offset_of_m_firstChar_1() { return static_cast<int32_t>(offsetof(String_t, ___m_firstChar_1)); }
	inline Il2CppChar get_m_firstChar_1() const { return ___m_firstChar_1; }
	inline Il2CppChar* get_address_of_m_firstChar_1() { return &___m_firstChar_1; }
	inline void set_m_firstChar_1(Il2CppChar value)
	{
		___m_firstChar_1 = value;
	}
};

struct String_t_StaticFields
{
public:
	// System.String System.String::Empty
	String_t* ___Empty_5;

public:
	inline static int32_t get_offset_of_Empty_5() { return static_cast<int32_t>(offsetof(String_t_StaticFields, ___Empty_5)); }
	inline String_t* get_Empty_5() const { return ___Empty_5; }
	inline String_t** get_address_of_Empty_5() { return &___Empty_5; }
	inline void set_Empty_5(String_t* value)
	{
		___Empty_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Empty_5), (void*)value);
	}
};


// System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52  : public RuntimeObject
{
public:

public:
};

// Native definition for P/Invoke marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.ValueType
struct ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52_marshaled_com
{
};

// System.Boolean
struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37 
{
public:
	// System.Boolean System.Boolean::m_value
	bool ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37, ___m_value_0)); }
	inline bool get_m_value_0() const { return ___m_value_0; }
	inline bool* get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(bool value)
	{
		___m_value_0 = value;
	}
};

struct Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields
{
public:
	// System.String System.Boolean::TrueString
	String_t* ___TrueString_5;
	// System.String System.Boolean::FalseString
	String_t* ___FalseString_6;

public:
	inline static int32_t get_offset_of_TrueString_5() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___TrueString_5)); }
	inline String_t* get_TrueString_5() const { return ___TrueString_5; }
	inline String_t** get_address_of_TrueString_5() { return &___TrueString_5; }
	inline void set_TrueString_5(String_t* value)
	{
		___TrueString_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___TrueString_5), (void*)value);
	}

	inline static int32_t get_offset_of_FalseString_6() { return static_cast<int32_t>(offsetof(Boolean_t07D1E3F34E4813023D64F584DFF7B34C9D922F37_StaticFields, ___FalseString_6)); }
	inline String_t* get_FalseString_6() const { return ___FalseString_6; }
	inline String_t** get_address_of_FalseString_6() { return &___FalseString_6; }
	inline void set_FalseString_6(String_t* value)
	{
		___FalseString_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FalseString_6), (void*)value);
	}
};


// System.Runtime.CompilerServices.CompilationRelaxationsAttribute
struct CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 System.Runtime.CompilerServices.CompilationRelaxationsAttribute::m_relaxations
	int32_t ___m_relaxations_0;

public:
	inline static int32_t get_offset_of_m_relaxations_0() { return static_cast<int32_t>(offsetof(CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF, ___m_relaxations_0)); }
	inline int32_t get_m_relaxations_0() const { return ___m_relaxations_0; }
	inline int32_t* get_address_of_m_relaxations_0() { return &___m_relaxations_0; }
	inline void set_m_relaxations_0(int32_t value)
	{
		___m_relaxations_0 = value;
	}
};


// System.Runtime.CompilerServices.CompilerGeneratedAttribute
struct CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Diagnostics.DebuggerHiddenAttribute
struct DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.DefaultExecutionOrder
struct DefaultExecutionOrder_t8495D3D4ECDFC3590621D31C3677D234D8A9BB1F  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Int32 UnityEngine.DefaultExecutionOrder::m_Order
	int32_t ___m_Order_0;

public:
	inline static int32_t get_offset_of_m_Order_0() { return static_cast<int32_t>(offsetof(DefaultExecutionOrder_t8495D3D4ECDFC3590621D31C3677D234D8A9BB1F, ___m_Order_0)); }
	inline int32_t get_m_Order_0() const { return ___m_Order_0; }
	inline int32_t* get_address_of_m_Order_0() { return &___m_Order_0; }
	inline void set_m_Order_0(int32_t value)
	{
		___m_Order_0 = value;
	}
};


// System.Reflection.DefaultMemberAttribute
struct DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.String System.Reflection.DefaultMemberAttribute::m_memberName
	String_t* ___m_memberName_0;

public:
	inline static int32_t get_offset_of_m_memberName_0() { return static_cast<int32_t>(offsetof(DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5, ___m_memberName_0)); }
	inline String_t* get_m_memberName_0() const { return ___m_memberName_0; }
	inline String_t** get_address_of_m_memberName_0() { return &___m_memberName_0; }
	inline void set_m_memberName_0(String_t* value)
	{
		___m_memberName_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_memberName_0), (void*)value);
	}
};


// System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA  : public ValueType_tDBF999C1B75C48C68621878250DBF6CDBCF51E52
{
public:

public:
};

struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields
{
public:
	// System.Char[] System.Enum::enumSeperatorCharArray
	CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* ___enumSeperatorCharArray_0;

public:
	inline static int32_t get_offset_of_enumSeperatorCharArray_0() { return static_cast<int32_t>(offsetof(Enum_t23B90B40F60E677A8025267341651C94AE079CDA_StaticFields, ___enumSeperatorCharArray_0)); }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* get_enumSeperatorCharArray_0() const { return ___enumSeperatorCharArray_0; }
	inline CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34** get_address_of_enumSeperatorCharArray_0() { return &___enumSeperatorCharArray_0; }
	inline void set_enumSeperatorCharArray_0(CharU5BU5D_t7B7FC5BC8091AA3B9CB0B29CDD80B5EE9254AA34* value)
	{
		___enumSeperatorCharArray_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___enumSeperatorCharArray_0), (void*)value);
	}
};

// Native definition for P/Invoke marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_pinvoke
{
};
// Native definition for COM marshalling of System.Enum
struct Enum_t23B90B40F60E677A8025267341651C94AE079CDA_marshaled_com
{
};

// UnityEngine.ExecuteInEditMode
struct ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.HideInInspector
struct HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.IntPtr
struct IntPtr_t 
{
public:
	// System.Void* System.IntPtr::m_value
	void* ___m_value_0;

public:
	inline static int32_t get_offset_of_m_value_0() { return static_cast<int32_t>(offsetof(IntPtr_t, ___m_value_0)); }
	inline void* get_m_value_0() const { return ___m_value_0; }
	inline void** get_address_of_m_value_0() { return &___m_value_0; }
	inline void set_m_value_0(void* value)
	{
		___m_value_0 = value;
	}
};

struct IntPtr_t_StaticFields
{
public:
	// System.IntPtr System.IntPtr::Zero
	intptr_t ___Zero_1;

public:
	inline static int32_t get_offset_of_Zero_1() { return static_cast<int32_t>(offsetof(IntPtr_t_StaticFields, ___Zero_1)); }
	inline intptr_t get_Zero_1() const { return ___Zero_1; }
	inline intptr_t* get_address_of_Zero_1() { return &___Zero_1; }
	inline void set_Zero_1(intptr_t value)
	{
		___Zero_1 = value;
	}
};


// UnityEngine.PropertyAttribute
struct PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// UnityEngine.RequireComponent
struct RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type UnityEngine.RequireComponent::m_Type0
	Type_t * ___m_Type0_0;
	// System.Type UnityEngine.RequireComponent::m_Type1
	Type_t * ___m_Type1_1;
	// System.Type UnityEngine.RequireComponent::m_Type2
	Type_t * ___m_Type2_2;

public:
	inline static int32_t get_offset_of_m_Type0_0() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type0_0)); }
	inline Type_t * get_m_Type0_0() const { return ___m_Type0_0; }
	inline Type_t ** get_address_of_m_Type0_0() { return &___m_Type0_0; }
	inline void set_m_Type0_0(Type_t * value)
	{
		___m_Type0_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type0_0), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type1_1() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type1_1)); }
	inline Type_t * get_m_Type1_1() const { return ___m_Type1_1; }
	inline Type_t ** get_address_of_m_Type1_1() { return &___m_Type1_1; }
	inline void set_m_Type1_1(Type_t * value)
	{
		___m_Type1_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type1_1), (void*)value);
	}

	inline static int32_t get_offset_of_m_Type2_2() { return static_cast<int32_t>(offsetof(RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91, ___m_Type2_2)); }
	inline Type_t * get_m_Type2_2() const { return ___m_Type2_2; }
	inline Type_t ** get_address_of_m_Type2_2() { return &___m_Type2_2; }
	inline void set_m_Type2_2(Type_t * value)
	{
		___m_Type2_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___m_Type2_2), (void*)value);
	}
};


// System.Runtime.CompilerServices.RuntimeCompatibilityAttribute
struct RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Boolean System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::m_wrapNonExceptionThrows
	bool ___m_wrapNonExceptionThrows_0;

public:
	inline static int32_t get_offset_of_m_wrapNonExceptionThrows_0() { return static_cast<int32_t>(offsetof(RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80, ___m_wrapNonExceptionThrows_0)); }
	inline bool get_m_wrapNonExceptionThrows_0() const { return ___m_wrapNonExceptionThrows_0; }
	inline bool* get_address_of_m_wrapNonExceptionThrows_0() { return &___m_wrapNonExceptionThrows_0; }
	inline void set_m_wrapNonExceptionThrows_0(bool value)
	{
		___m_wrapNonExceptionThrows_0 = value;
	}
};


// UnityEngine.SerializeField
struct SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Runtime.CompilerServices.StateMachineAttribute
struct StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Type System.Runtime.CompilerServices.StateMachineAttribute::<StateMachineType>k__BackingField
	Type_t * ___U3CStateMachineTypeU3Ek__BackingField_0;

public:
	inline static int32_t get_offset_of_U3CStateMachineTypeU3Ek__BackingField_0() { return static_cast<int32_t>(offsetof(StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3, ___U3CStateMachineTypeU3Ek__BackingField_0)); }
	inline Type_t * get_U3CStateMachineTypeU3Ek__BackingField_0() const { return ___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline Type_t ** get_address_of_U3CStateMachineTypeU3Ek__BackingField_0() { return &___U3CStateMachineTypeU3Ek__BackingField_0; }
	inline void set_U3CStateMachineTypeU3Ek__BackingField_0(Type_t * value)
	{
		___U3CStateMachineTypeU3Ek__BackingField_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___U3CStateMachineTypeU3Ek__BackingField_0), (void*)value);
	}
};


// System.ThreadStaticAttribute
struct ThreadStaticAttribute_tD3A8F4870EC5B163383AB888C364217A38134F14  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:

public:
};


// System.Void
struct Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5 
{
public:
	union
	{
		struct
		{
		};
		uint8_t Void_t700C6383A2A510C2CF4DD86DABD5CA9FF70ADAC5__padding[1];
	};

public:
};


// System.Reflection.BindingFlags
struct BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733 
{
public:
	// System.Int32 System.Reflection.BindingFlags::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(BindingFlags_tAAAB07D9AC588F0D55D844E51D7035E96DF94733, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// UnityEngine.HeaderAttribute
struct HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.String UnityEngine.HeaderAttribute::header
	String_t* ___header_0;

public:
	inline static int32_t get_offset_of_header_0() { return static_cast<int32_t>(offsetof(HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB, ___header_0)); }
	inline String_t* get_header_0() const { return ___header_0; }
	inline String_t** get_address_of_header_0() { return &___header_0; }
	inline void set_header_0(String_t* value)
	{
		___header_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___header_0), (void*)value);
	}
};


// System.Runtime.CompilerServices.IteratorStateMachineAttribute
struct IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830  : public StateMachineAttribute_tA6E77C77F821508E405473BA1C4C08A69FDA0AC3
{
public:

public:
};


// UnityEngine.RangeAttribute
struct RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5  : public PropertyAttribute_t4A352471DF625C56C811E27AC86B7E1CE6444052
{
public:
	// System.Single UnityEngine.RangeAttribute::min
	float ___min_0;
	// System.Single UnityEngine.RangeAttribute::max
	float ___max_1;

public:
	inline static int32_t get_offset_of_min_0() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___min_0)); }
	inline float get_min_0() const { return ___min_0; }
	inline float* get_address_of_min_0() { return &___min_0; }
	inline void set_min_0(float value)
	{
		___min_0 = value;
	}

	inline static int32_t get_offset_of_max_1() { return static_cast<int32_t>(offsetof(RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5, ___max_1)); }
	inline float get_max_1() const { return ___max_1; }
	inline float* get_address_of_max_1() { return &___max_1; }
	inline void set_max_1(float value)
	{
		___max_1 = value;
	}
};


// System.RuntimeTypeHandle
struct RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 
{
public:
	// System.IntPtr System.RuntimeTypeHandle::value
	intptr_t ___value_0;

public:
	inline static int32_t get_offset_of_value_0() { return static_cast<int32_t>(offsetof(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9, ___value_0)); }
	inline intptr_t get_value_0() const { return ___value_0; }
	inline intptr_t* get_address_of_value_0() { return &___value_0; }
	inline void set_value_0(intptr_t value)
	{
		___value_0 = value;
	}
};


// System.Diagnostics.DebuggableAttribute/DebuggingModes
struct DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8 
{
public:
	// System.Int32 System.Diagnostics.DebuggableAttribute/DebuggingModes::value__
	int32_t ___value___2;

public:
	inline static int32_t get_offset_of_value___2() { return static_cast<int32_t>(offsetof(DebuggingModes_t279D5B9C012ABA935887CB73C5A63A1F46AF08A8, ___value___2)); }
	inline int32_t get_value___2() const { return ___value___2; }
	inline int32_t* get_address_of_value___2() { return &___value___2; }
	inline void set_value___2(int32_t value)
	{
		___value___2 = value;
	}
};


// System.Diagnostics.DebuggableAttribute
struct DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B  : public Attribute_t037CA9D9F3B742C063DB364D2EEBBF9FC5772C71
{
public:
	// System.Diagnostics.DebuggableAttribute/DebuggingModes System.Diagnostics.DebuggableAttribute::m_debuggingModes
	int32_t ___m_debuggingModes_0;

public:
	inline static int32_t get_offset_of_m_debuggingModes_0() { return static_cast<int32_t>(offsetof(DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B, ___m_debuggingModes_0)); }
	inline int32_t get_m_debuggingModes_0() const { return ___m_debuggingModes_0; }
	inline int32_t* get_address_of_m_debuggingModes_0() { return &___m_debuggingModes_0; }
	inline void set_m_debuggingModes_0(int32_t value)
	{
		___m_debuggingModes_0 = value;
	}
};


// System.Type
struct Type_t  : public MemberInfo_t
{
public:
	// System.RuntimeTypeHandle System.Type::_impl
	RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  ____impl_9;

public:
	inline static int32_t get_offset_of__impl_9() { return static_cast<int32_t>(offsetof(Type_t, ____impl_9)); }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  get__impl_9() const { return ____impl_9; }
	inline RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9 * get_address_of__impl_9() { return &____impl_9; }
	inline void set__impl_9(RuntimeTypeHandle_tC33965ADA3E041E0C94AF05E5CB527B56482CEF9  value)
	{
		____impl_9 = value;
	}
};

struct Type_t_StaticFields
{
public:
	// System.Reflection.MemberFilter System.Type::FilterAttribute
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterAttribute_0;
	// System.Reflection.MemberFilter System.Type::FilterName
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterName_1;
	// System.Reflection.MemberFilter System.Type::FilterNameIgnoreCase
	MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * ___FilterNameIgnoreCase_2;
	// System.Object System.Type::Missing
	RuntimeObject * ___Missing_3;
	// System.Char System.Type::Delimiter
	Il2CppChar ___Delimiter_4;
	// System.Type[] System.Type::EmptyTypes
	TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* ___EmptyTypes_5;
	// System.Reflection.Binder System.Type::defaultBinder
	Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * ___defaultBinder_6;

public:
	inline static int32_t get_offset_of_FilterAttribute_0() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterAttribute_0)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterAttribute_0() const { return ___FilterAttribute_0; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterAttribute_0() { return &___FilterAttribute_0; }
	inline void set_FilterAttribute_0(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterAttribute_0 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterAttribute_0), (void*)value);
	}

	inline static int32_t get_offset_of_FilterName_1() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterName_1)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterName_1() const { return ___FilterName_1; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterName_1() { return &___FilterName_1; }
	inline void set_FilterName_1(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterName_1 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterName_1), (void*)value);
	}

	inline static int32_t get_offset_of_FilterNameIgnoreCase_2() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___FilterNameIgnoreCase_2)); }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * get_FilterNameIgnoreCase_2() const { return ___FilterNameIgnoreCase_2; }
	inline MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 ** get_address_of_FilterNameIgnoreCase_2() { return &___FilterNameIgnoreCase_2; }
	inline void set_FilterNameIgnoreCase_2(MemberFilter_t48D0AA10105D186AF42428FA532D4B4332CF8B81 * value)
	{
		___FilterNameIgnoreCase_2 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___FilterNameIgnoreCase_2), (void*)value);
	}

	inline static int32_t get_offset_of_Missing_3() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Missing_3)); }
	inline RuntimeObject * get_Missing_3() const { return ___Missing_3; }
	inline RuntimeObject ** get_address_of_Missing_3() { return &___Missing_3; }
	inline void set_Missing_3(RuntimeObject * value)
	{
		___Missing_3 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___Missing_3), (void*)value);
	}

	inline static int32_t get_offset_of_Delimiter_4() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___Delimiter_4)); }
	inline Il2CppChar get_Delimiter_4() const { return ___Delimiter_4; }
	inline Il2CppChar* get_address_of_Delimiter_4() { return &___Delimiter_4; }
	inline void set_Delimiter_4(Il2CppChar value)
	{
		___Delimiter_4 = value;
	}

	inline static int32_t get_offset_of_EmptyTypes_5() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___EmptyTypes_5)); }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* get_EmptyTypes_5() const { return ___EmptyTypes_5; }
	inline TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755** get_address_of_EmptyTypes_5() { return &___EmptyTypes_5; }
	inline void set_EmptyTypes_5(TypeU5BU5D_t85B10489E46F06CEC7C4B1CCBD0E01FAB6649755* value)
	{
		___EmptyTypes_5 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___EmptyTypes_5), (void*)value);
	}

	inline static int32_t get_offset_of_defaultBinder_6() { return static_cast<int32_t>(offsetof(Type_t_StaticFields, ___defaultBinder_6)); }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * get_defaultBinder_6() const { return ___defaultBinder_6; }
	inline Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 ** get_address_of_defaultBinder_6() { return &___defaultBinder_6; }
	inline void set_defaultBinder_6(Binder_t2BEE27FD84737D1E79BC47FD67F6D3DD2F2DDA30 * value)
	{
		___defaultBinder_6 = value;
		Il2CppCodeGenWriteBarrier((void**)(&___defaultBinder_6), (void*)value);
	}
};

#ifdef __clang__
#pragma clang diagnostic pop
#endif



// System.Void System.Runtime.CompilerServices.CompilationRelaxationsAttribute::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * __this, int32_t ___relaxations0, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.RuntimeCompatibilityAttribute::set_WrapNonExceptionThrows(System.Boolean)
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggableAttribute::.ctor(System.Diagnostics.DebuggableAttribute/DebuggingModes)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550 (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * __this, int32_t ___modes0, const RuntimeMethod* method);
// System.Void UnityEngine.RequireComponent::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4 (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * __this, Type_t * ___requiredComponent0, const RuntimeMethod* method);
// System.Void UnityEngine.ExecuteInEditMode::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.HideInInspector::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9 (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * __this, const RuntimeMethod* method);
// System.Void UnityEngine.HeaderAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * __this, String_t* ___header0, const RuntimeMethod* method);
// System.Void UnityEngine.SerializeField::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3 (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.CompilerGeneratedAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35 (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * __this, const RuntimeMethod* method);
// System.Void System.Runtime.CompilerServices.IteratorStateMachineAttribute::.ctor(System.Type)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481 (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * __this, Type_t * ___stateMachineType0, const RuntimeMethod* method);
// System.Void System.Diagnostics.DebuggerHiddenAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3 (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * __this, const RuntimeMethod* method);
// System.Void UnityEngine.DefaultExecutionOrder::.ctor(System.Int32)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DefaultExecutionOrder__ctor_m18F4188D26702C2E3EC0AB3C1FF4AA4F5329E7A9 (DefaultExecutionOrder_t8495D3D4ECDFC3590621D31C3677D234D8A9BB1F * __this, int32_t ___order0, const RuntimeMethod* method);
// System.Void UnityEngine.RangeAttribute::.ctor(System.Single,System.Single)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000 (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * __this, float ___min0, float ___max1, const RuntimeMethod* method);
// System.Void System.Reflection.DefaultMemberAttribute::.ctor(System.String)
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7 (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * __this, String_t* ___memberName0, const RuntimeMethod* method);
// System.Void System.ThreadStaticAttribute::.ctor()
IL2CPP_EXTERN_C IL2CPP_METHOD_ATTR void ThreadStaticAttribute__ctor_m2F60E2FA27DEC1E9FE581440EF3445F3B5E7F16A (ThreadStaticAttribute_tD3A8F4870EC5B163383AB888C364217A38134F14 * __this, const RuntimeMethod* method);
static void AssemblyU2DCSharp_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF * tmp = (CompilationRelaxationsAttribute_t661FDDC06629BDA607A42BD660944F039FE03AFF *)cache->attributes[0];
		CompilationRelaxationsAttribute__ctor_mAC3079EBC4EEAB474EED8208EF95DB39C922333B(tmp, 8LL, NULL);
	}
	{
		RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * tmp = (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 *)cache->attributes[1];
		RuntimeCompatibilityAttribute__ctor_m551DDF1438CE97A984571949723F30F44CF7317C(tmp, NULL);
		RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline(tmp, true, NULL);
	}
	{
		DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B * tmp = (DebuggableAttribute_tA8054EBD0FC7511695D494B690B5771658E3191B *)cache->attributes[2];
		DebuggableAttribute__ctor_m7FF445C8435494A4847123A668D889E692E55550(tmp, 2LL, NULL);
	}
}
static void WaterManager_t34FC69E413CAB56189B29DA6B188017FB99DA41A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_0_0_0_var);
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[0];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(MeshFilter_t763BB2BBF3881176AD25E4570E6DD215BA0AA51A_0_0_0_var), NULL);
	}
	{
		RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 * tmp = (RequireComponent_tEDA546F9722B8874DA9658BDAB821BA49647FC91 *)cache->attributes[1];
		RequireComponent__ctor_m5EC89D3D22D7D880E1B88A5C9FADF1FBDC713EE4(tmp, il2cpp_codegen_type_get_object(MeshRenderer_tCD983A2F635E12BCB0BAA2E635D96A318757908B_0_0_0_var), NULL);
	}
}
static void WaterReflection_tC56DDAE9A17F940A57C0EA5F0EC5FD8CE60FC710_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 * tmp = (ExecuteInEditMode_tAA3B5DE8B7E207BC6CAAFDB1F2502350C0546173 *)cache->attributes[0];
		ExecuteInEditMode__ctor_m52849B67DB46F6CF090D45E523FAE97A10825C0A(tmp, NULL);
	}
}
static void CardBluePrint_t6DA4D665AE5A5EC248F1607C55E32F6BFE3DAD82_CustomAttributesCacheGenerator_HeroName(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void ElementBluePrint_tA8826B6A1C64AD3C2DC8F8DF3A3C717010DDF870_CustomAttributesCacheGenerator_Fire6_Add_BestFireDamage(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x36\x20\x46\x69\x72\x65\x20\x53\x74\x61\x74\x75\x73\x20\x42\x6F\x6F\x73\x74"), NULL);
	}
}
static void ElementBluePrint_tA8826B6A1C64AD3C2DC8F8DF3A3C717010DDF870_CustomAttributesCacheGenerator_Fire4_Add_MoreFireDamage(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x34\x20\x46\x69\x72\x65\x20\x53\x74\x61\x74\x75\x73\x20\x42\x6F\x6F\x73\x74"), NULL);
	}
}
static void ElementBluePrint_tA8826B6A1C64AD3C2DC8F8DF3A3C717010DDF870_CustomAttributesCacheGenerator_Fire2_Add_FireDamage(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x32\x20\x46\x69\x72\x65\x20\x53\x74\x61\x74\x75\x73\x20\x42\x6F\x6F\x73\x74"), NULL);
	}
}
static void ElementBluePrint_tA8826B6A1C64AD3C2DC8F8DF3A3C717010DDF870_CustomAttributesCacheGenerator_Water6_Add_Add_BestWaterDamage(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x36\x20\x57\x61\x74\x65\x72\x20\x53\x74\x61\x74\x75\x73\x20\x42\x6F\x6F\x73\x74"), NULL);
	}
}
static void ElementBluePrint_tA8826B6A1C64AD3C2DC8F8DF3A3C717010DDF870_CustomAttributesCacheGenerator_Water4_Add_MoreWaterDamage(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x34\x20\x57\x61\x74\x65\x72\x20\x53\x74\x61\x74\x75\x73\x20\x42\x6F\x6F\x73\x74"), NULL);
	}
}
static void ElementBluePrint_tA8826B6A1C64AD3C2DC8F8DF3A3C717010DDF870_CustomAttributesCacheGenerator_Water2_Add_WaterDamage(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x32\x20\x57\x61\x74\x65\x72\x20\x53\x74\x61\x74\x75\x73\x20\x42\x6F\x6F\x73\x74"), NULL);
	}
}
static void ElementBluePrint_tA8826B6A1C64AD3C2DC8F8DF3A3C717010DDF870_CustomAttributesCacheGenerator_Plant6_Add_BestPlantDamage(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x36\x20\x50\x6C\x61\x6E\x74\x20\x53\x74\x61\x74\x75\x73\x20\x42\x6F\x6F\x73\x74"), NULL);
	}
}
static void ElementBluePrint_tA8826B6A1C64AD3C2DC8F8DF3A3C717010DDF870_CustomAttributesCacheGenerator_Plant4_Add_MorePlantDamage(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x34\x20\x50\x6C\x61\x6E\x74\x20\x53\x74\x61\x74\x75\x73\x20\x42\x6F\x6F\x73\x74"), NULL);
	}
}
static void ElementBluePrint_tA8826B6A1C64AD3C2DC8F8DF3A3C717010DDF870_CustomAttributesCacheGenerator_Plant2_Add_PlantDamage(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x32\x20\x50\x6C\x61\x6E\x74\x20\x53\x74\x61\x74\x75\x73\x20\x42\x6F\x6F\x73\x74"), NULL);
	}
}
static void ElementBluePrint_tA8826B6A1C64AD3C2DC8F8DF3A3C717010DDF870_CustomAttributesCacheGenerator_LoseElementDamage(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x45\x6C\x65\x6D\x65\x6E\x74\x20\x53\x65\x74\x74\x69\x6E\x67"), NULL);
	}
}
static void SkillBluePrint_t3CAC732F339E5C98B3239E3F4FA51B88B8C0F46C_CustomAttributesCacheGenerator_name(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SkillBluePrint_t3CAC732F339E5C98B3239E3F4FA51B88B8C0F46C_CustomAttributesCacheGenerator_HeroRequirements(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x48\x65\x72\x6F\x52\x65\x71\x75\x69\x72\x65"), NULL);
	}
}
static void SkillBluePrint_t3CAC732F339E5C98B3239E3F4FA51B88B8C0F46C_CustomAttributesCacheGenerator_AttackDamageBuff(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x74\x61\x74\x75\x73\x20\x42\x6F\x6F\x73\x74"), NULL);
	}
}
static void SkillBluePrint_t3CAC732F339E5C98B3239E3F4FA51B88B8C0F46C_CustomAttributesCacheGenerator_FireDamageBuff(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x45\x6C\x65\x6D\x65\x6E\x74\x20\x42\x6F\x6F\x73\x74"), NULL);
	}
}
static void SkillBluePrint_t3CAC732F339E5C98B3239E3F4FA51B88B8C0F46C_CustomAttributesCacheGenerator_IsForMachine_Gun(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x42\x75\x66\x66\x20\x4F\x6E\x6C\x79\x20\x41\x74\x74\x61\x63\x6B\x54\x79\x70\x65"), NULL);
	}
}
static void SkillBluePrint_t3CAC732F339E5C98B3239E3F4FA51B88B8C0F46C_CustomAttributesCacheGenerator_IsActive(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x68\x65\x63\x6B\x4C\x69\x73\x74"), NULL);
	}
}
static void CinemachineSwitcher_t8D51217EA2F92830409525BC6939FBF5BCF7E445_CustomAttributesCacheGenerator_U3CInstanceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CinemachineSwitcher_t8D51217EA2F92830409525BC6939FBF5BCF7E445_CustomAttributesCacheGenerator_canvas(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void CinemachineSwitcher_t8D51217EA2F92830409525BC6939FBF5BCF7E445_CustomAttributesCacheGenerator_CinemachineSwitcher_get_Instance_mF6ABBBE7B4E38636BEB09D7ADF3940BA80DC808B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CinemachineSwitcher_t8D51217EA2F92830409525BC6939FBF5BCF7E445_CustomAttributesCacheGenerator_CinemachineSwitcher_set_Instance_mC4980BCB3320D1E7B59956E65C60DCA174B641EA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void CinemachineSwitcher_t8D51217EA2F92830409525BC6939FBF5BCF7E445_CustomAttributesCacheGenerator_CinemachineSwitcher_WaitToStart_mFB3F4280F74BD988595B53EE4F855CCDBA6708F1(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitToStartU3Ed__14_tAD265766599EEE9474750E00FA4939AEAABB823A_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWaitToStartU3Ed__14_tAD265766599EEE9474750E00FA4939AEAABB823A_0_0_0_var), NULL);
	}
}
static void CinemachineSwitcher_t8D51217EA2F92830409525BC6939FBF5BCF7E445_CustomAttributesCacheGenerator_CinemachineSwitcher_WaitForCam_m20F16C627FA98D38C064729772CE31961D3735AE(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitForCamU3Ed__18_t189EA3BB11BFA6AC795F202CDE0699E1C9029277_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWaitForCamU3Ed__18_t189EA3BB11BFA6AC795F202CDE0699E1C9029277_0_0_0_var), NULL);
	}
}
static void CinemachineSwitcher_t8D51217EA2F92830409525BC6939FBF5BCF7E445_CustomAttributesCacheGenerator_CinemachineSwitcher_WaitForShowCanvas_m66CF0EE7E5A38923F80DA4040DD48571364BE4AD(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitForShowCanvasU3Ed__21_t6623746B1B2635840CC2F08A9B9C83127716121E_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWaitForShowCanvasU3Ed__21_t6623746B1B2635840CC2F08A9B9C83127716121E_0_0_0_var), NULL);
	}
}
static void U3CWaitToStartU3Ed__14_tAD265766599EEE9474750E00FA4939AEAABB823A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitToStartU3Ed__14_tAD265766599EEE9474750E00FA4939AEAABB823A_CustomAttributesCacheGenerator_U3CWaitToStartU3Ed__14__ctor_m29B7C44F25045CA2AF686E43C9FC185788C40503(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitToStartU3Ed__14_tAD265766599EEE9474750E00FA4939AEAABB823A_CustomAttributesCacheGenerator_U3CWaitToStartU3Ed__14_System_IDisposable_Dispose_mD5DE83D793D38D79C4BFC2C7300A08CBA9A27C3E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitToStartU3Ed__14_tAD265766599EEE9474750E00FA4939AEAABB823A_CustomAttributesCacheGenerator_U3CWaitToStartU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9EC5D5E119B878E35802A8030693DCAEE73E1886(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitToStartU3Ed__14_tAD265766599EEE9474750E00FA4939AEAABB823A_CustomAttributesCacheGenerator_U3CWaitToStartU3Ed__14_System_Collections_IEnumerator_Reset_m524B6AAAA453CEFB7943404F24E2BAB89E723FD3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitToStartU3Ed__14_tAD265766599EEE9474750E00FA4939AEAABB823A_CustomAttributesCacheGenerator_U3CWaitToStartU3Ed__14_System_Collections_IEnumerator_get_Current_m54B43C5EE89291B63D84359793AF6F746A48A102(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForCamU3Ed__18_t189EA3BB11BFA6AC795F202CDE0699E1C9029277_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitForCamU3Ed__18_t189EA3BB11BFA6AC795F202CDE0699E1C9029277_CustomAttributesCacheGenerator_U3CWaitForCamU3Ed__18__ctor_m0F9FDC3F7885AD8CB9300502DAB0CF3BD03ADE43(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForCamU3Ed__18_t189EA3BB11BFA6AC795F202CDE0699E1C9029277_CustomAttributesCacheGenerator_U3CWaitForCamU3Ed__18_System_IDisposable_Dispose_m60C82B3C406C8BD576F9B5C7C8A3E9399F2844F0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForCamU3Ed__18_t189EA3BB11BFA6AC795F202CDE0699E1C9029277_CustomAttributesCacheGenerator_U3CWaitForCamU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4D0F0E0221A015D42D448233811E1E3F1D18844A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForCamU3Ed__18_t189EA3BB11BFA6AC795F202CDE0699E1C9029277_CustomAttributesCacheGenerator_U3CWaitForCamU3Ed__18_System_Collections_IEnumerator_Reset_m3A75E79EA70C002A7A8E6357119F039688749E5E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForCamU3Ed__18_t189EA3BB11BFA6AC795F202CDE0699E1C9029277_CustomAttributesCacheGenerator_U3CWaitForCamU3Ed__18_System_Collections_IEnumerator_get_Current_mE2168EB7F89A76D61B57517E899160E4E598659C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForShowCanvasU3Ed__21_t6623746B1B2635840CC2F08A9B9C83127716121E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitForShowCanvasU3Ed__21_t6623746B1B2635840CC2F08A9B9C83127716121E_CustomAttributesCacheGenerator_U3CWaitForShowCanvasU3Ed__21__ctor_m35D4BF98BB8BA1BC8821E567536E8DFF2D90F18D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForShowCanvasU3Ed__21_t6623746B1B2635840CC2F08A9B9C83127716121E_CustomAttributesCacheGenerator_U3CWaitForShowCanvasU3Ed__21_System_IDisposable_Dispose_m82BACA53350D7E4E634FA212196B7DADBE60FEFA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForShowCanvasU3Ed__21_t6623746B1B2635840CC2F08A9B9C83127716121E_CustomAttributesCacheGenerator_U3CWaitForShowCanvasU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4AC62F2A7C2D79CA96258278EB16915408AB0A0F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForShowCanvasU3Ed__21_t6623746B1B2635840CC2F08A9B9C83127716121E_CustomAttributesCacheGenerator_U3CWaitForShowCanvasU3Ed__21_System_Collections_IEnumerator_Reset_m0919B8BEE473A81A79E5048EE957A3EF9DD9922B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForShowCanvasU3Ed__21_t6623746B1B2635840CC2F08A9B9C83127716121E_CustomAttributesCacheGenerator_U3CWaitForShowCanvasU3Ed__21_System_Collections_IEnumerator_get_Current_m75D2DA098B2ECBB96569E0DF8A16E85DF389FBF2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultExecutionOrder_t8495D3D4ECDFC3590621D31C3677D234D8A9BB1F * tmp = (DefaultExecutionOrder_t8495D3D4ECDFC3590621D31C3677D234D8A9BB1F *)cache->attributes[0];
		DefaultExecutionOrder__ctor_m18F4188D26702C2E3EC0AB3C1FF4AA4F5329E7A9(tmp, -1LL, NULL);
	}
}
static void InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A_CustomAttributesCacheGenerator_U3CInstanceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A_CustomAttributesCacheGenerator_OnStartTouch(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A_CustomAttributesCacheGenerator_OnEndTouch(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A_CustomAttributesCacheGenerator_InputManager_get_Instance_mC5B4EA7BB8A909DB0FDD5D9CBF67B3A1CC40181D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A_CustomAttributesCacheGenerator_InputManager_set_Instance_mB94B3A652E1A869CBB1FFBBE8CA25842E26A3C34(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A_CustomAttributesCacheGenerator_InputManager_add_OnStartTouch_mCF106264B78FB07537E71524C99EE140805FF25D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A_CustomAttributesCacheGenerator_InputManager_remove_OnStartTouch_m24C6AC368EC9AA24B40A5D1F3EF7B07A72766C7D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A_CustomAttributesCacheGenerator_InputManager_add_OnEndTouch_m50D940EFE21F790A04BCF54CE88FDF55F953CDFF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A_CustomAttributesCacheGenerator_InputManager_remove_OnEndTouch_m3B6F0D7B26BC509080CA8F30E8D80E0FCA5423D2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A_CustomAttributesCacheGenerator_InputManager_U3CStartU3Eb__17_0_m6D32B313D99F02F691422CF3763619364E7A8936(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A_CustomAttributesCacheGenerator_InputManager_U3CStartU3Eb__17_1_m3F85443FB5E717AC691727FFF4C91444705B47AD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MobileInput_t9563AE9C626D49165E67BDAF4BD0356FA1C96F0F_CustomAttributesCacheGenerator_U3CassetU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MobileInput_t9563AE9C626D49165E67BDAF4BD0356FA1C96F0F_CustomAttributesCacheGenerator_MobileInput_get_asset_m8065F2CC02A1444F16ADA2794E8071D867C46888(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void SwipeDetection_t145E193968BD5765E38B632D928348C9740796C2_CustomAttributesCacheGenerator_minimumDistance(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SwipeDetection_t145E193968BD5765E38B632D928348C9740796C2_CustomAttributesCacheGenerator_maximumTime(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void SwipeDetection_t145E193968BD5765E38B632D928348C9740796C2_CustomAttributesCacheGenerator_directionThreshold(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[1];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
}
static void ScreenCapture_t7F39027079C42E816BE74855BED21EDD4C6B940E_CustomAttributesCacheGenerator_ScreenCapture_ImageShowCase_m7C1802B6832D044746834F97B1D7D55505D707AA(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CImageShowCaseU3Ed__17_tB869F8E282B1CD6D682B87084152A7B70D6A3589_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CImageShowCaseU3Ed__17_tB869F8E282B1CD6D682B87084152A7B70D6A3589_0_0_0_var), NULL);
	}
}
static void U3CU3Ec__DisplayClass15_0_tD3EDBEF01A2BA2A697FFF2AEF29C9B3B62251261_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CImageShowCaseU3Ed__17_tB869F8E282B1CD6D682B87084152A7B70D6A3589_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CImageShowCaseU3Ed__17_tB869F8E282B1CD6D682B87084152A7B70D6A3589_CustomAttributesCacheGenerator_U3CImageShowCaseU3Ed__17__ctor_mCD4899C7ABD62E58107628266F40BCE1A71BD8C5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CImageShowCaseU3Ed__17_tB869F8E282B1CD6D682B87084152A7B70D6A3589_CustomAttributesCacheGenerator_U3CImageShowCaseU3Ed__17_System_IDisposable_Dispose_m0C3DEEFF1C66F08CCB7AFDC7309909C55D39EBBA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CImageShowCaseU3Ed__17_tB869F8E282B1CD6D682B87084152A7B70D6A3589_CustomAttributesCacheGenerator_U3CImageShowCaseU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF252504AC2355F64820C71DF7B5DBCF6FF71F9B5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CImageShowCaseU3Ed__17_tB869F8E282B1CD6D682B87084152A7B70D6A3589_CustomAttributesCacheGenerator_U3CImageShowCaseU3Ed__17_System_Collections_IEnumerator_Reset_mCA401AA94E70446FFEED94191F1061F4E0FE5AD3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CImageShowCaseU3Ed__17_tB869F8E282B1CD6D682B87084152A7B70D6A3589_CustomAttributesCacheGenerator_U3CImageShowCaseU3Ed__17_System_Collections_IEnumerator_get_Current_m470E7F0C70ED4B0BA7C5E8FA54FE620C60160B7C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void LongClickButton_t31F3E8BB2ECFE3CD6585DC84FE8F0E4F624B5402_CustomAttributesCacheGenerator_holdTime(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LongClickButton_t31F3E8BB2ECFE3CD6585DC84FE8F0E4F624B5402_CustomAttributesCacheGenerator_fillImg(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void LongClickButton_t31F3E8BB2ECFE3CD6585DC84FE8F0E4F624B5402_CustomAttributesCacheGenerator_LongClickButton_DelayClick_m57566FE8544386D752B832895C7A3C424E60F152(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDelayClickU3Ed__8_tBF346C93C4D6EAB42D93211E5296F33032B7BC72_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDelayClickU3Ed__8_tBF346C93C4D6EAB42D93211E5296F33032B7BC72_0_0_0_var), NULL);
	}
}
static void U3CDelayClickU3Ed__8_tBF346C93C4D6EAB42D93211E5296F33032B7BC72_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDelayClickU3Ed__8_tBF346C93C4D6EAB42D93211E5296F33032B7BC72_CustomAttributesCacheGenerator_U3CDelayClickU3Ed__8__ctor_m482F03E4612F42F141D0E23529A7F47BD9D322D7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelayClickU3Ed__8_tBF346C93C4D6EAB42D93211E5296F33032B7BC72_CustomAttributesCacheGenerator_U3CDelayClickU3Ed__8_System_IDisposable_Dispose_mD47B909BCEBDDC24A464483BCCF4456D5566180F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelayClickU3Ed__8_tBF346C93C4D6EAB42D93211E5296F33032B7BC72_CustomAttributesCacheGenerator_U3CDelayClickU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m120379B849FCB4CEA022C33647A4ED839DA3B925(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelayClickU3Ed__8_tBF346C93C4D6EAB42D93211E5296F33032B7BC72_CustomAttributesCacheGenerator_U3CDelayClickU3Ed__8_System_Collections_IEnumerator_Reset_m227BC1F635FF1B103C8353698AE384738441998A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelayClickU3Ed__8_tBF346C93C4D6EAB42D93211E5296F33032B7BC72_CustomAttributesCacheGenerator_U3CDelayClickU3Ed__8_System_Collections_IEnumerator_get_Current_m4DB8DE888D03118595DF28332153EAE8662A0447(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec_t885743CA8B20F056197CF0A08B7EE1CB42AF27DC_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EnemyWayPoint_tF3382B5960D751B735AE89323A5B1E25D2AD2901_CustomAttributesCacheGenerator_U3CWayPointsU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EnemyWayPoint_tF3382B5960D751B735AE89323A5B1E25D2AD2901_CustomAttributesCacheGenerator_U3CInstanceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EnemyWayPoint_tF3382B5960D751B735AE89323A5B1E25D2AD2901_CustomAttributesCacheGenerator_EnemyWayPoint_get_WayPoints_mA9B0EC26719F8C9A85CE11979FA70ACAA767FC8D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EnemyWayPoint_tF3382B5960D751B735AE89323A5B1E25D2AD2901_CustomAttributesCacheGenerator_EnemyWayPoint_set_WayPoints_m29C19EC458381493C0C989548754E56303BB6B6F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EnemyWayPoint_tF3382B5960D751B735AE89323A5B1E25D2AD2901_CustomAttributesCacheGenerator_EnemyWayPoint_get_Instance_m9E8A4D26FF4B696112549EDA6B14E2A419990393(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void EnemyWayPoint_tF3382B5960D751B735AE89323A5B1E25D2AD2901_CustomAttributesCacheGenerator_EnemyWayPoint_set_Instance_m7FCF1F5029861DE0A65F2271EB287F6CC3D0C4F6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_PlayAsGuestBuuton(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x42\x75\x74\x74\x6F\x6E"), NULL);
	}
}
static void FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_logoutButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_connectIdButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_PlayerName(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6C\x61\x79\x65\x72\x20\x49\x6E\x66\x6F"), NULL);
	}
}
static void FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_NotificationTextTitel(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x6F\x74\x69\x66\x69\x63\x61\x74\x69\x6F\x6E\x20\x49\x6E\x66\x6F"), NULL);
	}
}
static void FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_Input_Url(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x55\x70\x6C\x6F\x61\x64\x20\x50\x68\x6F\x74\x6F"), NULL);
	}
}
static void FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_rankPlayer(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x65\x61\x64\x65\x72\x20\x42\x6F\x61\x72\x64"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_rankPanel(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_leaderboardPanel(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_rankDataPrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_photoLeaderBoard(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_waveLeaderBoard(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_FirebaseManager_CheckAlreadyName_m25B27E898AA4C9F8DA81920ED46EE6B052C2DDE5(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCheckAlreadyNameU3Ed__50_t81299FEF47EDC5385754950A82C9FE4E5176462D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCheckAlreadyNameU3Ed__50_t81299FEF47EDC5385754950A82C9FE4E5176462D_0_0_0_var), NULL);
	}
}
static void FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_FirebaseManager_CloseNotification_m909268520C1AA67BBB1CC394D2A6BC6E63520F43(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCloseNotificationU3Ed__62_t50067E67A5466C3585675771E09DE822592CB4D0_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCloseNotificationU3Ed__62_t50067E67A5466C3585675771E09DE822592CB4D0_0_0_0_var), NULL);
	}
}
static void FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_FirebaseManager_CheckAlreadyNameIdTransfer_m5BFE3B6936231D494314BE845F42A5AF4AC949FB(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCheckAlreadyNameIdTransferU3Ed__74_t7451AB99B91CA94A2D6E4F546CA0FD8CC686ED92_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCheckAlreadyNameIdTransferU3Ed__74_t7451AB99B91CA94A2D6E4F546CA0FD8CC686ED92_0_0_0_var), NULL);
	}
}
static void FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_FirebaseManager_LoadUserScoreFromDB_mD894FC790D4E61DCE6BA00C7CD7FDDAA39CD1E36(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLoadUserScoreFromDBU3Ed__75_tB538B410886D8F26EB89210EE854B5F7389C75DB_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CLoadUserScoreFromDBU3Ed__75_tB538B410886D8F26EB89210EE854B5F7389C75DB_0_0_0_var), NULL);
	}
}
static void FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_FirebaseManager_DownloadImage_m7181991C7853AADCE4D73A9D254FD97D898B378A(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDownloadImageU3Ed__76_t723888129E1A8E7C4732F71C5E435C1DEC50CE12_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDownloadImageU3Ed__76_t723888129E1A8E7C4732F71C5E435C1DEC50CE12_0_0_0_var), NULL);
	}
}
static void FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_FirebaseManager_LoadLeaderBoardData_m06C184117B561767D286D1B7BD89173A9A4AB3CD(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLoadLeaderBoardDataU3Ed__80_t53D5F08F28E7939D79D6BB3754783EF0607ABF3D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CLoadLeaderBoardDataU3Ed__80_t53D5F08F28E7939D79D6BB3754783EF0607ABF3D_0_0_0_var), NULL);
	}
}
static void FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_FirebaseManager_U3CStartU3Eb__48_0_m36581C2AEC12E1C90BB72B87048D131734928B3C(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_FirebaseManager_U3CSignInUserU3Eb__55_0_mC0D0D74A9B0AC51CEA0C4944D148B7D608C0EDA1(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_FirebaseManager_U3CSendPasswordResetEmailU3Eb__59_0_m91D2331DFDE02DA0307FB03795621C211246F4A2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_FirebaseManager_U3CDeleteUserU3Eb__67_0_mBFE0C01FAAF9354F30490CB854BF25624839A5D9(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_FirebaseManager_U3CPlayAsGuestU3Eb__71_0_m0D1455F6948BC1B608F5C3150AEEFD920DE04982(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass50_0_t21EE77FA2D4C57BAEF865E100B9BFC9FADF79542_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCheckAlreadyNameU3Ed__50_t81299FEF47EDC5385754950A82C9FE4E5176462D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCheckAlreadyNameU3Ed__50_t81299FEF47EDC5385754950A82C9FE4E5176462D_CustomAttributesCacheGenerator_U3CCheckAlreadyNameU3Ed__50__ctor_m84595AAC374EC74BF9C5457425E54D84BEAB35AD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCheckAlreadyNameU3Ed__50_t81299FEF47EDC5385754950A82C9FE4E5176462D_CustomAttributesCacheGenerator_U3CCheckAlreadyNameU3Ed__50_System_IDisposable_Dispose_m35313E7C7FD768948368F6DA71DB5F5E08EE80AF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCheckAlreadyNameU3Ed__50_t81299FEF47EDC5385754950A82C9FE4E5176462D_CustomAttributesCacheGenerator_U3CCheckAlreadyNameU3Ed__50_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCE98E48779C30473658D59AC7A4E3708DB45F871(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCheckAlreadyNameU3Ed__50_t81299FEF47EDC5385754950A82C9FE4E5176462D_CustomAttributesCacheGenerator_U3CCheckAlreadyNameU3Ed__50_System_Collections_IEnumerator_Reset_mCD04C1434BC6EB6D7F385D7255F507715CE0413E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCheckAlreadyNameU3Ed__50_t81299FEF47EDC5385754950A82C9FE4E5176462D_CustomAttributesCacheGenerator_U3CCheckAlreadyNameU3Ed__50_System_Collections_IEnumerator_get_Current_mB63916D8978C57C9AE01329190AFA4B9DB1C4251(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass52_0_t190C905043F8FB0FAA7184C28B9AD7236C110A18_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass53_0_t066EC8A4868845AB2F7569EF240D1DF01C06ED67_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCloseNotificationU3Ed__62_t50067E67A5466C3585675771E09DE822592CB4D0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCloseNotificationU3Ed__62_t50067E67A5466C3585675771E09DE822592CB4D0_CustomAttributesCacheGenerator_U3CCloseNotificationU3Ed__62__ctor_m43778604560229E1A3384C14D2CB78F31BD3EC62(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCloseNotificationU3Ed__62_t50067E67A5466C3585675771E09DE822592CB4D0_CustomAttributesCacheGenerator_U3CCloseNotificationU3Ed__62_System_IDisposable_Dispose_m8707E38A3EE8232B8D3B016AB054931AE47A8550(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCloseNotificationU3Ed__62_t50067E67A5466C3585675771E09DE822592CB4D0_CustomAttributesCacheGenerator_U3CCloseNotificationU3Ed__62_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8EC69539785A0FFFA97B6B1E2BECD465CAA38899(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCloseNotificationU3Ed__62_t50067E67A5466C3585675771E09DE822592CB4D0_CustomAttributesCacheGenerator_U3CCloseNotificationU3Ed__62_System_Collections_IEnumerator_Reset_mDF06AABB22CECEE0C65B06A046FDC6A0B0B07F2E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCloseNotificationU3Ed__62_t50067E67A5466C3585675771E09DE822592CB4D0_CustomAttributesCacheGenerator_U3CCloseNotificationU3Ed__62_System_Collections_IEnumerator_get_Current_m9ECE534C9D9315FD7B66D40424D4390992F60552(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass73_0_t82FB778BF3C3CE9AAEDF94879073078125BA2776_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass74_0_t9BA379A9F07DDE57A0A173B68851C4252C3B15F2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCheckAlreadyNameIdTransferU3Ed__74_t7451AB99B91CA94A2D6E4F546CA0FD8CC686ED92_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCheckAlreadyNameIdTransferU3Ed__74_t7451AB99B91CA94A2D6E4F546CA0FD8CC686ED92_CustomAttributesCacheGenerator_U3CCheckAlreadyNameIdTransferU3Ed__74__ctor_mF54BC3DA1886144F3C8CBFD0F06CBFB613D1250E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCheckAlreadyNameIdTransferU3Ed__74_t7451AB99B91CA94A2D6E4F546CA0FD8CC686ED92_CustomAttributesCacheGenerator_U3CCheckAlreadyNameIdTransferU3Ed__74_System_IDisposable_Dispose_m6840128964E0260E22537A16FBC442377B5CB78B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCheckAlreadyNameIdTransferU3Ed__74_t7451AB99B91CA94A2D6E4F546CA0FD8CC686ED92_CustomAttributesCacheGenerator_U3CCheckAlreadyNameIdTransferU3Ed__74_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAC8343C114D017BB1AEE74AB38D9A4AC6D46E47D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCheckAlreadyNameIdTransferU3Ed__74_t7451AB99B91CA94A2D6E4F546CA0FD8CC686ED92_CustomAttributesCacheGenerator_U3CCheckAlreadyNameIdTransferU3Ed__74_System_Collections_IEnumerator_Reset_m58D65B1A3ABB708128F90AC7B13AC4C48862B114(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCheckAlreadyNameIdTransferU3Ed__74_t7451AB99B91CA94A2D6E4F546CA0FD8CC686ED92_CustomAttributesCacheGenerator_U3CCheckAlreadyNameIdTransferU3Ed__74_System_Collections_IEnumerator_get_Current_mAECF9042F87AB9BBDA616C14ADAF015C8CABDC4A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass75_0_tE401FBB76B127E3CED9E4D9EF0D025E97503F7A0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadUserScoreFromDBU3Ed__75_tB538B410886D8F26EB89210EE854B5F7389C75DB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadUserScoreFromDBU3Ed__75_tB538B410886D8F26EB89210EE854B5F7389C75DB_CustomAttributesCacheGenerator_U3CLoadUserScoreFromDBU3Ed__75__ctor_m704CC19001BACD6E7621DBB5487CF8F23B19EB79(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadUserScoreFromDBU3Ed__75_tB538B410886D8F26EB89210EE854B5F7389C75DB_CustomAttributesCacheGenerator_U3CLoadUserScoreFromDBU3Ed__75_System_IDisposable_Dispose_mB57F1F147F4E74A61906CF48147B58D654B61655(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadUserScoreFromDBU3Ed__75_tB538B410886D8F26EB89210EE854B5F7389C75DB_CustomAttributesCacheGenerator_U3CLoadUserScoreFromDBU3Ed__75_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7EC769D8AC3147416F9224A21B0C4DB92846B4F7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadUserScoreFromDBU3Ed__75_tB538B410886D8F26EB89210EE854B5F7389C75DB_CustomAttributesCacheGenerator_U3CLoadUserScoreFromDBU3Ed__75_System_Collections_IEnumerator_Reset_m5DDB2E8C37B38A0BAEEB860654298AC7E64DD21A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadUserScoreFromDBU3Ed__75_tB538B410886D8F26EB89210EE854B5F7389C75DB_CustomAttributesCacheGenerator_U3CLoadUserScoreFromDBU3Ed__75_System_Collections_IEnumerator_get_Current_m07AF984DCC7A697BD9F97B5D7FE103676B722BFB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloadImageU3Ed__76_t723888129E1A8E7C4732F71C5E435C1DEC50CE12_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDownloadImageU3Ed__76_t723888129E1A8E7C4732F71C5E435C1DEC50CE12_CustomAttributesCacheGenerator_U3CDownloadImageU3Ed__76__ctor_m5CB98433BF9CD23864038893E2E3B3A6A6DE27EF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloadImageU3Ed__76_t723888129E1A8E7C4732F71C5E435C1DEC50CE12_CustomAttributesCacheGenerator_U3CDownloadImageU3Ed__76_System_IDisposable_Dispose_mD04A6581A340602E2B175B8886A700423A8F9509(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloadImageU3Ed__76_t723888129E1A8E7C4732F71C5E435C1DEC50CE12_CustomAttributesCacheGenerator_U3CDownloadImageU3Ed__76_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFBDB22DD4A78362581BFEC7DCBAC6DC48D18F23A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloadImageU3Ed__76_t723888129E1A8E7C4732F71C5E435C1DEC50CE12_CustomAttributesCacheGenerator_U3CDownloadImageU3Ed__76_System_Collections_IEnumerator_Reset_m88F7C5F894E29D5CB0C542490E7A8EDF17C01245(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloadImageU3Ed__76_t723888129E1A8E7C4732F71C5E435C1DEC50CE12_CustomAttributesCacheGenerator_U3CDownloadImageU3Ed__76_System_Collections_IEnumerator_get_Current_m46B996CB3BECE15A4B97173AF9BB4449777E971C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass80_0_tA73234661199F943E3E509B8AA692AC64E2733D3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadLeaderBoardDataU3Ed__80_t53D5F08F28E7939D79D6BB3754783EF0607ABF3D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadLeaderBoardDataU3Ed__80_t53D5F08F28E7939D79D6BB3754783EF0607ABF3D_CustomAttributesCacheGenerator_U3CLoadLeaderBoardDataU3Ed__80__ctor_mC00F58C654FE55829D55D858BBEAB5EBA0E804C1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadLeaderBoardDataU3Ed__80_t53D5F08F28E7939D79D6BB3754783EF0607ABF3D_CustomAttributesCacheGenerator_U3CLoadLeaderBoardDataU3Ed__80_System_IDisposable_Dispose_m0C8C77ADD2592E7F7409CEE5C220CDADDA47B9BB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadLeaderBoardDataU3Ed__80_t53D5F08F28E7939D79D6BB3754783EF0607ABF3D_CustomAttributesCacheGenerator_U3CLoadLeaderBoardDataU3Ed__80_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8F6B7887F6E6B15F168E5A764040545E91375F54(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadLeaderBoardDataU3Ed__80_t53D5F08F28E7939D79D6BB3754783EF0607ABF3D_CustomAttributesCacheGenerator_U3CLoadLeaderBoardDataU3Ed__80_System_Collections_IEnumerator_Reset_m9CB4781BA05AC6B10FD795F9C3DB8BC690E62CB0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadLeaderBoardDataU3Ed__80_t53D5F08F28E7939D79D6BB3754783EF0607ABF3D_CustomAttributesCacheGenerator_U3CLoadLeaderBoardDataU3Ed__80_System_Collections_IEnumerator_get_Current_m0E44B192105C8A4F010F1976F1E212B3B6287568(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void RankData_tFBD8F1297575E2FBA581C146C7D9926339021105_CustomAttributesCacheGenerator_NumberText(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x55\x49"), NULL);
	}
}
static void RankData_tFBD8F1297575E2FBA581C146C7D9926339021105_CustomAttributesCacheGenerator_RankData_DownloadImage_mB3634704E17275CFFC430EA174C96A48CDFCA196(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDownloadImageU3Ed__5_t2794312741EA00E7B69D55B73B3136C5C9F354C0_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDownloadImageU3Ed__5_t2794312741EA00E7B69D55B73B3136C5C9F354C0_0_0_0_var), NULL);
	}
}
static void U3CDownloadImageU3Ed__5_t2794312741EA00E7B69D55B73B3136C5C9F354C0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDownloadImageU3Ed__5_t2794312741EA00E7B69D55B73B3136C5C9F354C0_CustomAttributesCacheGenerator_U3CDownloadImageU3Ed__5__ctor_m56A893022DA599581E1A11DCF1D1BF961B1A0316(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloadImageU3Ed__5_t2794312741EA00E7B69D55B73B3136C5C9F354C0_CustomAttributesCacheGenerator_U3CDownloadImageU3Ed__5_System_IDisposable_Dispose_m2398236E2D1FB99231363CFF876F7EB857FA7042(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloadImageU3Ed__5_t2794312741EA00E7B69D55B73B3136C5C9F354C0_CustomAttributesCacheGenerator_U3CDownloadImageU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m26063764B51701B2831DCC38CAB2DA3C0A819609(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloadImageU3Ed__5_t2794312741EA00E7B69D55B73B3136C5C9F354C0_CustomAttributesCacheGenerator_U3CDownloadImageU3Ed__5_System_Collections_IEnumerator_Reset_m3EFD1AFEB3C0C56382735B8E26FA7B102D127CCB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloadImageU3Ed__5_t2794312741EA00E7B69D55B73B3136C5C9F354C0_CustomAttributesCacheGenerator_U3CDownloadImageU3Ed__5_System_Collections_IEnumerator_get_Current_mABB560831447A0159C1C41B02A70EEA0E3292041(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void RankManager_t046274A614E123E3C455190866CE2B98C88B87F0_CustomAttributesCacheGenerator_waveSurvived(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x50\x6C\x61\x79\x65\x72\x20\x44\x61\x74\x61"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RankManager_t046274A614E123E3C455190866CE2B98C88B87F0_CustomAttributesCacheGenerator_rankPlayer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RankManager_t046274A614E123E3C455190866CE2B98C88B87F0_CustomAttributesCacheGenerator_playerImage(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RankManager_t046274A614E123E3C455190866CE2B98C88B87F0_CustomAttributesCacheGenerator_RankManager_CheckDamageOverCurrentScore_mCB8486642C343A640FA9437DE98B3B4D87616034(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCheckDamageOverCurrentScoreU3Ed__12_t07A41FEF6FA87A2ECE1E7E0FF28AADDFA79B4546_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCheckDamageOverCurrentScoreU3Ed__12_t07A41FEF6FA87A2ECE1E7E0FF28AADDFA79B4546_0_0_0_var), NULL);
	}
}
static void RankManager_t046274A614E123E3C455190866CE2B98C88B87F0_CustomAttributesCacheGenerator_RankManager_CheckTimeSurvivedOverCurrentScore_m04A388299D839D31AAD14DF4E736989BDB46EFA0(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCheckTimeSurvivedOverCurrentScoreU3Ed__13_t361F8E558207AE97EA78DAC6651DDAA2FB989EE1_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCheckTimeSurvivedOverCurrentScoreU3Ed__13_t361F8E558207AE97EA78DAC6651DDAA2FB989EE1_0_0_0_var), NULL);
	}
}
static void RankManager_t046274A614E123E3C455190866CE2B98C88B87F0_CustomAttributesCacheGenerator_RankManager_UpdateDamageDone_mD0D78C65C914F189377894AE8F83E0788D6B2683(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CUpdateDamageDoneU3Ed__14_t1D42A96E93CA3C068716D61E6E4C4A8CE4087D21_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CUpdateDamageDoneU3Ed__14_t1D42A96E93CA3C068716D61E6E4C4A8CE4087D21_0_0_0_var), NULL);
	}
}
static void RankManager_t046274A614E123E3C455190866CE2B98C88B87F0_CustomAttributesCacheGenerator_RankManager_UpdateWaveSurvived_mF9400B91A4798D5BBE0AFCF5267A32977115979C(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CUpdateWaveSurvivedU3Ed__15_tF7DE1FBC2C2CC828B58C7F6228A9912502633D6F_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CUpdateWaveSurvivedU3Ed__15_tF7DE1FBC2C2CC828B58C7F6228A9912502633D6F_0_0_0_var), NULL);
	}
}
static void RankManager_t046274A614E123E3C455190866CE2B98C88B87F0_CustomAttributesCacheGenerator_RankManager_UpdateTimeSurvived_m6B3EFE193E94C4FBCCA5FA99EF20AE3D7417235F(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CUpdateTimeSurvivedU3Ed__16_tD6AF10B6C261DD6230297852068466DCA37C64DA_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CUpdateTimeSurvivedU3Ed__16_tD6AF10B6C261DD6230297852068466DCA37C64DA_0_0_0_var), NULL);
	}
}
static void RankManager_t046274A614E123E3C455190866CE2B98C88B87F0_CustomAttributesCacheGenerator_RankManager_LoadUserScoreFromDB_m0C1F32AE08783D1EFE74247850F9423A856FA627(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLoadUserScoreFromDBU3Ed__17_tA72B0A24BFD2A694425FDB145D618C9F8A7C6AA8_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CLoadUserScoreFromDBU3Ed__17_tA72B0A24BFD2A694425FDB145D618C9F8A7C6AA8_0_0_0_var), NULL);
	}
}
static void RankManager_t046274A614E123E3C455190866CE2B98C88B87F0_CustomAttributesCacheGenerator_RankManager_LoadLeaderBoardData_mEA3E6CB77B24A2D858BF2423FCB7DCD52FC032B6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLoadLeaderBoardDataU3Ed__18_t85C0AE47A469B364C930201232E48E31F00B91D1_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CLoadLeaderBoardDataU3Ed__18_t85C0AE47A469B364C930201232E48E31F00B91D1_0_0_0_var), NULL);
	}
}
static void RankManager_t046274A614E123E3C455190866CE2B98C88B87F0_CustomAttributesCacheGenerator_RankManager_DownloadImage_m9D8910B3E34BE8AEB8BDC0DD2E91135FF024FED7(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDownloadImageU3Ed__19_tC075483F4C0D4B7D7065D99C2135C5A8A0BCA1D7_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDownloadImageU3Ed__19_tC075483F4C0D4B7D7065D99C2135C5A8A0BCA1D7_0_0_0_var), NULL);
	}
}
static void U3CU3Ec__DisplayClass12_0_t88DE0C69335FF40F58BD2B9215EA227CE32600D9_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCheckDamageOverCurrentScoreU3Ed__12_t07A41FEF6FA87A2ECE1E7E0FF28AADDFA79B4546_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCheckDamageOverCurrentScoreU3Ed__12_t07A41FEF6FA87A2ECE1E7E0FF28AADDFA79B4546_CustomAttributesCacheGenerator_U3CCheckDamageOverCurrentScoreU3Ed__12__ctor_m9FBF500C44EB1E27C562C6EB65C62D9ABF486BB5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCheckDamageOverCurrentScoreU3Ed__12_t07A41FEF6FA87A2ECE1E7E0FF28AADDFA79B4546_CustomAttributesCacheGenerator_U3CCheckDamageOverCurrentScoreU3Ed__12_System_IDisposable_Dispose_m7D36BC8498D8CBDFF8D4E22E822E2573EE8A31A0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCheckDamageOverCurrentScoreU3Ed__12_t07A41FEF6FA87A2ECE1E7E0FF28AADDFA79B4546_CustomAttributesCacheGenerator_U3CCheckDamageOverCurrentScoreU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m64A78464A601CF744FD9495D04B11CB0AB3B3332(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCheckDamageOverCurrentScoreU3Ed__12_t07A41FEF6FA87A2ECE1E7E0FF28AADDFA79B4546_CustomAttributesCacheGenerator_U3CCheckDamageOverCurrentScoreU3Ed__12_System_Collections_IEnumerator_Reset_m0ED25AEE06E5E9ACAE43E03BE0DD6424A38EB4B1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCheckDamageOverCurrentScoreU3Ed__12_t07A41FEF6FA87A2ECE1E7E0FF28AADDFA79B4546_CustomAttributesCacheGenerator_U3CCheckDamageOverCurrentScoreU3Ed__12_System_Collections_IEnumerator_get_Current_m5D792B1CE7190B73BC86974F72B0B35ACC2DCCC7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass13_0_t0382DE6831C707ADD7AF5B404C2C4A2EFBFF664D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCheckTimeSurvivedOverCurrentScoreU3Ed__13_t361F8E558207AE97EA78DAC6651DDAA2FB989EE1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCheckTimeSurvivedOverCurrentScoreU3Ed__13_t361F8E558207AE97EA78DAC6651DDAA2FB989EE1_CustomAttributesCacheGenerator_U3CCheckTimeSurvivedOverCurrentScoreU3Ed__13__ctor_mC125234B920EDD35FA13597070656207C0559A01(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCheckTimeSurvivedOverCurrentScoreU3Ed__13_t361F8E558207AE97EA78DAC6651DDAA2FB989EE1_CustomAttributesCacheGenerator_U3CCheckTimeSurvivedOverCurrentScoreU3Ed__13_System_IDisposable_Dispose_m5C4AD400A1C79CDE7A58680CDD80589512DF527A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCheckTimeSurvivedOverCurrentScoreU3Ed__13_t361F8E558207AE97EA78DAC6651DDAA2FB989EE1_CustomAttributesCacheGenerator_U3CCheckTimeSurvivedOverCurrentScoreU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m79CDF24809F8E9C369BC5D7C6C5D6E52F49464A0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCheckTimeSurvivedOverCurrentScoreU3Ed__13_t361F8E558207AE97EA78DAC6651DDAA2FB989EE1_CustomAttributesCacheGenerator_U3CCheckTimeSurvivedOverCurrentScoreU3Ed__13_System_Collections_IEnumerator_Reset_mA71171569E9A5B046E37F9679E3A6514B4AAFEAB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCheckTimeSurvivedOverCurrentScoreU3Ed__13_t361F8E558207AE97EA78DAC6651DDAA2FB989EE1_CustomAttributesCacheGenerator_U3CCheckTimeSurvivedOverCurrentScoreU3Ed__13_System_Collections_IEnumerator_get_Current_mED6D775E0EAD9B2B09E6410C85BFC9E7852E6D10(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass14_0_t90ACA593716E00F111572479B558FEDB152693B6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CUpdateDamageDoneU3Ed__14_t1D42A96E93CA3C068716D61E6E4C4A8CE4087D21_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CUpdateDamageDoneU3Ed__14_t1D42A96E93CA3C068716D61E6E4C4A8CE4087D21_CustomAttributesCacheGenerator_U3CUpdateDamageDoneU3Ed__14__ctor_mAB8EF0D4F0930E31C58ED12AFE14E283DDC7EE50(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUpdateDamageDoneU3Ed__14_t1D42A96E93CA3C068716D61E6E4C4A8CE4087D21_CustomAttributesCacheGenerator_U3CUpdateDamageDoneU3Ed__14_System_IDisposable_Dispose_mF8D5C1001B4D15B1CE1F7795A5753AC7EF9911EF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUpdateDamageDoneU3Ed__14_t1D42A96E93CA3C068716D61E6E4C4A8CE4087D21_CustomAttributesCacheGenerator_U3CUpdateDamageDoneU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF5C127E539E04AB52223DBC2368D5924E4E03973(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUpdateDamageDoneU3Ed__14_t1D42A96E93CA3C068716D61E6E4C4A8CE4087D21_CustomAttributesCacheGenerator_U3CUpdateDamageDoneU3Ed__14_System_Collections_IEnumerator_Reset_m0D826E9EC709E9DA07E20F68E4D1B7D201161304(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUpdateDamageDoneU3Ed__14_t1D42A96E93CA3C068716D61E6E4C4A8CE4087D21_CustomAttributesCacheGenerator_U3CUpdateDamageDoneU3Ed__14_System_Collections_IEnumerator_get_Current_m28E02CEDC813B4CDF8898076064DC9FDA23E56E6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass15_0_t3700D12D1F7A05220C5F1CB9C571ABA78035F41B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CUpdateWaveSurvivedU3Ed__15_tF7DE1FBC2C2CC828B58C7F6228A9912502633D6F_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CUpdateWaveSurvivedU3Ed__15_tF7DE1FBC2C2CC828B58C7F6228A9912502633D6F_CustomAttributesCacheGenerator_U3CUpdateWaveSurvivedU3Ed__15__ctor_m32610F33973E8345603BD6EA5FA6009B3F3FBF79(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUpdateWaveSurvivedU3Ed__15_tF7DE1FBC2C2CC828B58C7F6228A9912502633D6F_CustomAttributesCacheGenerator_U3CUpdateWaveSurvivedU3Ed__15_System_IDisposable_Dispose_mD827AC0451DBDBDC047E281486D890B27354253D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUpdateWaveSurvivedU3Ed__15_tF7DE1FBC2C2CC828B58C7F6228A9912502633D6F_CustomAttributesCacheGenerator_U3CUpdateWaveSurvivedU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m54EA5333230F4F5CE9F3E1A7E97BFCA71F16B538(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUpdateWaveSurvivedU3Ed__15_tF7DE1FBC2C2CC828B58C7F6228A9912502633D6F_CustomAttributesCacheGenerator_U3CUpdateWaveSurvivedU3Ed__15_System_Collections_IEnumerator_Reset_mAEF947D6C4ED3C809ECBEDFE1836E725B5D05A45(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUpdateWaveSurvivedU3Ed__15_tF7DE1FBC2C2CC828B58C7F6228A9912502633D6F_CustomAttributesCacheGenerator_U3CUpdateWaveSurvivedU3Ed__15_System_Collections_IEnumerator_get_Current_mDC7061BB2AF7F2A042B2C0E6D4F137EC8F5F1AD3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass16_0_t836D24D6A1DC7B774566A3BB1F148849AE96B0DD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CUpdateTimeSurvivedU3Ed__16_tD6AF10B6C261DD6230297852068466DCA37C64DA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CUpdateTimeSurvivedU3Ed__16_tD6AF10B6C261DD6230297852068466DCA37C64DA_CustomAttributesCacheGenerator_U3CUpdateTimeSurvivedU3Ed__16__ctor_mE7CA8949AA742F0CD2614EC3BF5905CEA3373323(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUpdateTimeSurvivedU3Ed__16_tD6AF10B6C261DD6230297852068466DCA37C64DA_CustomAttributesCacheGenerator_U3CUpdateTimeSurvivedU3Ed__16_System_IDisposable_Dispose_mB43ADA3CF19E76A5808A135D3E7E3E0D03793A8B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUpdateTimeSurvivedU3Ed__16_tD6AF10B6C261DD6230297852068466DCA37C64DA_CustomAttributesCacheGenerator_U3CUpdateTimeSurvivedU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE11BE326E56200BFE14E5915BFEAB2B74C051FF2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUpdateTimeSurvivedU3Ed__16_tD6AF10B6C261DD6230297852068466DCA37C64DA_CustomAttributesCacheGenerator_U3CUpdateTimeSurvivedU3Ed__16_System_Collections_IEnumerator_Reset_m9C672B2F72A298CB037B3B5753468644270E4BEF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CUpdateTimeSurvivedU3Ed__16_tD6AF10B6C261DD6230297852068466DCA37C64DA_CustomAttributesCacheGenerator_U3CUpdateTimeSurvivedU3Ed__16_System_Collections_IEnumerator_get_Current_m3E8EBFA8DC883686D75A92E8E7E3EDF97EDD4DD7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass17_0_t899D640FF0B7889B20492FAE59103FCE6CB1B4EB_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadUserScoreFromDBU3Ed__17_tA72B0A24BFD2A694425FDB145D618C9F8A7C6AA8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadUserScoreFromDBU3Ed__17_tA72B0A24BFD2A694425FDB145D618C9F8A7C6AA8_CustomAttributesCacheGenerator_U3CLoadUserScoreFromDBU3Ed__17__ctor_m8D1FA3BBE852BAE443A006877DE97924D717A842(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadUserScoreFromDBU3Ed__17_tA72B0A24BFD2A694425FDB145D618C9F8A7C6AA8_CustomAttributesCacheGenerator_U3CLoadUserScoreFromDBU3Ed__17_System_IDisposable_Dispose_m2D7F9A2A3A5BF15A09C5E667906D55A8F3A7243D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadUserScoreFromDBU3Ed__17_tA72B0A24BFD2A694425FDB145D618C9F8A7C6AA8_CustomAttributesCacheGenerator_U3CLoadUserScoreFromDBU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m930F67A39ADE639B8D30C6DB54037E755C365308(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadUserScoreFromDBU3Ed__17_tA72B0A24BFD2A694425FDB145D618C9F8A7C6AA8_CustomAttributesCacheGenerator_U3CLoadUserScoreFromDBU3Ed__17_System_Collections_IEnumerator_Reset_m19BD1A5B2D3986337D43651A6346B78AFA0EAB93(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadUserScoreFromDBU3Ed__17_tA72B0A24BFD2A694425FDB145D618C9F8A7C6AA8_CustomAttributesCacheGenerator_U3CLoadUserScoreFromDBU3Ed__17_System_Collections_IEnumerator_get_Current_m16A3FB960C8208C64ABA393EEA909C9C289F0481(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass18_0_t026DBAF0800E778494BBAAF965A1219FFF4EF201_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadLeaderBoardDataU3Ed__18_t85C0AE47A469B364C930201232E48E31F00B91D1_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLoadLeaderBoardDataU3Ed__18_t85C0AE47A469B364C930201232E48E31F00B91D1_CustomAttributesCacheGenerator_U3CLoadLeaderBoardDataU3Ed__18__ctor_mC41A8A5403C2606BB13F4B461653F8E5D36C1F2F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadLeaderBoardDataU3Ed__18_t85C0AE47A469B364C930201232E48E31F00B91D1_CustomAttributesCacheGenerator_U3CLoadLeaderBoardDataU3Ed__18_System_IDisposable_Dispose_m1618C92B5A0462FA30052FFC0F1FCBF0F97A6875(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadLeaderBoardDataU3Ed__18_t85C0AE47A469B364C930201232E48E31F00B91D1_CustomAttributesCacheGenerator_U3CLoadLeaderBoardDataU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBFA6427E3891D37646831F9AFA8290094EFA9D71(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadLeaderBoardDataU3Ed__18_t85C0AE47A469B364C930201232E48E31F00B91D1_CustomAttributesCacheGenerator_U3CLoadLeaderBoardDataU3Ed__18_System_Collections_IEnumerator_Reset_mEF596DF92F906E40A50D828F7AEA4E7897DE215C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLoadLeaderBoardDataU3Ed__18_t85C0AE47A469B364C930201232E48E31F00B91D1_CustomAttributesCacheGenerator_U3CLoadLeaderBoardDataU3Ed__18_System_Collections_IEnumerator_get_Current_m8CDA43B0A3DCCC9938B4E6E503DE3A784B6FB011(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloadImageU3Ed__19_tC075483F4C0D4B7D7065D99C2135C5A8A0BCA1D7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDownloadImageU3Ed__19_tC075483F4C0D4B7D7065D99C2135C5A8A0BCA1D7_CustomAttributesCacheGenerator_U3CDownloadImageU3Ed__19__ctor_mA337C682C0E9601A354AF20655032AEA526D026C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloadImageU3Ed__19_tC075483F4C0D4B7D7065D99C2135C5A8A0BCA1D7_CustomAttributesCacheGenerator_U3CDownloadImageU3Ed__19_System_IDisposable_Dispose_mE416C30E9AE2D2DEE3C4D14332D65CF86208C5B5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloadImageU3Ed__19_tC075483F4C0D4B7D7065D99C2135C5A8A0BCA1D7_CustomAttributesCacheGenerator_U3CDownloadImageU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDB1F79F0F8367FFE5586098C13EB91E7547542EA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloadImageU3Ed__19_tC075483F4C0D4B7D7065D99C2135C5A8A0BCA1D7_CustomAttributesCacheGenerator_U3CDownloadImageU3Ed__19_System_Collections_IEnumerator_Reset_mA266BA2E8E8DCC326775AC8272102BB1B324ED9A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDownloadImageU3Ed__19_tC075483F4C0D4B7D7065D99C2135C5A8A0BCA1D7_CustomAttributesCacheGenerator_U3CDownloadImageU3Ed__19_System_Collections_IEnumerator_get_Current_mFD392F88C8F541BEC86F9DC21B847FACC19DB873(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void AnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709_CustomAttributesCacheGenerator_U3CInstanceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709_CustomAttributesCacheGenerator_mapField(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x61\x70"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709_CustomAttributesCacheGenerator_itemFirework(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x33\x44\x20\x42\x75\x74\x74\x6F\x6E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709_CustomAttributesCacheGenerator_itemCookie(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709_CustomAttributesCacheGenerator_itemCandy(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709_CustomAttributesCacheGenerator_screenShot(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709_CustomAttributesCacheGenerator_leaderBoard(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709_CustomAttributesCacheGenerator_noticeTimer(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x6F\x74\x69\x63\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709_CustomAttributesCacheGenerator_notice(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void AnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709_CustomAttributesCacheGenerator_AnimationManager_get_Instance_m6006600F80794DD76BB6AC3235F42B08C76DFC21(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void AnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709_CustomAttributesCacheGenerator_AnimationManager_set_Instance_m129B4A95F6E1A05D296C65F5FF4654A34E191C1A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HeroUI_t08B1AAECF81A540C2A88B585759B9A05188B2373_CustomAttributesCacheGenerator_heroUI(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void HeroUI_t08B1AAECF81A540C2A88B585759B9A05188B2373_CustomAttributesCacheGenerator_sellCostText(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x54\x65\x78\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void HeroUI_t08B1AAECF81A540C2A88B585759B9A05188B2373_CustomAttributesCacheGenerator_heroNameText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void HeroUI_t08B1AAECF81A540C2A88B585759B9A05188B2373_CustomAttributesCacheGenerator_subHeroNameText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void HeroUI_t08B1AAECF81A540C2A88B585759B9A05188B2373_CustomAttributesCacheGenerator_evoCostText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void HeroUI_t08B1AAECF81A540C2A88B585759B9A05188B2373_CustomAttributesCacheGenerator_upgradeButtonText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void HeroUI_t08B1AAECF81A540C2A88B585759B9A05188B2373_CustomAttributesCacheGenerator_levelPanel(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x55\x70\x67\x72\x61\x64\x65\x20\x53\x65\x74\x75\x70"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void HeroUI_t08B1AAECF81A540C2A88B585759B9A05188B2373_CustomAttributesCacheGenerator_upgradeSlider(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void HeroUI_t08B1AAECF81A540C2A88B585759B9A05188B2373_CustomAttributesCacheGenerator_upgradeCostText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void HeroUI_t08B1AAECF81A540C2A88B585759B9A05188B2373_CustomAttributesCacheGenerator_levelText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void HeroUI_t08B1AAECF81A540C2A88B585759B9A05188B2373_CustomAttributesCacheGenerator_heroLevelText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void HeroUI_t08B1AAECF81A540C2A88B585759B9A05188B2373_CustomAttributesCacheGenerator_upgradeButton(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x42\x75\x74\x74\x6F\x6E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void HeroUI_t08B1AAECF81A540C2A88B585759B9A05188B2373_CustomAttributesCacheGenerator_evoButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void HeroUI_t08B1AAECF81A540C2A88B585759B9A05188B2373_CustomAttributesCacheGenerator_attackRangeImg(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x69\x72\x63\x6C\x65\x20\x52\x61\x6E\x67\x65\x20\x49\x6D\x61\x67\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void HeroUI_t08B1AAECF81A540C2A88B585759B9A05188B2373_CustomAttributesCacheGenerator_evoEffect(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x45\x76\x6F\x20\x45\x66\x66\x65\x63\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void HeroUI_t08B1AAECF81A540C2A88B585759B9A05188B2373_CustomAttributesCacheGenerator_HeroUI_ChangeButtonColor_m57C3A34E9EFD62ECB84BB942B1369D742BD003B3(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CChangeButtonColorU3Ed__21_tD9B8A0568D190D5F2BD60EEA784E0B908A3AE624_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CChangeButtonColorU3Ed__21_tD9B8A0568D190D5F2BD60EEA784E0B908A3AE624_0_0_0_var), NULL);
	}
}
static void HeroUI_t08B1AAECF81A540C2A88B585759B9A05188B2373_CustomAttributesCacheGenerator_HeroUI_WaitForUpdateUi_mB163302A7AC7999CDB49AD6C3F1E8D1891C6F1A4(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitForUpdateUiU3Ed__27_t36E7FB8307E80FED7AB34985D4632506BC952711_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWaitForUpdateUiU3Ed__27_t36E7FB8307E80FED7AB34985D4632506BC952711_0_0_0_var), NULL);
	}
}
static void HeroUI_t08B1AAECF81A540C2A88B585759B9A05188B2373_CustomAttributesCacheGenerator_HeroUI_U3CSetTargetU3Eb__19_0_m1F49EFF4785EA6875D6F430195C5647A22F490D4(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HeroUI_t08B1AAECF81A540C2A88B585759B9A05188B2373_CustomAttributesCacheGenerator_HeroUI_U3CEvoHeroU3Eb__26_0_m890A8A5597D9EAAC6DAF8ED08FE6C8F3DF37798B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CChangeButtonColorU3Ed__21_tD9B8A0568D190D5F2BD60EEA784E0B908A3AE624_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CChangeButtonColorU3Ed__21_tD9B8A0568D190D5F2BD60EEA784E0B908A3AE624_CustomAttributesCacheGenerator_U3CChangeButtonColorU3Ed__21__ctor_m022B1F3DDDC026D92DE234A1607B268DF00F2C97(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CChangeButtonColorU3Ed__21_tD9B8A0568D190D5F2BD60EEA784E0B908A3AE624_CustomAttributesCacheGenerator_U3CChangeButtonColorU3Ed__21_System_IDisposable_Dispose_m3517C42530E1127D15045D7C88535291F122D6B0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CChangeButtonColorU3Ed__21_tD9B8A0568D190D5F2BD60EEA784E0B908A3AE624_CustomAttributesCacheGenerator_U3CChangeButtonColorU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8821EF38042F4839506600E9F1669248B0928E96(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CChangeButtonColorU3Ed__21_tD9B8A0568D190D5F2BD60EEA784E0B908A3AE624_CustomAttributesCacheGenerator_U3CChangeButtonColorU3Ed__21_System_Collections_IEnumerator_Reset_m3A87C825847F51A6BB2A1DB026FF22945CA831D0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CChangeButtonColorU3Ed__21_tD9B8A0568D190D5F2BD60EEA784E0B908A3AE624_CustomAttributesCacheGenerator_U3CChangeButtonColorU3Ed__21_System_Collections_IEnumerator_get_Current_mE05A61B3BF11D846169720E6B7E6DAB5F7E79DB4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForUpdateUiU3Ed__27_t36E7FB8307E80FED7AB34985D4632506BC952711_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitForUpdateUiU3Ed__27_t36E7FB8307E80FED7AB34985D4632506BC952711_CustomAttributesCacheGenerator_U3CWaitForUpdateUiU3Ed__27__ctor_mEFF8FCAE0E22B88642092D496E94249D3B9F9FB5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForUpdateUiU3Ed__27_t36E7FB8307E80FED7AB34985D4632506BC952711_CustomAttributesCacheGenerator_U3CWaitForUpdateUiU3Ed__27_System_IDisposable_Dispose_m19B749EC2C6F276D35A0BE03B51D69A659170E46(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForUpdateUiU3Ed__27_t36E7FB8307E80FED7AB34985D4632506BC952711_CustomAttributesCacheGenerator_U3CWaitForUpdateUiU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m99553AC5B564ED4462C3587A1AE492105F8773D4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForUpdateUiU3Ed__27_t36E7FB8307E80FED7AB34985D4632506BC952711_CustomAttributesCacheGenerator_U3CWaitForUpdateUiU3Ed__27_System_Collections_IEnumerator_Reset_m269F0236C1C5426233ECF692D38DB744748508EB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForUpdateUiU3Ed__27_t36E7FB8307E80FED7AB34985D4632506BC952711_CustomAttributesCacheGenerator_U3CWaitForUpdateUiU3Ed__27_System_Collections_IEnumerator_get_Current_mB1C2059DD75E7B11DFA1E993B2BBA333AF79F63D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ItemManager_t050717EEAFEA02D5E67D1509322618C59CBAE94A_CustomAttributesCacheGenerator_U3CInstanceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ItemManager_t050717EEAFEA02D5E67D1509322618C59CBAE94A_CustomAttributesCacheGenerator_OnCandyUsed(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ItemManager_t050717EEAFEA02D5E67D1509322618C59CBAE94A_CustomAttributesCacheGenerator_OnCookieUsed(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ItemManager_t050717EEAFEA02D5E67D1509322618C59CBAE94A_CustomAttributesCacheGenerator_damageTemp(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ItemManager_t050717EEAFEA02D5E67D1509322618C59CBAE94A_CustomAttributesCacheGenerator_speedTemp(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ItemManager_t050717EEAFEA02D5E67D1509322618C59CBAE94A_CustomAttributesCacheGenerator_firework(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x45\x66\x66\x65\x63\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ItemManager_t050717EEAFEA02D5E67D1509322618C59CBAE94A_CustomAttributesCacheGenerator_ItemManager_get_Instance_mC07F08CD08726E948EF32F708CA18982076190B6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ItemManager_t050717EEAFEA02D5E67D1509322618C59CBAE94A_CustomAttributesCacheGenerator_ItemManager_set_Instance_m91F8EBA1EDB2F3C0C38B96B94567147A357531FC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ItemManager_t050717EEAFEA02D5E67D1509322618C59CBAE94A_CustomAttributesCacheGenerator_ItemManager_add_OnCandyUsed_mE11A67279CF917C592370DC3E5F19F43624F4B82(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ItemManager_t050717EEAFEA02D5E67D1509322618C59CBAE94A_CustomAttributesCacheGenerator_ItemManager_remove_OnCandyUsed_m095FF8D6FEB3DE6137B118DB77A0E92606FFEF72(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ItemManager_t050717EEAFEA02D5E67D1509322618C59CBAE94A_CustomAttributesCacheGenerator_ItemManager_add_OnCookieUsed_m28C6881C57300A2FB437BFF027FF42500973E98E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ItemManager_t050717EEAFEA02D5E67D1509322618C59CBAE94A_CustomAttributesCacheGenerator_ItemManager_remove_OnCookieUsed_mAC2B81471A8D57878164BE8A6905EB88B9AC9CF3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ItemManager_t050717EEAFEA02D5E67D1509322618C59CBAE94A_CustomAttributesCacheGenerator_ItemManager_ActiveCookie30s_m2159094A265F281AD920E8F646A10FF9F2B429A3(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CActiveCookie30sU3Ed__19_t0119DB09C6FEC13C8307690325CEE99855E3C782_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CActiveCookie30sU3Ed__19_t0119DB09C6FEC13C8307690325CEE99855E3C782_0_0_0_var), NULL);
	}
}
static void ItemManager_t050717EEAFEA02D5E67D1509322618C59CBAE94A_CustomAttributesCacheGenerator_ItemManager_ActiveCandy30s_mD5662C359943A8F22218D041056106C2FAFB2AF6(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CActiveCandy30sU3Ed__21_t679D864AD14DE99063DD90DAE48F757780A913F7_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CActiveCandy30sU3Ed__21_t679D864AD14DE99063DD90DAE48F757780A913F7_0_0_0_var), NULL);
	}
}
static void U3CActiveCookie30sU3Ed__19_t0119DB09C6FEC13C8307690325CEE99855E3C782_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CActiveCookie30sU3Ed__19_t0119DB09C6FEC13C8307690325CEE99855E3C782_CustomAttributesCacheGenerator_U3CActiveCookie30sU3Ed__19__ctor_m8FCF61703691001EFA7961AE2E1D787D16F7A5B5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CActiveCookie30sU3Ed__19_t0119DB09C6FEC13C8307690325CEE99855E3C782_CustomAttributesCacheGenerator_U3CActiveCookie30sU3Ed__19_System_IDisposable_Dispose_m045AB7688AF359AF2B0E85847CB3ED26389AE618(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CActiveCookie30sU3Ed__19_t0119DB09C6FEC13C8307690325CEE99855E3C782_CustomAttributesCacheGenerator_U3CActiveCookie30sU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEF460E178959DB2B321C7DB1AABCB6B86E2D6829(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CActiveCookie30sU3Ed__19_t0119DB09C6FEC13C8307690325CEE99855E3C782_CustomAttributesCacheGenerator_U3CActiveCookie30sU3Ed__19_System_Collections_IEnumerator_Reset_m8854B0851AC842D51089315669AFF02D7908C1EC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CActiveCookie30sU3Ed__19_t0119DB09C6FEC13C8307690325CEE99855E3C782_CustomAttributesCacheGenerator_U3CActiveCookie30sU3Ed__19_System_Collections_IEnumerator_get_Current_m440F33539D6609523F55870DB875411892221539(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CActiveCandy30sU3Ed__21_t679D864AD14DE99063DD90DAE48F757780A913F7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CActiveCandy30sU3Ed__21_t679D864AD14DE99063DD90DAE48F757780A913F7_CustomAttributesCacheGenerator_U3CActiveCandy30sU3Ed__21__ctor_m6125A56FB311570E4C1DA52533D5BC1E8D963429(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CActiveCandy30sU3Ed__21_t679D864AD14DE99063DD90DAE48F757780A913F7_CustomAttributesCacheGenerator_U3CActiveCandy30sU3Ed__21_System_IDisposable_Dispose_m398275D5D386B790D02DCE00C807A13C41F141D4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CActiveCandy30sU3Ed__21_t679D864AD14DE99063DD90DAE48F757780A913F7_CustomAttributesCacheGenerator_U3CActiveCandy30sU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE97A411E62DD8FD2994CDA0381430C7B376D09FE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CActiveCandy30sU3Ed__21_t679D864AD14DE99063DD90DAE48F757780A913F7_CustomAttributesCacheGenerator_U3CActiveCandy30sU3Ed__21_System_Collections_IEnumerator_Reset_m0D06ED95FB91F1B91B0A9BC2A0ACB2557BCD6729(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CActiveCandy30sU3Ed__21_t679D864AD14DE99063DD90DAE48F757780A913F7_CustomAttributesCacheGenerator_U3CActiveCandy30sU3Ed__21_System_Collections_IEnumerator_get_Current_mF71FC42683C0F4C24A8DD650B36D6B141CF43735(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void MainMenuManager_t7A113A7F5C26F3BD9FB68E3D52C1221E8D8386EE_CustomAttributesCacheGenerator_UploadPanel(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TimeManager_t7C9EC9E1F31A3BAAE33B119C0B91347D37ABAF82_CustomAttributesCacheGenerator_PausetTimeScale(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 3.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TimeManager_t7C9EC9E1F31A3BAAE33B119C0B91347D37ABAF82_CustomAttributesCacheGenerator_defaultTimeScale(CustomAttributesCache* cache)
{
	{
		RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 * tmp = (RangeAttribute_t14A6532D68168764C15E7CF1FDABCD99CB32D0C5 *)cache->attributes[0];
		RangeAttribute__ctor_mC74D39A9F20DD2A0D4174F05785ABE4F0DAEF000(tmp, 0.0f, 1.0f, NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TimeManager_t7C9EC9E1F31A3BAAE33B119C0B91347D37ABAF82_CustomAttributesCacheGenerator_U3CInstanceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TimeManager_t7C9EC9E1F31A3BAAE33B119C0B91347D37ABAF82_CustomAttributesCacheGenerator_TimeManager_get_Instance_m19259A61D1B020A332EC31181A9E667EF907151A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TimeManager_t7C9EC9E1F31A3BAAE33B119C0B91347D37ABAF82_CustomAttributesCacheGenerator_TimeManager_set_Instance_mB699CF404EFA8C02A254633270C30C6E0EA0EFF6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void TimeManager_t7C9EC9E1F31A3BAAE33B119C0B91347D37ABAF82_CustomAttributesCacheGenerator_TimeManager_WaitForPanel_m68280F25263044850C9A68C113FDEC174C3D7625(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitForPanelU3Ed__14_t21DF4D5D04488D4635896E34F3A4930EBF6331AA_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWaitForPanelU3Ed__14_t21DF4D5D04488D4635896E34F3A4930EBF6331AA_0_0_0_var), NULL);
	}
}
static void TimeManager_t7C9EC9E1F31A3BAAE33B119C0B91347D37ABAF82_CustomAttributesCacheGenerator_TimeManager_WaitForUnPause_mB05F71F812DAD1687CFF92931741779EA69EB465(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitForUnPauseU3Ed__15_t7C2E4C470D88899BE03226E81A5D36543A1ACF77_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWaitForUnPauseU3Ed__15_t7C2E4C470D88899BE03226E81A5D36543A1ACF77_0_0_0_var), NULL);
	}
}
static void U3CWaitForPanelU3Ed__14_t21DF4D5D04488D4635896E34F3A4930EBF6331AA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitForPanelU3Ed__14_t21DF4D5D04488D4635896E34F3A4930EBF6331AA_CustomAttributesCacheGenerator_U3CWaitForPanelU3Ed__14__ctor_m59D042B5E4A18907C5B493929DE55BB2793A8A20(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForPanelU3Ed__14_t21DF4D5D04488D4635896E34F3A4930EBF6331AA_CustomAttributesCacheGenerator_U3CWaitForPanelU3Ed__14_System_IDisposable_Dispose_m629FF274124309F78AEE0A0EE1E78820401E3659(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForPanelU3Ed__14_t21DF4D5D04488D4635896E34F3A4930EBF6331AA_CustomAttributesCacheGenerator_U3CWaitForPanelU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1BA1E11BDC976FFDA71876728621CC3580B339C9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForPanelU3Ed__14_t21DF4D5D04488D4635896E34F3A4930EBF6331AA_CustomAttributesCacheGenerator_U3CWaitForPanelU3Ed__14_System_Collections_IEnumerator_Reset_mFDFA0E2253D02365F762B257186B1B188BA591BC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForPanelU3Ed__14_t21DF4D5D04488D4635896E34F3A4930EBF6331AA_CustomAttributesCacheGenerator_U3CWaitForPanelU3Ed__14_System_Collections_IEnumerator_get_Current_m7FF353409E4AB340F005DEE86648F7D8B53A47D0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForUnPauseU3Ed__15_t7C2E4C470D88899BE03226E81A5D36543A1ACF77_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitForUnPauseU3Ed__15_t7C2E4C470D88899BE03226E81A5D36543A1ACF77_CustomAttributesCacheGenerator_U3CWaitForUnPauseU3Ed__15__ctor_m0FD75504981C15A71618791163C95DA6832B1558(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForUnPauseU3Ed__15_t7C2E4C470D88899BE03226E81A5D36543A1ACF77_CustomAttributesCacheGenerator_U3CWaitForUnPauseU3Ed__15_System_IDisposable_Dispose_m1967E6755E3C99E168315655FDBC938D996A7C80(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForUnPauseU3Ed__15_t7C2E4C470D88899BE03226E81A5D36543A1ACF77_CustomAttributesCacheGenerator_U3CWaitForUnPauseU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3ABE4E9FEA6847127D2EBA79A3BDCC495BF3A886(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForUnPauseU3Ed__15_t7C2E4C470D88899BE03226E81A5D36543A1ACF77_CustomAttributesCacheGenerator_U3CWaitForUnPauseU3Ed__15_System_Collections_IEnumerator_Reset_mF18699718458033B6427B047F0C6892055FFD52E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForUnPauseU3Ed__15_t7C2E4C470D88899BE03226E81A5D36543A1ACF77_CustomAttributesCacheGenerator_U3CWaitForUnPauseU3Ed__15_System_Collections_IEnumerator_get_Current_mA42AEF9003416FA995E1070577B3323FCD65A232(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void MapGenerate_tA7712825E563ECC0B019CEBE5DF4C4CC7941593D_CustomAttributesCacheGenerator_mapWidth(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MapGenerate_tA7712825E563ECC0B019CEBE5DF4C4CC7941593D_CustomAttributesCacheGenerator_mapHeight(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void ChainSkill_t389CC8B529E3C83A6741CA8AC63DF33C553D8927_CustomAttributesCacheGenerator_U3CInstanceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChainSkill_t389CC8B529E3C83A6741CA8AC63DF33C553D8927_CustomAttributesCacheGenerator_ChainSkill_get_Instance_mFF57A1E3160BADBC15485C65C3459453C8388C8E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void ChainSkill_t389CC8B529E3C83A6741CA8AC63DF33C553D8927_CustomAttributesCacheGenerator_ChainSkill_set_Instance_m463EFBE4C3064E87C11ECFB2FBD865ADF6E335A5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_t1CF2C7B5B1DCB59A7D5A83EFBB783E135CE83A3D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass21_0_tA12769560FC967DD8C1AF06D6DB4CA81B85AAD67_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void DropdownSample_t925F66C7435D75374A0E26DE15B4F1D95697F0DF_CustomAttributesCacheGenerator_text(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DropdownSample_t925F66C7435D75374A0E26DE15B4F1D95697F0DF_CustomAttributesCacheGenerator_dropdownWithoutPlaceholder(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void DropdownSample_t925F66C7435D75374A0E26DE15B4F1D95697F0DF_CustomAttributesCacheGenerator_dropdownWithPlaceholder(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void EnvMapAnimator_t7C30A094A3786710A896AD9A6FA46B13EFEB72FD_CustomAttributesCacheGenerator_EnvMapAnimator_Start_mC0D348CAB0F0DC920EF3D3A008688B533F66D1BE(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__4_t85DC3CED72C53244AC5D2467B8CB12EF944D3266_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__4_t85DC3CED72C53244AC5D2467B8CB12EF944D3266_0_0_0_var), NULL);
	}
}
static void U3CStartU3Ed__4_t85DC3CED72C53244AC5D2467B8CB12EF944D3266_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__4_t85DC3CED72C53244AC5D2467B8CB12EF944D3266_CustomAttributesCacheGenerator_U3CStartU3Ed__4__ctor_m4D65F4FC2207AE4B6BE963AF9B5EDC55C7E29B23(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__4_t85DC3CED72C53244AC5D2467B8CB12EF944D3266_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_IDisposable_Dispose_mFEE2ACED70A3D825988E28CC61FEF8DCD7660A5B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__4_t85DC3CED72C53244AC5D2467B8CB12EF944D3266_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF03E2A57659B8F598AC450183F55D43F903C0A1E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__4_t85DC3CED72C53244AC5D2467B8CB12EF944D3266_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m268BA538CF6812C56EB281C0CE29D5AA2E9A2CAB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__4_t85DC3CED72C53244AC5D2467B8CB12EF944D3266_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mFE9CA9F52F9AFA1E4C9DBADA2064F854D6931CFF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553_CustomAttributesCacheGenerator_m_OnCharacterSelection(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553_CustomAttributesCacheGenerator_m_OnSpriteSelection(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553_CustomAttributesCacheGenerator_m_OnWordSelection(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553_CustomAttributesCacheGenerator_m_OnLineSelection(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553_CustomAttributesCacheGenerator_m_OnLinkSelection(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Benchmark01_tB93AE57128496E02C0B3D8AC80185B077A1B6934_CustomAttributesCacheGenerator_Benchmark01_Start_mE7E5146B0D8D926CC410CAA48F3B617A0A7D955C(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__10_t4C97F7E5F1F2D654FF76C297F92CF423F472A640_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__10_t4C97F7E5F1F2D654FF76C297F92CF423F472A640_0_0_0_var), NULL);
	}
}
static void U3CStartU3Ed__10_t4C97F7E5F1F2D654FF76C297F92CF423F472A640_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t4C97F7E5F1F2D654FF76C297F92CF423F472A640_CustomAttributesCacheGenerator_U3CStartU3Ed__10__ctor_mBE5D8B4B98C372BD6DC3936437999DF3048DE3AB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t4C97F7E5F1F2D654FF76C297F92CF423F472A640_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_IDisposable_Dispose_m1F4180A7FDAE9AC5F11F30B67F813B3D7CD56D73(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t4C97F7E5F1F2D654FF76C297F92CF423F472A640_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m22B2975D42326E3DE437174BBE4F0E8790CB6591(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t4C97F7E5F1F2D654FF76C297F92CF423F472A640_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m514DA545ECA10AA62BFC239BB582FF0017B91D2B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t4C97F7E5F1F2D654FF76C297F92CF423F472A640_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m6904A738F04118BA24E473F1809B3D69E711627E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Benchmark01_UGUI_t45B3D7BFA1AB9A4A60B48F15D684C558FD619D4B_CustomAttributesCacheGenerator_Benchmark01_UGUI_Start_m2F8F9E1798943DA8086A7C7E73BA9D7C67482BD4(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__10_t3D69371599D2A84DED6689D4FA6EA1720611EAD3_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__10_t3D69371599D2A84DED6689D4FA6EA1720611EAD3_0_0_0_var), NULL);
	}
}
static void U3CStartU3Ed__10_t3D69371599D2A84DED6689D4FA6EA1720611EAD3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t3D69371599D2A84DED6689D4FA6EA1720611EAD3_CustomAttributesCacheGenerator_U3CStartU3Ed__10__ctor_m653B757B49A674E239C804FFF4EDEF325B5DB651(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t3D69371599D2A84DED6689D4FA6EA1720611EAD3_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_IDisposable_Dispose_mAD0BC985E1E01C8369A7A955A60D3E545B0B3561(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t3D69371599D2A84DED6689D4FA6EA1720611EAD3_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF438D42D7C5A811FF9A283F8780F015FF55826CD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t3D69371599D2A84DED6689D4FA6EA1720611EAD3_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m65545EB5B6DDDDAB5ACF92600137C1C754B322BA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__10_t3D69371599D2A84DED6689D4FA6EA1720611EAD3_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mD4523BE1D1014AAF1C0709EB767C0B0F47D739D2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void ShaderPropAnimator_t44FB2882ECA0D2C780444EB96B9550F5D245CFB8_CustomAttributesCacheGenerator_ShaderPropAnimator_AnimateProperties_mAF49CD157AD41377CE00AA10F0C06C8BF5AA0469(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAnimatePropertiesU3Ed__6_t6EF9435006F3F335B99084BECD0ABFEFFEF4A32D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CAnimatePropertiesU3Ed__6_t6EF9435006F3F335B99084BECD0ABFEFFEF4A32D_0_0_0_var), NULL);
	}
}
static void U3CAnimatePropertiesU3Ed__6_t6EF9435006F3F335B99084BECD0ABFEFFEF4A32D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAnimatePropertiesU3Ed__6_t6EF9435006F3F335B99084BECD0ABFEFFEF4A32D_CustomAttributesCacheGenerator_U3CAnimatePropertiesU3Ed__6__ctor_mFAC0F8A7368D9D35AD2780C118E13414DA79B56A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimatePropertiesU3Ed__6_t6EF9435006F3F335B99084BECD0ABFEFFEF4A32D_CustomAttributesCacheGenerator_U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_m628EECBFCBC49087298185F17BC2AE7A73FC8A7B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimatePropertiesU3Ed__6_t6EF9435006F3F335B99084BECD0ABFEFFEF4A32D_CustomAttributesCacheGenerator_U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0742A0FA62C518EBD119ED5FEBF849F918E12390(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimatePropertiesU3Ed__6_t6EF9435006F3F335B99084BECD0ABFEFFEF4A32D_CustomAttributesCacheGenerator_U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_mC076F281DD17668D6CE42EB04EA974DE1FFB3F6B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimatePropertiesU3Ed__6_t6EF9435006F3F335B99084BECD0ABFEFFEF4A32D_CustomAttributesCacheGenerator_U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_mA585E786DA86D2589EBCA029AD9E64E8B5135362(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void SkewTextExample_tA9F165D1BE969ED73FB86CA6C641386C22F1EE07_CustomAttributesCacheGenerator_SkewTextExample_WarpText_m0E0C46988600673F0E5DFA3133534DC6CA5950D3(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B_0_0_0_var), NULL);
	}
}
static void U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__7__ctor_mEAD3C39209B75514446A44B6C2FA76F8097EBD6F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m32AA7120BE15547799BDC515FA3486488952BDCF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3CF506B740C16FEDE58B6E1BE4556CAC2C6AEAD1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_mDDCDBB794F21DF70178A75747D8D8398F38A9C2F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m6D89CF1910F76577E23C924B535E93FD1B01CF0A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void TeleType_t92B5ED4FE818F912A562F085CE035F6B42834934_CustomAttributesCacheGenerator_TeleType_Start_m72DE9DE597F4FA0B1CA02115CBC1E74EB53F63F7(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CStartU3Ed__4_t70319341791F5D1BC6F878503A507E845E98C44E_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CStartU3Ed__4_t70319341791F5D1BC6F878503A507E845E98C44E_0_0_0_var), NULL);
	}
}
static void U3CStartU3Ed__4_t70319341791F5D1BC6F878503A507E845E98C44E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CStartU3Ed__4_t70319341791F5D1BC6F878503A507E845E98C44E_CustomAttributesCacheGenerator_U3CStartU3Ed__4__ctor_mF3575DBEBF4F153F6A899AF3362940298C11B629(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__4_t70319341791F5D1BC6F878503A507E845E98C44E_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_IDisposable_Dispose_m6DABFDBC2A313BF6DD90AA1C43B4EF9D6249CBE9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__4_t70319341791F5D1BC6F878503A507E845E98C44E_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m66DC06D52865D30DAA78DBD70B6554D13E2EA70B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__4_t70319341791F5D1BC6F878503A507E845E98C44E_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_mE854A2CECA0E01F3C2D90DA4F720CF0F387994F8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CStartU3Ed__4_t70319341791F5D1BC6F878503A507E845E98C44E_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m77D3936B94684C817EE9F6D2C903238A457D5261(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void TextConsoleSimulator_t82904A7668D46B20E576B30E4CE367C0B66C5289_CustomAttributesCacheGenerator_TextConsoleSimulator_RevealCharacters_mE8E415644F7BD2056D0809F7BB0FA9A2F9FE8534(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E_0_0_0_var), NULL);
	}
}
static void TextConsoleSimulator_t82904A7668D46B20E576B30E4CE367C0B66C5289_CustomAttributesCacheGenerator_TextConsoleSimulator_RevealWords_mFDBC863D30BC63ADCF2860F303AF252E27D0F4F4(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408_0_0_0_var), NULL);
	}
}
static void U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E_CustomAttributesCacheGenerator_U3CRevealCharactersU3Ed__7__ctor_mD45A85F5F50909F70C80AC8CE460F4FD261CDE9D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E_CustomAttributesCacheGenerator_U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m85F270FDC11A4D79E9CF47AADC9FA1FBF032F86C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E_CustomAttributesCacheGenerator_U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5BBAE6868EB7F1C11BE5DF001641E18C9D625F83(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E_CustomAttributesCacheGenerator_U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_mA9F4893381AB24E01EAEBFC38A88AF363A9A0691(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E_CustomAttributesCacheGenerator_U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_mFD0B7538B1A650FB389FFE9296B0E51AEA5B6B6F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408_CustomAttributesCacheGenerator_U3CRevealWordsU3Ed__8__ctor_m22FF9E770988107A928C5D1EA639F60239BFFEF0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408_CustomAttributesCacheGenerator_U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m3EAF6EF4A8C99A71FEED251BF3F28A26BF6AD7F8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408_CustomAttributesCacheGenerator_U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3F93E4AA532F15D52D68B7121805C014AB2D7FB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408_CustomAttributesCacheGenerator_U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_mCDC8982948ED5F7743567569CA1D4A354218714F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408_CustomAttributesCacheGenerator_U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_mF39E492D576810F58DB9031556CFD6806FD32E27(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void TextMeshProFloatingText_t55B28EA1D0CCDF61ABE6CF421C752EBED7B37200_CustomAttributesCacheGenerator_TextMeshProFloatingText_DisplayTextMeshProFloatingText_m3C381B8A53C58CF001D7A9212B8BDA6F368CC2C7(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDisplayTextMeshProFloatingTextU3Ed__15_tF7B0AC090E8E1BC19444DC46DEA61B2D608F7E20_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDisplayTextMeshProFloatingTextU3Ed__15_tF7B0AC090E8E1BC19444DC46DEA61B2D608F7E20_0_0_0_var), NULL);
	}
}
static void TextMeshProFloatingText_t55B28EA1D0CCDF61ABE6CF421C752EBED7B37200_CustomAttributesCacheGenerator_TextMeshProFloatingText_DisplayTextMeshFloatingText_m22691E6EA41B7FCF782B03954865D8E3B890E4DF(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDisplayTextMeshFloatingTextU3Ed__16_tACCF2F4E02674FBB7D0A61F908094C4EC0DAD2A2_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDisplayTextMeshFloatingTextU3Ed__16_tACCF2F4E02674FBB7D0A61F908094C4EC0DAD2A2_0_0_0_var), NULL);
	}
}
static void U3CDisplayTextMeshProFloatingTextU3Ed__15_tF7B0AC090E8E1BC19444DC46DEA61B2D608F7E20_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDisplayTextMeshProFloatingTextU3Ed__15_tF7B0AC090E8E1BC19444DC46DEA61B2D608F7E20_CustomAttributesCacheGenerator_U3CDisplayTextMeshProFloatingTextU3Ed__15__ctor_m3F7F9561735FF8403090A93E708E781B3997B81B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDisplayTextMeshProFloatingTextU3Ed__15_tF7B0AC090E8E1BC19444DC46DEA61B2D608F7E20_CustomAttributesCacheGenerator_U3CDisplayTextMeshProFloatingTextU3Ed__15_System_IDisposable_Dispose_m6F24E1DC5777D4682375D8742BC9272EE6D4EA2C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDisplayTextMeshProFloatingTextU3Ed__15_tF7B0AC090E8E1BC19444DC46DEA61B2D608F7E20_CustomAttributesCacheGenerator_U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m14A7C9DEBD0FF0C7A6979AFEC78B0BEE9E4C3963(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDisplayTextMeshProFloatingTextU3Ed__15_tF7B0AC090E8E1BC19444DC46DEA61B2D608F7E20_CustomAttributesCacheGenerator_U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_Reset_mC0D5548A92F57550F60EE76E17B58125B57B94A8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDisplayTextMeshProFloatingTextU3Ed__15_tF7B0AC090E8E1BC19444DC46DEA61B2D608F7E20_CustomAttributesCacheGenerator_U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_get_Current_mD3CBFD6F8558EF09F2B4A3D8D1B76BD7FDB3A98B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDisplayTextMeshFloatingTextU3Ed__16_tACCF2F4E02674FBB7D0A61F908094C4EC0DAD2A2_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDisplayTextMeshFloatingTextU3Ed__16_tACCF2F4E02674FBB7D0A61F908094C4EC0DAD2A2_CustomAttributesCacheGenerator_U3CDisplayTextMeshFloatingTextU3Ed__16__ctor_m0466586E44A01852547C6CECCA91C14673B98C20(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDisplayTextMeshFloatingTextU3Ed__16_tACCF2F4E02674FBB7D0A61F908094C4EC0DAD2A2_CustomAttributesCacheGenerator_U3CDisplayTextMeshFloatingTextU3Ed__16_System_IDisposable_Dispose_m8730A3A2EFBF759A51F0E5330C3BBF8EFC1736A3(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDisplayTextMeshFloatingTextU3Ed__16_tACCF2F4E02674FBB7D0A61F908094C4EC0DAD2A2_CustomAttributesCacheGenerator_U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA4076BE73343A473EFE46E73ABA5DE66CC797598(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDisplayTextMeshFloatingTextU3Ed__16_tACCF2F4E02674FBB7D0A61F908094C4EC0DAD2A2_CustomAttributesCacheGenerator_U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_Reset_mC33B355BB392C5A52DCA0AAF2588E1E78AF6D3A5(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDisplayTextMeshFloatingTextU3Ed__16_tACCF2F4E02674FBB7D0A61F908094C4EC0DAD2A2_CustomAttributesCacheGenerator_U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_get_Current_m28CCE2E51295AD5F368C504BADDDFF008C2CC0A6(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void VertexColorCycler_t37E80E87D8EAD0D7757CDA45BD5D3AA82FD28374_CustomAttributesCacheGenerator_VertexColorCycler_AnimateVertexColors_m6960F777E4876DFCA726BCAE7A8163850D58FA42(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF_0_0_0_var), NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__3__ctor_m0038B1054BCC928D35F8C0021ED7D2E1C533E35F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_m5BAD394A0B09B3E0FF19E91521E02C2B3ADD6007(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAE810D7968957A09C88E61C29DAAEC68E4AF1E51(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_m72ABAC50E9E4D972FB44CAFF387F3E23FEC5D932(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_m8DE595A1D01F3A507A356F8BCE020D0851412B52(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void VertexJitter_t4BF9FCCB84221E69D9E3727E32E49710F3B4342A_CustomAttributesCacheGenerator_VertexJitter_AnimateVertexColors_mECAE037FC0CBA52CAC71C0B61E88829FF18BCC16(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741_0_0_0_var), NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11__ctor_m0222C3457F5ACA497FE3A8EC829DE4AD11A169F8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m717E79A39A8161ADDA9E62F7CDFB67B8F2D65099(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m920EBA43D59A1A88E27FED92CF0AC0DF90179479(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m6C045A6DD0B60F1512457448E615877EAB86D75D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m017345FE58B497EAFC9D0CB1FB733F76EB3449AF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void VertexShakeA_t9DED8926E2D8F5E9D457C678C85062CB3B7A923A_CustomAttributesCacheGenerator_VertexShakeA_AnimateVertexColors_mC451E2732A4E3E90E2553811AD75903EEF974599(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155_0_0_0_var), NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11__ctor_m92612416BEC0EBF5E9849FB603629C0F2F95FEF2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m77D966994D4717EAFD8EFE169F3E8A4EE8B05B81(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDCCD3645ACF9B18D760B341C863F853996FA9BCE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_mB9444B5E58B4E97105C447F547C0F74C51BCFBFA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m5CC464185D5C0251C6206E20AFFA681BA0525A7E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void VertexShakeB_t348FF1466CEF3BFED37C140B7AEFA2EA90D703C9_CustomAttributesCacheGenerator_VertexShakeB_AnimateVertexColors_mDF7A7E7028361D288DF588D9301541E4FA1EFA87(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6_0_0_0_var), NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10__ctor_m3E3B1D286DEBED2BC028AD490308568B930C3760(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m0F9D4B2A6ED0500C2120DBA29932CC279E8908DC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF2A8BAFC0261ACBBE8EEA74DE4B498D30C68AE3D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m5F12D7C6F2523BEB6326FE49AE972116D6157CBB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_mB9C12786011A3B78517AEFAE8D5A78B95A4219AD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void VertexZoom_t02E8994D3A9EB41AEEF1544EF8C93009F6153163_CustomAttributesCacheGenerator_VertexZoom_AnimateVertexColors_mEFBA6C940DFECB485236C247358A32011A7963ED(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40_0_0_0_var), NULL);
	}
}
static void U3CU3Ec__DisplayClass10_0_t1AC05F5499B93AC94E9727B8B0F08D8037AD52A6_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10__ctor_m537CF0A5ADF5BBC2DF784BF526E3F32EB528E1B2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_mF0CD944C393771101D2718A660E1DFF22385819F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m093E922A5A046910B2A7EE826D804CA3064A7BD9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_mDED63FD52741D58D8D5A3E53415F91B6560F680C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m933EBF7C4300C7C1E3EE60E4741110025877CA5F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void WarpTextExample_t9CE7000EFB1B8696E32CC86E87E3D0EDEA3D5A96_CustomAttributesCacheGenerator_WarpTextExample_WarpText_mA29C98CF3B92F253C2DD2BADFC76618F8324BEF2(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76_0_0_0_var), NULL);
	}
}
static void U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__8__ctor_m9FADE04C27A0034C5A276232FCA187AECDC6BF49(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m5AB0DCF4B3DE6C290487F3DBBC7BAE931130DE60(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D5776AF80C4227401ADD7E13D98F2530CB9E7A1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_m4F2FBBE7B375E6FC837736B4275A7B601A01F535(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m01FA935D93D737C9DDD88A051F7DFC393DD7BD25(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void Area_t7013267738E4CA59CA15A2E450701A99F6A70197_CustomAttributesCacheGenerator_U3CCurrentHeroAmountU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Area_t7013267738E4CA59CA15A2E450701A99F6A70197_CustomAttributesCacheGenerator_U3CCurrentSubHeroAmountU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Area_t7013267738E4CA59CA15A2E450701A99F6A70197_CustomAttributesCacheGenerator_hero(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void Area_t7013267738E4CA59CA15A2E450701A99F6A70197_CustomAttributesCacheGenerator_HeroBluePrint(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void Area_t7013267738E4CA59CA15A2E450701A99F6A70197_CustomAttributesCacheGenerator_Area_get_CurrentHeroAmount_mF7EE46F068C4AF7A0C0EE036CF6853C75337B957(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Area_t7013267738E4CA59CA15A2E450701A99F6A70197_CustomAttributesCacheGenerator_Area_set_CurrentHeroAmount_m7D64716F3C5D20FC426581D30EED068869264294(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Area_t7013267738E4CA59CA15A2E450701A99F6A70197_CustomAttributesCacheGenerator_Area_get_CurrentSubHeroAmount_mB1AE1AEACA136C7E29DD9855DCF10ACC8B7C3D66(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void Area_t7013267738E4CA59CA15A2E450701A99F6A70197_CustomAttributesCacheGenerator_Area_set_CurrentSubHeroAmount_m3E01B5026F9398B8D8B3012ED50B3BAE83B620E6(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tAA80530146653D50F0D63C9976285F15D0B03AC3_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void JSONNode_tDEACFC45BBAB7B7189263CF1BA7D4B7FE179132A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void JSONNode_tDEACFC45BBAB7B7189263CF1BA7D4B7FE179132A_CustomAttributesCacheGenerator_m_EscapeBuilder(CustomAttributesCache* cache)
{
	{
		ThreadStaticAttribute_tD3A8F4870EC5B163383AB888C364217A38134F14 * tmp = (ThreadStaticAttribute_tD3A8F4870EC5B163383AB888C364217A38134F14 *)cache->attributes[0];
		ThreadStaticAttribute__ctor_m2F60E2FA27DEC1E9FE581440EF3445F3B5E7F16A(tmp, NULL);
	}
}
static void JSONNode_tDEACFC45BBAB7B7189263CF1BA7D4B7FE179132A_CustomAttributesCacheGenerator_JSONNode_get_Children_mB9A68101166B1C5176311D2EC1ABCC832248FDFF(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3Cget_ChildrenU3Ed__39_tB70A86AF5415D0A3C8B93D07712335F4FE14F44B_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3Cget_ChildrenU3Ed__39_tB70A86AF5415D0A3C8B93D07712335F4FE14F44B_0_0_0_var), NULL);
	}
}
static void JSONNode_tDEACFC45BBAB7B7189263CF1BA7D4B7FE179132A_CustomAttributesCacheGenerator_JSONNode_get_DeepChildren_m3E3CE542A4856D0BD557B7E121B4CAA999C8110C(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3Cget_DeepChildrenU3Ed__41_t1EFE91681671B35682051A9DF5D99D8012D7DDD7_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3Cget_DeepChildrenU3Ed__41_t1EFE91681671B35682051A9DF5D99D8012D7DDD7_0_0_0_var), NULL);
	}
}
static void U3Cget_ChildrenU3Ed__39_tB70A86AF5415D0A3C8B93D07712335F4FE14F44B_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__39_tB70A86AF5415D0A3C8B93D07712335F4FE14F44B_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__39__ctor_m0BF33D6A9ACF56C849BB64AB175B43D7692499A8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__39_tB70A86AF5415D0A3C8B93D07712335F4FE14F44B_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__39_System_IDisposable_Dispose_mFD004DCB3358CE2D7764E776A031A077B21BEB29(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__39_tB70A86AF5415D0A3C8B93D07712335F4FE14F44B_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__39_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_mB88A038112706EAD24B23A14267B794021C761C2(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__39_tB70A86AF5415D0A3C8B93D07712335F4FE14F44B_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__39_System_Collections_IEnumerator_Reset_m716ACA15194120CF8192937025FD3D4D7F8C710F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__39_tB70A86AF5415D0A3C8B93D07712335F4FE14F44B_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__39_System_Collections_IEnumerator_get_Current_m831C58F860DBA03146F00BB7DC77DD1852943881(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__39_tB70A86AF5415D0A3C8B93D07712335F4FE14F44B_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__39_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_mC22210E7538A1A259E64227EEAC5440FE868ED60(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__39_tB70A86AF5415D0A3C8B93D07712335F4FE14F44B_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__39_System_Collections_IEnumerable_GetEnumerator_mDB57DC3D1B30B06919F6F3985E3B3564974C1CAA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_DeepChildrenU3Ed__41_t1EFE91681671B35682051A9DF5D99D8012D7DDD7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3Cget_DeepChildrenU3Ed__41_t1EFE91681671B35682051A9DF5D99D8012D7DDD7_CustomAttributesCacheGenerator_U3Cget_DeepChildrenU3Ed__41__ctor_mAF92DE35182876CDDB7F9E2831AA0078B111A5F9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_DeepChildrenU3Ed__41_t1EFE91681671B35682051A9DF5D99D8012D7DDD7_CustomAttributesCacheGenerator_U3Cget_DeepChildrenU3Ed__41_System_IDisposable_Dispose_m0C1243483A3118059E0FB871E404B606DD55CBBD(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_DeepChildrenU3Ed__41_t1EFE91681671B35682051A9DF5D99D8012D7DDD7_CustomAttributesCacheGenerator_U3Cget_DeepChildrenU3Ed__41_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_mF12A4534E7D4122F67B420801B2522BC9F839868(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_DeepChildrenU3Ed__41_t1EFE91681671B35682051A9DF5D99D8012D7DDD7_CustomAttributesCacheGenerator_U3Cget_DeepChildrenU3Ed__41_System_Collections_IEnumerator_Reset_m3F120F44D1BEB26137AF14E11165003EF8E18190(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_DeepChildrenU3Ed__41_t1EFE91681671B35682051A9DF5D99D8012D7DDD7_CustomAttributesCacheGenerator_U3Cget_DeepChildrenU3Ed__41_System_Collections_IEnumerator_get_Current_mF43CD05D3BF0CD3E76A265F0A684D43EFA4F22EC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_DeepChildrenU3Ed__41_t1EFE91681671B35682051A9DF5D99D8012D7DDD7_CustomAttributesCacheGenerator_U3Cget_DeepChildrenU3Ed__41_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_mAD435B3462420F0485567562B0FF3F1DB60B6AF8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_DeepChildrenU3Ed__41_t1EFE91681671B35682051A9DF5D99D8012D7DDD7_CustomAttributesCacheGenerator_U3Cget_DeepChildrenU3Ed__41_System_Collections_IEnumerable_GetEnumerator_m554A68924D4655591E09B01C0EFF3A9204E5E117(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void JSONArray_t478FF5B6520D2CAB00FA50A61F808A3E09F928A0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void JSONArray_t478FF5B6520D2CAB00FA50A61F808A3E09F928A0_CustomAttributesCacheGenerator_JSONArray_get_Children_m56D8A696E09F5EEFF1FF9F8BE06011CE5EAFA197(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3Cget_ChildrenU3Ed__22_t411188099DCF9174894F420F6390B7E43C48B481_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3Cget_ChildrenU3Ed__22_t411188099DCF9174894F420F6390B7E43C48B481_0_0_0_var), NULL);
	}
}
static void U3Cget_ChildrenU3Ed__22_t411188099DCF9174894F420F6390B7E43C48B481_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__22_t411188099DCF9174894F420F6390B7E43C48B481_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__22__ctor_m95EBCC8BC9794B6D9F4C6B5885704BF181FC9DBF(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__22_t411188099DCF9174894F420F6390B7E43C48B481_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__22_System_IDisposable_Dispose_mCBFE04EF85659884667F66453D36323A90F4A37E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__22_t411188099DCF9174894F420F6390B7E43C48B481_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__22_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_mA7F43EFC69B364F48AC29BF1C37A5001990511FE(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__22_t411188099DCF9174894F420F6390B7E43C48B481_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerator_Reset_m531EF5FB0BD16A29769CBDE11FDF26AEC2D55F5E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__22_t411188099DCF9174894F420F6390B7E43C48B481_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerator_get_Current_m9C20E2FAE0940D92F51C5E1C9D804E406BF2F34F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__22_t411188099DCF9174894F420F6390B7E43C48B481_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__22_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m7F40779C5E4EF43C9898EFDECF0FA816A7E5DCFA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__22_t411188099DCF9174894F420F6390B7E43C48B481_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerable_GetEnumerator_mCC77091552DD207B63E4B7C2B33693B4467ED68D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void JSONObject_t401528CAA28B4B8AAA03BBD681A13864E62CE865_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void JSONObject_t401528CAA28B4B8AAA03BBD681A13864E62CE865_CustomAttributesCacheGenerator_JSONObject_get_Children_mDC37B11CC04E072B9E1DFE56A3B176A1E22F8E6D(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3Cget_ChildrenU3Ed__23_t213CCC2BA3B684AD1871D45A4278367199EFBCA8_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3Cget_ChildrenU3Ed__23_t213CCC2BA3B684AD1871D45A4278367199EFBCA8_0_0_0_var), NULL);
	}
}
static void U3CU3Ec__DisplayClass21_0_t373BF0D6FE8FC7A0C471A9DE43255479FD1A6F0C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__23_t213CCC2BA3B684AD1871D45A4278367199EFBCA8_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__23_t213CCC2BA3B684AD1871D45A4278367199EFBCA8_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__23__ctor_m24E00E4270F8E59C9ECF5B85CDB1A34683FC1141(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__23_t213CCC2BA3B684AD1871D45A4278367199EFBCA8_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__23_System_IDisposable_Dispose_m413B24FB7D7B0156DFC7E240DA8391C859440847(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__23_t213CCC2BA3B684AD1871D45A4278367199EFBCA8_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__23_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_m226D4CC6A995605411240005C904F701A7E7D224(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__23_t213CCC2BA3B684AD1871D45A4278367199EFBCA8_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerator_Reset_m60B145CE869947C90DB4665A23EDDF48AE63AC25(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__23_t213CCC2BA3B684AD1871D45A4278367199EFBCA8_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerator_get_Current_m51D1A00926BFE5BDA287FE9A878C6E949F9D0DBB(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__23_t213CCC2BA3B684AD1871D45A4278367199EFBCA8_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__23_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_mBE8B0928518FD2517C5F344791F32EA64F13C45C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3Cget_ChildrenU3Ed__23_t213CCC2BA3B684AD1871D45A4278367199EFBCA8_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerable_GetEnumerator_m19CBB01715DEE09E679303048771E8ABC2A2D4C0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void JSONLazyCreator_t296BB4494EC996EF3B55A2F0A82DEA5E35C9D85A_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 * tmp = (DefaultMemberAttribute_t8C9B3330DEA69EE364962477FF14FD2CFE30D4B5 *)cache->attributes[0];
		DefaultMemberAttribute__ctor_mA025B6F5B3A9292696E01108027840C8DFF7F4D7(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D"), NULL);
	}
}
static void Hero_t477360619EB702BB183DA8CA44857619B9AA12FC_CustomAttributesCacheGenerator_heroRotatePoint(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x47\x65\x6E\x65\x72\x61\x6C"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Hero_t477360619EB702BB183DA8CA44857619B9AA12FC_CustomAttributesCacheGenerator_heroTurnSpeed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Hero_t477360619EB702BB183DA8CA44857619B9AA12FC_CustomAttributesCacheGenerator_enemyTag(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Hero_t477360619EB702BB183DA8CA44857619B9AA12FC_CustomAttributesCacheGenerator_UseLaser(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x61\x73\x65\x72\x20\x54\x6F\x77\x65\x72"), NULL);
	}
}
static void Hero_t477360619EB702BB183DA8CA44857619B9AA12FC_CustomAttributesCacheGenerator_bulletPrefab(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x42\x75\x6C\x6C\x65\x74\x20\x54\x6F\x77\x65\x72"), NULL);
	}
}
static void Hero_t477360619EB702BB183DA8CA44857619B9AA12FC_CustomAttributesCacheGenerator_hero(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void Hero_t477360619EB702BB183DA8CA44857619B9AA12FC_CustomAttributesCacheGenerator_cookieAura(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x49\x74\x65\x6D\x20\x45\x66\x66\x65\x63\x74"), NULL);
	}
}
static void BaseEnemy_tCCDDF8C04729B5368581508D806525594CB0BC9F_CustomAttributesCacheGenerator_U3CEnemySpeedU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BaseEnemy_tCCDDF8C04729B5368581508D806525594CB0BC9F_CustomAttributesCacheGenerator_U3CEnemyHpU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BaseEnemy_tCCDDF8C04729B5368581508D806525594CB0BC9F_CustomAttributesCacheGenerator_U3CGoldDropU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BaseEnemy_tCCDDF8C04729B5368581508D806525594CB0BC9F_CustomAttributesCacheGenerator_U3CEnemyElementU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BaseEnemy_tCCDDF8C04729B5368581508D806525594CB0BC9F_CustomAttributesCacheGenerator_healthBar(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BaseEnemy_tCCDDF8C04729B5368581508D806525594CB0BC9F_CustomAttributesCacheGenerator_popUpDamageText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BaseEnemy_tCCDDF8C04729B5368581508D806525594CB0BC9F_CustomAttributesCacheGenerator_rotateCanvas(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BaseEnemy_tCCDDF8C04729B5368581508D806525594CB0BC9F_CustomAttributesCacheGenerator_BaseEnemy_get_EnemySpeed_m8E1469EAEFB76EE59CC6FEE9D45438D94EB0D703(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BaseEnemy_tCCDDF8C04729B5368581508D806525594CB0BC9F_CustomAttributesCacheGenerator_BaseEnemy_set_EnemySpeed_m17416B6AA011CE66A743C53E4D38FABABF30A19E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BaseEnemy_tCCDDF8C04729B5368581508D806525594CB0BC9F_CustomAttributesCacheGenerator_BaseEnemy_get_EnemyHp_m60279B2A0B27BCC826D13D21434E45C48CE5BE1B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BaseEnemy_tCCDDF8C04729B5368581508D806525594CB0BC9F_CustomAttributesCacheGenerator_BaseEnemy_set_EnemyHp_mA8ABDB93079B24396F5D3BF6065D9E3EF53DC7DD(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BaseEnemy_tCCDDF8C04729B5368581508D806525594CB0BC9F_CustomAttributesCacheGenerator_BaseEnemy_get_GoldDrop_m561D5F9F207855E6B2ACE07E9DDF833C3054E2D8(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BaseEnemy_tCCDDF8C04729B5368581508D806525594CB0BC9F_CustomAttributesCacheGenerator_BaseEnemy_set_GoldDrop_mA3990C857287CAE8CB85B7736238292CFBD872AC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BaseEnemy_tCCDDF8C04729B5368581508D806525594CB0BC9F_CustomAttributesCacheGenerator_BaseEnemy_get_EnemyElement_m7A2942567C4D07705628C3545EB72DE376C01239(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BaseEnemy_tCCDDF8C04729B5368581508D806525594CB0BC9F_CustomAttributesCacheGenerator_BaseEnemy_set_EnemyElement_mC9B8C24FEA90E86264C0F771E4E51FD46BEFCF69(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BaseEnemy_tCCDDF8C04729B5368581508D806525594CB0BC9F_CustomAttributesCacheGenerator_BaseEnemy_DelayLaserPopUp_m7060C6E807ED81FF861F30EB54208E527CC98D58(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDelayLaserPopUpU3Ed__28_tF0267396241BEFE79B23A545D65A2D3DFAE18C0D_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDelayLaserPopUpU3Ed__28_tF0267396241BEFE79B23A545D65A2D3DFAE18C0D_0_0_0_var), NULL);
	}
}
static void U3CDelayLaserPopUpU3Ed__28_tF0267396241BEFE79B23A545D65A2D3DFAE18C0D_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDelayLaserPopUpU3Ed__28_tF0267396241BEFE79B23A545D65A2D3DFAE18C0D_CustomAttributesCacheGenerator_U3CDelayLaserPopUpU3Ed__28__ctor_m1DD030E25497BC7EAFFF48B12878BC3F6E42E867(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelayLaserPopUpU3Ed__28_tF0267396241BEFE79B23A545D65A2D3DFAE18C0D_CustomAttributesCacheGenerator_U3CDelayLaserPopUpU3Ed__28_System_IDisposable_Dispose_m4ACA82D241FAA199208AD68B89AB6A6D558B6F7F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelayLaserPopUpU3Ed__28_tF0267396241BEFE79B23A545D65A2D3DFAE18C0D_CustomAttributesCacheGenerator_U3CDelayLaserPopUpU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2836850B7B9251D61A015650525BA37CFEE489AC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelayLaserPopUpU3Ed__28_tF0267396241BEFE79B23A545D65A2D3DFAE18C0D_CustomAttributesCacheGenerator_U3CDelayLaserPopUpU3Ed__28_System_Collections_IEnumerator_Reset_m418F0BB7A31FD5BEAFECE3CA6971390F3E76542F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDelayLaserPopUpU3Ed__28_tF0267396241BEFE79B23A545D65A2D3DFAE18C0D_CustomAttributesCacheGenerator_U3CDelayLaserPopUpU3Ed__28_System_Collections_IEnumerator_get_Current_m1E4F5A5594B28D5034EFE6A5E55EEC44FC0C490B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void BaseBullet_tFAE58989A37F312A7E63097449CBC236A34B540D_CustomAttributesCacheGenerator_hitEffect(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass7_0_t6FCA6AC5CD5682942D529BD81F5B10FF02C778D7_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void HeroBluePrint_t0FEB6738C704A98AD3A68AC443A82C03B0429098_CustomAttributesCacheGenerator_Name(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void HeroBluePrint_t0FEB6738C704A98AD3A68AC443A82C03B0429098_CustomAttributesCacheGenerator_HeroName(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x48\x65\x72\x6F\x20\x4E\x61\x6D\x65"), NULL);
	}
}
static void HeroBluePrint_t0FEB6738C704A98AD3A68AC443A82C03B0429098_CustomAttributesCacheGenerator_AttackType(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x41\x74\x74\x61\x63\x6B\x20\x54\x79\x70\x65"), NULL);
	}
}
static void HeroBluePrint_t0FEB6738C704A98AD3A68AC443A82C03B0429098_CustomAttributesCacheGenerator_Prefab(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x50\x72\x65\x66\x61\x62"), NULL);
	}
}
static void HeroBluePrint_t0FEB6738C704A98AD3A68AC443A82C03B0429098_CustomAttributesCacheGenerator_ElementType(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x45\x6C\x65\x6D\x65\x6E\x74"), NULL);
	}
}
static void HeroBluePrint_t0FEB6738C704A98AD3A68AC443A82C03B0429098_CustomAttributesCacheGenerator_IsHero(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x68\x65\x63\x6B\x4C\x69\x73\x74"), NULL);
	}
}
static void HeroBluePrint_t0FEB6738C704A98AD3A68AC443A82C03B0429098_CustomAttributesCacheGenerator_IsBuilt(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void HeroBluePrint_t0FEB6738C704A98AD3A68AC443A82C03B0429098_CustomAttributesCacheGenerator_IsEvo(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void HeroBluePrint_t0FEB6738C704A98AD3A68AC443A82C03B0429098_CustomAttributesCacheGenerator_IsPreEvo(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void HeroBluePrint_t0FEB6738C704A98AD3A68AC443A82C03B0429098_CustomAttributesCacheGenerator_DamageOverTime(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4C\x61\x73\x65\x72\x20\x54\x6F\x77\x65\x72"), NULL);
	}
}
static void HeroBluePrint_t0FEB6738C704A98AD3A68AC443A82C03B0429098_CustomAttributesCacheGenerator_BulletSpeed(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x74\x61\x74\x75\x73"), NULL);
	}
}
static void HeroBluePrint_t0FEB6738C704A98AD3A68AC443A82C03B0429098_CustomAttributesCacheGenerator_Level(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void HeroBluePrint_t0FEB6738C704A98AD3A68AC443A82C03B0429098_CustomAttributesCacheGenerator_HeroCostAmount(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void BuildManager_tC38D9095FCAD1EDD6498011089D3EE2043101750_CustomAttributesCacheGenerator_U3CInstanceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BuildManager_tC38D9095FCAD1EDD6498011089D3EE2043101750_CustomAttributesCacheGenerator_HeroLimit(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x42\x75\x69\x6C\x64\x20\x53\x65\x74\x74\x69\x6E\x67"), NULL);
	}
}
static void BuildManager_tC38D9095FCAD1EDD6498011089D3EE2043101750_CustomAttributesCacheGenerator_heroUI(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void BuildManager_tC38D9095FCAD1EDD6498011089D3EE2043101750_CustomAttributesCacheGenerator_BuildManager_get_Instance_m447C53DC52D9B8E9352F7CF07F13C1345CD8DA6F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void BuildManager_tC38D9095FCAD1EDD6498011089D3EE2043101750_CustomAttributesCacheGenerator_BuildManager_set_Instance_mA6204A4F0E4CA49361D88BE862C74472578890F3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_U3CInstanceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_GameOver(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_U3CEnemyCountU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_countdownGameOver(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x74\x20\x45\x6E\x65\x6D\x79\x20\x4C\x69\x6D\x69\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_upgradeEnemyIn5Wave(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x74\x20\x45\x6E\x65\x6D\x79\x20\x55\x70\x67\x72\x61\x64\x65\x20\x49\x6E\x20\x35\x20\x57\x61\x76\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_UpgradeAttackDamagePreEvo(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x74\x20\x45\x76\x6F\x48\x65\x72\x6F\x20\x53\x74\x61\x74\x75\x73"), NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_colubted_Name(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x6F\x6C\x75\x62\x74\x65\x64"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_colubted_MoveSpeed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_colubted_Health(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_colubted_GoldDrop(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_colubted_EnemyElement(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_colubted_SpawnTime(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_colubtedPrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_embio_Name(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x45\x6D\x62\x69\x6F"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_embio_MoveSpeed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_embio_Health(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_embio_GoldDrop(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_embio_EnemyElement(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_embio_SpawnTime(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_embioPrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_eclipseside_Name(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x45\x63\x6C\x69\x70\x73\x65\x73\x69\x64\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_eclipseside_MoveSpeed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_eclipseside_Health(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_eclipseside_GoldDrop(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_eclipseside_EnemyElement(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_eclipseside_SpawnTime(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_eclipsesidePrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_crossDive_Name(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x72\x6F\x73\x73\x44\x69\x76\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_crossDive_MoveSpeed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_crossDive_Health(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_crossDive_GoldDrop(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_crossDive_EnemyElement(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_crossDive_SpawnTime(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_crossDivePrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_shiro_Name(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x68\x69\x72\x6F"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_shiro_MoveSpeed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_shiro_Health(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_shiro_GoldDrop(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_shiro_EnemyElement(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_shiro_SpawnTime(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_shiroPrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_kana_Name(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4B\x61\x6E\x61"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_kana_MoveSpeed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_kana_Health(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_kana_GoldDrop(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_kana_EnemyElement(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_kana_SpawnTime(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_kanaPrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_chest_Name(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x68\x65\x73\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_chest_MoveSpeed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_chest_Health(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_chest_GoldDrop(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_chest_EnemyElement(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_chest_SpawnTime(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_chestPrefab(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_enemyUpgrade_HP(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x55\x70\x67\x72\x61\x64\x65\x20\x45\x6E\x65\x6D\x79\x20\x50\x65\x72\x20\x57\x61\x76\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_enemyUpgrade_Gold(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_enemyUpgrade_Speed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_GameManager_get_Instance_m705AC8D9CAF114B59D3AF0563C19B5EE0B711074(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_GameManager_set_Instance_m9DB0F13876CD49066CC3A818D68686B3600F792E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_GameManager_add_GameOver_mBED5EF8A33E9E9D30CCF2FF978C82C91AE824089(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_GameManager_remove_GameOver_m4A4097C770BD88B02898662A4165CDD35CD06677(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_GameManager_get_EnemyCount_mC5D539611B83ABD18DDE25362AAF39D6820E2588(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_GameManager_set_EnemyCount_mD5A07C5A7544A51C663A9921CC8F86E7041EA7D2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InventoryManager_tD1BFBF734B63F17BD4E44640A9B2DFB3FE2EF742_CustomAttributesCacheGenerator_AtkDamage(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x55\x70\x67\x72\x61\x64\x65\x20\x48\x65\x72\x6F\x20\x53\x74\x61\x74\x75\x73"), NULL);
	}
}
static void InventoryManager_tD1BFBF734B63F17BD4E44640A9B2DFB3FE2EF742_CustomAttributesCacheGenerator_HeroTeam(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x48\x65\x72\x6F\x20\x4D\x61\x6E\x61\x67\x65"), NULL);
	}
}
static void InventoryManager_tD1BFBF734B63F17BD4E44640A9B2DFB3FE2EF742_CustomAttributesCacheGenerator_Cards(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79"), NULL);
	}
}
static void InventoryManager_tD1BFBF734B63F17BD4E44640A9B2DFB3FE2EF742_CustomAttributesCacheGenerator_CardsInventory(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79\x20\x4F\x70\x74\x69\x6F\x6E\x61\x6C"), NULL);
	}
}
static void InventoryManager_tD1BFBF734B63F17BD4E44640A9B2DFB3FE2EF742_CustomAttributesCacheGenerator_inventoryPanel(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x49\x6E\x76\x65\x6E\x74\x6F\x72\x79\x20\x53\x65\x74\x75\x70"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void InventoryManager_tD1BFBF734B63F17BD4E44640A9B2DFB3FE2EF742_CustomAttributesCacheGenerator_usedCardField(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void InventoryManager_tD1BFBF734B63F17BD4E44640A9B2DFB3FE2EF742_CustomAttributesCacheGenerator_UsedCardTemp(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void InventoryManager_tD1BFBF734B63F17BD4E44640A9B2DFB3FE2EF742_CustomAttributesCacheGenerator_IsBuildMode(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void InventoryManager_tD1BFBF734B63F17BD4E44640A9B2DFB3FE2EF742_CustomAttributesCacheGenerator_U3CInstanceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InventoryManager_tD1BFBF734B63F17BD4E44640A9B2DFB3FE2EF742_CustomAttributesCacheGenerator_InventoryManager_get_Instance_mD984E22ADAC6EE38F0CE613A6AD261064F62E121(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void InventoryManager_tD1BFBF734B63F17BD4E44640A9B2DFB3FE2EF742_CustomAttributesCacheGenerator_InventoryManager_set_Instance_m290F386A14BBDD7A90E7A57D1F43D0CBE9680AF2(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec_tFDDEA42123744E3A1DA3325ECA1A91BF01F8AECA_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass23_0_t468F43C7C71C22486353B27517D8AE977A794F00_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass24_0_t510EDD7DFCD45F4DBF040986CD6FD88A798E5310_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass25_0_tD7670C47745BE4B4934DF304248F9B138F3C1CBD_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass26_0_tDDEDBED6B1B9FF2B9A78B6C5BBFD82D7FD1F1277_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MoneyManager_tC109A492276276C2C8B6EC463FB501014D6859FE_CustomAttributesCacheGenerator_U3CInstanceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MoneyManager_tC109A492276276C2C8B6EC463FB501014D6859FE_CustomAttributesCacheGenerator_U3CMoneyU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MoneyManager_tC109A492276276C2C8B6EC463FB501014D6859FE_CustomAttributesCacheGenerator_startMoney(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void MoneyManager_tC109A492276276C2C8B6EC463FB501014D6859FE_CustomAttributesCacheGenerator_MoneyManager_get_Instance_mD33F844A651475BDB3C97054EAAF8B8198A9AA5E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MoneyManager_tC109A492276276C2C8B6EC463FB501014D6859FE_CustomAttributesCacheGenerator_MoneyManager_set_Instance_m9FDBDFA722BB515C6AB48DC35A5EF19C7BAB2798(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MoneyManager_tC109A492276276C2C8B6EC463FB501014D6859FE_CustomAttributesCacheGenerator_MoneyManager_get_Money_m9F70B21FCD308184615E006F48FC5DDCB43BEA8D(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void MoneyManager_tC109A492276276C2C8B6EC463FB501014D6859FE_CustomAttributesCacheGenerator_MoneyManager_set_Money_m39C6D08CC4093DAFBE32E4B86887FCB14882C0DB(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RandomScrollManager_t46FEAC1380C11393456FD90E799CF376B89253E1_CustomAttributesCacheGenerator_U3CInstanceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RandomScrollManager_t46FEAC1380C11393456FD90E799CF376B89253E1_CustomAttributesCacheGenerator_heroPrefabs(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RandomScrollManager_t46FEAC1380C11393456FD90E799CF376B89253E1_CustomAttributesCacheGenerator_subHeroPrefabs(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RandomScrollManager_t46FEAC1380C11393456FD90E799CF376B89253E1_CustomAttributesCacheGenerator_heroRandomRate(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x65\x74\x74\x69\x6E\x67"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RandomScrollManager_t46FEAC1380C11393456FD90E799CF376B89253E1_CustomAttributesCacheGenerator_subHeroRandomRate(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RandomScrollManager_t46FEAC1380C11393456FD90E799CF376B89253E1_CustomAttributesCacheGenerator_heroRandomPlusRate(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RandomScrollManager_t46FEAC1380C11393456FD90E799CF376B89253E1_CustomAttributesCacheGenerator_subHeroRandomPlusRate(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RandomScrollManager_t46FEAC1380C11393456FD90E799CF376B89253E1_CustomAttributesCacheGenerator_randomField(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RandomScrollManager_t46FEAC1380C11393456FD90E799CF376B89253E1_CustomAttributesCacheGenerator_randomArea(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RandomScrollManager_t46FEAC1380C11393456FD90E799CF376B89253E1_CustomAttributesCacheGenerator_maxCard(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RandomScrollManager_t46FEAC1380C11393456FD90E799CF376B89253E1_CustomAttributesCacheGenerator_cards(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void RandomScrollManager_t46FEAC1380C11393456FD90E799CF376B89253E1_CustomAttributesCacheGenerator_OnFirstRandomCard(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RandomScrollManager_t46FEAC1380C11393456FD90E799CF376B89253E1_CustomAttributesCacheGenerator_LongClickCheck(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void RandomScrollManager_t46FEAC1380C11393456FD90E799CF376B89253E1_CustomAttributesCacheGenerator_CardTemp(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void RandomScrollManager_t46FEAC1380C11393456FD90E799CF376B89253E1_CustomAttributesCacheGenerator_AddCard(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RandomScrollManager_t46FEAC1380C11393456FD90E799CF376B89253E1_CustomAttributesCacheGenerator_RandomScrollManager_get_Instance_m71192DB4EC184AB40057DDB9A1A58BF2F682D6DC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RandomScrollManager_t46FEAC1380C11393456FD90E799CF376B89253E1_CustomAttributesCacheGenerator_RandomScrollManager_set_Instance_mC51D12544F256ABBFB0604230F7E9FBD14A34627(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RandomScrollManager_t46FEAC1380C11393456FD90E799CF376B89253E1_CustomAttributesCacheGenerator_RandomScrollManager_add_OnFirstRandomCard_m55B9B6C95F99B39B0283EE6771C362967EEF59A3(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RandomScrollManager_t46FEAC1380C11393456FD90E799CF376B89253E1_CustomAttributesCacheGenerator_RandomScrollManager_remove_OnFirstRandomCard_m75726C3DEC3C365DE5DFB8A44F5E0D99E658DD6B(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RandomScrollManager_t46FEAC1380C11393456FD90E799CF376B89253E1_CustomAttributesCacheGenerator_RandomScrollManager_add_AddCard_m1DFB2FEF7AB61EE4C232D430BF6434FB96FDDC89(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void RandomScrollManager_t46FEAC1380C11393456FD90E799CF376B89253E1_CustomAttributesCacheGenerator_RandomScrollManager_remove_AddCard_m16A0C1743E0F9F8152FA98C089624F21376AD0AC(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UiManager_tBCDA10D96B04DE5423A278745391F4F80A54A66A_CustomAttributesCacheGenerator_U3CInstanceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UiManager_tBCDA10D96B04DE5423A278745391F4F80A54A66A_CustomAttributesCacheGenerator_RandomScrollButtonArea(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x52\x61\x6E\x64\x6F\x6D\x53\x63\x72\x6F\x6C\x6C"), NULL);
	}
}
static void UiManager_tBCDA10D96B04DE5423A278745391F4F80A54A66A_CustomAttributesCacheGenerator_reRandomButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UiManager_tBCDA10D96B04DE5423A278745391F4F80A54A66A_CustomAttributesCacheGenerator_closeRandomScrollButton(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UiManager_tBCDA10D96B04DE5423A278745391F4F80A54A66A_CustomAttributesCacheGenerator_money(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4D\x6F\x6E\x65\x79"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UiManager_tBCDA10D96B04DE5423A278745391F4F80A54A66A_CustomAttributesCacheGenerator_timeCountBeforeOver(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x47\x61\x6D\x65\x4F\x76\x65\x72"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UiManager_tBCDA10D96B04DE5423A278745391F4F80A54A66A_CustomAttributesCacheGenerator_gameOver(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UiManager_tBCDA10D96B04DE5423A278745391F4F80A54A66A_CustomAttributesCacheGenerator_textNotification(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x4E\x6F\x74\x69\x66\x69\x63\x61\x74\x69\x6F\x6E"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UiManager_tBCDA10D96B04DE5423A278745391F4F80A54A66A_CustomAttributesCacheGenerator_skillChainArea(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x68\x61\x69\x6E\x20\x53\x6B\x69\x6C\x6C"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UiManager_tBCDA10D96B04DE5423A278745391F4F80A54A66A_CustomAttributesCacheGenerator_skillChainText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UiManager_tBCDA10D96B04DE5423A278745391F4F80A54A66A_CustomAttributesCacheGenerator_skillChainsList(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UiManager_tBCDA10D96B04DE5423A278745391F4F80A54A66A_CustomAttributesCacheGenerator_elementChainArea(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x68\x61\x69\x6E\x20\x45\x6C\x65\x6D\x65\x6E\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UiManager_tBCDA10D96B04DE5423A278745391F4F80A54A66A_CustomAttributesCacheGenerator_elementChainsImg(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UiManager_tBCDA10D96B04DE5423A278745391F4F80A54A66A_CustomAttributesCacheGenerator_blockUI(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x42\x6C\x6F\x63\x6B\x43\x61\x6E\x50\x6C\x61\x63\x65\x55\x49"), NULL);
	}
}
static void UiManager_tBCDA10D96B04DE5423A278745391F4F80A54A66A_CustomAttributesCacheGenerator_cardAmountText(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x43\x61\x72\x64\x41\x6D\x6F\x75\x6E\x74"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UiManager_tBCDA10D96B04DE5423A278745391F4F80A54A66A_CustomAttributesCacheGenerator_timeSurvived(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x53\x63\x6F\x72\x65\x45\x6E\x64\x47\x61\x6D\x65"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UiManager_tBCDA10D96B04DE5423A278745391F4F80A54A66A_CustomAttributesCacheGenerator_waveSurvived(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UiManager_tBCDA10D96B04DE5423A278745391F4F80A54A66A_CustomAttributesCacheGenerator_damageDone(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UiManager_tBCDA10D96B04DE5423A278745391F4F80A54A66A_CustomAttributesCacheGenerator_compUsed(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void UiManager_tBCDA10D96B04DE5423A278745391F4F80A54A66A_CustomAttributesCacheGenerator_AdsRewardButton(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x52\x65\x77\x61\x72\x64\x42\x75\x74\x74\x6F\x6E"), NULL);
	}
}
static void UiManager_tBCDA10D96B04DE5423A278745391F4F80A54A66A_CustomAttributesCacheGenerator_DamageDoneGameOver(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void UiManager_tBCDA10D96B04DE5423A278745391F4F80A54A66A_CustomAttributesCacheGenerator_IsRandomPlus(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void UiManager_tBCDA10D96B04DE5423A278745391F4F80A54A66A_CustomAttributesCacheGenerator_UiManager_get_Instance_mD22B7B526318C512A940851D7C5F3605A3188F72(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UiManager_tBCDA10D96B04DE5423A278745391F4F80A54A66A_CustomAttributesCacheGenerator_UiManager_set_Instance_mC55D7E31E56BDE146DC3270088C52E0D5C7CEBEA(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void UiManager_tBCDA10D96B04DE5423A278745391F4F80A54A66A_CustomAttributesCacheGenerator_UiManager_WaitForNoticeTimerOut_mC2A0447A655995E085F1CF523E140BE180B9BBE9(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitForNoticeTimerOutU3Ed__43_t872B5C580D4D7A86DDD89BA935867637E1EDF564_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWaitForNoticeTimerOutU3Ed__43_t872B5C580D4D7A86DDD89BA935867637E1EDF564_0_0_0_var), NULL);
	}
}
static void UiManager_tBCDA10D96B04DE5423A278745391F4F80A54A66A_CustomAttributesCacheGenerator_UiManager_WaitForNoticeOut_m80849B33FF4B00687BDF23BDA6DE02123DEF6831(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CWaitForNoticeOutU3Ed__46_tB40B7C9E84E36CF0E86D5063901513398B490775_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CWaitForNoticeOutU3Ed__46_tB40B7C9E84E36CF0E86D5063901513398B490775_0_0_0_var), NULL);
	}
}
static void U3CWaitForNoticeTimerOutU3Ed__43_t872B5C580D4D7A86DDD89BA935867637E1EDF564_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitForNoticeTimerOutU3Ed__43_t872B5C580D4D7A86DDD89BA935867637E1EDF564_CustomAttributesCacheGenerator_U3CWaitForNoticeTimerOutU3Ed__43__ctor_mC64694756F43EEF9DCC1F4101737D83C1DC8BB93(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForNoticeTimerOutU3Ed__43_t872B5C580D4D7A86DDD89BA935867637E1EDF564_CustomAttributesCacheGenerator_U3CWaitForNoticeTimerOutU3Ed__43_System_IDisposable_Dispose_m01791A5F038C1BCF58742292247713EFA6C770CC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForNoticeTimerOutU3Ed__43_t872B5C580D4D7A86DDD89BA935867637E1EDF564_CustomAttributesCacheGenerator_U3CWaitForNoticeTimerOutU3Ed__43_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m03D682641A449C18CD1C00484E7509D3C8D843EC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForNoticeTimerOutU3Ed__43_t872B5C580D4D7A86DDD89BA935867637E1EDF564_CustomAttributesCacheGenerator_U3CWaitForNoticeTimerOutU3Ed__43_System_Collections_IEnumerator_Reset_mDDA5A5A3E227742BEFD8545DFC4EDA5B33430563(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForNoticeTimerOutU3Ed__43_t872B5C580D4D7A86DDD89BA935867637E1EDF564_CustomAttributesCacheGenerator_U3CWaitForNoticeTimerOutU3Ed__43_System_Collections_IEnumerator_get_Current_mB7841FEE52274FEA64951C4EF021BC1825F42031(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForNoticeOutU3Ed__46_tB40B7C9E84E36CF0E86D5063901513398B490775_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CWaitForNoticeOutU3Ed__46_tB40B7C9E84E36CF0E86D5063901513398B490775_CustomAttributesCacheGenerator_U3CWaitForNoticeOutU3Ed__46__ctor_m6F5A034947277CE289E43F8D368B15C565DA34DA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForNoticeOutU3Ed__46_tB40B7C9E84E36CF0E86D5063901513398B490775_CustomAttributesCacheGenerator_U3CWaitForNoticeOutU3Ed__46_System_IDisposable_Dispose_m5E3269113CF4303B8B8D4081E875B335EB3FEE1C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForNoticeOutU3Ed__46_tB40B7C9E84E36CF0E86D5063901513398B490775_CustomAttributesCacheGenerator_U3CWaitForNoticeOutU3Ed__46_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m389C85183678A455CB9EFF03218F4F9459269270(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForNoticeOutU3Ed__46_tB40B7C9E84E36CF0E86D5063901513398B490775_CustomAttributesCacheGenerator_U3CWaitForNoticeOutU3Ed__46_System_Collections_IEnumerator_Reset_m84D84F466AE4D9EC09F0827B66E1C4085133898A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CWaitForNoticeOutU3Ed__46_tB40B7C9E84E36CF0E86D5063901513398B490775_CustomAttributesCacheGenerator_U3CWaitForNoticeOutU3Ed__46_System_Collections_IEnumerator_get_Current_mE2FCDD664F3D5710AF71599273562A7143A1B9E9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CU3Ec__DisplayClass53_0_t9D3690D507A903EC111455BB3CA8F376D6B35138_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WaveManager_t712FD5213A90156B815DC5E3AEDBD86017E3CE8B_CustomAttributesCacheGenerator_U3CInstanceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WaveManager_t712FD5213A90156B815DC5E3AEDBD86017E3CE8B_CustomAttributesCacheGenerator_timePerWaves(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WaveManager_t712FD5213A90156B815DC5E3AEDBD86017E3CE8B_CustomAttributesCacheGenerator_timeCooldownWaves(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WaveManager_t712FD5213A90156B815DC5E3AEDBD86017E3CE8B_CustomAttributesCacheGenerator_timer(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WaveManager_t712FD5213A90156B815DC5E3AEDBD86017E3CE8B_CustomAttributesCacheGenerator_OnNextWave(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WaveManager_t712FD5213A90156B815DC5E3AEDBD86017E3CE8B_CustomAttributesCacheGenerator_RandomEnemy(CustomAttributesCache* cache)
{
	{
		HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA * tmp = (HideInInspector_tDD5B9D3AD8D48C93E23FE6CA3ECDA5589D60CCDA *)cache->attributes[0];
		HideInInspector__ctor_mE2B7FB1D206A74BA583C7812CDB4EBDD83EB66F9(tmp, NULL);
	}
}
static void WaveManager_t712FD5213A90156B815DC5E3AEDBD86017E3CE8B_CustomAttributesCacheGenerator_WaveManager_get_Instance_m245870AA93671235A9952030594169263629EA4F(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WaveManager_t712FD5213A90156B815DC5E3AEDBD86017E3CE8B_CustomAttributesCacheGenerator_WaveManager_set_Instance_mEDF39195B0EDFB8F8661CA8614CAE135274FDA64(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WaveManager_t712FD5213A90156B815DC5E3AEDBD86017E3CE8B_CustomAttributesCacheGenerator_WaveManager_add_OnNextWave_mBFB31E8EE88D78103A44F33E610C07051A7A209E(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WaveManager_t712FD5213A90156B815DC5E3AEDBD86017E3CE8B_CustomAttributesCacheGenerator_WaveManager_remove_OnNextWave_mB3182F529CF54627875EBADF430F16687BD211F5(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WorldUIManager_t8F19F2357842FE0470C8C0E492D3FE318B67ED2F_CustomAttributesCacheGenerator_U3CInstanceU3Ek__BackingField(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WorldUIManager_t8F19F2357842FE0470C8C0E492D3FE318B67ED2F_CustomAttributesCacheGenerator_infoPanel(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WorldUIManager_t8F19F2357842FE0470C8C0E492D3FE318B67ED2F_CustomAttributesCacheGenerator_infoText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WorldUIManager_t8F19F2357842FE0470C8C0E492D3FE318B67ED2F_CustomAttributesCacheGenerator_enemyInfoField(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x45\x6E\x65\x6D\x79\x20\x49\x6E\x66\x6F"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WorldUIManager_t8F19F2357842FE0470C8C0E492D3FE318B67ED2F_CustomAttributesCacheGenerator_enemyInfoText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WorldUIManager_t8F19F2357842FE0470C8C0E492D3FE318B67ED2F_CustomAttributesCacheGenerator_heroInfoField(CustomAttributesCache* cache)
{
	{
		HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB * tmp = (HeaderAttribute_t9B431E6BA0524D46406D9C413D6A71CB5F2DD1AB *)cache->attributes[0];
		HeaderAttribute__ctor_m601319E0BCE8C44A9E79B2C0ABAAD0FEF46A9F1E(tmp, il2cpp_codegen_string_new_wrapper("\x48\x65\x72\x6F\x20\x49\x6E\x66\x6F"), NULL);
	}
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[1];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WorldUIManager_t8F19F2357842FE0470C8C0E492D3FE318B67ED2F_CustomAttributesCacheGenerator_heroInfoText(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WorldUIManager_t8F19F2357842FE0470C8C0E492D3FE318B67ED2F_CustomAttributesCacheGenerator_showScreenShotImage(CustomAttributesCache* cache)
{
	{
		SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 * tmp = (SerializeField_t6B23EE6CC99B21C3EBD946352112832A70E67E25 *)cache->attributes[0];
		SerializeField__ctor_mDE6A7673BA2C1FAD03CFEC65C6D473CC37889DD3(tmp, NULL);
	}
}
static void WorldUIManager_t8F19F2357842FE0470C8C0E492D3FE318B67ED2F_CustomAttributesCacheGenerator_WorldUIManager_get_Instance_m69CA555FDFB6797BFB2D7E2CBF758AD7CF1BE7EF(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WorldUIManager_t8F19F2357842FE0470C8C0E492D3FE318B67ED2F_CustomAttributesCacheGenerator_WorldUIManager_set_Instance_m4F34BA28C67F98C9F4152355F3B13247D259AB0A(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void WorldUIManager_t8F19F2357842FE0470C8C0E492D3FE318B67ED2F_CustomAttributesCacheGenerator_WorldUIManager_NextEnemy_m9B9022BB73E120504B17458013796E023FB5AE30(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CNextEnemyU3Ed__26_t4F6CB5EE30FB1BC45025C47422CA0BA6F4CFBA05_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CNextEnemyU3Ed__26_t4F6CB5EE30FB1BC45025C47422CA0BA6F4CFBA05_0_0_0_var), NULL);
	}
}
static void WorldUIManager_t8F19F2357842FE0470C8C0E492D3FE318B67ED2F_CustomAttributesCacheGenerator_WorldUIManager_NextHero_m31ABAD8168C9EAB5D6829B341F1E759AE2D8D137(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CNextHeroU3Ed__27_tA3FA225A9F245C060D7D1285EC0ED7F01B031AA0_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CNextHeroU3Ed__27_tA3FA225A9F245C060D7D1285EC0ED7F01B031AA0_0_0_0_var), NULL);
	}
}
static void WorldUIManager_t8F19F2357842FE0470C8C0E492D3FE318B67ED2F_CustomAttributesCacheGenerator_WorldUIManager_DisableInfo_m486646FD2278E1A68A2ACC89D9B5C195CB5D6D0C(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CDisableInfoU3Ed__33_t6EB1180E7648EE0413BB51BA7BFB27F30B17C577_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CDisableInfoU3Ed__33_t6EB1180E7648EE0413BB51BA7BFB27F30B17C577_0_0_0_var), NULL);
	}
}
static void WorldUIManager_t8F19F2357842FE0470C8C0E492D3FE318B67ED2F_CustomAttributesCacheGenerator_WorldUIManager_FireWorkClicked_m2E89B09A70BB788222B38B1CC8704857A97B43BE(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CFireWorkClickedU3Ed__34_t0A3C43FA8003F5D86FE232DD5D91E4F1EF7CFE24_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CFireWorkClickedU3Ed__34_t0A3C43FA8003F5D86FE232DD5D91E4F1EF7CFE24_0_0_0_var), NULL);
	}
}
static void WorldUIManager_t8F19F2357842FE0470C8C0E492D3FE318B67ED2F_CustomAttributesCacheGenerator_WorldUIManager_CookieClicked_mCB4A623F5D5D4231D2EC0C4BAD9FC47E9BF83F1E(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCookieClickedU3Ed__35_tDA1263DFFDD0773D381B1D787122AEEABE86AF9C_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCookieClickedU3Ed__35_tDA1263DFFDD0773D381B1D787122AEEABE86AF9C_0_0_0_var), NULL);
	}
}
static void WorldUIManager_t8F19F2357842FE0470C8C0E492D3FE318B67ED2F_CustomAttributesCacheGenerator_WorldUIManager_CandyClicked_m004176E7D20CC7412E7303ACA8CDF6ED8C81D6B3(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CCandyClickedU3Ed__36_tCA313AA216D873516B3C1A392A0302BBB97B86B0_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CCandyClickedU3Ed__36_tCA313AA216D873516B3C1A392A0302BBB97B86B0_0_0_0_var), NULL);
	}
}
static void WorldUIManager_t8F19F2357842FE0470C8C0E492D3FE318B67ED2F_CustomAttributesCacheGenerator_WorldUIManager_ScreenShotClicked_m957D2F14806168935EB078D5FCF9CCBA2EB21B5A(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CScreenShotClickedU3Ed__37_t982CA263942B0C8A08EC834CA778F5356084D169_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CScreenShotClickedU3Ed__37_t982CA263942B0C8A08EC834CA778F5356084D169_0_0_0_var), NULL);
	}
}
static void WorldUIManager_t8F19F2357842FE0470C8C0E492D3FE318B67ED2F_CustomAttributesCacheGenerator_WorldUIManager_LeaderBoardClicked_m7933E88951F99F4887431F5E90C732CDC4D74627(CustomAttributesCache* cache)
{
	static bool s_Il2CppMethodInitialized;
	if (!s_Il2CppMethodInitialized)
	{
		il2cpp_codegen_initialize_runtime_metadata((uintptr_t*)&U3CLeaderBoardClickedU3Ed__38_tBAE695B5560CCB49D1653EAD612C1CE08B42F361_0_0_0_var);
		s_Il2CppMethodInitialized = true;
	}
	{
		IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 * tmp = (IteratorStateMachineAttribute_t6C72F3EC15FB34D08D47727AA7A86AB7FEA27830 *)cache->attributes[0];
		IteratorStateMachineAttribute__ctor_m019CD62C4E5301F55EDF4723107B608AE8F12481(tmp, il2cpp_codegen_type_get_object(U3CLeaderBoardClickedU3Ed__38_tBAE695B5560CCB49D1653EAD612C1CE08B42F361_0_0_0_var), NULL);
	}
}
static void U3CNextEnemyU3Ed__26_t4F6CB5EE30FB1BC45025C47422CA0BA6F4CFBA05_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CNextEnemyU3Ed__26_t4F6CB5EE30FB1BC45025C47422CA0BA6F4CFBA05_CustomAttributesCacheGenerator_U3CNextEnemyU3Ed__26__ctor_mF7F906DB2AE8D88AD0A7E5231306EFF4CD401EE0(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CNextEnemyU3Ed__26_t4F6CB5EE30FB1BC45025C47422CA0BA6F4CFBA05_CustomAttributesCacheGenerator_U3CNextEnemyU3Ed__26_System_IDisposable_Dispose_m51376AE841CDA01FEDF72193251CD600592192CA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CNextEnemyU3Ed__26_t4F6CB5EE30FB1BC45025C47422CA0BA6F4CFBA05_CustomAttributesCacheGenerator_U3CNextEnemyU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0644F6F37C2E4EB196F8C3EE1EAE560375A86B61(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CNextEnemyU3Ed__26_t4F6CB5EE30FB1BC45025C47422CA0BA6F4CFBA05_CustomAttributesCacheGenerator_U3CNextEnemyU3Ed__26_System_Collections_IEnumerator_Reset_mC1135533A36ACAF17E80743D6142CD29C926476B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CNextEnemyU3Ed__26_t4F6CB5EE30FB1BC45025C47422CA0BA6F4CFBA05_CustomAttributesCacheGenerator_U3CNextEnemyU3Ed__26_System_Collections_IEnumerator_get_Current_m2FF04E7D947B7C1A1ABCB509822D255ADEE518EC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CNextHeroU3Ed__27_tA3FA225A9F245C060D7D1285EC0ED7F01B031AA0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CNextHeroU3Ed__27_tA3FA225A9F245C060D7D1285EC0ED7F01B031AA0_CustomAttributesCacheGenerator_U3CNextHeroU3Ed__27__ctor_m223862A8FD430B9194B60EB590B188E6761543A9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CNextHeroU3Ed__27_tA3FA225A9F245C060D7D1285EC0ED7F01B031AA0_CustomAttributesCacheGenerator_U3CNextHeroU3Ed__27_System_IDisposable_Dispose_m5E5704CA63BBBD3107B59C4A610ABEFBE5175BEC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CNextHeroU3Ed__27_tA3FA225A9F245C060D7D1285EC0ED7F01B031AA0_CustomAttributesCacheGenerator_U3CNextHeroU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0AF8B7EC64336EA9C996CD13B146749D3EA9816C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CNextHeroU3Ed__27_tA3FA225A9F245C060D7D1285EC0ED7F01B031AA0_CustomAttributesCacheGenerator_U3CNextHeroU3Ed__27_System_Collections_IEnumerator_Reset_mADB82B690752F5485FD704C02A3BBFF637267083(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CNextHeroU3Ed__27_tA3FA225A9F245C060D7D1285EC0ED7F01B031AA0_CustomAttributesCacheGenerator_U3CNextHeroU3Ed__27_System_Collections_IEnumerator_get_Current_m94202AE088D04C101BF4F74AB5F76AD0286EA924(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDisableInfoU3Ed__33_t6EB1180E7648EE0413BB51BA7BFB27F30B17C577_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CDisableInfoU3Ed__33_t6EB1180E7648EE0413BB51BA7BFB27F30B17C577_CustomAttributesCacheGenerator_U3CDisableInfoU3Ed__33__ctor_m60DCAC3ED8F8B0CC7BCBE7E03BABF63357A6FA6D(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDisableInfoU3Ed__33_t6EB1180E7648EE0413BB51BA7BFB27F30B17C577_CustomAttributesCacheGenerator_U3CDisableInfoU3Ed__33_System_IDisposable_Dispose_m932AC83042484FA097910F368C56EC1CC724865F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDisableInfoU3Ed__33_t6EB1180E7648EE0413BB51BA7BFB27F30B17C577_CustomAttributesCacheGenerator_U3CDisableInfoU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m370655C1AD58BE840BC850118005790511801227(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDisableInfoU3Ed__33_t6EB1180E7648EE0413BB51BA7BFB27F30B17C577_CustomAttributesCacheGenerator_U3CDisableInfoU3Ed__33_System_Collections_IEnumerator_Reset_m028A5C860CB4B798DDF150CFF80F4E64A1F419E9(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CDisableInfoU3Ed__33_t6EB1180E7648EE0413BB51BA7BFB27F30B17C577_CustomAttributesCacheGenerator_U3CDisableInfoU3Ed__33_System_Collections_IEnumerator_get_Current_mC802088660CAAE6D1999E1F89950C8372657FF82(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFireWorkClickedU3Ed__34_t0A3C43FA8003F5D86FE232DD5D91E4F1EF7CFE24_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CFireWorkClickedU3Ed__34_t0A3C43FA8003F5D86FE232DD5D91E4F1EF7CFE24_CustomAttributesCacheGenerator_U3CFireWorkClickedU3Ed__34__ctor_m7A1B53D72F6553C8AEF3C2D73B1DC7C5E9E56B91(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFireWorkClickedU3Ed__34_t0A3C43FA8003F5D86FE232DD5D91E4F1EF7CFE24_CustomAttributesCacheGenerator_U3CFireWorkClickedU3Ed__34_System_IDisposable_Dispose_m46CCAE3CD88625D4F9544A098BCD88E0AB58DD9A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFireWorkClickedU3Ed__34_t0A3C43FA8003F5D86FE232DD5D91E4F1EF7CFE24_CustomAttributesCacheGenerator_U3CFireWorkClickedU3Ed__34_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF91C50E982052F7A84C122811F02FB459B0E63E1(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFireWorkClickedU3Ed__34_t0A3C43FA8003F5D86FE232DD5D91E4F1EF7CFE24_CustomAttributesCacheGenerator_U3CFireWorkClickedU3Ed__34_System_Collections_IEnumerator_Reset_mFCCA5DE1786D2F6C82570827018FC03D4E1773F8(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CFireWorkClickedU3Ed__34_t0A3C43FA8003F5D86FE232DD5D91E4F1EF7CFE24_CustomAttributesCacheGenerator_U3CFireWorkClickedU3Ed__34_System_Collections_IEnumerator_get_Current_mD0C5FEE89BAAAEFDD74726E93B4C290812B0764C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCookieClickedU3Ed__35_tDA1263DFFDD0773D381B1D787122AEEABE86AF9C_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCookieClickedU3Ed__35_tDA1263DFFDD0773D381B1D787122AEEABE86AF9C_CustomAttributesCacheGenerator_U3CCookieClickedU3Ed__35__ctor_m0B575DE0D60FB548252B06CE74082E1EBDF42D57(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCookieClickedU3Ed__35_tDA1263DFFDD0773D381B1D787122AEEABE86AF9C_CustomAttributesCacheGenerator_U3CCookieClickedU3Ed__35_System_IDisposable_Dispose_mA29E952E03BC1A952D9B75AF867842C8C285DADA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCookieClickedU3Ed__35_tDA1263DFFDD0773D381B1D787122AEEABE86AF9C_CustomAttributesCacheGenerator_U3CCookieClickedU3Ed__35_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB2959A148D9B6ECFE069BB4DA16D939690F29580(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCookieClickedU3Ed__35_tDA1263DFFDD0773D381B1D787122AEEABE86AF9C_CustomAttributesCacheGenerator_U3CCookieClickedU3Ed__35_System_Collections_IEnumerator_Reset_m26D0EA370FA27109A9812968AF7E2C4D9C53C38E(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCookieClickedU3Ed__35_tDA1263DFFDD0773D381B1D787122AEEABE86AF9C_CustomAttributesCacheGenerator_U3CCookieClickedU3Ed__35_System_Collections_IEnumerator_get_Current_mD356438C0477B5CAC2CE60DCE85ACC6FAFA10FEC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCandyClickedU3Ed__36_tCA313AA216D873516B3C1A392A0302BBB97B86B0_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CCandyClickedU3Ed__36_tCA313AA216D873516B3C1A392A0302BBB97B86B0_CustomAttributesCacheGenerator_U3CCandyClickedU3Ed__36__ctor_m4A8D08E3E4BCF74EC885E69C8CC1CF1F0F4AACD4(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCandyClickedU3Ed__36_tCA313AA216D873516B3C1A392A0302BBB97B86B0_CustomAttributesCacheGenerator_U3CCandyClickedU3Ed__36_System_IDisposable_Dispose_m5A18D55FE5C43EE1DD366017D649C02BB71BEA7B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCandyClickedU3Ed__36_tCA313AA216D873516B3C1A392A0302BBB97B86B0_CustomAttributesCacheGenerator_U3CCandyClickedU3Ed__36_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m35E28353074F1165B229CE56346CB372606A7FBC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCandyClickedU3Ed__36_tCA313AA216D873516B3C1A392A0302BBB97B86B0_CustomAttributesCacheGenerator_U3CCandyClickedU3Ed__36_System_Collections_IEnumerator_Reset_m85D7FF94205CA8A2640EB611D6CECFC4FFBF9DB7(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CCandyClickedU3Ed__36_tCA313AA216D873516B3C1A392A0302BBB97B86B0_CustomAttributesCacheGenerator_U3CCandyClickedU3Ed__36_System_Collections_IEnumerator_get_Current_m8AE9C3DD85D994C5A0EACACEC1365F49104F17AC(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CScreenShotClickedU3Ed__37_t982CA263942B0C8A08EC834CA778F5356084D169_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CScreenShotClickedU3Ed__37_t982CA263942B0C8A08EC834CA778F5356084D169_CustomAttributesCacheGenerator_U3CScreenShotClickedU3Ed__37__ctor_m4DF76FA6E783CE5C95EC9174F493FE6E32391324(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CScreenShotClickedU3Ed__37_t982CA263942B0C8A08EC834CA778F5356084D169_CustomAttributesCacheGenerator_U3CScreenShotClickedU3Ed__37_System_IDisposable_Dispose_mF247A4A440E9BCC6723DBD6F4829A6A85901EC04(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CScreenShotClickedU3Ed__37_t982CA263942B0C8A08EC834CA778F5356084D169_CustomAttributesCacheGenerator_U3CScreenShotClickedU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m672E78976F63D8246C55DFA344FBD7F0B5E7062B(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CScreenShotClickedU3Ed__37_t982CA263942B0C8A08EC834CA778F5356084D169_CustomAttributesCacheGenerator_U3CScreenShotClickedU3Ed__37_System_Collections_IEnumerator_Reset_m4926A46FCFF998299A791CD8D19835398F2D0851(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CScreenShotClickedU3Ed__37_t982CA263942B0C8A08EC834CA778F5356084D169_CustomAttributesCacheGenerator_U3CScreenShotClickedU3Ed__37_System_Collections_IEnumerator_get_Current_m2A8F29B42E02863B6334D1099147F59F611D35AA(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLeaderBoardClickedU3Ed__38_tBAE695B5560CCB49D1653EAD612C1CE08B42F361_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
static void U3CLeaderBoardClickedU3Ed__38_tBAE695B5560CCB49D1653EAD612C1CE08B42F361_CustomAttributesCacheGenerator_U3CLeaderBoardClickedU3Ed__38__ctor_mE7E26DB8380A3579B64BC586E0253B6F18867F6F(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLeaderBoardClickedU3Ed__38_tBAE695B5560CCB49D1653EAD612C1CE08B42F361_CustomAttributesCacheGenerator_U3CLeaderBoardClickedU3Ed__38_System_IDisposable_Dispose_m7CB383587DF40BD43652C5FC78C26B604462A95A(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLeaderBoardClickedU3Ed__38_tBAE695B5560CCB49D1653EAD612C1CE08B42F361_CustomAttributesCacheGenerator_U3CLeaderBoardClickedU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9EC830344B8F7C475D00C335A2411922FFD4E25C(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLeaderBoardClickedU3Ed__38_tBAE695B5560CCB49D1653EAD612C1CE08B42F361_CustomAttributesCacheGenerator_U3CLeaderBoardClickedU3Ed__38_System_Collections_IEnumerator_Reset_m576E4DED7EC5D84BDEEFDDB2EE94C963C2915406(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CLeaderBoardClickedU3Ed__38_tBAE695B5560CCB49D1653EAD612C1CE08B42F361_CustomAttributesCacheGenerator_U3CLeaderBoardClickedU3Ed__38_System_Collections_IEnumerator_get_Current_m5C0AFE5F19E6B3B18AF2CAB6BA66B991D14EF762(CustomAttributesCache* cache)
{
	{
		DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 * tmp = (DebuggerHiddenAttribute_tD84728997C009D6F540FB29D88F032350E046A88 *)cache->attributes[0];
		DebuggerHiddenAttribute__ctor_mB40799BB5DAFE439BEFE895836CF792B8DBEA7F3(tmp, NULL);
	}
}
static void U3CPrivateImplementationDetailsU3E_t6BC7664D9CD46304D39A7D175BB8FFBE0B9F4528_CustomAttributesCacheGenerator(CustomAttributesCache* cache)
{
	{
		CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C * tmp = (CompilerGeneratedAttribute_t39106AB982658D7A94C27DEF3C48DB2F5F7CD75C *)cache->attributes[0];
		CompilerGeneratedAttribute__ctor_m9DC3E4E2DA76FE93948D44199213E2E924DCBE35(tmp, NULL);
	}
}
IL2CPP_EXTERN_C const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[];
const CustomAttributesCacheGenerator g_AssemblyU2DCSharp_AttributeGenerators[766] = 
{
	WaterManager_t34FC69E413CAB56189B29DA6B188017FB99DA41A_CustomAttributesCacheGenerator,
	WaterReflection_tC56DDAE9A17F940A57C0EA5F0EC5FD8CE60FC710_CustomAttributesCacheGenerator,
	InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A_CustomAttributesCacheGenerator,
	JSONNode_tDEACFC45BBAB7B7189263CF1BA7D4B7FE179132A_CustomAttributesCacheGenerator,
	JSONArray_t478FF5B6520D2CAB00FA50A61F808A3E09F928A0_CustomAttributesCacheGenerator,
	JSONObject_t401528CAA28B4B8AAA03BBD681A13864E62CE865_CustomAttributesCacheGenerator,
	JSONLazyCreator_t296BB4494EC996EF3B55A2F0A82DEA5E35C9D85A_CustomAttributesCacheGenerator,
	U3CPrivateImplementationDetailsU3E_t6BC7664D9CD46304D39A7D175BB8FFBE0B9F4528_CustomAttributesCacheGenerator,
	U3CWaitToStartU3Ed__14_tAD265766599EEE9474750E00FA4939AEAABB823A_CustomAttributesCacheGenerator,
	U3CWaitForCamU3Ed__18_t189EA3BB11BFA6AC795F202CDE0699E1C9029277_CustomAttributesCacheGenerator,
	U3CWaitForShowCanvasU3Ed__21_t6623746B1B2635840CC2F08A9B9C83127716121E_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass15_0_tD3EDBEF01A2BA2A697FFF2AEF29C9B3B62251261_CustomAttributesCacheGenerator,
	U3CImageShowCaseU3Ed__17_tB869F8E282B1CD6D682B87084152A7B70D6A3589_CustomAttributesCacheGenerator,
	U3CDelayClickU3Ed__8_tBF346C93C4D6EAB42D93211E5296F33032B7BC72_CustomAttributesCacheGenerator,
	U3CU3Ec_t885743CA8B20F056197CF0A08B7EE1CB42AF27DC_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass50_0_t21EE77FA2D4C57BAEF865E100B9BFC9FADF79542_CustomAttributesCacheGenerator,
	U3CCheckAlreadyNameU3Ed__50_t81299FEF47EDC5385754950A82C9FE4E5176462D_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass52_0_t190C905043F8FB0FAA7184C28B9AD7236C110A18_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass53_0_t066EC8A4868845AB2F7569EF240D1DF01C06ED67_CustomAttributesCacheGenerator,
	U3CCloseNotificationU3Ed__62_t50067E67A5466C3585675771E09DE822592CB4D0_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass73_0_t82FB778BF3C3CE9AAEDF94879073078125BA2776_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass74_0_t9BA379A9F07DDE57A0A173B68851C4252C3B15F2_CustomAttributesCacheGenerator,
	U3CCheckAlreadyNameIdTransferU3Ed__74_t7451AB99B91CA94A2D6E4F546CA0FD8CC686ED92_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass75_0_tE401FBB76B127E3CED9E4D9EF0D025E97503F7A0_CustomAttributesCacheGenerator,
	U3CLoadUserScoreFromDBU3Ed__75_tB538B410886D8F26EB89210EE854B5F7389C75DB_CustomAttributesCacheGenerator,
	U3CDownloadImageU3Ed__76_t723888129E1A8E7C4732F71C5E435C1DEC50CE12_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass80_0_tA73234661199F943E3E509B8AA692AC64E2733D3_CustomAttributesCacheGenerator,
	U3CLoadLeaderBoardDataU3Ed__80_t53D5F08F28E7939D79D6BB3754783EF0607ABF3D_CustomAttributesCacheGenerator,
	U3CDownloadImageU3Ed__5_t2794312741EA00E7B69D55B73B3136C5C9F354C0_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass12_0_t88DE0C69335FF40F58BD2B9215EA227CE32600D9_CustomAttributesCacheGenerator,
	U3CCheckDamageOverCurrentScoreU3Ed__12_t07A41FEF6FA87A2ECE1E7E0FF28AADDFA79B4546_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass13_0_t0382DE6831C707ADD7AF5B404C2C4A2EFBFF664D_CustomAttributesCacheGenerator,
	U3CCheckTimeSurvivedOverCurrentScoreU3Ed__13_t361F8E558207AE97EA78DAC6651DDAA2FB989EE1_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass14_0_t90ACA593716E00F111572479B558FEDB152693B6_CustomAttributesCacheGenerator,
	U3CUpdateDamageDoneU3Ed__14_t1D42A96E93CA3C068716D61E6E4C4A8CE4087D21_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass15_0_t3700D12D1F7A05220C5F1CB9C571ABA78035F41B_CustomAttributesCacheGenerator,
	U3CUpdateWaveSurvivedU3Ed__15_tF7DE1FBC2C2CC828B58C7F6228A9912502633D6F_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass16_0_t836D24D6A1DC7B774566A3BB1F148849AE96B0DD_CustomAttributesCacheGenerator,
	U3CUpdateTimeSurvivedU3Ed__16_tD6AF10B6C261DD6230297852068466DCA37C64DA_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass17_0_t899D640FF0B7889B20492FAE59103FCE6CB1B4EB_CustomAttributesCacheGenerator,
	U3CLoadUserScoreFromDBU3Ed__17_tA72B0A24BFD2A694425FDB145D618C9F8A7C6AA8_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass18_0_t026DBAF0800E778494BBAAF965A1219FFF4EF201_CustomAttributesCacheGenerator,
	U3CLoadLeaderBoardDataU3Ed__18_t85C0AE47A469B364C930201232E48E31F00B91D1_CustomAttributesCacheGenerator,
	U3CDownloadImageU3Ed__19_tC075483F4C0D4B7D7065D99C2135C5A8A0BCA1D7_CustomAttributesCacheGenerator,
	U3CChangeButtonColorU3Ed__21_tD9B8A0568D190D5F2BD60EEA784E0B908A3AE624_CustomAttributesCacheGenerator,
	U3CWaitForUpdateUiU3Ed__27_t36E7FB8307E80FED7AB34985D4632506BC952711_CustomAttributesCacheGenerator,
	U3CActiveCookie30sU3Ed__19_t0119DB09C6FEC13C8307690325CEE99855E3C782_CustomAttributesCacheGenerator,
	U3CActiveCandy30sU3Ed__21_t679D864AD14DE99063DD90DAE48F757780A913F7_CustomAttributesCacheGenerator,
	U3CWaitForPanelU3Ed__14_t21DF4D5D04488D4635896E34F3A4930EBF6331AA_CustomAttributesCacheGenerator,
	U3CWaitForUnPauseU3Ed__15_t7C2E4C470D88899BE03226E81A5D36543A1ACF77_CustomAttributesCacheGenerator,
	U3CU3Ec_t1CF2C7B5B1DCB59A7D5A83EFBB783E135CE83A3D_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass21_0_tA12769560FC967DD8C1AF06D6DB4CA81B85AAD67_CustomAttributesCacheGenerator,
	U3CStartU3Ed__4_t85DC3CED72C53244AC5D2467B8CB12EF944D3266_CustomAttributesCacheGenerator,
	U3CStartU3Ed__10_t4C97F7E5F1F2D654FF76C297F92CF423F472A640_CustomAttributesCacheGenerator,
	U3CStartU3Ed__10_t3D69371599D2A84DED6689D4FA6EA1720611EAD3_CustomAttributesCacheGenerator,
	U3CAnimatePropertiesU3Ed__6_t6EF9435006F3F335B99084BECD0ABFEFFEF4A32D_CustomAttributesCacheGenerator,
	U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B_CustomAttributesCacheGenerator,
	U3CStartU3Ed__4_t70319341791F5D1BC6F878503A507E845E98C44E_CustomAttributesCacheGenerator,
	U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E_CustomAttributesCacheGenerator,
	U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408_CustomAttributesCacheGenerator,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_tF7B0AC090E8E1BC19444DC46DEA61B2D608F7E20_CustomAttributesCacheGenerator,
	U3CDisplayTextMeshFloatingTextU3Ed__16_tACCF2F4E02674FBB7D0A61F908094C4EC0DAD2A2_CustomAttributesCacheGenerator,
	U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF_CustomAttributesCacheGenerator,
	U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741_CustomAttributesCacheGenerator,
	U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155_CustomAttributesCacheGenerator,
	U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass10_0_t1AC05F5499B93AC94E9727B8B0F08D8037AD52A6_CustomAttributesCacheGenerator,
	U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40_CustomAttributesCacheGenerator,
	U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76_CustomAttributesCacheGenerator,
	U3CU3Ec_tAA80530146653D50F0D63C9976285F15D0B03AC3_CustomAttributesCacheGenerator,
	U3Cget_ChildrenU3Ed__39_tB70A86AF5415D0A3C8B93D07712335F4FE14F44B_CustomAttributesCacheGenerator,
	U3Cget_DeepChildrenU3Ed__41_t1EFE91681671B35682051A9DF5D99D8012D7DDD7_CustomAttributesCacheGenerator,
	U3Cget_ChildrenU3Ed__22_t411188099DCF9174894F420F6390B7E43C48B481_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass21_0_t373BF0D6FE8FC7A0C471A9DE43255479FD1A6F0C_CustomAttributesCacheGenerator,
	U3Cget_ChildrenU3Ed__23_t213CCC2BA3B684AD1871D45A4278367199EFBCA8_CustomAttributesCacheGenerator,
	U3CDelayLaserPopUpU3Ed__28_tF0267396241BEFE79B23A545D65A2D3DFAE18C0D_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass7_0_t6FCA6AC5CD5682942D529BD81F5B10FF02C778D7_CustomAttributesCacheGenerator,
	U3CU3Ec_tFDDEA42123744E3A1DA3325ECA1A91BF01F8AECA_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass23_0_t468F43C7C71C22486353B27517D8AE977A794F00_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass24_0_t510EDD7DFCD45F4DBF040986CD6FD88A798E5310_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass25_0_tD7670C47745BE4B4934DF304248F9B138F3C1CBD_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass26_0_tDDEDBED6B1B9FF2B9A78B6C5BBFD82D7FD1F1277_CustomAttributesCacheGenerator,
	U3CWaitForNoticeTimerOutU3Ed__43_t872B5C580D4D7A86DDD89BA935867637E1EDF564_CustomAttributesCacheGenerator,
	U3CWaitForNoticeOutU3Ed__46_tB40B7C9E84E36CF0E86D5063901513398B490775_CustomAttributesCacheGenerator,
	U3CU3Ec__DisplayClass53_0_t9D3690D507A903EC111455BB3CA8F376D6B35138_CustomAttributesCacheGenerator,
	U3CNextEnemyU3Ed__26_t4F6CB5EE30FB1BC45025C47422CA0BA6F4CFBA05_CustomAttributesCacheGenerator,
	U3CNextHeroU3Ed__27_tA3FA225A9F245C060D7D1285EC0ED7F01B031AA0_CustomAttributesCacheGenerator,
	U3CDisableInfoU3Ed__33_t6EB1180E7648EE0413BB51BA7BFB27F30B17C577_CustomAttributesCacheGenerator,
	U3CFireWorkClickedU3Ed__34_t0A3C43FA8003F5D86FE232DD5D91E4F1EF7CFE24_CustomAttributesCacheGenerator,
	U3CCookieClickedU3Ed__35_tDA1263DFFDD0773D381B1D787122AEEABE86AF9C_CustomAttributesCacheGenerator,
	U3CCandyClickedU3Ed__36_tCA313AA216D873516B3C1A392A0302BBB97B86B0_CustomAttributesCacheGenerator,
	U3CScreenShotClickedU3Ed__37_t982CA263942B0C8A08EC834CA778F5356084D169_CustomAttributesCacheGenerator,
	U3CLeaderBoardClickedU3Ed__38_tBAE695B5560CCB49D1653EAD612C1CE08B42F361_CustomAttributesCacheGenerator,
	CardBluePrint_t6DA4D665AE5A5EC248F1607C55E32F6BFE3DAD82_CustomAttributesCacheGenerator_HeroName,
	ElementBluePrint_tA8826B6A1C64AD3C2DC8F8DF3A3C717010DDF870_CustomAttributesCacheGenerator_Fire6_Add_BestFireDamage,
	ElementBluePrint_tA8826B6A1C64AD3C2DC8F8DF3A3C717010DDF870_CustomAttributesCacheGenerator_Fire4_Add_MoreFireDamage,
	ElementBluePrint_tA8826B6A1C64AD3C2DC8F8DF3A3C717010DDF870_CustomAttributesCacheGenerator_Fire2_Add_FireDamage,
	ElementBluePrint_tA8826B6A1C64AD3C2DC8F8DF3A3C717010DDF870_CustomAttributesCacheGenerator_Water6_Add_Add_BestWaterDamage,
	ElementBluePrint_tA8826B6A1C64AD3C2DC8F8DF3A3C717010DDF870_CustomAttributesCacheGenerator_Water4_Add_MoreWaterDamage,
	ElementBluePrint_tA8826B6A1C64AD3C2DC8F8DF3A3C717010DDF870_CustomAttributesCacheGenerator_Water2_Add_WaterDamage,
	ElementBluePrint_tA8826B6A1C64AD3C2DC8F8DF3A3C717010DDF870_CustomAttributesCacheGenerator_Plant6_Add_BestPlantDamage,
	ElementBluePrint_tA8826B6A1C64AD3C2DC8F8DF3A3C717010DDF870_CustomAttributesCacheGenerator_Plant4_Add_MorePlantDamage,
	ElementBluePrint_tA8826B6A1C64AD3C2DC8F8DF3A3C717010DDF870_CustomAttributesCacheGenerator_Plant2_Add_PlantDamage,
	ElementBluePrint_tA8826B6A1C64AD3C2DC8F8DF3A3C717010DDF870_CustomAttributesCacheGenerator_LoseElementDamage,
	SkillBluePrint_t3CAC732F339E5C98B3239E3F4FA51B88B8C0F46C_CustomAttributesCacheGenerator_name,
	SkillBluePrint_t3CAC732F339E5C98B3239E3F4FA51B88B8C0F46C_CustomAttributesCacheGenerator_HeroRequirements,
	SkillBluePrint_t3CAC732F339E5C98B3239E3F4FA51B88B8C0F46C_CustomAttributesCacheGenerator_AttackDamageBuff,
	SkillBluePrint_t3CAC732F339E5C98B3239E3F4FA51B88B8C0F46C_CustomAttributesCacheGenerator_FireDamageBuff,
	SkillBluePrint_t3CAC732F339E5C98B3239E3F4FA51B88B8C0F46C_CustomAttributesCacheGenerator_IsForMachine_Gun,
	SkillBluePrint_t3CAC732F339E5C98B3239E3F4FA51B88B8C0F46C_CustomAttributesCacheGenerator_IsActive,
	CinemachineSwitcher_t8D51217EA2F92830409525BC6939FBF5BCF7E445_CustomAttributesCacheGenerator_U3CInstanceU3Ek__BackingField,
	CinemachineSwitcher_t8D51217EA2F92830409525BC6939FBF5BCF7E445_CustomAttributesCacheGenerator_canvas,
	InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A_CustomAttributesCacheGenerator_U3CInstanceU3Ek__BackingField,
	InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A_CustomAttributesCacheGenerator_OnStartTouch,
	InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A_CustomAttributesCacheGenerator_OnEndTouch,
	MobileInput_t9563AE9C626D49165E67BDAF4BD0356FA1C96F0F_CustomAttributesCacheGenerator_U3CassetU3Ek__BackingField,
	SwipeDetection_t145E193968BD5765E38B632D928348C9740796C2_CustomAttributesCacheGenerator_minimumDistance,
	SwipeDetection_t145E193968BD5765E38B632D928348C9740796C2_CustomAttributesCacheGenerator_maximumTime,
	SwipeDetection_t145E193968BD5765E38B632D928348C9740796C2_CustomAttributesCacheGenerator_directionThreshold,
	LongClickButton_t31F3E8BB2ECFE3CD6585DC84FE8F0E4F624B5402_CustomAttributesCacheGenerator_holdTime,
	LongClickButton_t31F3E8BB2ECFE3CD6585DC84FE8F0E4F624B5402_CustomAttributesCacheGenerator_fillImg,
	EnemyWayPoint_tF3382B5960D751B735AE89323A5B1E25D2AD2901_CustomAttributesCacheGenerator_U3CWayPointsU3Ek__BackingField,
	EnemyWayPoint_tF3382B5960D751B735AE89323A5B1E25D2AD2901_CustomAttributesCacheGenerator_U3CInstanceU3Ek__BackingField,
	FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_PlayAsGuestBuuton,
	FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_logoutButton,
	FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_connectIdButton,
	FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_PlayerName,
	FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_NotificationTextTitel,
	FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_Input_Url,
	FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_rankPlayer,
	FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_rankPanel,
	FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_leaderboardPanel,
	FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_rankDataPrefab,
	FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_photoLeaderBoard,
	FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_waveLeaderBoard,
	RankData_tFBD8F1297575E2FBA581C146C7D9926339021105_CustomAttributesCacheGenerator_NumberText,
	RankManager_t046274A614E123E3C455190866CE2B98C88B87F0_CustomAttributesCacheGenerator_waveSurvived,
	RankManager_t046274A614E123E3C455190866CE2B98C88B87F0_CustomAttributesCacheGenerator_rankPlayer,
	RankManager_t046274A614E123E3C455190866CE2B98C88B87F0_CustomAttributesCacheGenerator_playerImage,
	AnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709_CustomAttributesCacheGenerator_U3CInstanceU3Ek__BackingField,
	AnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709_CustomAttributesCacheGenerator_mapField,
	AnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709_CustomAttributesCacheGenerator_itemFirework,
	AnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709_CustomAttributesCacheGenerator_itemCookie,
	AnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709_CustomAttributesCacheGenerator_itemCandy,
	AnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709_CustomAttributesCacheGenerator_screenShot,
	AnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709_CustomAttributesCacheGenerator_leaderBoard,
	AnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709_CustomAttributesCacheGenerator_noticeTimer,
	AnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709_CustomAttributesCacheGenerator_notice,
	HeroUI_t08B1AAECF81A540C2A88B585759B9A05188B2373_CustomAttributesCacheGenerator_heroUI,
	HeroUI_t08B1AAECF81A540C2A88B585759B9A05188B2373_CustomAttributesCacheGenerator_sellCostText,
	HeroUI_t08B1AAECF81A540C2A88B585759B9A05188B2373_CustomAttributesCacheGenerator_heroNameText,
	HeroUI_t08B1AAECF81A540C2A88B585759B9A05188B2373_CustomAttributesCacheGenerator_subHeroNameText,
	HeroUI_t08B1AAECF81A540C2A88B585759B9A05188B2373_CustomAttributesCacheGenerator_evoCostText,
	HeroUI_t08B1AAECF81A540C2A88B585759B9A05188B2373_CustomAttributesCacheGenerator_upgradeButtonText,
	HeroUI_t08B1AAECF81A540C2A88B585759B9A05188B2373_CustomAttributesCacheGenerator_levelPanel,
	HeroUI_t08B1AAECF81A540C2A88B585759B9A05188B2373_CustomAttributesCacheGenerator_upgradeSlider,
	HeroUI_t08B1AAECF81A540C2A88B585759B9A05188B2373_CustomAttributesCacheGenerator_upgradeCostText,
	HeroUI_t08B1AAECF81A540C2A88B585759B9A05188B2373_CustomAttributesCacheGenerator_levelText,
	HeroUI_t08B1AAECF81A540C2A88B585759B9A05188B2373_CustomAttributesCacheGenerator_heroLevelText,
	HeroUI_t08B1AAECF81A540C2A88B585759B9A05188B2373_CustomAttributesCacheGenerator_upgradeButton,
	HeroUI_t08B1AAECF81A540C2A88B585759B9A05188B2373_CustomAttributesCacheGenerator_evoButton,
	HeroUI_t08B1AAECF81A540C2A88B585759B9A05188B2373_CustomAttributesCacheGenerator_attackRangeImg,
	HeroUI_t08B1AAECF81A540C2A88B585759B9A05188B2373_CustomAttributesCacheGenerator_evoEffect,
	ItemManager_t050717EEAFEA02D5E67D1509322618C59CBAE94A_CustomAttributesCacheGenerator_U3CInstanceU3Ek__BackingField,
	ItemManager_t050717EEAFEA02D5E67D1509322618C59CBAE94A_CustomAttributesCacheGenerator_OnCandyUsed,
	ItemManager_t050717EEAFEA02D5E67D1509322618C59CBAE94A_CustomAttributesCacheGenerator_OnCookieUsed,
	ItemManager_t050717EEAFEA02D5E67D1509322618C59CBAE94A_CustomAttributesCacheGenerator_damageTemp,
	ItemManager_t050717EEAFEA02D5E67D1509322618C59CBAE94A_CustomAttributesCacheGenerator_speedTemp,
	ItemManager_t050717EEAFEA02D5E67D1509322618C59CBAE94A_CustomAttributesCacheGenerator_firework,
	MainMenuManager_t7A113A7F5C26F3BD9FB68E3D52C1221E8D8386EE_CustomAttributesCacheGenerator_UploadPanel,
	TimeManager_t7C9EC9E1F31A3BAAE33B119C0B91347D37ABAF82_CustomAttributesCacheGenerator_PausetTimeScale,
	TimeManager_t7C9EC9E1F31A3BAAE33B119C0B91347D37ABAF82_CustomAttributesCacheGenerator_defaultTimeScale,
	TimeManager_t7C9EC9E1F31A3BAAE33B119C0B91347D37ABAF82_CustomAttributesCacheGenerator_U3CInstanceU3Ek__BackingField,
	MapGenerate_tA7712825E563ECC0B019CEBE5DF4C4CC7941593D_CustomAttributesCacheGenerator_mapWidth,
	MapGenerate_tA7712825E563ECC0B019CEBE5DF4C4CC7941593D_CustomAttributesCacheGenerator_mapHeight,
	ChainSkill_t389CC8B529E3C83A6741CA8AC63DF33C553D8927_CustomAttributesCacheGenerator_U3CInstanceU3Ek__BackingField,
	DropdownSample_t925F66C7435D75374A0E26DE15B4F1D95697F0DF_CustomAttributesCacheGenerator_text,
	DropdownSample_t925F66C7435D75374A0E26DE15B4F1D95697F0DF_CustomAttributesCacheGenerator_dropdownWithoutPlaceholder,
	DropdownSample_t925F66C7435D75374A0E26DE15B4F1D95697F0DF_CustomAttributesCacheGenerator_dropdownWithPlaceholder,
	TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553_CustomAttributesCacheGenerator_m_OnCharacterSelection,
	TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553_CustomAttributesCacheGenerator_m_OnSpriteSelection,
	TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553_CustomAttributesCacheGenerator_m_OnWordSelection,
	TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553_CustomAttributesCacheGenerator_m_OnLineSelection,
	TMP_TextEventHandler_t46E96E25C3C3ABB2F300593BAECE9FB9E3B10553_CustomAttributesCacheGenerator_m_OnLinkSelection,
	Area_t7013267738E4CA59CA15A2E450701A99F6A70197_CustomAttributesCacheGenerator_U3CCurrentHeroAmountU3Ek__BackingField,
	Area_t7013267738E4CA59CA15A2E450701A99F6A70197_CustomAttributesCacheGenerator_U3CCurrentSubHeroAmountU3Ek__BackingField,
	Area_t7013267738E4CA59CA15A2E450701A99F6A70197_CustomAttributesCacheGenerator_hero,
	Area_t7013267738E4CA59CA15A2E450701A99F6A70197_CustomAttributesCacheGenerator_HeroBluePrint,
	JSONNode_tDEACFC45BBAB7B7189263CF1BA7D4B7FE179132A_CustomAttributesCacheGenerator_m_EscapeBuilder,
	Hero_t477360619EB702BB183DA8CA44857619B9AA12FC_CustomAttributesCacheGenerator_heroRotatePoint,
	Hero_t477360619EB702BB183DA8CA44857619B9AA12FC_CustomAttributesCacheGenerator_heroTurnSpeed,
	Hero_t477360619EB702BB183DA8CA44857619B9AA12FC_CustomAttributesCacheGenerator_enemyTag,
	Hero_t477360619EB702BB183DA8CA44857619B9AA12FC_CustomAttributesCacheGenerator_UseLaser,
	Hero_t477360619EB702BB183DA8CA44857619B9AA12FC_CustomAttributesCacheGenerator_bulletPrefab,
	Hero_t477360619EB702BB183DA8CA44857619B9AA12FC_CustomAttributesCacheGenerator_hero,
	Hero_t477360619EB702BB183DA8CA44857619B9AA12FC_CustomAttributesCacheGenerator_cookieAura,
	BaseEnemy_tCCDDF8C04729B5368581508D806525594CB0BC9F_CustomAttributesCacheGenerator_U3CEnemySpeedU3Ek__BackingField,
	BaseEnemy_tCCDDF8C04729B5368581508D806525594CB0BC9F_CustomAttributesCacheGenerator_U3CEnemyHpU3Ek__BackingField,
	BaseEnemy_tCCDDF8C04729B5368581508D806525594CB0BC9F_CustomAttributesCacheGenerator_U3CGoldDropU3Ek__BackingField,
	BaseEnemy_tCCDDF8C04729B5368581508D806525594CB0BC9F_CustomAttributesCacheGenerator_U3CEnemyElementU3Ek__BackingField,
	BaseEnemy_tCCDDF8C04729B5368581508D806525594CB0BC9F_CustomAttributesCacheGenerator_healthBar,
	BaseEnemy_tCCDDF8C04729B5368581508D806525594CB0BC9F_CustomAttributesCacheGenerator_popUpDamageText,
	BaseEnemy_tCCDDF8C04729B5368581508D806525594CB0BC9F_CustomAttributesCacheGenerator_rotateCanvas,
	BaseBullet_tFAE58989A37F312A7E63097449CBC236A34B540D_CustomAttributesCacheGenerator_hitEffect,
	HeroBluePrint_t0FEB6738C704A98AD3A68AC443A82C03B0429098_CustomAttributesCacheGenerator_Name,
	HeroBluePrint_t0FEB6738C704A98AD3A68AC443A82C03B0429098_CustomAttributesCacheGenerator_HeroName,
	HeroBluePrint_t0FEB6738C704A98AD3A68AC443A82C03B0429098_CustomAttributesCacheGenerator_AttackType,
	HeroBluePrint_t0FEB6738C704A98AD3A68AC443A82C03B0429098_CustomAttributesCacheGenerator_Prefab,
	HeroBluePrint_t0FEB6738C704A98AD3A68AC443A82C03B0429098_CustomAttributesCacheGenerator_ElementType,
	HeroBluePrint_t0FEB6738C704A98AD3A68AC443A82C03B0429098_CustomAttributesCacheGenerator_IsHero,
	HeroBluePrint_t0FEB6738C704A98AD3A68AC443A82C03B0429098_CustomAttributesCacheGenerator_IsBuilt,
	HeroBluePrint_t0FEB6738C704A98AD3A68AC443A82C03B0429098_CustomAttributesCacheGenerator_IsEvo,
	HeroBluePrint_t0FEB6738C704A98AD3A68AC443A82C03B0429098_CustomAttributesCacheGenerator_IsPreEvo,
	HeroBluePrint_t0FEB6738C704A98AD3A68AC443A82C03B0429098_CustomAttributesCacheGenerator_DamageOverTime,
	HeroBluePrint_t0FEB6738C704A98AD3A68AC443A82C03B0429098_CustomAttributesCacheGenerator_BulletSpeed,
	HeroBluePrint_t0FEB6738C704A98AD3A68AC443A82C03B0429098_CustomAttributesCacheGenerator_Level,
	HeroBluePrint_t0FEB6738C704A98AD3A68AC443A82C03B0429098_CustomAttributesCacheGenerator_HeroCostAmount,
	BuildManager_tC38D9095FCAD1EDD6498011089D3EE2043101750_CustomAttributesCacheGenerator_U3CInstanceU3Ek__BackingField,
	BuildManager_tC38D9095FCAD1EDD6498011089D3EE2043101750_CustomAttributesCacheGenerator_HeroLimit,
	BuildManager_tC38D9095FCAD1EDD6498011089D3EE2043101750_CustomAttributesCacheGenerator_heroUI,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_U3CInstanceU3Ek__BackingField,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_GameOver,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_U3CEnemyCountU3Ek__BackingField,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_countdownGameOver,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_upgradeEnemyIn5Wave,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_UpgradeAttackDamagePreEvo,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_colubted_Name,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_colubted_MoveSpeed,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_colubted_Health,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_colubted_GoldDrop,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_colubted_EnemyElement,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_colubted_SpawnTime,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_colubtedPrefab,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_embio_Name,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_embio_MoveSpeed,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_embio_Health,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_embio_GoldDrop,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_embio_EnemyElement,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_embio_SpawnTime,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_embioPrefab,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_eclipseside_Name,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_eclipseside_MoveSpeed,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_eclipseside_Health,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_eclipseside_GoldDrop,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_eclipseside_EnemyElement,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_eclipseside_SpawnTime,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_eclipsesidePrefab,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_crossDive_Name,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_crossDive_MoveSpeed,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_crossDive_Health,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_crossDive_GoldDrop,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_crossDive_EnemyElement,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_crossDive_SpawnTime,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_crossDivePrefab,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_shiro_Name,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_shiro_MoveSpeed,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_shiro_Health,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_shiro_GoldDrop,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_shiro_EnemyElement,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_shiro_SpawnTime,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_shiroPrefab,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_kana_Name,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_kana_MoveSpeed,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_kana_Health,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_kana_GoldDrop,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_kana_EnemyElement,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_kana_SpawnTime,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_kanaPrefab,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_chest_Name,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_chest_MoveSpeed,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_chest_Health,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_chest_GoldDrop,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_chest_EnemyElement,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_chest_SpawnTime,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_chestPrefab,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_enemyUpgrade_HP,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_enemyUpgrade_Gold,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_enemyUpgrade_Speed,
	InventoryManager_tD1BFBF734B63F17BD4E44640A9B2DFB3FE2EF742_CustomAttributesCacheGenerator_AtkDamage,
	InventoryManager_tD1BFBF734B63F17BD4E44640A9B2DFB3FE2EF742_CustomAttributesCacheGenerator_HeroTeam,
	InventoryManager_tD1BFBF734B63F17BD4E44640A9B2DFB3FE2EF742_CustomAttributesCacheGenerator_Cards,
	InventoryManager_tD1BFBF734B63F17BD4E44640A9B2DFB3FE2EF742_CustomAttributesCacheGenerator_CardsInventory,
	InventoryManager_tD1BFBF734B63F17BD4E44640A9B2DFB3FE2EF742_CustomAttributesCacheGenerator_inventoryPanel,
	InventoryManager_tD1BFBF734B63F17BD4E44640A9B2DFB3FE2EF742_CustomAttributesCacheGenerator_usedCardField,
	InventoryManager_tD1BFBF734B63F17BD4E44640A9B2DFB3FE2EF742_CustomAttributesCacheGenerator_UsedCardTemp,
	InventoryManager_tD1BFBF734B63F17BD4E44640A9B2DFB3FE2EF742_CustomAttributesCacheGenerator_IsBuildMode,
	InventoryManager_tD1BFBF734B63F17BD4E44640A9B2DFB3FE2EF742_CustomAttributesCacheGenerator_U3CInstanceU3Ek__BackingField,
	MoneyManager_tC109A492276276C2C8B6EC463FB501014D6859FE_CustomAttributesCacheGenerator_U3CInstanceU3Ek__BackingField,
	MoneyManager_tC109A492276276C2C8B6EC463FB501014D6859FE_CustomAttributesCacheGenerator_U3CMoneyU3Ek__BackingField,
	MoneyManager_tC109A492276276C2C8B6EC463FB501014D6859FE_CustomAttributesCacheGenerator_startMoney,
	RandomScrollManager_t46FEAC1380C11393456FD90E799CF376B89253E1_CustomAttributesCacheGenerator_U3CInstanceU3Ek__BackingField,
	RandomScrollManager_t46FEAC1380C11393456FD90E799CF376B89253E1_CustomAttributesCacheGenerator_heroPrefabs,
	RandomScrollManager_t46FEAC1380C11393456FD90E799CF376B89253E1_CustomAttributesCacheGenerator_subHeroPrefabs,
	RandomScrollManager_t46FEAC1380C11393456FD90E799CF376B89253E1_CustomAttributesCacheGenerator_heroRandomRate,
	RandomScrollManager_t46FEAC1380C11393456FD90E799CF376B89253E1_CustomAttributesCacheGenerator_subHeroRandomRate,
	RandomScrollManager_t46FEAC1380C11393456FD90E799CF376B89253E1_CustomAttributesCacheGenerator_heroRandomPlusRate,
	RandomScrollManager_t46FEAC1380C11393456FD90E799CF376B89253E1_CustomAttributesCacheGenerator_subHeroRandomPlusRate,
	RandomScrollManager_t46FEAC1380C11393456FD90E799CF376B89253E1_CustomAttributesCacheGenerator_randomField,
	RandomScrollManager_t46FEAC1380C11393456FD90E799CF376B89253E1_CustomAttributesCacheGenerator_randomArea,
	RandomScrollManager_t46FEAC1380C11393456FD90E799CF376B89253E1_CustomAttributesCacheGenerator_maxCard,
	RandomScrollManager_t46FEAC1380C11393456FD90E799CF376B89253E1_CustomAttributesCacheGenerator_cards,
	RandomScrollManager_t46FEAC1380C11393456FD90E799CF376B89253E1_CustomAttributesCacheGenerator_OnFirstRandomCard,
	RandomScrollManager_t46FEAC1380C11393456FD90E799CF376B89253E1_CustomAttributesCacheGenerator_LongClickCheck,
	RandomScrollManager_t46FEAC1380C11393456FD90E799CF376B89253E1_CustomAttributesCacheGenerator_CardTemp,
	RandomScrollManager_t46FEAC1380C11393456FD90E799CF376B89253E1_CustomAttributesCacheGenerator_AddCard,
	UiManager_tBCDA10D96B04DE5423A278745391F4F80A54A66A_CustomAttributesCacheGenerator_U3CInstanceU3Ek__BackingField,
	UiManager_tBCDA10D96B04DE5423A278745391F4F80A54A66A_CustomAttributesCacheGenerator_RandomScrollButtonArea,
	UiManager_tBCDA10D96B04DE5423A278745391F4F80A54A66A_CustomAttributesCacheGenerator_reRandomButton,
	UiManager_tBCDA10D96B04DE5423A278745391F4F80A54A66A_CustomAttributesCacheGenerator_closeRandomScrollButton,
	UiManager_tBCDA10D96B04DE5423A278745391F4F80A54A66A_CustomAttributesCacheGenerator_money,
	UiManager_tBCDA10D96B04DE5423A278745391F4F80A54A66A_CustomAttributesCacheGenerator_timeCountBeforeOver,
	UiManager_tBCDA10D96B04DE5423A278745391F4F80A54A66A_CustomAttributesCacheGenerator_gameOver,
	UiManager_tBCDA10D96B04DE5423A278745391F4F80A54A66A_CustomAttributesCacheGenerator_textNotification,
	UiManager_tBCDA10D96B04DE5423A278745391F4F80A54A66A_CustomAttributesCacheGenerator_skillChainArea,
	UiManager_tBCDA10D96B04DE5423A278745391F4F80A54A66A_CustomAttributesCacheGenerator_skillChainText,
	UiManager_tBCDA10D96B04DE5423A278745391F4F80A54A66A_CustomAttributesCacheGenerator_skillChainsList,
	UiManager_tBCDA10D96B04DE5423A278745391F4F80A54A66A_CustomAttributesCacheGenerator_elementChainArea,
	UiManager_tBCDA10D96B04DE5423A278745391F4F80A54A66A_CustomAttributesCacheGenerator_elementChainsImg,
	UiManager_tBCDA10D96B04DE5423A278745391F4F80A54A66A_CustomAttributesCacheGenerator_blockUI,
	UiManager_tBCDA10D96B04DE5423A278745391F4F80A54A66A_CustomAttributesCacheGenerator_cardAmountText,
	UiManager_tBCDA10D96B04DE5423A278745391F4F80A54A66A_CustomAttributesCacheGenerator_timeSurvived,
	UiManager_tBCDA10D96B04DE5423A278745391F4F80A54A66A_CustomAttributesCacheGenerator_waveSurvived,
	UiManager_tBCDA10D96B04DE5423A278745391F4F80A54A66A_CustomAttributesCacheGenerator_damageDone,
	UiManager_tBCDA10D96B04DE5423A278745391F4F80A54A66A_CustomAttributesCacheGenerator_compUsed,
	UiManager_tBCDA10D96B04DE5423A278745391F4F80A54A66A_CustomAttributesCacheGenerator_AdsRewardButton,
	UiManager_tBCDA10D96B04DE5423A278745391F4F80A54A66A_CustomAttributesCacheGenerator_DamageDoneGameOver,
	UiManager_tBCDA10D96B04DE5423A278745391F4F80A54A66A_CustomAttributesCacheGenerator_IsRandomPlus,
	WaveManager_t712FD5213A90156B815DC5E3AEDBD86017E3CE8B_CustomAttributesCacheGenerator_U3CInstanceU3Ek__BackingField,
	WaveManager_t712FD5213A90156B815DC5E3AEDBD86017E3CE8B_CustomAttributesCacheGenerator_timePerWaves,
	WaveManager_t712FD5213A90156B815DC5E3AEDBD86017E3CE8B_CustomAttributesCacheGenerator_timeCooldownWaves,
	WaveManager_t712FD5213A90156B815DC5E3AEDBD86017E3CE8B_CustomAttributesCacheGenerator_timer,
	WaveManager_t712FD5213A90156B815DC5E3AEDBD86017E3CE8B_CustomAttributesCacheGenerator_OnNextWave,
	WaveManager_t712FD5213A90156B815DC5E3AEDBD86017E3CE8B_CustomAttributesCacheGenerator_RandomEnemy,
	WorldUIManager_t8F19F2357842FE0470C8C0E492D3FE318B67ED2F_CustomAttributesCacheGenerator_U3CInstanceU3Ek__BackingField,
	WorldUIManager_t8F19F2357842FE0470C8C0E492D3FE318B67ED2F_CustomAttributesCacheGenerator_infoPanel,
	WorldUIManager_t8F19F2357842FE0470C8C0E492D3FE318B67ED2F_CustomAttributesCacheGenerator_infoText,
	WorldUIManager_t8F19F2357842FE0470C8C0E492D3FE318B67ED2F_CustomAttributesCacheGenerator_enemyInfoField,
	WorldUIManager_t8F19F2357842FE0470C8C0E492D3FE318B67ED2F_CustomAttributesCacheGenerator_enemyInfoText,
	WorldUIManager_t8F19F2357842FE0470C8C0E492D3FE318B67ED2F_CustomAttributesCacheGenerator_heroInfoField,
	WorldUIManager_t8F19F2357842FE0470C8C0E492D3FE318B67ED2F_CustomAttributesCacheGenerator_heroInfoText,
	WorldUIManager_t8F19F2357842FE0470C8C0E492D3FE318B67ED2F_CustomAttributesCacheGenerator_showScreenShotImage,
	CinemachineSwitcher_t8D51217EA2F92830409525BC6939FBF5BCF7E445_CustomAttributesCacheGenerator_CinemachineSwitcher_get_Instance_mF6ABBBE7B4E38636BEB09D7ADF3940BA80DC808B,
	CinemachineSwitcher_t8D51217EA2F92830409525BC6939FBF5BCF7E445_CustomAttributesCacheGenerator_CinemachineSwitcher_set_Instance_mC4980BCB3320D1E7B59956E65C60DCA174B641EA,
	CinemachineSwitcher_t8D51217EA2F92830409525BC6939FBF5BCF7E445_CustomAttributesCacheGenerator_CinemachineSwitcher_WaitToStart_mFB3F4280F74BD988595B53EE4F855CCDBA6708F1,
	CinemachineSwitcher_t8D51217EA2F92830409525BC6939FBF5BCF7E445_CustomAttributesCacheGenerator_CinemachineSwitcher_WaitForCam_m20F16C627FA98D38C064729772CE31961D3735AE,
	CinemachineSwitcher_t8D51217EA2F92830409525BC6939FBF5BCF7E445_CustomAttributesCacheGenerator_CinemachineSwitcher_WaitForShowCanvas_m66CF0EE7E5A38923F80DA4040DD48571364BE4AD,
	InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A_CustomAttributesCacheGenerator_InputManager_get_Instance_mC5B4EA7BB8A909DB0FDD5D9CBF67B3A1CC40181D,
	InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A_CustomAttributesCacheGenerator_InputManager_set_Instance_mB94B3A652E1A869CBB1FFBBE8CA25842E26A3C34,
	InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A_CustomAttributesCacheGenerator_InputManager_add_OnStartTouch_mCF106264B78FB07537E71524C99EE140805FF25D,
	InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A_CustomAttributesCacheGenerator_InputManager_remove_OnStartTouch_m24C6AC368EC9AA24B40A5D1F3EF7B07A72766C7D,
	InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A_CustomAttributesCacheGenerator_InputManager_add_OnEndTouch_m50D940EFE21F790A04BCF54CE88FDF55F953CDFF,
	InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A_CustomAttributesCacheGenerator_InputManager_remove_OnEndTouch_m3B6F0D7B26BC509080CA8F30E8D80E0FCA5423D2,
	InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A_CustomAttributesCacheGenerator_InputManager_U3CStartU3Eb__17_0_m6D32B313D99F02F691422CF3763619364E7A8936,
	InputManager_tA87D02E2EAE74F98ECCF2AF13C75554B63A1F89A_CustomAttributesCacheGenerator_InputManager_U3CStartU3Eb__17_1_m3F85443FB5E717AC691727FFF4C91444705B47AD,
	MobileInput_t9563AE9C626D49165E67BDAF4BD0356FA1C96F0F_CustomAttributesCacheGenerator_MobileInput_get_asset_m8065F2CC02A1444F16ADA2794E8071D867C46888,
	ScreenCapture_t7F39027079C42E816BE74855BED21EDD4C6B940E_CustomAttributesCacheGenerator_ScreenCapture_ImageShowCase_m7C1802B6832D044746834F97B1D7D55505D707AA,
	LongClickButton_t31F3E8BB2ECFE3CD6585DC84FE8F0E4F624B5402_CustomAttributesCacheGenerator_LongClickButton_DelayClick_m57566FE8544386D752B832895C7A3C424E60F152,
	EnemyWayPoint_tF3382B5960D751B735AE89323A5B1E25D2AD2901_CustomAttributesCacheGenerator_EnemyWayPoint_get_WayPoints_mA9B0EC26719F8C9A85CE11979FA70ACAA767FC8D,
	EnemyWayPoint_tF3382B5960D751B735AE89323A5B1E25D2AD2901_CustomAttributesCacheGenerator_EnemyWayPoint_set_WayPoints_m29C19EC458381493C0C989548754E56303BB6B6F,
	EnemyWayPoint_tF3382B5960D751B735AE89323A5B1E25D2AD2901_CustomAttributesCacheGenerator_EnemyWayPoint_get_Instance_m9E8A4D26FF4B696112549EDA6B14E2A419990393,
	EnemyWayPoint_tF3382B5960D751B735AE89323A5B1E25D2AD2901_CustomAttributesCacheGenerator_EnemyWayPoint_set_Instance_m7FCF1F5029861DE0A65F2271EB287F6CC3D0C4F6,
	FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_FirebaseManager_CheckAlreadyName_m25B27E898AA4C9F8DA81920ED46EE6B052C2DDE5,
	FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_FirebaseManager_CloseNotification_m909268520C1AA67BBB1CC394D2A6BC6E63520F43,
	FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_FirebaseManager_CheckAlreadyNameIdTransfer_m5BFE3B6936231D494314BE845F42A5AF4AC949FB,
	FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_FirebaseManager_LoadUserScoreFromDB_mD894FC790D4E61DCE6BA00C7CD7FDDAA39CD1E36,
	FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_FirebaseManager_DownloadImage_m7181991C7853AADCE4D73A9D254FD97D898B378A,
	FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_FirebaseManager_LoadLeaderBoardData_m06C184117B561767D286D1B7BD89173A9A4AB3CD,
	FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_FirebaseManager_U3CStartU3Eb__48_0_m36581C2AEC12E1C90BB72B87048D131734928B3C,
	FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_FirebaseManager_U3CSignInUserU3Eb__55_0_mC0D0D74A9B0AC51CEA0C4944D148B7D608C0EDA1,
	FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_FirebaseManager_U3CSendPasswordResetEmailU3Eb__59_0_m91D2331DFDE02DA0307FB03795621C211246F4A2,
	FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_FirebaseManager_U3CDeleteUserU3Eb__67_0_mBFE0C01FAAF9354F30490CB854BF25624839A5D9,
	FirebaseManager_t39C3187EFE2EC3C3B043AD46BA4802463E5EEC7E_CustomAttributesCacheGenerator_FirebaseManager_U3CPlayAsGuestU3Eb__71_0_m0D1455F6948BC1B608F5C3150AEEFD920DE04982,
	RankData_tFBD8F1297575E2FBA581C146C7D9926339021105_CustomAttributesCacheGenerator_RankData_DownloadImage_mB3634704E17275CFFC430EA174C96A48CDFCA196,
	RankManager_t046274A614E123E3C455190866CE2B98C88B87F0_CustomAttributesCacheGenerator_RankManager_CheckDamageOverCurrentScore_mCB8486642C343A640FA9437DE98B3B4D87616034,
	RankManager_t046274A614E123E3C455190866CE2B98C88B87F0_CustomAttributesCacheGenerator_RankManager_CheckTimeSurvivedOverCurrentScore_m04A388299D839D31AAD14DF4E736989BDB46EFA0,
	RankManager_t046274A614E123E3C455190866CE2B98C88B87F0_CustomAttributesCacheGenerator_RankManager_UpdateDamageDone_mD0D78C65C914F189377894AE8F83E0788D6B2683,
	RankManager_t046274A614E123E3C455190866CE2B98C88B87F0_CustomAttributesCacheGenerator_RankManager_UpdateWaveSurvived_mF9400B91A4798D5BBE0AFCF5267A32977115979C,
	RankManager_t046274A614E123E3C455190866CE2B98C88B87F0_CustomAttributesCacheGenerator_RankManager_UpdateTimeSurvived_m6B3EFE193E94C4FBCCA5FA99EF20AE3D7417235F,
	RankManager_t046274A614E123E3C455190866CE2B98C88B87F0_CustomAttributesCacheGenerator_RankManager_LoadUserScoreFromDB_m0C1F32AE08783D1EFE74247850F9423A856FA627,
	RankManager_t046274A614E123E3C455190866CE2B98C88B87F0_CustomAttributesCacheGenerator_RankManager_LoadLeaderBoardData_mEA3E6CB77B24A2D858BF2423FCB7DCD52FC032B6,
	RankManager_t046274A614E123E3C455190866CE2B98C88B87F0_CustomAttributesCacheGenerator_RankManager_DownloadImage_m9D8910B3E34BE8AEB8BDC0DD2E91135FF024FED7,
	AnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709_CustomAttributesCacheGenerator_AnimationManager_get_Instance_m6006600F80794DD76BB6AC3235F42B08C76DFC21,
	AnimationManager_tB16546D4D71E0076AC824FC79FAD6FE732D88709_CustomAttributesCacheGenerator_AnimationManager_set_Instance_m129B4A95F6E1A05D296C65F5FF4654A34E191C1A,
	HeroUI_t08B1AAECF81A540C2A88B585759B9A05188B2373_CustomAttributesCacheGenerator_HeroUI_ChangeButtonColor_m57C3A34E9EFD62ECB84BB942B1369D742BD003B3,
	HeroUI_t08B1AAECF81A540C2A88B585759B9A05188B2373_CustomAttributesCacheGenerator_HeroUI_WaitForUpdateUi_mB163302A7AC7999CDB49AD6C3F1E8D1891C6F1A4,
	HeroUI_t08B1AAECF81A540C2A88B585759B9A05188B2373_CustomAttributesCacheGenerator_HeroUI_U3CSetTargetU3Eb__19_0_m1F49EFF4785EA6875D6F430195C5647A22F490D4,
	HeroUI_t08B1AAECF81A540C2A88B585759B9A05188B2373_CustomAttributesCacheGenerator_HeroUI_U3CEvoHeroU3Eb__26_0_m890A8A5597D9EAAC6DAF8ED08FE6C8F3DF37798B,
	ItemManager_t050717EEAFEA02D5E67D1509322618C59CBAE94A_CustomAttributesCacheGenerator_ItemManager_get_Instance_mC07F08CD08726E948EF32F708CA18982076190B6,
	ItemManager_t050717EEAFEA02D5E67D1509322618C59CBAE94A_CustomAttributesCacheGenerator_ItemManager_set_Instance_m91F8EBA1EDB2F3C0C38B96B94567147A357531FC,
	ItemManager_t050717EEAFEA02D5E67D1509322618C59CBAE94A_CustomAttributesCacheGenerator_ItemManager_add_OnCandyUsed_mE11A67279CF917C592370DC3E5F19F43624F4B82,
	ItemManager_t050717EEAFEA02D5E67D1509322618C59CBAE94A_CustomAttributesCacheGenerator_ItemManager_remove_OnCandyUsed_m095FF8D6FEB3DE6137B118DB77A0E92606FFEF72,
	ItemManager_t050717EEAFEA02D5E67D1509322618C59CBAE94A_CustomAttributesCacheGenerator_ItemManager_add_OnCookieUsed_m28C6881C57300A2FB437BFF027FF42500973E98E,
	ItemManager_t050717EEAFEA02D5E67D1509322618C59CBAE94A_CustomAttributesCacheGenerator_ItemManager_remove_OnCookieUsed_mAC2B81471A8D57878164BE8A6905EB88B9AC9CF3,
	ItemManager_t050717EEAFEA02D5E67D1509322618C59CBAE94A_CustomAttributesCacheGenerator_ItemManager_ActiveCookie30s_m2159094A265F281AD920E8F646A10FF9F2B429A3,
	ItemManager_t050717EEAFEA02D5E67D1509322618C59CBAE94A_CustomAttributesCacheGenerator_ItemManager_ActiveCandy30s_mD5662C359943A8F22218D041056106C2FAFB2AF6,
	TimeManager_t7C9EC9E1F31A3BAAE33B119C0B91347D37ABAF82_CustomAttributesCacheGenerator_TimeManager_get_Instance_m19259A61D1B020A332EC31181A9E667EF907151A,
	TimeManager_t7C9EC9E1F31A3BAAE33B119C0B91347D37ABAF82_CustomAttributesCacheGenerator_TimeManager_set_Instance_mB699CF404EFA8C02A254633270C30C6E0EA0EFF6,
	TimeManager_t7C9EC9E1F31A3BAAE33B119C0B91347D37ABAF82_CustomAttributesCacheGenerator_TimeManager_WaitForPanel_m68280F25263044850C9A68C113FDEC174C3D7625,
	TimeManager_t7C9EC9E1F31A3BAAE33B119C0B91347D37ABAF82_CustomAttributesCacheGenerator_TimeManager_WaitForUnPause_mB05F71F812DAD1687CFF92931741779EA69EB465,
	ChainSkill_t389CC8B529E3C83A6741CA8AC63DF33C553D8927_CustomAttributesCacheGenerator_ChainSkill_get_Instance_mFF57A1E3160BADBC15485C65C3459453C8388C8E,
	ChainSkill_t389CC8B529E3C83A6741CA8AC63DF33C553D8927_CustomAttributesCacheGenerator_ChainSkill_set_Instance_m463EFBE4C3064E87C11ECFB2FBD865ADF6E335A5,
	EnvMapAnimator_t7C30A094A3786710A896AD9A6FA46B13EFEB72FD_CustomAttributesCacheGenerator_EnvMapAnimator_Start_mC0D348CAB0F0DC920EF3D3A008688B533F66D1BE,
	Benchmark01_tB93AE57128496E02C0B3D8AC80185B077A1B6934_CustomAttributesCacheGenerator_Benchmark01_Start_mE7E5146B0D8D926CC410CAA48F3B617A0A7D955C,
	Benchmark01_UGUI_t45B3D7BFA1AB9A4A60B48F15D684C558FD619D4B_CustomAttributesCacheGenerator_Benchmark01_UGUI_Start_m2F8F9E1798943DA8086A7C7E73BA9D7C67482BD4,
	ShaderPropAnimator_t44FB2882ECA0D2C780444EB96B9550F5D245CFB8_CustomAttributesCacheGenerator_ShaderPropAnimator_AnimateProperties_mAF49CD157AD41377CE00AA10F0C06C8BF5AA0469,
	SkewTextExample_tA9F165D1BE969ED73FB86CA6C641386C22F1EE07_CustomAttributesCacheGenerator_SkewTextExample_WarpText_m0E0C46988600673F0E5DFA3133534DC6CA5950D3,
	TeleType_t92B5ED4FE818F912A562F085CE035F6B42834934_CustomAttributesCacheGenerator_TeleType_Start_m72DE9DE597F4FA0B1CA02115CBC1E74EB53F63F7,
	TextConsoleSimulator_t82904A7668D46B20E576B30E4CE367C0B66C5289_CustomAttributesCacheGenerator_TextConsoleSimulator_RevealCharacters_mE8E415644F7BD2056D0809F7BB0FA9A2F9FE8534,
	TextConsoleSimulator_t82904A7668D46B20E576B30E4CE367C0B66C5289_CustomAttributesCacheGenerator_TextConsoleSimulator_RevealWords_mFDBC863D30BC63ADCF2860F303AF252E27D0F4F4,
	TextMeshProFloatingText_t55B28EA1D0CCDF61ABE6CF421C752EBED7B37200_CustomAttributesCacheGenerator_TextMeshProFloatingText_DisplayTextMeshProFloatingText_m3C381B8A53C58CF001D7A9212B8BDA6F368CC2C7,
	TextMeshProFloatingText_t55B28EA1D0CCDF61ABE6CF421C752EBED7B37200_CustomAttributesCacheGenerator_TextMeshProFloatingText_DisplayTextMeshFloatingText_m22691E6EA41B7FCF782B03954865D8E3B890E4DF,
	VertexColorCycler_t37E80E87D8EAD0D7757CDA45BD5D3AA82FD28374_CustomAttributesCacheGenerator_VertexColorCycler_AnimateVertexColors_m6960F777E4876DFCA726BCAE7A8163850D58FA42,
	VertexJitter_t4BF9FCCB84221E69D9E3727E32E49710F3B4342A_CustomAttributesCacheGenerator_VertexJitter_AnimateVertexColors_mECAE037FC0CBA52CAC71C0B61E88829FF18BCC16,
	VertexShakeA_t9DED8926E2D8F5E9D457C678C85062CB3B7A923A_CustomAttributesCacheGenerator_VertexShakeA_AnimateVertexColors_mC451E2732A4E3E90E2553811AD75903EEF974599,
	VertexShakeB_t348FF1466CEF3BFED37C140B7AEFA2EA90D703C9_CustomAttributesCacheGenerator_VertexShakeB_AnimateVertexColors_mDF7A7E7028361D288DF588D9301541E4FA1EFA87,
	VertexZoom_t02E8994D3A9EB41AEEF1544EF8C93009F6153163_CustomAttributesCacheGenerator_VertexZoom_AnimateVertexColors_mEFBA6C940DFECB485236C247358A32011A7963ED,
	WarpTextExample_t9CE7000EFB1B8696E32CC86E87E3D0EDEA3D5A96_CustomAttributesCacheGenerator_WarpTextExample_WarpText_mA29C98CF3B92F253C2DD2BADFC76618F8324BEF2,
	Area_t7013267738E4CA59CA15A2E450701A99F6A70197_CustomAttributesCacheGenerator_Area_get_CurrentHeroAmount_mF7EE46F068C4AF7A0C0EE036CF6853C75337B957,
	Area_t7013267738E4CA59CA15A2E450701A99F6A70197_CustomAttributesCacheGenerator_Area_set_CurrentHeroAmount_m7D64716F3C5D20FC426581D30EED068869264294,
	Area_t7013267738E4CA59CA15A2E450701A99F6A70197_CustomAttributesCacheGenerator_Area_get_CurrentSubHeroAmount_mB1AE1AEACA136C7E29DD9855DCF10ACC8B7C3D66,
	Area_t7013267738E4CA59CA15A2E450701A99F6A70197_CustomAttributesCacheGenerator_Area_set_CurrentSubHeroAmount_m3E01B5026F9398B8D8B3012ED50B3BAE83B620E6,
	JSONNode_tDEACFC45BBAB7B7189263CF1BA7D4B7FE179132A_CustomAttributesCacheGenerator_JSONNode_get_Children_mB9A68101166B1C5176311D2EC1ABCC832248FDFF,
	JSONNode_tDEACFC45BBAB7B7189263CF1BA7D4B7FE179132A_CustomAttributesCacheGenerator_JSONNode_get_DeepChildren_m3E3CE542A4856D0BD557B7E121B4CAA999C8110C,
	JSONArray_t478FF5B6520D2CAB00FA50A61F808A3E09F928A0_CustomAttributesCacheGenerator_JSONArray_get_Children_m56D8A696E09F5EEFF1FF9F8BE06011CE5EAFA197,
	JSONObject_t401528CAA28B4B8AAA03BBD681A13864E62CE865_CustomAttributesCacheGenerator_JSONObject_get_Children_mDC37B11CC04E072B9E1DFE56A3B176A1E22F8E6D,
	BaseEnemy_tCCDDF8C04729B5368581508D806525594CB0BC9F_CustomAttributesCacheGenerator_BaseEnemy_get_EnemySpeed_m8E1469EAEFB76EE59CC6FEE9D45438D94EB0D703,
	BaseEnemy_tCCDDF8C04729B5368581508D806525594CB0BC9F_CustomAttributesCacheGenerator_BaseEnemy_set_EnemySpeed_m17416B6AA011CE66A743C53E4D38FABABF30A19E,
	BaseEnemy_tCCDDF8C04729B5368581508D806525594CB0BC9F_CustomAttributesCacheGenerator_BaseEnemy_get_EnemyHp_m60279B2A0B27BCC826D13D21434E45C48CE5BE1B,
	BaseEnemy_tCCDDF8C04729B5368581508D806525594CB0BC9F_CustomAttributesCacheGenerator_BaseEnemy_set_EnemyHp_mA8ABDB93079B24396F5D3BF6065D9E3EF53DC7DD,
	BaseEnemy_tCCDDF8C04729B5368581508D806525594CB0BC9F_CustomAttributesCacheGenerator_BaseEnemy_get_GoldDrop_m561D5F9F207855E6B2ACE07E9DDF833C3054E2D8,
	BaseEnemy_tCCDDF8C04729B5368581508D806525594CB0BC9F_CustomAttributesCacheGenerator_BaseEnemy_set_GoldDrop_mA3990C857287CAE8CB85B7736238292CFBD872AC,
	BaseEnemy_tCCDDF8C04729B5368581508D806525594CB0BC9F_CustomAttributesCacheGenerator_BaseEnemy_get_EnemyElement_m7A2942567C4D07705628C3545EB72DE376C01239,
	BaseEnemy_tCCDDF8C04729B5368581508D806525594CB0BC9F_CustomAttributesCacheGenerator_BaseEnemy_set_EnemyElement_mC9B8C24FEA90E86264C0F771E4E51FD46BEFCF69,
	BaseEnemy_tCCDDF8C04729B5368581508D806525594CB0BC9F_CustomAttributesCacheGenerator_BaseEnemy_DelayLaserPopUp_m7060C6E807ED81FF861F30EB54208E527CC98D58,
	BuildManager_tC38D9095FCAD1EDD6498011089D3EE2043101750_CustomAttributesCacheGenerator_BuildManager_get_Instance_m447C53DC52D9B8E9352F7CF07F13C1345CD8DA6F,
	BuildManager_tC38D9095FCAD1EDD6498011089D3EE2043101750_CustomAttributesCacheGenerator_BuildManager_set_Instance_mA6204A4F0E4CA49361D88BE862C74472578890F3,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_GameManager_get_Instance_m705AC8D9CAF114B59D3AF0563C19B5EE0B711074,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_GameManager_set_Instance_m9DB0F13876CD49066CC3A818D68686B3600F792E,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_GameManager_add_GameOver_mBED5EF8A33E9E9D30CCF2FF978C82C91AE824089,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_GameManager_remove_GameOver_m4A4097C770BD88B02898662A4165CDD35CD06677,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_GameManager_get_EnemyCount_mC5D539611B83ABD18DDE25362AAF39D6820E2588,
	GameManager_tACD5CFB91A43D9016F2D0B7637B5F83F27230947_CustomAttributesCacheGenerator_GameManager_set_EnemyCount_mD5A07C5A7544A51C663A9921CC8F86E7041EA7D2,
	InventoryManager_tD1BFBF734B63F17BD4E44640A9B2DFB3FE2EF742_CustomAttributesCacheGenerator_InventoryManager_get_Instance_mD984E22ADAC6EE38F0CE613A6AD261064F62E121,
	InventoryManager_tD1BFBF734B63F17BD4E44640A9B2DFB3FE2EF742_CustomAttributesCacheGenerator_InventoryManager_set_Instance_m290F386A14BBDD7A90E7A57D1F43D0CBE9680AF2,
	MoneyManager_tC109A492276276C2C8B6EC463FB501014D6859FE_CustomAttributesCacheGenerator_MoneyManager_get_Instance_mD33F844A651475BDB3C97054EAAF8B8198A9AA5E,
	MoneyManager_tC109A492276276C2C8B6EC463FB501014D6859FE_CustomAttributesCacheGenerator_MoneyManager_set_Instance_m9FDBDFA722BB515C6AB48DC35A5EF19C7BAB2798,
	MoneyManager_tC109A492276276C2C8B6EC463FB501014D6859FE_CustomAttributesCacheGenerator_MoneyManager_get_Money_m9F70B21FCD308184615E006F48FC5DDCB43BEA8D,
	MoneyManager_tC109A492276276C2C8B6EC463FB501014D6859FE_CustomAttributesCacheGenerator_MoneyManager_set_Money_m39C6D08CC4093DAFBE32E4B86887FCB14882C0DB,
	RandomScrollManager_t46FEAC1380C11393456FD90E799CF376B89253E1_CustomAttributesCacheGenerator_RandomScrollManager_get_Instance_m71192DB4EC184AB40057DDB9A1A58BF2F682D6DC,
	RandomScrollManager_t46FEAC1380C11393456FD90E799CF376B89253E1_CustomAttributesCacheGenerator_RandomScrollManager_set_Instance_mC51D12544F256ABBFB0604230F7E9FBD14A34627,
	RandomScrollManager_t46FEAC1380C11393456FD90E799CF376B89253E1_CustomAttributesCacheGenerator_RandomScrollManager_add_OnFirstRandomCard_m55B9B6C95F99B39B0283EE6771C362967EEF59A3,
	RandomScrollManager_t46FEAC1380C11393456FD90E799CF376B89253E1_CustomAttributesCacheGenerator_RandomScrollManager_remove_OnFirstRandomCard_m75726C3DEC3C365DE5DFB8A44F5E0D99E658DD6B,
	RandomScrollManager_t46FEAC1380C11393456FD90E799CF376B89253E1_CustomAttributesCacheGenerator_RandomScrollManager_add_AddCard_m1DFB2FEF7AB61EE4C232D430BF6434FB96FDDC89,
	RandomScrollManager_t46FEAC1380C11393456FD90E799CF376B89253E1_CustomAttributesCacheGenerator_RandomScrollManager_remove_AddCard_m16A0C1743E0F9F8152FA98C089624F21376AD0AC,
	UiManager_tBCDA10D96B04DE5423A278745391F4F80A54A66A_CustomAttributesCacheGenerator_UiManager_get_Instance_mD22B7B526318C512A940851D7C5F3605A3188F72,
	UiManager_tBCDA10D96B04DE5423A278745391F4F80A54A66A_CustomAttributesCacheGenerator_UiManager_set_Instance_mC55D7E31E56BDE146DC3270088C52E0D5C7CEBEA,
	UiManager_tBCDA10D96B04DE5423A278745391F4F80A54A66A_CustomAttributesCacheGenerator_UiManager_WaitForNoticeTimerOut_mC2A0447A655995E085F1CF523E140BE180B9BBE9,
	UiManager_tBCDA10D96B04DE5423A278745391F4F80A54A66A_CustomAttributesCacheGenerator_UiManager_WaitForNoticeOut_m80849B33FF4B00687BDF23BDA6DE02123DEF6831,
	WaveManager_t712FD5213A90156B815DC5E3AEDBD86017E3CE8B_CustomAttributesCacheGenerator_WaveManager_get_Instance_m245870AA93671235A9952030594169263629EA4F,
	WaveManager_t712FD5213A90156B815DC5E3AEDBD86017E3CE8B_CustomAttributesCacheGenerator_WaveManager_set_Instance_mEDF39195B0EDFB8F8661CA8614CAE135274FDA64,
	WaveManager_t712FD5213A90156B815DC5E3AEDBD86017E3CE8B_CustomAttributesCacheGenerator_WaveManager_add_OnNextWave_mBFB31E8EE88D78103A44F33E610C07051A7A209E,
	WaveManager_t712FD5213A90156B815DC5E3AEDBD86017E3CE8B_CustomAttributesCacheGenerator_WaveManager_remove_OnNextWave_mB3182F529CF54627875EBADF430F16687BD211F5,
	WorldUIManager_t8F19F2357842FE0470C8C0E492D3FE318B67ED2F_CustomAttributesCacheGenerator_WorldUIManager_get_Instance_m69CA555FDFB6797BFB2D7E2CBF758AD7CF1BE7EF,
	WorldUIManager_t8F19F2357842FE0470C8C0E492D3FE318B67ED2F_CustomAttributesCacheGenerator_WorldUIManager_set_Instance_m4F34BA28C67F98C9F4152355F3B13247D259AB0A,
	WorldUIManager_t8F19F2357842FE0470C8C0E492D3FE318B67ED2F_CustomAttributesCacheGenerator_WorldUIManager_NextEnemy_m9B9022BB73E120504B17458013796E023FB5AE30,
	WorldUIManager_t8F19F2357842FE0470C8C0E492D3FE318B67ED2F_CustomAttributesCacheGenerator_WorldUIManager_NextHero_m31ABAD8168C9EAB5D6829B341F1E759AE2D8D137,
	WorldUIManager_t8F19F2357842FE0470C8C0E492D3FE318B67ED2F_CustomAttributesCacheGenerator_WorldUIManager_DisableInfo_m486646FD2278E1A68A2ACC89D9B5C195CB5D6D0C,
	WorldUIManager_t8F19F2357842FE0470C8C0E492D3FE318B67ED2F_CustomAttributesCacheGenerator_WorldUIManager_FireWorkClicked_m2E89B09A70BB788222B38B1CC8704857A97B43BE,
	WorldUIManager_t8F19F2357842FE0470C8C0E492D3FE318B67ED2F_CustomAttributesCacheGenerator_WorldUIManager_CookieClicked_mCB4A623F5D5D4231D2EC0C4BAD9FC47E9BF83F1E,
	WorldUIManager_t8F19F2357842FE0470C8C0E492D3FE318B67ED2F_CustomAttributesCacheGenerator_WorldUIManager_CandyClicked_m004176E7D20CC7412E7303ACA8CDF6ED8C81D6B3,
	WorldUIManager_t8F19F2357842FE0470C8C0E492D3FE318B67ED2F_CustomAttributesCacheGenerator_WorldUIManager_ScreenShotClicked_m957D2F14806168935EB078D5FCF9CCBA2EB21B5A,
	WorldUIManager_t8F19F2357842FE0470C8C0E492D3FE318B67ED2F_CustomAttributesCacheGenerator_WorldUIManager_LeaderBoardClicked_m7933E88951F99F4887431F5E90C732CDC4D74627,
	U3CWaitToStartU3Ed__14_tAD265766599EEE9474750E00FA4939AEAABB823A_CustomAttributesCacheGenerator_U3CWaitToStartU3Ed__14__ctor_m29B7C44F25045CA2AF686E43C9FC185788C40503,
	U3CWaitToStartU3Ed__14_tAD265766599EEE9474750E00FA4939AEAABB823A_CustomAttributesCacheGenerator_U3CWaitToStartU3Ed__14_System_IDisposable_Dispose_mD5DE83D793D38D79C4BFC2C7300A08CBA9A27C3E,
	U3CWaitToStartU3Ed__14_tAD265766599EEE9474750E00FA4939AEAABB823A_CustomAttributesCacheGenerator_U3CWaitToStartU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9EC5D5E119B878E35802A8030693DCAEE73E1886,
	U3CWaitToStartU3Ed__14_tAD265766599EEE9474750E00FA4939AEAABB823A_CustomAttributesCacheGenerator_U3CWaitToStartU3Ed__14_System_Collections_IEnumerator_Reset_m524B6AAAA453CEFB7943404F24E2BAB89E723FD3,
	U3CWaitToStartU3Ed__14_tAD265766599EEE9474750E00FA4939AEAABB823A_CustomAttributesCacheGenerator_U3CWaitToStartU3Ed__14_System_Collections_IEnumerator_get_Current_m54B43C5EE89291B63D84359793AF6F746A48A102,
	U3CWaitForCamU3Ed__18_t189EA3BB11BFA6AC795F202CDE0699E1C9029277_CustomAttributesCacheGenerator_U3CWaitForCamU3Ed__18__ctor_m0F9FDC3F7885AD8CB9300502DAB0CF3BD03ADE43,
	U3CWaitForCamU3Ed__18_t189EA3BB11BFA6AC795F202CDE0699E1C9029277_CustomAttributesCacheGenerator_U3CWaitForCamU3Ed__18_System_IDisposable_Dispose_m60C82B3C406C8BD576F9B5C7C8A3E9399F2844F0,
	U3CWaitForCamU3Ed__18_t189EA3BB11BFA6AC795F202CDE0699E1C9029277_CustomAttributesCacheGenerator_U3CWaitForCamU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4D0F0E0221A015D42D448233811E1E3F1D18844A,
	U3CWaitForCamU3Ed__18_t189EA3BB11BFA6AC795F202CDE0699E1C9029277_CustomAttributesCacheGenerator_U3CWaitForCamU3Ed__18_System_Collections_IEnumerator_Reset_m3A75E79EA70C002A7A8E6357119F039688749E5E,
	U3CWaitForCamU3Ed__18_t189EA3BB11BFA6AC795F202CDE0699E1C9029277_CustomAttributesCacheGenerator_U3CWaitForCamU3Ed__18_System_Collections_IEnumerator_get_Current_mE2168EB7F89A76D61B57517E899160E4E598659C,
	U3CWaitForShowCanvasU3Ed__21_t6623746B1B2635840CC2F08A9B9C83127716121E_CustomAttributesCacheGenerator_U3CWaitForShowCanvasU3Ed__21__ctor_m35D4BF98BB8BA1BC8821E567536E8DFF2D90F18D,
	U3CWaitForShowCanvasU3Ed__21_t6623746B1B2635840CC2F08A9B9C83127716121E_CustomAttributesCacheGenerator_U3CWaitForShowCanvasU3Ed__21_System_IDisposable_Dispose_m82BACA53350D7E4E634FA212196B7DADBE60FEFA,
	U3CWaitForShowCanvasU3Ed__21_t6623746B1B2635840CC2F08A9B9C83127716121E_CustomAttributesCacheGenerator_U3CWaitForShowCanvasU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m4AC62F2A7C2D79CA96258278EB16915408AB0A0F,
	U3CWaitForShowCanvasU3Ed__21_t6623746B1B2635840CC2F08A9B9C83127716121E_CustomAttributesCacheGenerator_U3CWaitForShowCanvasU3Ed__21_System_Collections_IEnumerator_Reset_m0919B8BEE473A81A79E5048EE957A3EF9DD9922B,
	U3CWaitForShowCanvasU3Ed__21_t6623746B1B2635840CC2F08A9B9C83127716121E_CustomAttributesCacheGenerator_U3CWaitForShowCanvasU3Ed__21_System_Collections_IEnumerator_get_Current_m75D2DA098B2ECBB96569E0DF8A16E85DF389FBF2,
	U3CImageShowCaseU3Ed__17_tB869F8E282B1CD6D682B87084152A7B70D6A3589_CustomAttributesCacheGenerator_U3CImageShowCaseU3Ed__17__ctor_mCD4899C7ABD62E58107628266F40BCE1A71BD8C5,
	U3CImageShowCaseU3Ed__17_tB869F8E282B1CD6D682B87084152A7B70D6A3589_CustomAttributesCacheGenerator_U3CImageShowCaseU3Ed__17_System_IDisposable_Dispose_m0C3DEEFF1C66F08CCB7AFDC7309909C55D39EBBA,
	U3CImageShowCaseU3Ed__17_tB869F8E282B1CD6D682B87084152A7B70D6A3589_CustomAttributesCacheGenerator_U3CImageShowCaseU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF252504AC2355F64820C71DF7B5DBCF6FF71F9B5,
	U3CImageShowCaseU3Ed__17_tB869F8E282B1CD6D682B87084152A7B70D6A3589_CustomAttributesCacheGenerator_U3CImageShowCaseU3Ed__17_System_Collections_IEnumerator_Reset_mCA401AA94E70446FFEED94191F1061F4E0FE5AD3,
	U3CImageShowCaseU3Ed__17_tB869F8E282B1CD6D682B87084152A7B70D6A3589_CustomAttributesCacheGenerator_U3CImageShowCaseU3Ed__17_System_Collections_IEnumerator_get_Current_m470E7F0C70ED4B0BA7C5E8FA54FE620C60160B7C,
	U3CDelayClickU3Ed__8_tBF346C93C4D6EAB42D93211E5296F33032B7BC72_CustomAttributesCacheGenerator_U3CDelayClickU3Ed__8__ctor_m482F03E4612F42F141D0E23529A7F47BD9D322D7,
	U3CDelayClickU3Ed__8_tBF346C93C4D6EAB42D93211E5296F33032B7BC72_CustomAttributesCacheGenerator_U3CDelayClickU3Ed__8_System_IDisposable_Dispose_mD47B909BCEBDDC24A464483BCCF4456D5566180F,
	U3CDelayClickU3Ed__8_tBF346C93C4D6EAB42D93211E5296F33032B7BC72_CustomAttributesCacheGenerator_U3CDelayClickU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m120379B849FCB4CEA022C33647A4ED839DA3B925,
	U3CDelayClickU3Ed__8_tBF346C93C4D6EAB42D93211E5296F33032B7BC72_CustomAttributesCacheGenerator_U3CDelayClickU3Ed__8_System_Collections_IEnumerator_Reset_m227BC1F635FF1B103C8353698AE384738441998A,
	U3CDelayClickU3Ed__8_tBF346C93C4D6EAB42D93211E5296F33032B7BC72_CustomAttributesCacheGenerator_U3CDelayClickU3Ed__8_System_Collections_IEnumerator_get_Current_m4DB8DE888D03118595DF28332153EAE8662A0447,
	U3CCheckAlreadyNameU3Ed__50_t81299FEF47EDC5385754950A82C9FE4E5176462D_CustomAttributesCacheGenerator_U3CCheckAlreadyNameU3Ed__50__ctor_m84595AAC374EC74BF9C5457425E54D84BEAB35AD,
	U3CCheckAlreadyNameU3Ed__50_t81299FEF47EDC5385754950A82C9FE4E5176462D_CustomAttributesCacheGenerator_U3CCheckAlreadyNameU3Ed__50_System_IDisposable_Dispose_m35313E7C7FD768948368F6DA71DB5F5E08EE80AF,
	U3CCheckAlreadyNameU3Ed__50_t81299FEF47EDC5385754950A82C9FE4E5176462D_CustomAttributesCacheGenerator_U3CCheckAlreadyNameU3Ed__50_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mCE98E48779C30473658D59AC7A4E3708DB45F871,
	U3CCheckAlreadyNameU3Ed__50_t81299FEF47EDC5385754950A82C9FE4E5176462D_CustomAttributesCacheGenerator_U3CCheckAlreadyNameU3Ed__50_System_Collections_IEnumerator_Reset_mCD04C1434BC6EB6D7F385D7255F507715CE0413E,
	U3CCheckAlreadyNameU3Ed__50_t81299FEF47EDC5385754950A82C9FE4E5176462D_CustomAttributesCacheGenerator_U3CCheckAlreadyNameU3Ed__50_System_Collections_IEnumerator_get_Current_mB63916D8978C57C9AE01329190AFA4B9DB1C4251,
	U3CCloseNotificationU3Ed__62_t50067E67A5466C3585675771E09DE822592CB4D0_CustomAttributesCacheGenerator_U3CCloseNotificationU3Ed__62__ctor_m43778604560229E1A3384C14D2CB78F31BD3EC62,
	U3CCloseNotificationU3Ed__62_t50067E67A5466C3585675771E09DE822592CB4D0_CustomAttributesCacheGenerator_U3CCloseNotificationU3Ed__62_System_IDisposable_Dispose_m8707E38A3EE8232B8D3B016AB054931AE47A8550,
	U3CCloseNotificationU3Ed__62_t50067E67A5466C3585675771E09DE822592CB4D0_CustomAttributesCacheGenerator_U3CCloseNotificationU3Ed__62_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8EC69539785A0FFFA97B6B1E2BECD465CAA38899,
	U3CCloseNotificationU3Ed__62_t50067E67A5466C3585675771E09DE822592CB4D0_CustomAttributesCacheGenerator_U3CCloseNotificationU3Ed__62_System_Collections_IEnumerator_Reset_mDF06AABB22CECEE0C65B06A046FDC6A0B0B07F2E,
	U3CCloseNotificationU3Ed__62_t50067E67A5466C3585675771E09DE822592CB4D0_CustomAttributesCacheGenerator_U3CCloseNotificationU3Ed__62_System_Collections_IEnumerator_get_Current_m9ECE534C9D9315FD7B66D40424D4390992F60552,
	U3CCheckAlreadyNameIdTransferU3Ed__74_t7451AB99B91CA94A2D6E4F546CA0FD8CC686ED92_CustomAttributesCacheGenerator_U3CCheckAlreadyNameIdTransferU3Ed__74__ctor_mF54BC3DA1886144F3C8CBFD0F06CBFB613D1250E,
	U3CCheckAlreadyNameIdTransferU3Ed__74_t7451AB99B91CA94A2D6E4F546CA0FD8CC686ED92_CustomAttributesCacheGenerator_U3CCheckAlreadyNameIdTransferU3Ed__74_System_IDisposable_Dispose_m6840128964E0260E22537A16FBC442377B5CB78B,
	U3CCheckAlreadyNameIdTransferU3Ed__74_t7451AB99B91CA94A2D6E4F546CA0FD8CC686ED92_CustomAttributesCacheGenerator_U3CCheckAlreadyNameIdTransferU3Ed__74_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAC8343C114D017BB1AEE74AB38D9A4AC6D46E47D,
	U3CCheckAlreadyNameIdTransferU3Ed__74_t7451AB99B91CA94A2D6E4F546CA0FD8CC686ED92_CustomAttributesCacheGenerator_U3CCheckAlreadyNameIdTransferU3Ed__74_System_Collections_IEnumerator_Reset_m58D65B1A3ABB708128F90AC7B13AC4C48862B114,
	U3CCheckAlreadyNameIdTransferU3Ed__74_t7451AB99B91CA94A2D6E4F546CA0FD8CC686ED92_CustomAttributesCacheGenerator_U3CCheckAlreadyNameIdTransferU3Ed__74_System_Collections_IEnumerator_get_Current_mAECF9042F87AB9BBDA616C14ADAF015C8CABDC4A,
	U3CLoadUserScoreFromDBU3Ed__75_tB538B410886D8F26EB89210EE854B5F7389C75DB_CustomAttributesCacheGenerator_U3CLoadUserScoreFromDBU3Ed__75__ctor_m704CC19001BACD6E7621DBB5487CF8F23B19EB79,
	U3CLoadUserScoreFromDBU3Ed__75_tB538B410886D8F26EB89210EE854B5F7389C75DB_CustomAttributesCacheGenerator_U3CLoadUserScoreFromDBU3Ed__75_System_IDisposable_Dispose_mB57F1F147F4E74A61906CF48147B58D654B61655,
	U3CLoadUserScoreFromDBU3Ed__75_tB538B410886D8F26EB89210EE854B5F7389C75DB_CustomAttributesCacheGenerator_U3CLoadUserScoreFromDBU3Ed__75_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m7EC769D8AC3147416F9224A21B0C4DB92846B4F7,
	U3CLoadUserScoreFromDBU3Ed__75_tB538B410886D8F26EB89210EE854B5F7389C75DB_CustomAttributesCacheGenerator_U3CLoadUserScoreFromDBU3Ed__75_System_Collections_IEnumerator_Reset_m5DDB2E8C37B38A0BAEEB860654298AC7E64DD21A,
	U3CLoadUserScoreFromDBU3Ed__75_tB538B410886D8F26EB89210EE854B5F7389C75DB_CustomAttributesCacheGenerator_U3CLoadUserScoreFromDBU3Ed__75_System_Collections_IEnumerator_get_Current_m07AF984DCC7A697BD9F97B5D7FE103676B722BFB,
	U3CDownloadImageU3Ed__76_t723888129E1A8E7C4732F71C5E435C1DEC50CE12_CustomAttributesCacheGenerator_U3CDownloadImageU3Ed__76__ctor_m5CB98433BF9CD23864038893E2E3B3A6A6DE27EF,
	U3CDownloadImageU3Ed__76_t723888129E1A8E7C4732F71C5E435C1DEC50CE12_CustomAttributesCacheGenerator_U3CDownloadImageU3Ed__76_System_IDisposable_Dispose_mD04A6581A340602E2B175B8886A700423A8F9509,
	U3CDownloadImageU3Ed__76_t723888129E1A8E7C4732F71C5E435C1DEC50CE12_CustomAttributesCacheGenerator_U3CDownloadImageU3Ed__76_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mFBDB22DD4A78362581BFEC7DCBAC6DC48D18F23A,
	U3CDownloadImageU3Ed__76_t723888129E1A8E7C4732F71C5E435C1DEC50CE12_CustomAttributesCacheGenerator_U3CDownloadImageU3Ed__76_System_Collections_IEnumerator_Reset_m88F7C5F894E29D5CB0C542490E7A8EDF17C01245,
	U3CDownloadImageU3Ed__76_t723888129E1A8E7C4732F71C5E435C1DEC50CE12_CustomAttributesCacheGenerator_U3CDownloadImageU3Ed__76_System_Collections_IEnumerator_get_Current_m46B996CB3BECE15A4B97173AF9BB4449777E971C,
	U3CLoadLeaderBoardDataU3Ed__80_t53D5F08F28E7939D79D6BB3754783EF0607ABF3D_CustomAttributesCacheGenerator_U3CLoadLeaderBoardDataU3Ed__80__ctor_mC00F58C654FE55829D55D858BBEAB5EBA0E804C1,
	U3CLoadLeaderBoardDataU3Ed__80_t53D5F08F28E7939D79D6BB3754783EF0607ABF3D_CustomAttributesCacheGenerator_U3CLoadLeaderBoardDataU3Ed__80_System_IDisposable_Dispose_m0C8C77ADD2592E7F7409CEE5C220CDADDA47B9BB,
	U3CLoadLeaderBoardDataU3Ed__80_t53D5F08F28E7939D79D6BB3754783EF0607ABF3D_CustomAttributesCacheGenerator_U3CLoadLeaderBoardDataU3Ed__80_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8F6B7887F6E6B15F168E5A764040545E91375F54,
	U3CLoadLeaderBoardDataU3Ed__80_t53D5F08F28E7939D79D6BB3754783EF0607ABF3D_CustomAttributesCacheGenerator_U3CLoadLeaderBoardDataU3Ed__80_System_Collections_IEnumerator_Reset_m9CB4781BA05AC6B10FD795F9C3DB8BC690E62CB0,
	U3CLoadLeaderBoardDataU3Ed__80_t53D5F08F28E7939D79D6BB3754783EF0607ABF3D_CustomAttributesCacheGenerator_U3CLoadLeaderBoardDataU3Ed__80_System_Collections_IEnumerator_get_Current_m0E44B192105C8A4F010F1976F1E212B3B6287568,
	U3CDownloadImageU3Ed__5_t2794312741EA00E7B69D55B73B3136C5C9F354C0_CustomAttributesCacheGenerator_U3CDownloadImageU3Ed__5__ctor_m56A893022DA599581E1A11DCF1D1BF961B1A0316,
	U3CDownloadImageU3Ed__5_t2794312741EA00E7B69D55B73B3136C5C9F354C0_CustomAttributesCacheGenerator_U3CDownloadImageU3Ed__5_System_IDisposable_Dispose_m2398236E2D1FB99231363CFF876F7EB857FA7042,
	U3CDownloadImageU3Ed__5_t2794312741EA00E7B69D55B73B3136C5C9F354C0_CustomAttributesCacheGenerator_U3CDownloadImageU3Ed__5_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m26063764B51701B2831DCC38CAB2DA3C0A819609,
	U3CDownloadImageU3Ed__5_t2794312741EA00E7B69D55B73B3136C5C9F354C0_CustomAttributesCacheGenerator_U3CDownloadImageU3Ed__5_System_Collections_IEnumerator_Reset_m3EFD1AFEB3C0C56382735B8E26FA7B102D127CCB,
	U3CDownloadImageU3Ed__5_t2794312741EA00E7B69D55B73B3136C5C9F354C0_CustomAttributesCacheGenerator_U3CDownloadImageU3Ed__5_System_Collections_IEnumerator_get_Current_mABB560831447A0159C1C41B02A70EEA0E3292041,
	U3CCheckDamageOverCurrentScoreU3Ed__12_t07A41FEF6FA87A2ECE1E7E0FF28AADDFA79B4546_CustomAttributesCacheGenerator_U3CCheckDamageOverCurrentScoreU3Ed__12__ctor_m9FBF500C44EB1E27C562C6EB65C62D9ABF486BB5,
	U3CCheckDamageOverCurrentScoreU3Ed__12_t07A41FEF6FA87A2ECE1E7E0FF28AADDFA79B4546_CustomAttributesCacheGenerator_U3CCheckDamageOverCurrentScoreU3Ed__12_System_IDisposable_Dispose_m7D36BC8498D8CBDFF8D4E22E822E2573EE8A31A0,
	U3CCheckDamageOverCurrentScoreU3Ed__12_t07A41FEF6FA87A2ECE1E7E0FF28AADDFA79B4546_CustomAttributesCacheGenerator_U3CCheckDamageOverCurrentScoreU3Ed__12_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m64A78464A601CF744FD9495D04B11CB0AB3B3332,
	U3CCheckDamageOverCurrentScoreU3Ed__12_t07A41FEF6FA87A2ECE1E7E0FF28AADDFA79B4546_CustomAttributesCacheGenerator_U3CCheckDamageOverCurrentScoreU3Ed__12_System_Collections_IEnumerator_Reset_m0ED25AEE06E5E9ACAE43E03BE0DD6424A38EB4B1,
	U3CCheckDamageOverCurrentScoreU3Ed__12_t07A41FEF6FA87A2ECE1E7E0FF28AADDFA79B4546_CustomAttributesCacheGenerator_U3CCheckDamageOverCurrentScoreU3Ed__12_System_Collections_IEnumerator_get_Current_m5D792B1CE7190B73BC86974F72B0B35ACC2DCCC7,
	U3CCheckTimeSurvivedOverCurrentScoreU3Ed__13_t361F8E558207AE97EA78DAC6651DDAA2FB989EE1_CustomAttributesCacheGenerator_U3CCheckTimeSurvivedOverCurrentScoreU3Ed__13__ctor_mC125234B920EDD35FA13597070656207C0559A01,
	U3CCheckTimeSurvivedOverCurrentScoreU3Ed__13_t361F8E558207AE97EA78DAC6651DDAA2FB989EE1_CustomAttributesCacheGenerator_U3CCheckTimeSurvivedOverCurrentScoreU3Ed__13_System_IDisposable_Dispose_m5C4AD400A1C79CDE7A58680CDD80589512DF527A,
	U3CCheckTimeSurvivedOverCurrentScoreU3Ed__13_t361F8E558207AE97EA78DAC6651DDAA2FB989EE1_CustomAttributesCacheGenerator_U3CCheckTimeSurvivedOverCurrentScoreU3Ed__13_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m79CDF24809F8E9C369BC5D7C6C5D6E52F49464A0,
	U3CCheckTimeSurvivedOverCurrentScoreU3Ed__13_t361F8E558207AE97EA78DAC6651DDAA2FB989EE1_CustomAttributesCacheGenerator_U3CCheckTimeSurvivedOverCurrentScoreU3Ed__13_System_Collections_IEnumerator_Reset_mA71171569E9A5B046E37F9679E3A6514B4AAFEAB,
	U3CCheckTimeSurvivedOverCurrentScoreU3Ed__13_t361F8E558207AE97EA78DAC6651DDAA2FB989EE1_CustomAttributesCacheGenerator_U3CCheckTimeSurvivedOverCurrentScoreU3Ed__13_System_Collections_IEnumerator_get_Current_mED6D775E0EAD9B2B09E6410C85BFC9E7852E6D10,
	U3CUpdateDamageDoneU3Ed__14_t1D42A96E93CA3C068716D61E6E4C4A8CE4087D21_CustomAttributesCacheGenerator_U3CUpdateDamageDoneU3Ed__14__ctor_mAB8EF0D4F0930E31C58ED12AFE14E283DDC7EE50,
	U3CUpdateDamageDoneU3Ed__14_t1D42A96E93CA3C068716D61E6E4C4A8CE4087D21_CustomAttributesCacheGenerator_U3CUpdateDamageDoneU3Ed__14_System_IDisposable_Dispose_mF8D5C1001B4D15B1CE1F7795A5753AC7EF9911EF,
	U3CUpdateDamageDoneU3Ed__14_t1D42A96E93CA3C068716D61E6E4C4A8CE4087D21_CustomAttributesCacheGenerator_U3CUpdateDamageDoneU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF5C127E539E04AB52223DBC2368D5924E4E03973,
	U3CUpdateDamageDoneU3Ed__14_t1D42A96E93CA3C068716D61E6E4C4A8CE4087D21_CustomAttributesCacheGenerator_U3CUpdateDamageDoneU3Ed__14_System_Collections_IEnumerator_Reset_m0D826E9EC709E9DA07E20F68E4D1B7D201161304,
	U3CUpdateDamageDoneU3Ed__14_t1D42A96E93CA3C068716D61E6E4C4A8CE4087D21_CustomAttributesCacheGenerator_U3CUpdateDamageDoneU3Ed__14_System_Collections_IEnumerator_get_Current_m28E02CEDC813B4CDF8898076064DC9FDA23E56E6,
	U3CUpdateWaveSurvivedU3Ed__15_tF7DE1FBC2C2CC828B58C7F6228A9912502633D6F_CustomAttributesCacheGenerator_U3CUpdateWaveSurvivedU3Ed__15__ctor_m32610F33973E8345603BD6EA5FA6009B3F3FBF79,
	U3CUpdateWaveSurvivedU3Ed__15_tF7DE1FBC2C2CC828B58C7F6228A9912502633D6F_CustomAttributesCacheGenerator_U3CUpdateWaveSurvivedU3Ed__15_System_IDisposable_Dispose_mD827AC0451DBDBDC047E281486D890B27354253D,
	U3CUpdateWaveSurvivedU3Ed__15_tF7DE1FBC2C2CC828B58C7F6228A9912502633D6F_CustomAttributesCacheGenerator_U3CUpdateWaveSurvivedU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m54EA5333230F4F5CE9F3E1A7E97BFCA71F16B538,
	U3CUpdateWaveSurvivedU3Ed__15_tF7DE1FBC2C2CC828B58C7F6228A9912502633D6F_CustomAttributesCacheGenerator_U3CUpdateWaveSurvivedU3Ed__15_System_Collections_IEnumerator_Reset_mAEF947D6C4ED3C809ECBEDFE1836E725B5D05A45,
	U3CUpdateWaveSurvivedU3Ed__15_tF7DE1FBC2C2CC828B58C7F6228A9912502633D6F_CustomAttributesCacheGenerator_U3CUpdateWaveSurvivedU3Ed__15_System_Collections_IEnumerator_get_Current_mDC7061BB2AF7F2A042B2C0E6D4F137EC8F5F1AD3,
	U3CUpdateTimeSurvivedU3Ed__16_tD6AF10B6C261DD6230297852068466DCA37C64DA_CustomAttributesCacheGenerator_U3CUpdateTimeSurvivedU3Ed__16__ctor_mE7CA8949AA742F0CD2614EC3BF5905CEA3373323,
	U3CUpdateTimeSurvivedU3Ed__16_tD6AF10B6C261DD6230297852068466DCA37C64DA_CustomAttributesCacheGenerator_U3CUpdateTimeSurvivedU3Ed__16_System_IDisposable_Dispose_mB43ADA3CF19E76A5808A135D3E7E3E0D03793A8B,
	U3CUpdateTimeSurvivedU3Ed__16_tD6AF10B6C261DD6230297852068466DCA37C64DA_CustomAttributesCacheGenerator_U3CUpdateTimeSurvivedU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE11BE326E56200BFE14E5915BFEAB2B74C051FF2,
	U3CUpdateTimeSurvivedU3Ed__16_tD6AF10B6C261DD6230297852068466DCA37C64DA_CustomAttributesCacheGenerator_U3CUpdateTimeSurvivedU3Ed__16_System_Collections_IEnumerator_Reset_m9C672B2F72A298CB037B3B5753468644270E4BEF,
	U3CUpdateTimeSurvivedU3Ed__16_tD6AF10B6C261DD6230297852068466DCA37C64DA_CustomAttributesCacheGenerator_U3CUpdateTimeSurvivedU3Ed__16_System_Collections_IEnumerator_get_Current_m3E8EBFA8DC883686D75A92E8E7E3EDF97EDD4DD7,
	U3CLoadUserScoreFromDBU3Ed__17_tA72B0A24BFD2A694425FDB145D618C9F8A7C6AA8_CustomAttributesCacheGenerator_U3CLoadUserScoreFromDBU3Ed__17__ctor_m8D1FA3BBE852BAE443A006877DE97924D717A842,
	U3CLoadUserScoreFromDBU3Ed__17_tA72B0A24BFD2A694425FDB145D618C9F8A7C6AA8_CustomAttributesCacheGenerator_U3CLoadUserScoreFromDBU3Ed__17_System_IDisposable_Dispose_m2D7F9A2A3A5BF15A09C5E667906D55A8F3A7243D,
	U3CLoadUserScoreFromDBU3Ed__17_tA72B0A24BFD2A694425FDB145D618C9F8A7C6AA8_CustomAttributesCacheGenerator_U3CLoadUserScoreFromDBU3Ed__17_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m930F67A39ADE639B8D30C6DB54037E755C365308,
	U3CLoadUserScoreFromDBU3Ed__17_tA72B0A24BFD2A694425FDB145D618C9F8A7C6AA8_CustomAttributesCacheGenerator_U3CLoadUserScoreFromDBU3Ed__17_System_Collections_IEnumerator_Reset_m19BD1A5B2D3986337D43651A6346B78AFA0EAB93,
	U3CLoadUserScoreFromDBU3Ed__17_tA72B0A24BFD2A694425FDB145D618C9F8A7C6AA8_CustomAttributesCacheGenerator_U3CLoadUserScoreFromDBU3Ed__17_System_Collections_IEnumerator_get_Current_m16A3FB960C8208C64ABA393EEA909C9C289F0481,
	U3CLoadLeaderBoardDataU3Ed__18_t85C0AE47A469B364C930201232E48E31F00B91D1_CustomAttributesCacheGenerator_U3CLoadLeaderBoardDataU3Ed__18__ctor_mC41A8A5403C2606BB13F4B461653F8E5D36C1F2F,
	U3CLoadLeaderBoardDataU3Ed__18_t85C0AE47A469B364C930201232E48E31F00B91D1_CustomAttributesCacheGenerator_U3CLoadLeaderBoardDataU3Ed__18_System_IDisposable_Dispose_m1618C92B5A0462FA30052FFC0F1FCBF0F97A6875,
	U3CLoadLeaderBoardDataU3Ed__18_t85C0AE47A469B364C930201232E48E31F00B91D1_CustomAttributesCacheGenerator_U3CLoadLeaderBoardDataU3Ed__18_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mBFA6427E3891D37646831F9AFA8290094EFA9D71,
	U3CLoadLeaderBoardDataU3Ed__18_t85C0AE47A469B364C930201232E48E31F00B91D1_CustomAttributesCacheGenerator_U3CLoadLeaderBoardDataU3Ed__18_System_Collections_IEnumerator_Reset_mEF596DF92F906E40A50D828F7AEA4E7897DE215C,
	U3CLoadLeaderBoardDataU3Ed__18_t85C0AE47A469B364C930201232E48E31F00B91D1_CustomAttributesCacheGenerator_U3CLoadLeaderBoardDataU3Ed__18_System_Collections_IEnumerator_get_Current_m8CDA43B0A3DCCC9938B4E6E503DE3A784B6FB011,
	U3CDownloadImageU3Ed__19_tC075483F4C0D4B7D7065D99C2135C5A8A0BCA1D7_CustomAttributesCacheGenerator_U3CDownloadImageU3Ed__19__ctor_mA337C682C0E9601A354AF20655032AEA526D026C,
	U3CDownloadImageU3Ed__19_tC075483F4C0D4B7D7065D99C2135C5A8A0BCA1D7_CustomAttributesCacheGenerator_U3CDownloadImageU3Ed__19_System_IDisposable_Dispose_mE416C30E9AE2D2DEE3C4D14332D65CF86208C5B5,
	U3CDownloadImageU3Ed__19_tC075483F4C0D4B7D7065D99C2135C5A8A0BCA1D7_CustomAttributesCacheGenerator_U3CDownloadImageU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDB1F79F0F8367FFE5586098C13EB91E7547542EA,
	U3CDownloadImageU3Ed__19_tC075483F4C0D4B7D7065D99C2135C5A8A0BCA1D7_CustomAttributesCacheGenerator_U3CDownloadImageU3Ed__19_System_Collections_IEnumerator_Reset_mA266BA2E8E8DCC326775AC8272102BB1B324ED9A,
	U3CDownloadImageU3Ed__19_tC075483F4C0D4B7D7065D99C2135C5A8A0BCA1D7_CustomAttributesCacheGenerator_U3CDownloadImageU3Ed__19_System_Collections_IEnumerator_get_Current_mFD392F88C8F541BEC86F9DC21B847FACC19DB873,
	U3CChangeButtonColorU3Ed__21_tD9B8A0568D190D5F2BD60EEA784E0B908A3AE624_CustomAttributesCacheGenerator_U3CChangeButtonColorU3Ed__21__ctor_m022B1F3DDDC026D92DE234A1607B268DF00F2C97,
	U3CChangeButtonColorU3Ed__21_tD9B8A0568D190D5F2BD60EEA784E0B908A3AE624_CustomAttributesCacheGenerator_U3CChangeButtonColorU3Ed__21_System_IDisposable_Dispose_m3517C42530E1127D15045D7C88535291F122D6B0,
	U3CChangeButtonColorU3Ed__21_tD9B8A0568D190D5F2BD60EEA784E0B908A3AE624_CustomAttributesCacheGenerator_U3CChangeButtonColorU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m8821EF38042F4839506600E9F1669248B0928E96,
	U3CChangeButtonColorU3Ed__21_tD9B8A0568D190D5F2BD60EEA784E0B908A3AE624_CustomAttributesCacheGenerator_U3CChangeButtonColorU3Ed__21_System_Collections_IEnumerator_Reset_m3A87C825847F51A6BB2A1DB026FF22945CA831D0,
	U3CChangeButtonColorU3Ed__21_tD9B8A0568D190D5F2BD60EEA784E0B908A3AE624_CustomAttributesCacheGenerator_U3CChangeButtonColorU3Ed__21_System_Collections_IEnumerator_get_Current_mE05A61B3BF11D846169720E6B7E6DAB5F7E79DB4,
	U3CWaitForUpdateUiU3Ed__27_t36E7FB8307E80FED7AB34985D4632506BC952711_CustomAttributesCacheGenerator_U3CWaitForUpdateUiU3Ed__27__ctor_mEFF8FCAE0E22B88642092D496E94249D3B9F9FB5,
	U3CWaitForUpdateUiU3Ed__27_t36E7FB8307E80FED7AB34985D4632506BC952711_CustomAttributesCacheGenerator_U3CWaitForUpdateUiU3Ed__27_System_IDisposable_Dispose_m19B749EC2C6F276D35A0BE03B51D69A659170E46,
	U3CWaitForUpdateUiU3Ed__27_t36E7FB8307E80FED7AB34985D4632506BC952711_CustomAttributesCacheGenerator_U3CWaitForUpdateUiU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m99553AC5B564ED4462C3587A1AE492105F8773D4,
	U3CWaitForUpdateUiU3Ed__27_t36E7FB8307E80FED7AB34985D4632506BC952711_CustomAttributesCacheGenerator_U3CWaitForUpdateUiU3Ed__27_System_Collections_IEnumerator_Reset_m269F0236C1C5426233ECF692D38DB744748508EB,
	U3CWaitForUpdateUiU3Ed__27_t36E7FB8307E80FED7AB34985D4632506BC952711_CustomAttributesCacheGenerator_U3CWaitForUpdateUiU3Ed__27_System_Collections_IEnumerator_get_Current_mB1C2059DD75E7B11DFA1E993B2BBA333AF79F63D,
	U3CActiveCookie30sU3Ed__19_t0119DB09C6FEC13C8307690325CEE99855E3C782_CustomAttributesCacheGenerator_U3CActiveCookie30sU3Ed__19__ctor_m8FCF61703691001EFA7961AE2E1D787D16F7A5B5,
	U3CActiveCookie30sU3Ed__19_t0119DB09C6FEC13C8307690325CEE99855E3C782_CustomAttributesCacheGenerator_U3CActiveCookie30sU3Ed__19_System_IDisposable_Dispose_m045AB7688AF359AF2B0E85847CB3ED26389AE618,
	U3CActiveCookie30sU3Ed__19_t0119DB09C6FEC13C8307690325CEE99855E3C782_CustomAttributesCacheGenerator_U3CActiveCookie30sU3Ed__19_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mEF460E178959DB2B321C7DB1AABCB6B86E2D6829,
	U3CActiveCookie30sU3Ed__19_t0119DB09C6FEC13C8307690325CEE99855E3C782_CustomAttributesCacheGenerator_U3CActiveCookie30sU3Ed__19_System_Collections_IEnumerator_Reset_m8854B0851AC842D51089315669AFF02D7908C1EC,
	U3CActiveCookie30sU3Ed__19_t0119DB09C6FEC13C8307690325CEE99855E3C782_CustomAttributesCacheGenerator_U3CActiveCookie30sU3Ed__19_System_Collections_IEnumerator_get_Current_m440F33539D6609523F55870DB875411892221539,
	U3CActiveCandy30sU3Ed__21_t679D864AD14DE99063DD90DAE48F757780A913F7_CustomAttributesCacheGenerator_U3CActiveCandy30sU3Ed__21__ctor_m6125A56FB311570E4C1DA52533D5BC1E8D963429,
	U3CActiveCandy30sU3Ed__21_t679D864AD14DE99063DD90DAE48F757780A913F7_CustomAttributesCacheGenerator_U3CActiveCandy30sU3Ed__21_System_IDisposable_Dispose_m398275D5D386B790D02DCE00C807A13C41F141D4,
	U3CActiveCandy30sU3Ed__21_t679D864AD14DE99063DD90DAE48F757780A913F7_CustomAttributesCacheGenerator_U3CActiveCandy30sU3Ed__21_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mE97A411E62DD8FD2994CDA0381430C7B376D09FE,
	U3CActiveCandy30sU3Ed__21_t679D864AD14DE99063DD90DAE48F757780A913F7_CustomAttributesCacheGenerator_U3CActiveCandy30sU3Ed__21_System_Collections_IEnumerator_Reset_m0D06ED95FB91F1B91B0A9BC2A0ACB2557BCD6729,
	U3CActiveCandy30sU3Ed__21_t679D864AD14DE99063DD90DAE48F757780A913F7_CustomAttributesCacheGenerator_U3CActiveCandy30sU3Ed__21_System_Collections_IEnumerator_get_Current_mF71FC42683C0F4C24A8DD650B36D6B141CF43735,
	U3CWaitForPanelU3Ed__14_t21DF4D5D04488D4635896E34F3A4930EBF6331AA_CustomAttributesCacheGenerator_U3CWaitForPanelU3Ed__14__ctor_m59D042B5E4A18907C5B493929DE55BB2793A8A20,
	U3CWaitForPanelU3Ed__14_t21DF4D5D04488D4635896E34F3A4930EBF6331AA_CustomAttributesCacheGenerator_U3CWaitForPanelU3Ed__14_System_IDisposable_Dispose_m629FF274124309F78AEE0A0EE1E78820401E3659,
	U3CWaitForPanelU3Ed__14_t21DF4D5D04488D4635896E34F3A4930EBF6331AA_CustomAttributesCacheGenerator_U3CWaitForPanelU3Ed__14_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m1BA1E11BDC976FFDA71876728621CC3580B339C9,
	U3CWaitForPanelU3Ed__14_t21DF4D5D04488D4635896E34F3A4930EBF6331AA_CustomAttributesCacheGenerator_U3CWaitForPanelU3Ed__14_System_Collections_IEnumerator_Reset_mFDFA0E2253D02365F762B257186B1B188BA591BC,
	U3CWaitForPanelU3Ed__14_t21DF4D5D04488D4635896E34F3A4930EBF6331AA_CustomAttributesCacheGenerator_U3CWaitForPanelU3Ed__14_System_Collections_IEnumerator_get_Current_m7FF353409E4AB340F005DEE86648F7D8B53A47D0,
	U3CWaitForUnPauseU3Ed__15_t7C2E4C470D88899BE03226E81A5D36543A1ACF77_CustomAttributesCacheGenerator_U3CWaitForUnPauseU3Ed__15__ctor_m0FD75504981C15A71618791163C95DA6832B1558,
	U3CWaitForUnPauseU3Ed__15_t7C2E4C470D88899BE03226E81A5D36543A1ACF77_CustomAttributesCacheGenerator_U3CWaitForUnPauseU3Ed__15_System_IDisposable_Dispose_m1967E6755E3C99E168315655FDBC938D996A7C80,
	U3CWaitForUnPauseU3Ed__15_t7C2E4C470D88899BE03226E81A5D36543A1ACF77_CustomAttributesCacheGenerator_U3CWaitForUnPauseU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3ABE4E9FEA6847127D2EBA79A3BDCC495BF3A886,
	U3CWaitForUnPauseU3Ed__15_t7C2E4C470D88899BE03226E81A5D36543A1ACF77_CustomAttributesCacheGenerator_U3CWaitForUnPauseU3Ed__15_System_Collections_IEnumerator_Reset_mF18699718458033B6427B047F0C6892055FFD52E,
	U3CWaitForUnPauseU3Ed__15_t7C2E4C470D88899BE03226E81A5D36543A1ACF77_CustomAttributesCacheGenerator_U3CWaitForUnPauseU3Ed__15_System_Collections_IEnumerator_get_Current_mA42AEF9003416FA995E1070577B3323FCD65A232,
	U3CStartU3Ed__4_t85DC3CED72C53244AC5D2467B8CB12EF944D3266_CustomAttributesCacheGenerator_U3CStartU3Ed__4__ctor_m4D65F4FC2207AE4B6BE963AF9B5EDC55C7E29B23,
	U3CStartU3Ed__4_t85DC3CED72C53244AC5D2467B8CB12EF944D3266_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_IDisposable_Dispose_mFEE2ACED70A3D825988E28CC61FEF8DCD7660A5B,
	U3CStartU3Ed__4_t85DC3CED72C53244AC5D2467B8CB12EF944D3266_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF03E2A57659B8F598AC450183F55D43F903C0A1E,
	U3CStartU3Ed__4_t85DC3CED72C53244AC5D2467B8CB12EF944D3266_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_m268BA538CF6812C56EB281C0CE29D5AA2E9A2CAB,
	U3CStartU3Ed__4_t85DC3CED72C53244AC5D2467B8CB12EF944D3266_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_mFE9CA9F52F9AFA1E4C9DBADA2064F854D6931CFF,
	U3CStartU3Ed__10_t4C97F7E5F1F2D654FF76C297F92CF423F472A640_CustomAttributesCacheGenerator_U3CStartU3Ed__10__ctor_mBE5D8B4B98C372BD6DC3936437999DF3048DE3AB,
	U3CStartU3Ed__10_t4C97F7E5F1F2D654FF76C297F92CF423F472A640_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_IDisposable_Dispose_m1F4180A7FDAE9AC5F11F30B67F813B3D7CD56D73,
	U3CStartU3Ed__10_t4C97F7E5F1F2D654FF76C297F92CF423F472A640_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m22B2975D42326E3DE437174BBE4F0E8790CB6591,
	U3CStartU3Ed__10_t4C97F7E5F1F2D654FF76C297F92CF423F472A640_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m514DA545ECA10AA62BFC239BB582FF0017B91D2B,
	U3CStartU3Ed__10_t4C97F7E5F1F2D654FF76C297F92CF423F472A640_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_m6904A738F04118BA24E473F1809B3D69E711627E,
	U3CStartU3Ed__10_t3D69371599D2A84DED6689D4FA6EA1720611EAD3_CustomAttributesCacheGenerator_U3CStartU3Ed__10__ctor_m653B757B49A674E239C804FFF4EDEF325B5DB651,
	U3CStartU3Ed__10_t3D69371599D2A84DED6689D4FA6EA1720611EAD3_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_IDisposable_Dispose_mAD0BC985E1E01C8369A7A955A60D3E545B0B3561,
	U3CStartU3Ed__10_t3D69371599D2A84DED6689D4FA6EA1720611EAD3_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF438D42D7C5A811FF9A283F8780F015FF55826CD,
	U3CStartU3Ed__10_t3D69371599D2A84DED6689D4FA6EA1720611EAD3_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_IEnumerator_Reset_m65545EB5B6DDDDAB5ACF92600137C1C754B322BA,
	U3CStartU3Ed__10_t3D69371599D2A84DED6689D4FA6EA1720611EAD3_CustomAttributesCacheGenerator_U3CStartU3Ed__10_System_Collections_IEnumerator_get_Current_mD4523BE1D1014AAF1C0709EB767C0B0F47D739D2,
	U3CAnimatePropertiesU3Ed__6_t6EF9435006F3F335B99084BECD0ABFEFFEF4A32D_CustomAttributesCacheGenerator_U3CAnimatePropertiesU3Ed__6__ctor_mFAC0F8A7368D9D35AD2780C118E13414DA79B56A,
	U3CAnimatePropertiesU3Ed__6_t6EF9435006F3F335B99084BECD0ABFEFFEF4A32D_CustomAttributesCacheGenerator_U3CAnimatePropertiesU3Ed__6_System_IDisposable_Dispose_m628EECBFCBC49087298185F17BC2AE7A73FC8A7B,
	U3CAnimatePropertiesU3Ed__6_t6EF9435006F3F335B99084BECD0ABFEFFEF4A32D_CustomAttributesCacheGenerator_U3CAnimatePropertiesU3Ed__6_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0742A0FA62C518EBD119ED5FEBF849F918E12390,
	U3CAnimatePropertiesU3Ed__6_t6EF9435006F3F335B99084BECD0ABFEFFEF4A32D_CustomAttributesCacheGenerator_U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_Reset_mC076F281DD17668D6CE42EB04EA974DE1FFB3F6B,
	U3CAnimatePropertiesU3Ed__6_t6EF9435006F3F335B99084BECD0ABFEFFEF4A32D_CustomAttributesCacheGenerator_U3CAnimatePropertiesU3Ed__6_System_Collections_IEnumerator_get_Current_mA585E786DA86D2589EBCA029AD9E64E8B5135362,
	U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__7__ctor_mEAD3C39209B75514446A44B6C2FA76F8097EBD6F,
	U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__7_System_IDisposable_Dispose_m32AA7120BE15547799BDC515FA3486488952BDCF,
	U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m3CF506B740C16FEDE58B6E1BE4556CAC2C6AEAD1,
	U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__7_System_Collections_IEnumerator_Reset_mDDCDBB794F21DF70178A75747D8D8398F38A9C2F,
	U3CWarpTextU3Ed__7_tD8218C916B979E7CA67B10A730BD2ACF4B97CA2B_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__7_System_Collections_IEnumerator_get_Current_m6D89CF1910F76577E23C924B535E93FD1B01CF0A,
	U3CStartU3Ed__4_t70319341791F5D1BC6F878503A507E845E98C44E_CustomAttributesCacheGenerator_U3CStartU3Ed__4__ctor_mF3575DBEBF4F153F6A899AF3362940298C11B629,
	U3CStartU3Ed__4_t70319341791F5D1BC6F878503A507E845E98C44E_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_IDisposable_Dispose_m6DABFDBC2A313BF6DD90AA1C43B4EF9D6249CBE9,
	U3CStartU3Ed__4_t70319341791F5D1BC6F878503A507E845E98C44E_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m66DC06D52865D30DAA78DBD70B6554D13E2EA70B,
	U3CStartU3Ed__4_t70319341791F5D1BC6F878503A507E845E98C44E_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_Collections_IEnumerator_Reset_mE854A2CECA0E01F3C2D90DA4F720CF0F387994F8,
	U3CStartU3Ed__4_t70319341791F5D1BC6F878503A507E845E98C44E_CustomAttributesCacheGenerator_U3CStartU3Ed__4_System_Collections_IEnumerator_get_Current_m77D3936B94684C817EE9F6D2C903238A457D5261,
	U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E_CustomAttributesCacheGenerator_U3CRevealCharactersU3Ed__7__ctor_mD45A85F5F50909F70C80AC8CE460F4FD261CDE9D,
	U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E_CustomAttributesCacheGenerator_U3CRevealCharactersU3Ed__7_System_IDisposable_Dispose_m85F270FDC11A4D79E9CF47AADC9FA1FBF032F86C,
	U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E_CustomAttributesCacheGenerator_U3CRevealCharactersU3Ed__7_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m5BBAE6868EB7F1C11BE5DF001641E18C9D625F83,
	U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E_CustomAttributesCacheGenerator_U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_Reset_mA9F4893381AB24E01EAEBFC38A88AF363A9A0691,
	U3CRevealCharactersU3Ed__7_t9DC94DD1629B430917FDD9FD90D4EC000BB1B15E_CustomAttributesCacheGenerator_U3CRevealCharactersU3Ed__7_System_Collections_IEnumerator_get_Current_mFD0B7538B1A650FB389FFE9296B0E51AEA5B6B6F,
	U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408_CustomAttributesCacheGenerator_U3CRevealWordsU3Ed__8__ctor_m22FF9E770988107A928C5D1EA639F60239BFFEF0,
	U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408_CustomAttributesCacheGenerator_U3CRevealWordsU3Ed__8_System_IDisposable_Dispose_m3EAF6EF4A8C99A71FEED251BF3F28A26BF6AD7F8,
	U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408_CustomAttributesCacheGenerator_U3CRevealWordsU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA3F93E4AA532F15D52D68B7121805C014AB2D7FB,
	U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408_CustomAttributesCacheGenerator_U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_Reset_mCDC8982948ED5F7743567569CA1D4A354218714F,
	U3CRevealWordsU3Ed__8_tF7649A9A0F92FFA0D69DB3B69680480E1B50D408_CustomAttributesCacheGenerator_U3CRevealWordsU3Ed__8_System_Collections_IEnumerator_get_Current_mF39E492D576810F58DB9031556CFD6806FD32E27,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_tF7B0AC090E8E1BC19444DC46DEA61B2D608F7E20_CustomAttributesCacheGenerator_U3CDisplayTextMeshProFloatingTextU3Ed__15__ctor_m3F7F9561735FF8403090A93E708E781B3997B81B,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_tF7B0AC090E8E1BC19444DC46DEA61B2D608F7E20_CustomAttributesCacheGenerator_U3CDisplayTextMeshProFloatingTextU3Ed__15_System_IDisposable_Dispose_m6F24E1DC5777D4682375D8742BC9272EE6D4EA2C,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_tF7B0AC090E8E1BC19444DC46DEA61B2D608F7E20_CustomAttributesCacheGenerator_U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m14A7C9DEBD0FF0C7A6979AFEC78B0BEE9E4C3963,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_tF7B0AC090E8E1BC19444DC46DEA61B2D608F7E20_CustomAttributesCacheGenerator_U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_Reset_mC0D5548A92F57550F60EE76E17B58125B57B94A8,
	U3CDisplayTextMeshProFloatingTextU3Ed__15_tF7B0AC090E8E1BC19444DC46DEA61B2D608F7E20_CustomAttributesCacheGenerator_U3CDisplayTextMeshProFloatingTextU3Ed__15_System_Collections_IEnumerator_get_Current_mD3CBFD6F8558EF09F2B4A3D8D1B76BD7FDB3A98B,
	U3CDisplayTextMeshFloatingTextU3Ed__16_tACCF2F4E02674FBB7D0A61F908094C4EC0DAD2A2_CustomAttributesCacheGenerator_U3CDisplayTextMeshFloatingTextU3Ed__16__ctor_m0466586E44A01852547C6CECCA91C14673B98C20,
	U3CDisplayTextMeshFloatingTextU3Ed__16_tACCF2F4E02674FBB7D0A61F908094C4EC0DAD2A2_CustomAttributesCacheGenerator_U3CDisplayTextMeshFloatingTextU3Ed__16_System_IDisposable_Dispose_m8730A3A2EFBF759A51F0E5330C3BBF8EFC1736A3,
	U3CDisplayTextMeshFloatingTextU3Ed__16_tACCF2F4E02674FBB7D0A61F908094C4EC0DAD2A2_CustomAttributesCacheGenerator_U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mA4076BE73343A473EFE46E73ABA5DE66CC797598,
	U3CDisplayTextMeshFloatingTextU3Ed__16_tACCF2F4E02674FBB7D0A61F908094C4EC0DAD2A2_CustomAttributesCacheGenerator_U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_Reset_mC33B355BB392C5A52DCA0AAF2588E1E78AF6D3A5,
	U3CDisplayTextMeshFloatingTextU3Ed__16_tACCF2F4E02674FBB7D0A61F908094C4EC0DAD2A2_CustomAttributesCacheGenerator_U3CDisplayTextMeshFloatingTextU3Ed__16_System_Collections_IEnumerator_get_Current_m28CCE2E51295AD5F368C504BADDDFF008C2CC0A6,
	U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__3__ctor_m0038B1054BCC928D35F8C0021ED7D2E1C533E35F,
	U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__3_System_IDisposable_Dispose_m5BAD394A0B09B3E0FF19E91521E02C2B3ADD6007,
	U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__3_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mAE810D7968957A09C88E61C29DAAEC68E4AF1E51,
	U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_Reset_m72ABAC50E9E4D972FB44CAFF387F3E23FEC5D932,
	U3CAnimateVertexColorsU3Ed__3_tB3AD1CDAF88FD5FB6786CD89058781C6BDF67DAF_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__3_System_Collections_IEnumerator_get_Current_m8DE595A1D01F3A507A356F8BCE020D0851412B52,
	U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11__ctor_m0222C3457F5ACA497FE3A8EC829DE4AD11A169F8,
	U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m717E79A39A8161ADDA9E62F7CDFB67B8F2D65099,
	U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m920EBA43D59A1A88E27FED92CF0AC0DF90179479,
	U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_m6C045A6DD0B60F1512457448E615877EAB86D75D,
	U3CAnimateVertexColorsU3Ed__11_tB50A33D4FE1F8DD23F6652EA6967FFF7F855C741_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m017345FE58B497EAFC9D0CB1FB733F76EB3449AF,
	U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11__ctor_m92612416BEC0EBF5E9849FB603629C0F2F95FEF2,
	U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_IDisposable_Dispose_m77D966994D4717EAFD8EFE169F3E8A4EE8B05B81,
	U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mDCCD3645ACF9B18D760B341C863F853996FA9BCE,
	U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_Reset_mB9444B5E58B4E97105C447F547C0F74C51BCFBFA,
	U3CAnimateVertexColorsU3Ed__11_t6453F9CBEC4C77977786367165D55DA585BE7155_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__11_System_Collections_IEnumerator_get_Current_m5CC464185D5C0251C6206E20AFFA681BA0525A7E,
	U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10__ctor_m3E3B1D286DEBED2BC028AD490308568B930C3760,
	U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_m0F9D4B2A6ED0500C2120DBA29932CC279E8908DC,
	U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF2A8BAFC0261ACBBE8EEA74DE4B498D30C68AE3D,
	U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_m5F12D7C6F2523BEB6326FE49AE972116D6157CBB,
	U3CAnimateVertexColorsU3Ed__10_t740D0A5D063674119342F33329012F78E8D3DAC6_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_mB9C12786011A3B78517AEFAE8D5A78B95A4219AD,
	U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10__ctor_m537CF0A5ADF5BBC2DF784BF526E3F32EB528E1B2,
	U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_IDisposable_Dispose_mF0CD944C393771101D2718A660E1DFF22385819F,
	U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m093E922A5A046910B2A7EE826D804CA3064A7BD9,
	U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_Reset_mDED63FD52741D58D8D5A3E53415F91B6560F680C,
	U3CAnimateVertexColorsU3Ed__10_t0331DD44C492F52D541A0BC9A3FC88FC3D1C4B40_CustomAttributesCacheGenerator_U3CAnimateVertexColorsU3Ed__10_System_Collections_IEnumerator_get_Current_m933EBF7C4300C7C1E3EE60E4741110025877CA5F,
	U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__8__ctor_m9FADE04C27A0034C5A276232FCA187AECDC6BF49,
	U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__8_System_IDisposable_Dispose_m5AB0DCF4B3DE6C290487F3DBBC7BAE931130DE60,
	U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__8_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9D5776AF80C4227401ADD7E13D98F2530CB9E7A1,
	U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__8_System_Collections_IEnumerator_Reset_m4F2FBBE7B375E6FC837736B4275A7B601A01F535,
	U3CWarpTextU3Ed__8_t4846D84CD08AB0A242548B376B06FF86A4830D76_CustomAttributesCacheGenerator_U3CWarpTextU3Ed__8_System_Collections_IEnumerator_get_Current_m01FA935D93D737C9DDD88A051F7DFC393DD7BD25,
	U3Cget_ChildrenU3Ed__39_tB70A86AF5415D0A3C8B93D07712335F4FE14F44B_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__39__ctor_m0BF33D6A9ACF56C849BB64AB175B43D7692499A8,
	U3Cget_ChildrenU3Ed__39_tB70A86AF5415D0A3C8B93D07712335F4FE14F44B_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__39_System_IDisposable_Dispose_mFD004DCB3358CE2D7764E776A031A077B21BEB29,
	U3Cget_ChildrenU3Ed__39_tB70A86AF5415D0A3C8B93D07712335F4FE14F44B_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__39_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_mB88A038112706EAD24B23A14267B794021C761C2,
	U3Cget_ChildrenU3Ed__39_tB70A86AF5415D0A3C8B93D07712335F4FE14F44B_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__39_System_Collections_IEnumerator_Reset_m716ACA15194120CF8192937025FD3D4D7F8C710F,
	U3Cget_ChildrenU3Ed__39_tB70A86AF5415D0A3C8B93D07712335F4FE14F44B_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__39_System_Collections_IEnumerator_get_Current_m831C58F860DBA03146F00BB7DC77DD1852943881,
	U3Cget_ChildrenU3Ed__39_tB70A86AF5415D0A3C8B93D07712335F4FE14F44B_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__39_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_mC22210E7538A1A259E64227EEAC5440FE868ED60,
	U3Cget_ChildrenU3Ed__39_tB70A86AF5415D0A3C8B93D07712335F4FE14F44B_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__39_System_Collections_IEnumerable_GetEnumerator_mDB57DC3D1B30B06919F6F3985E3B3564974C1CAA,
	U3Cget_DeepChildrenU3Ed__41_t1EFE91681671B35682051A9DF5D99D8012D7DDD7_CustomAttributesCacheGenerator_U3Cget_DeepChildrenU3Ed__41__ctor_mAF92DE35182876CDDB7F9E2831AA0078B111A5F9,
	U3Cget_DeepChildrenU3Ed__41_t1EFE91681671B35682051A9DF5D99D8012D7DDD7_CustomAttributesCacheGenerator_U3Cget_DeepChildrenU3Ed__41_System_IDisposable_Dispose_m0C1243483A3118059E0FB871E404B606DD55CBBD,
	U3Cget_DeepChildrenU3Ed__41_t1EFE91681671B35682051A9DF5D99D8012D7DDD7_CustomAttributesCacheGenerator_U3Cget_DeepChildrenU3Ed__41_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_mF12A4534E7D4122F67B420801B2522BC9F839868,
	U3Cget_DeepChildrenU3Ed__41_t1EFE91681671B35682051A9DF5D99D8012D7DDD7_CustomAttributesCacheGenerator_U3Cget_DeepChildrenU3Ed__41_System_Collections_IEnumerator_Reset_m3F120F44D1BEB26137AF14E11165003EF8E18190,
	U3Cget_DeepChildrenU3Ed__41_t1EFE91681671B35682051A9DF5D99D8012D7DDD7_CustomAttributesCacheGenerator_U3Cget_DeepChildrenU3Ed__41_System_Collections_IEnumerator_get_Current_mF43CD05D3BF0CD3E76A265F0A684D43EFA4F22EC,
	U3Cget_DeepChildrenU3Ed__41_t1EFE91681671B35682051A9DF5D99D8012D7DDD7_CustomAttributesCacheGenerator_U3Cget_DeepChildrenU3Ed__41_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_mAD435B3462420F0485567562B0FF3F1DB60B6AF8,
	U3Cget_DeepChildrenU3Ed__41_t1EFE91681671B35682051A9DF5D99D8012D7DDD7_CustomAttributesCacheGenerator_U3Cget_DeepChildrenU3Ed__41_System_Collections_IEnumerable_GetEnumerator_m554A68924D4655591E09B01C0EFF3A9204E5E117,
	U3Cget_ChildrenU3Ed__22_t411188099DCF9174894F420F6390B7E43C48B481_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__22__ctor_m95EBCC8BC9794B6D9F4C6B5885704BF181FC9DBF,
	U3Cget_ChildrenU3Ed__22_t411188099DCF9174894F420F6390B7E43C48B481_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__22_System_IDisposable_Dispose_mCBFE04EF85659884667F66453D36323A90F4A37E,
	U3Cget_ChildrenU3Ed__22_t411188099DCF9174894F420F6390B7E43C48B481_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__22_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_mA7F43EFC69B364F48AC29BF1C37A5001990511FE,
	U3Cget_ChildrenU3Ed__22_t411188099DCF9174894F420F6390B7E43C48B481_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerator_Reset_m531EF5FB0BD16A29769CBDE11FDF26AEC2D55F5E,
	U3Cget_ChildrenU3Ed__22_t411188099DCF9174894F420F6390B7E43C48B481_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerator_get_Current_m9C20E2FAE0940D92F51C5E1C9D804E406BF2F34F,
	U3Cget_ChildrenU3Ed__22_t411188099DCF9174894F420F6390B7E43C48B481_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__22_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_m7F40779C5E4EF43C9898EFDECF0FA816A7E5DCFA,
	U3Cget_ChildrenU3Ed__22_t411188099DCF9174894F420F6390B7E43C48B481_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__22_System_Collections_IEnumerable_GetEnumerator_mCC77091552DD207B63E4B7C2B33693B4467ED68D,
	U3Cget_ChildrenU3Ed__23_t213CCC2BA3B684AD1871D45A4278367199EFBCA8_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__23__ctor_m24E00E4270F8E59C9ECF5B85CDB1A34683FC1141,
	U3Cget_ChildrenU3Ed__23_t213CCC2BA3B684AD1871D45A4278367199EFBCA8_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__23_System_IDisposable_Dispose_m413B24FB7D7B0156DFC7E240DA8391C859440847,
	U3Cget_ChildrenU3Ed__23_t213CCC2BA3B684AD1871D45A4278367199EFBCA8_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__23_System_Collections_Generic_IEnumeratorU3CSimpleJSON_JSONNodeU3E_get_Current_m226D4CC6A995605411240005C904F701A7E7D224,
	U3Cget_ChildrenU3Ed__23_t213CCC2BA3B684AD1871D45A4278367199EFBCA8_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerator_Reset_m60B145CE869947C90DB4665A23EDDF48AE63AC25,
	U3Cget_ChildrenU3Ed__23_t213CCC2BA3B684AD1871D45A4278367199EFBCA8_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerator_get_Current_m51D1A00926BFE5BDA287FE9A878C6E949F9D0DBB,
	U3Cget_ChildrenU3Ed__23_t213CCC2BA3B684AD1871D45A4278367199EFBCA8_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__23_System_Collections_Generic_IEnumerableU3CSimpleJSON_JSONNodeU3E_GetEnumerator_mBE8B0928518FD2517C5F344791F32EA64F13C45C,
	U3Cget_ChildrenU3Ed__23_t213CCC2BA3B684AD1871D45A4278367199EFBCA8_CustomAttributesCacheGenerator_U3Cget_ChildrenU3Ed__23_System_Collections_IEnumerable_GetEnumerator_m19CBB01715DEE09E679303048771E8ABC2A2D4C0,
	U3CDelayLaserPopUpU3Ed__28_tF0267396241BEFE79B23A545D65A2D3DFAE18C0D_CustomAttributesCacheGenerator_U3CDelayLaserPopUpU3Ed__28__ctor_m1DD030E25497BC7EAFFF48B12878BC3F6E42E867,
	U3CDelayLaserPopUpU3Ed__28_tF0267396241BEFE79B23A545D65A2D3DFAE18C0D_CustomAttributesCacheGenerator_U3CDelayLaserPopUpU3Ed__28_System_IDisposable_Dispose_m4ACA82D241FAA199208AD68B89AB6A6D558B6F7F,
	U3CDelayLaserPopUpU3Ed__28_tF0267396241BEFE79B23A545D65A2D3DFAE18C0D_CustomAttributesCacheGenerator_U3CDelayLaserPopUpU3Ed__28_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m2836850B7B9251D61A015650525BA37CFEE489AC,
	U3CDelayLaserPopUpU3Ed__28_tF0267396241BEFE79B23A545D65A2D3DFAE18C0D_CustomAttributesCacheGenerator_U3CDelayLaserPopUpU3Ed__28_System_Collections_IEnumerator_Reset_m418F0BB7A31FD5BEAFECE3CA6971390F3E76542F,
	U3CDelayLaserPopUpU3Ed__28_tF0267396241BEFE79B23A545D65A2D3DFAE18C0D_CustomAttributesCacheGenerator_U3CDelayLaserPopUpU3Ed__28_System_Collections_IEnumerator_get_Current_m1E4F5A5594B28D5034EFE6A5E55EEC44FC0C490B,
	U3CWaitForNoticeTimerOutU3Ed__43_t872B5C580D4D7A86DDD89BA935867637E1EDF564_CustomAttributesCacheGenerator_U3CWaitForNoticeTimerOutU3Ed__43__ctor_mC64694756F43EEF9DCC1F4101737D83C1DC8BB93,
	U3CWaitForNoticeTimerOutU3Ed__43_t872B5C580D4D7A86DDD89BA935867637E1EDF564_CustomAttributesCacheGenerator_U3CWaitForNoticeTimerOutU3Ed__43_System_IDisposable_Dispose_m01791A5F038C1BCF58742292247713EFA6C770CC,
	U3CWaitForNoticeTimerOutU3Ed__43_t872B5C580D4D7A86DDD89BA935867637E1EDF564_CustomAttributesCacheGenerator_U3CWaitForNoticeTimerOutU3Ed__43_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m03D682641A449C18CD1C00484E7509D3C8D843EC,
	U3CWaitForNoticeTimerOutU3Ed__43_t872B5C580D4D7A86DDD89BA935867637E1EDF564_CustomAttributesCacheGenerator_U3CWaitForNoticeTimerOutU3Ed__43_System_Collections_IEnumerator_Reset_mDDA5A5A3E227742BEFD8545DFC4EDA5B33430563,
	U3CWaitForNoticeTimerOutU3Ed__43_t872B5C580D4D7A86DDD89BA935867637E1EDF564_CustomAttributesCacheGenerator_U3CWaitForNoticeTimerOutU3Ed__43_System_Collections_IEnumerator_get_Current_mB7841FEE52274FEA64951C4EF021BC1825F42031,
	U3CWaitForNoticeOutU3Ed__46_tB40B7C9E84E36CF0E86D5063901513398B490775_CustomAttributesCacheGenerator_U3CWaitForNoticeOutU3Ed__46__ctor_m6F5A034947277CE289E43F8D368B15C565DA34DA,
	U3CWaitForNoticeOutU3Ed__46_tB40B7C9E84E36CF0E86D5063901513398B490775_CustomAttributesCacheGenerator_U3CWaitForNoticeOutU3Ed__46_System_IDisposable_Dispose_m5E3269113CF4303B8B8D4081E875B335EB3FEE1C,
	U3CWaitForNoticeOutU3Ed__46_tB40B7C9E84E36CF0E86D5063901513398B490775_CustomAttributesCacheGenerator_U3CWaitForNoticeOutU3Ed__46_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m389C85183678A455CB9EFF03218F4F9459269270,
	U3CWaitForNoticeOutU3Ed__46_tB40B7C9E84E36CF0E86D5063901513398B490775_CustomAttributesCacheGenerator_U3CWaitForNoticeOutU3Ed__46_System_Collections_IEnumerator_Reset_m84D84F466AE4D9EC09F0827B66E1C4085133898A,
	U3CWaitForNoticeOutU3Ed__46_tB40B7C9E84E36CF0E86D5063901513398B490775_CustomAttributesCacheGenerator_U3CWaitForNoticeOutU3Ed__46_System_Collections_IEnumerator_get_Current_mE2FCDD664F3D5710AF71599273562A7143A1B9E9,
	U3CNextEnemyU3Ed__26_t4F6CB5EE30FB1BC45025C47422CA0BA6F4CFBA05_CustomAttributesCacheGenerator_U3CNextEnemyU3Ed__26__ctor_mF7F906DB2AE8D88AD0A7E5231306EFF4CD401EE0,
	U3CNextEnemyU3Ed__26_t4F6CB5EE30FB1BC45025C47422CA0BA6F4CFBA05_CustomAttributesCacheGenerator_U3CNextEnemyU3Ed__26_System_IDisposable_Dispose_m51376AE841CDA01FEDF72193251CD600592192CA,
	U3CNextEnemyU3Ed__26_t4F6CB5EE30FB1BC45025C47422CA0BA6F4CFBA05_CustomAttributesCacheGenerator_U3CNextEnemyU3Ed__26_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0644F6F37C2E4EB196F8C3EE1EAE560375A86B61,
	U3CNextEnemyU3Ed__26_t4F6CB5EE30FB1BC45025C47422CA0BA6F4CFBA05_CustomAttributesCacheGenerator_U3CNextEnemyU3Ed__26_System_Collections_IEnumerator_Reset_mC1135533A36ACAF17E80743D6142CD29C926476B,
	U3CNextEnemyU3Ed__26_t4F6CB5EE30FB1BC45025C47422CA0BA6F4CFBA05_CustomAttributesCacheGenerator_U3CNextEnemyU3Ed__26_System_Collections_IEnumerator_get_Current_m2FF04E7D947B7C1A1ABCB509822D255ADEE518EC,
	U3CNextHeroU3Ed__27_tA3FA225A9F245C060D7D1285EC0ED7F01B031AA0_CustomAttributesCacheGenerator_U3CNextHeroU3Ed__27__ctor_m223862A8FD430B9194B60EB590B188E6761543A9,
	U3CNextHeroU3Ed__27_tA3FA225A9F245C060D7D1285EC0ED7F01B031AA0_CustomAttributesCacheGenerator_U3CNextHeroU3Ed__27_System_IDisposable_Dispose_m5E5704CA63BBBD3107B59C4A610ABEFBE5175BEC,
	U3CNextHeroU3Ed__27_tA3FA225A9F245C060D7D1285EC0ED7F01B031AA0_CustomAttributesCacheGenerator_U3CNextHeroU3Ed__27_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m0AF8B7EC64336EA9C996CD13B146749D3EA9816C,
	U3CNextHeroU3Ed__27_tA3FA225A9F245C060D7D1285EC0ED7F01B031AA0_CustomAttributesCacheGenerator_U3CNextHeroU3Ed__27_System_Collections_IEnumerator_Reset_mADB82B690752F5485FD704C02A3BBFF637267083,
	U3CNextHeroU3Ed__27_tA3FA225A9F245C060D7D1285EC0ED7F01B031AA0_CustomAttributesCacheGenerator_U3CNextHeroU3Ed__27_System_Collections_IEnumerator_get_Current_m94202AE088D04C101BF4F74AB5F76AD0286EA924,
	U3CDisableInfoU3Ed__33_t6EB1180E7648EE0413BB51BA7BFB27F30B17C577_CustomAttributesCacheGenerator_U3CDisableInfoU3Ed__33__ctor_m60DCAC3ED8F8B0CC7BCBE7E03BABF63357A6FA6D,
	U3CDisableInfoU3Ed__33_t6EB1180E7648EE0413BB51BA7BFB27F30B17C577_CustomAttributesCacheGenerator_U3CDisableInfoU3Ed__33_System_IDisposable_Dispose_m932AC83042484FA097910F368C56EC1CC724865F,
	U3CDisableInfoU3Ed__33_t6EB1180E7648EE0413BB51BA7BFB27F30B17C577_CustomAttributesCacheGenerator_U3CDisableInfoU3Ed__33_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m370655C1AD58BE840BC850118005790511801227,
	U3CDisableInfoU3Ed__33_t6EB1180E7648EE0413BB51BA7BFB27F30B17C577_CustomAttributesCacheGenerator_U3CDisableInfoU3Ed__33_System_Collections_IEnumerator_Reset_m028A5C860CB4B798DDF150CFF80F4E64A1F419E9,
	U3CDisableInfoU3Ed__33_t6EB1180E7648EE0413BB51BA7BFB27F30B17C577_CustomAttributesCacheGenerator_U3CDisableInfoU3Ed__33_System_Collections_IEnumerator_get_Current_mC802088660CAAE6D1999E1F89950C8372657FF82,
	U3CFireWorkClickedU3Ed__34_t0A3C43FA8003F5D86FE232DD5D91E4F1EF7CFE24_CustomAttributesCacheGenerator_U3CFireWorkClickedU3Ed__34__ctor_m7A1B53D72F6553C8AEF3C2D73B1DC7C5E9E56B91,
	U3CFireWorkClickedU3Ed__34_t0A3C43FA8003F5D86FE232DD5D91E4F1EF7CFE24_CustomAttributesCacheGenerator_U3CFireWorkClickedU3Ed__34_System_IDisposable_Dispose_m46CCAE3CD88625D4F9544A098BCD88E0AB58DD9A,
	U3CFireWorkClickedU3Ed__34_t0A3C43FA8003F5D86FE232DD5D91E4F1EF7CFE24_CustomAttributesCacheGenerator_U3CFireWorkClickedU3Ed__34_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mF91C50E982052F7A84C122811F02FB459B0E63E1,
	U3CFireWorkClickedU3Ed__34_t0A3C43FA8003F5D86FE232DD5D91E4F1EF7CFE24_CustomAttributesCacheGenerator_U3CFireWorkClickedU3Ed__34_System_Collections_IEnumerator_Reset_mFCCA5DE1786D2F6C82570827018FC03D4E1773F8,
	U3CFireWorkClickedU3Ed__34_t0A3C43FA8003F5D86FE232DD5D91E4F1EF7CFE24_CustomAttributesCacheGenerator_U3CFireWorkClickedU3Ed__34_System_Collections_IEnumerator_get_Current_mD0C5FEE89BAAAEFDD74726E93B4C290812B0764C,
	U3CCookieClickedU3Ed__35_tDA1263DFFDD0773D381B1D787122AEEABE86AF9C_CustomAttributesCacheGenerator_U3CCookieClickedU3Ed__35__ctor_m0B575DE0D60FB548252B06CE74082E1EBDF42D57,
	U3CCookieClickedU3Ed__35_tDA1263DFFDD0773D381B1D787122AEEABE86AF9C_CustomAttributesCacheGenerator_U3CCookieClickedU3Ed__35_System_IDisposable_Dispose_mA29E952E03BC1A952D9B75AF867842C8C285DADA,
	U3CCookieClickedU3Ed__35_tDA1263DFFDD0773D381B1D787122AEEABE86AF9C_CustomAttributesCacheGenerator_U3CCookieClickedU3Ed__35_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_mB2959A148D9B6ECFE069BB4DA16D939690F29580,
	U3CCookieClickedU3Ed__35_tDA1263DFFDD0773D381B1D787122AEEABE86AF9C_CustomAttributesCacheGenerator_U3CCookieClickedU3Ed__35_System_Collections_IEnumerator_Reset_m26D0EA370FA27109A9812968AF7E2C4D9C53C38E,
	U3CCookieClickedU3Ed__35_tDA1263DFFDD0773D381B1D787122AEEABE86AF9C_CustomAttributesCacheGenerator_U3CCookieClickedU3Ed__35_System_Collections_IEnumerator_get_Current_mD356438C0477B5CAC2CE60DCE85ACC6FAFA10FEC,
	U3CCandyClickedU3Ed__36_tCA313AA216D873516B3C1A392A0302BBB97B86B0_CustomAttributesCacheGenerator_U3CCandyClickedU3Ed__36__ctor_m4A8D08E3E4BCF74EC885E69C8CC1CF1F0F4AACD4,
	U3CCandyClickedU3Ed__36_tCA313AA216D873516B3C1A392A0302BBB97B86B0_CustomAttributesCacheGenerator_U3CCandyClickedU3Ed__36_System_IDisposable_Dispose_m5A18D55FE5C43EE1DD366017D649C02BB71BEA7B,
	U3CCandyClickedU3Ed__36_tCA313AA216D873516B3C1A392A0302BBB97B86B0_CustomAttributesCacheGenerator_U3CCandyClickedU3Ed__36_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m35E28353074F1165B229CE56346CB372606A7FBC,
	U3CCandyClickedU3Ed__36_tCA313AA216D873516B3C1A392A0302BBB97B86B0_CustomAttributesCacheGenerator_U3CCandyClickedU3Ed__36_System_Collections_IEnumerator_Reset_m85D7FF94205CA8A2640EB611D6CECFC4FFBF9DB7,
	U3CCandyClickedU3Ed__36_tCA313AA216D873516B3C1A392A0302BBB97B86B0_CustomAttributesCacheGenerator_U3CCandyClickedU3Ed__36_System_Collections_IEnumerator_get_Current_m8AE9C3DD85D994C5A0EACACEC1365F49104F17AC,
	U3CScreenShotClickedU3Ed__37_t982CA263942B0C8A08EC834CA778F5356084D169_CustomAttributesCacheGenerator_U3CScreenShotClickedU3Ed__37__ctor_m4DF76FA6E783CE5C95EC9174F493FE6E32391324,
	U3CScreenShotClickedU3Ed__37_t982CA263942B0C8A08EC834CA778F5356084D169_CustomAttributesCacheGenerator_U3CScreenShotClickedU3Ed__37_System_IDisposable_Dispose_mF247A4A440E9BCC6723DBD6F4829A6A85901EC04,
	U3CScreenShotClickedU3Ed__37_t982CA263942B0C8A08EC834CA778F5356084D169_CustomAttributesCacheGenerator_U3CScreenShotClickedU3Ed__37_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m672E78976F63D8246C55DFA344FBD7F0B5E7062B,
	U3CScreenShotClickedU3Ed__37_t982CA263942B0C8A08EC834CA778F5356084D169_CustomAttributesCacheGenerator_U3CScreenShotClickedU3Ed__37_System_Collections_IEnumerator_Reset_m4926A46FCFF998299A791CD8D19835398F2D0851,
	U3CScreenShotClickedU3Ed__37_t982CA263942B0C8A08EC834CA778F5356084D169_CustomAttributesCacheGenerator_U3CScreenShotClickedU3Ed__37_System_Collections_IEnumerator_get_Current_m2A8F29B42E02863B6334D1099147F59F611D35AA,
	U3CLeaderBoardClickedU3Ed__38_tBAE695B5560CCB49D1653EAD612C1CE08B42F361_CustomAttributesCacheGenerator_U3CLeaderBoardClickedU3Ed__38__ctor_mE7E26DB8380A3579B64BC586E0253B6F18867F6F,
	U3CLeaderBoardClickedU3Ed__38_tBAE695B5560CCB49D1653EAD612C1CE08B42F361_CustomAttributesCacheGenerator_U3CLeaderBoardClickedU3Ed__38_System_IDisposable_Dispose_m7CB383587DF40BD43652C5FC78C26B604462A95A,
	U3CLeaderBoardClickedU3Ed__38_tBAE695B5560CCB49D1653EAD612C1CE08B42F361_CustomAttributesCacheGenerator_U3CLeaderBoardClickedU3Ed__38_System_Collections_Generic_IEnumeratorU3CSystem_ObjectU3E_get_Current_m9EC830344B8F7C475D00C335A2411922FFD4E25C,
	U3CLeaderBoardClickedU3Ed__38_tBAE695B5560CCB49D1653EAD612C1CE08B42F361_CustomAttributesCacheGenerator_U3CLeaderBoardClickedU3Ed__38_System_Collections_IEnumerator_Reset_m576E4DED7EC5D84BDEEFDDB2EE94C963C2915406,
	U3CLeaderBoardClickedU3Ed__38_tBAE695B5560CCB49D1653EAD612C1CE08B42F361_CustomAttributesCacheGenerator_U3CLeaderBoardClickedU3Ed__38_System_Collections_IEnumerator_get_Current_m5C0AFE5F19E6B3B18AF2CAB6BA66B991D14EF762,
	AssemblyU2DCSharp_CustomAttributesCacheGenerator,
};
IL2CPP_MANAGED_FORCE_INLINE IL2CPP_METHOD_ATTR void RuntimeCompatibilityAttribute_set_WrapNonExceptionThrows_m8562196F90F3EBCEC23B5708EE0332842883C490_inline (RuntimeCompatibilityAttribute_tFF99AB2963098F9CBCD47A20D9FD3D51C17C1C80 * __this, bool ___value0, const RuntimeMethod* method)
{
	{
		bool L_0 = ___value0;
		__this->set_m_wrapNonExceptionThrows_0(L_0);
		return;
	}
}
