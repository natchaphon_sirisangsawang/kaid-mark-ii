﻿#include "pch-c.h"
#ifndef _MSC_VER
# include <alloca.h>
#else
# include <malloc.h>
#endif


#include "codegen/il2cpp-codegen-metadata.h"





// 0x00000001 System.Void UnityEngine.Advertisements.Advertisement::.cctor()
extern void Advertisement__cctor_mEB30FD458B55E90811E113F894F17BADF3ED8658 (void);
// 0x00000002 System.Boolean UnityEngine.Advertisements.Advertisement::get_isInitialized()
extern void Advertisement_get_isInitialized_mBC2B086E4633C893C4EED6FBDC1AB0615CD1B33A (void);
// 0x00000003 System.Boolean UnityEngine.Advertisements.Advertisement::get_isSupported()
extern void Advertisement_get_isSupported_m4ECFA817DC2CBFBF90A66AE629C38396E75B925E (void);
// 0x00000004 System.Boolean UnityEngine.Advertisements.Advertisement::get_debugMode()
extern void Advertisement_get_debugMode_m316718060F88EC9B2B64EA9E853D7E87A965881B (void);
// 0x00000005 System.Void UnityEngine.Advertisements.Advertisement::set_debugMode(System.Boolean)
extern void Advertisement_set_debugMode_mE8C7E59B822ABF1F8DC7ECA7244F61A2A77558E6 (void);
// 0x00000006 System.String UnityEngine.Advertisements.Advertisement::get_version()
extern void Advertisement_get_version_m0988FB62997EB8938B580B5EE3D37CDDFAEBAFA7 (void);
// 0x00000007 System.Boolean UnityEngine.Advertisements.Advertisement::get_isShowing()
extern void Advertisement_get_isShowing_m37CBCF24417233A4B6BD625A523CD849878F854F (void);
// 0x00000008 System.Void UnityEngine.Advertisements.Advertisement::Initialize(System.String)
extern void Advertisement_Initialize_m082D5F19215B919660A314D4E72A7ED61AF8F704 (void);
// 0x00000009 System.Void UnityEngine.Advertisements.Advertisement::Initialize(System.String,System.Boolean)
extern void Advertisement_Initialize_mD4FCDAE8D46A87C062B95FDA009085530825A66E (void);
// 0x0000000A System.Void UnityEngine.Advertisements.Advertisement::Initialize(System.String,System.Boolean,System.Boolean)
extern void Advertisement_Initialize_mBF05AEF7A6C7E63B9111EB57E266A26E314F9C15 (void);
// 0x0000000B System.Void UnityEngine.Advertisements.Advertisement::Initialize(System.String,System.Boolean,System.Boolean,UnityEngine.Advertisements.IUnityAdsInitializationListener)
extern void Advertisement_Initialize_m0FD976ECEA4D74BE3D60C60E6CCAB3027959C1AA (void);
// 0x0000000C System.Boolean UnityEngine.Advertisements.Advertisement::IsReady()
extern void Advertisement_IsReady_mFDE54C7D1C9D98D205C7DC5BF16E36E7041401C3 (void);
// 0x0000000D System.Boolean UnityEngine.Advertisements.Advertisement::IsReady(System.String)
extern void Advertisement_IsReady_m7C3F32AA27ECF25EBED4FA413C77BFEF7B3DD858 (void);
// 0x0000000E System.Void UnityEngine.Advertisements.Advertisement::Load(System.String)
extern void Advertisement_Load_m2E6903F8D053377A617C356FB804FE8F8BB71756 (void);
// 0x0000000F System.Void UnityEngine.Advertisements.Advertisement::Load(System.String,UnityEngine.Advertisements.IUnityAdsLoadListener)
extern void Advertisement_Load_mA1912CED23684003576EC04DD6E14E06E1E47387 (void);
// 0x00000010 System.Void UnityEngine.Advertisements.Advertisement::Show()
extern void Advertisement_Show_m138EDDD0BF8B3C822671234133F31C237E2F0988 (void);
// 0x00000011 System.Void UnityEngine.Advertisements.Advertisement::Show(UnityEngine.Advertisements.ShowOptions)
extern void Advertisement_Show_m4809AB1F96CC586C69AE6C6C94E036DEE956D670 (void);
// 0x00000012 System.Void UnityEngine.Advertisements.Advertisement::Show(System.String)
extern void Advertisement_Show_m72C78A977F924A61DAF5B5D92D88200DE9F9C98B (void);
// 0x00000013 System.Void UnityEngine.Advertisements.Advertisement::Show(System.String,UnityEngine.Advertisements.ShowOptions)
extern void Advertisement_Show_m5ACBA6D0BF333D43B7EC3B7C36F8E55A10741C1D (void);
// 0x00000014 System.Void UnityEngine.Advertisements.Advertisement::Show(System.String,UnityEngine.Advertisements.IUnityAdsShowListener)
extern void Advertisement_Show_m59789CD39DAC86DA1171BD01ABC0821AE1164D3D (void);
// 0x00000015 System.Void UnityEngine.Advertisements.Advertisement::Show(System.String,UnityEngine.Advertisements.ShowOptions,UnityEngine.Advertisements.IUnityAdsShowListener)
extern void Advertisement_Show_m51879852E416A638DC3789BBF67E36B33E2305D7 (void);
// 0x00000016 System.Void UnityEngine.Advertisements.Advertisement::SetMetaData(UnityEngine.Advertisements.MetaData)
extern void Advertisement_SetMetaData_mEDD78DDB6A5FB827E6060F446E485B67562BF7E1 (void);
// 0x00000017 System.Void UnityEngine.Advertisements.Advertisement::AddListener(UnityEngine.Advertisements.IUnityAdsListener)
extern void Advertisement_AddListener_mF100E2DDC96B1395807E4B43F7FE1159F7FAE76C (void);
// 0x00000018 System.Void UnityEngine.Advertisements.Advertisement::RemoveListener(UnityEngine.Advertisements.IUnityAdsListener)
extern void Advertisement_RemoveListener_mCBBCDA53A28FF8BD03BB168F6AEF5703D70BD3CC (void);
// 0x00000019 UnityEngine.Advertisements.PlacementState UnityEngine.Advertisements.Advertisement::GetPlacementState()
extern void Advertisement_GetPlacementState_mA1A327D28199EBA054FD361E22FF6AF2FBE7CDBE (void);
// 0x0000001A UnityEngine.Advertisements.PlacementState UnityEngine.Advertisements.Advertisement::GetPlacementState(System.String)
extern void Advertisement_GetPlacementState_m0EBA0959072DBF04306C5C75BDE4A7C2E44E3C05 (void);
// 0x0000001B UnityEngine.Advertisements.Platform.IPlatform UnityEngine.Advertisements.Advertisement::CreatePlatform()
extern void Advertisement_CreatePlatform_m7E8486F406293DCD2AA0A3690A6B2FD481BB5C63 (void);
// 0x0000001C System.Boolean UnityEngine.Advertisements.Advertisement::IsSupported()
extern void Advertisement_IsSupported_mE13C31757D843704A28EBD64C1878F9E359C6FD0 (void);
// 0x0000001D UnityEngine.Advertisements.Utilities.IUnityLifecycleManager UnityEngine.Advertisements.Banner::get_UnityLifecycleManager()
extern void Banner_get_UnityLifecycleManager_m8D1890BD72B78FB9DFF2E5351A146882DC4EE675 (void);
// 0x0000001E System.Boolean UnityEngine.Advertisements.Banner::get_IsLoaded()
extern void Banner_get_IsLoaded_m9381D3F941EBA8E7071C46ED56A3057CFAFFC0A2 (void);
// 0x0000001F System.Boolean UnityEngine.Advertisements.Banner::get_ShowAfterLoad()
extern void Banner_get_ShowAfterLoad_m0149C039231E658B625C6DBA0AE981B1111C9F09 (void);
// 0x00000020 System.Void UnityEngine.Advertisements.Banner::set_ShowAfterLoad(System.Boolean)
extern void Banner_set_ShowAfterLoad_m645CDCDB38358BFCE6030CD3064140CA287F4794 (void);
// 0x00000021 System.Void UnityEngine.Advertisements.Banner::.ctor(UnityEngine.Advertisements.INativeBanner,UnityEngine.Advertisements.Utilities.IUnityLifecycleManager)
extern void Banner__ctor_m5FA80FC5D6F4028AFDBBBAA5DD18517A53A3CD81 (void);
// 0x00000022 System.Void UnityEngine.Advertisements.Banner::Load(System.String,UnityEngine.Advertisements.BannerLoadOptions)
extern void Banner_Load_m7FB027911B94845C9AB4E5EEFEB2D98C3CC18544 (void);
// 0x00000023 System.Void UnityEngine.Advertisements.Banner::Show(System.String,UnityEngine.Advertisements.BannerOptions)
extern void Banner_Show_m92C480622ECB96A9EEC110B9C6D9B69618C01913 (void);
// 0x00000024 System.Void UnityEngine.Advertisements.Banner::Hide(System.Boolean)
extern void Banner_Hide_m46D677B526C45AADF7C18E7311EA7988271DBF34 (void);
// 0x00000025 System.Void UnityEngine.Advertisements.Banner::SetPosition(UnityEngine.Advertisements.BannerPosition)
extern void Banner_SetPosition_m796058CCB4891D57CD4B1668A60566E11E296B0D (void);
// 0x00000026 System.Void UnityEngine.Advertisements.Banner::UnityAdsBannerDidShow(System.String,UnityEngine.Advertisements.BannerOptions)
extern void Banner_UnityAdsBannerDidShow_m8A09CD159A21B06FFD8D8734F13E2DE04929D2CB (void);
// 0x00000027 System.Void UnityEngine.Advertisements.Banner::UnityAdsBannerDidHide(System.String,UnityEngine.Advertisements.BannerOptions)
extern void Banner_UnityAdsBannerDidHide_m64A40CE58299FEB1EC2AF5B8E8DD5587701A0CBF (void);
// 0x00000028 System.Void UnityEngine.Advertisements.Banner::UnityAdsBannerClick(System.String,UnityEngine.Advertisements.BannerOptions)
extern void Banner_UnityAdsBannerClick_m589DEF392D6EA31211B7D152F200DC82EBECB951 (void);
// 0x00000029 System.Void UnityEngine.Advertisements.Banner::UnityAdsBannerDidLoad(System.String,UnityEngine.Advertisements.BannerLoadOptions)
extern void Banner_UnityAdsBannerDidLoad_m67E46A4082BDB8E38BF6BB671DDB62FE506BBC6D (void);
// 0x0000002A System.Void UnityEngine.Advertisements.Banner::UnityAdsBannerDidError(System.String,UnityEngine.Advertisements.BannerLoadOptions)
extern void Banner_UnityAdsBannerDidError_mBAEDDC9BEEE536C436B8F2870883D011E09EFFD7 (void);
// 0x0000002B UnityEngine.Advertisements.BannerLoadOptions/LoadCallback UnityEngine.Advertisements.BannerLoadOptions::get_loadCallback()
extern void BannerLoadOptions_get_loadCallback_mAB4AE91F6982F5C4E06F2168C9F3D26BD6906578 (void);
// 0x0000002C System.Void UnityEngine.Advertisements.BannerLoadOptions::set_loadCallback(UnityEngine.Advertisements.BannerLoadOptions/LoadCallback)
extern void BannerLoadOptions_set_loadCallback_mF981A9D9527258C8F1524F4AAE178C0786B6518E (void);
// 0x0000002D UnityEngine.Advertisements.BannerLoadOptions/ErrorCallback UnityEngine.Advertisements.BannerLoadOptions::get_errorCallback()
extern void BannerLoadOptions_get_errorCallback_m53924413EB917C85AABAD9B66FDEC9C84F08A4EB (void);
// 0x0000002E System.Void UnityEngine.Advertisements.BannerLoadOptions::set_errorCallback(UnityEngine.Advertisements.BannerLoadOptions/ErrorCallback)
extern void BannerLoadOptions_set_errorCallback_mD8598AF5C0A7BE058286EEB566E5912B6AC7D6B1 (void);
// 0x0000002F System.Void UnityEngine.Advertisements.BannerLoadOptions::.ctor()
extern void BannerLoadOptions__ctor_m46ACB4C63B65D85BCB79D0B5FDA716E3A6957B04 (void);
// 0x00000030 UnityEngine.Advertisements.BannerOptions/BannerCallback UnityEngine.Advertisements.BannerOptions::get_showCallback()
extern void BannerOptions_get_showCallback_m3EC0B33B8E5D4E959BE9572B3F57A549E8331C2F (void);
// 0x00000031 System.Void UnityEngine.Advertisements.BannerOptions::set_showCallback(UnityEngine.Advertisements.BannerOptions/BannerCallback)
extern void BannerOptions_set_showCallback_mE32BBAD1A431BD6DB60AFDEA2828BDA3BAD75045 (void);
// 0x00000032 UnityEngine.Advertisements.BannerOptions/BannerCallback UnityEngine.Advertisements.BannerOptions::get_hideCallback()
extern void BannerOptions_get_hideCallback_m8FA965F59B65278AF69BFEB39B2FEDB17A511FE9 (void);
// 0x00000033 System.Void UnityEngine.Advertisements.BannerOptions::set_hideCallback(UnityEngine.Advertisements.BannerOptions/BannerCallback)
extern void BannerOptions_set_hideCallback_m5F492B1009CDC83A26AC89B24A74CE6349B0E716 (void);
// 0x00000034 UnityEngine.Advertisements.BannerOptions/BannerCallback UnityEngine.Advertisements.BannerOptions::get_clickCallback()
extern void BannerOptions_get_clickCallback_mC79C625FB89086CF9C4DE112EF464D207B35D197 (void);
// 0x00000035 System.Void UnityEngine.Advertisements.BannerOptions::set_clickCallback(UnityEngine.Advertisements.BannerOptions/BannerCallback)
extern void BannerOptions_set_clickCallback_m4919E420A2CCFCF1321E8BC6D4141B7386999134 (void);
// 0x00000036 System.Void UnityEngine.Advertisements.BannerOptions::.ctor()
extern void BannerOptions__ctor_mA3423377C564DF63B24BD7217C157639E4013218 (void);
// 0x00000037 System.Boolean UnityEngine.Advertisements.Configuration::get_enabled()
extern void Configuration_get_enabled_m7BE1C8F2EA4B6C3B9CD8E821C999BF6BA1F3E982 (void);
// 0x00000038 System.String UnityEngine.Advertisements.Configuration::get_defaultPlacement()
extern void Configuration_get_defaultPlacement_mACD234B00B1A8445668BA25D92E91142F21B9B65 (void);
// 0x00000039 System.Collections.Generic.Dictionary`2<System.String,System.Boolean> UnityEngine.Advertisements.Configuration::get_placements()
extern void Configuration_get_placements_m6787C062082EEABCEE45A1B809819650C2090BF4 (void);
// 0x0000003A System.Void UnityEngine.Advertisements.Configuration::.ctor(System.String)
extern void Configuration__ctor_mFE8855E479EC18DDF7612712DD5D2A6E83A5430E (void);
// 0x0000003B UnityEngine.Advertisements.Utilities.IUnityLifecycleManager UnityEngine.Advertisements.IBanner::get_UnityLifecycleManager()
// 0x0000003C System.Boolean UnityEngine.Advertisements.IBanner::get_IsLoaded()
// 0x0000003D System.Boolean UnityEngine.Advertisements.IBanner::get_ShowAfterLoad()
// 0x0000003E System.Void UnityEngine.Advertisements.IBanner::set_ShowAfterLoad(System.Boolean)
// 0x0000003F System.Void UnityEngine.Advertisements.IBanner::Load(System.String,UnityEngine.Advertisements.BannerLoadOptions)
// 0x00000040 System.Void UnityEngine.Advertisements.IBanner::Show(System.String,UnityEngine.Advertisements.BannerOptions)
// 0x00000041 System.Void UnityEngine.Advertisements.IBanner::Hide(System.Boolean)
// 0x00000042 System.Void UnityEngine.Advertisements.IBanner::SetPosition(UnityEngine.Advertisements.BannerPosition)
// 0x00000043 System.Void UnityEngine.Advertisements.IBanner::UnityAdsBannerDidShow(System.String,UnityEngine.Advertisements.BannerOptions)
// 0x00000044 System.Void UnityEngine.Advertisements.IBanner::UnityAdsBannerDidHide(System.String,UnityEngine.Advertisements.BannerOptions)
// 0x00000045 System.Void UnityEngine.Advertisements.IBanner::UnityAdsBannerClick(System.String,UnityEngine.Advertisements.BannerOptions)
// 0x00000046 System.Void UnityEngine.Advertisements.IBanner::UnityAdsBannerDidLoad(System.String,UnityEngine.Advertisements.BannerLoadOptions)
// 0x00000047 System.Void UnityEngine.Advertisements.IBanner::UnityAdsBannerDidError(System.String,UnityEngine.Advertisements.BannerLoadOptions)
// 0x00000048 System.Boolean UnityEngine.Advertisements.INativeBanner::get_IsLoaded()
// 0x00000049 System.Void UnityEngine.Advertisements.INativeBanner::SetupBanner(UnityEngine.Advertisements.IBanner)
// 0x0000004A System.Void UnityEngine.Advertisements.INativeBanner::Load(System.String,UnityEngine.Advertisements.BannerLoadOptions)
// 0x0000004B System.Void UnityEngine.Advertisements.INativeBanner::Show(System.String,UnityEngine.Advertisements.BannerOptions)
// 0x0000004C System.Void UnityEngine.Advertisements.INativeBanner::Hide(System.Boolean)
// 0x0000004D System.Void UnityEngine.Advertisements.INativeBanner::SetPosition(UnityEngine.Advertisements.BannerPosition)
// 0x0000004E System.Void UnityEngine.Advertisements.IUnityAdsListener::OnUnityAdsReady(System.String)
// 0x0000004F System.Void UnityEngine.Advertisements.IUnityAdsListener::OnUnityAdsDidError(System.String)
// 0x00000050 System.Void UnityEngine.Advertisements.IUnityAdsListener::OnUnityAdsDidStart(System.String)
// 0x00000051 System.Void UnityEngine.Advertisements.IUnityAdsListener::OnUnityAdsDidFinish(System.String,UnityEngine.Advertisements.ShowResult)
// 0x00000052 System.Void UnityEngine.Advertisements.IUnityAdsInitializationListener::OnInitializationComplete()
// 0x00000053 System.Void UnityEngine.Advertisements.IUnityAdsInitializationListener::OnInitializationFailed(UnityEngine.Advertisements.UnityAdsInitializationError,System.String)
// 0x00000054 System.Void UnityEngine.Advertisements.IUnityAdsLoadListener::OnUnityAdsAdLoaded(System.String)
// 0x00000055 System.Void UnityEngine.Advertisements.IUnityAdsLoadListener::OnUnityAdsFailedToLoad(System.String,UnityEngine.Advertisements.UnityAdsLoadError,System.String)
// 0x00000056 System.Void UnityEngine.Advertisements.IUnityAdsShowListener::OnUnityAdsShowFailure(System.String,UnityEngine.Advertisements.UnityAdsShowError,System.String)
// 0x00000057 System.Void UnityEngine.Advertisements.IUnityAdsShowListener::OnUnityAdsShowStart(System.String)
// 0x00000058 System.Void UnityEngine.Advertisements.IUnityAdsShowListener::OnUnityAdsShowClick(System.String)
// 0x00000059 System.Void UnityEngine.Advertisements.IUnityAdsShowListener::OnUnityAdsShowComplete(System.String,UnityEngine.Advertisements.UnityAdsShowCompletionState)
// 0x0000005A System.String UnityEngine.Advertisements.MetaData::get_category()
extern void MetaData_get_category_m88A7B16198B9F13FDE4BFA0D81B3476E245CEE82 (void);
// 0x0000005B System.Void UnityEngine.Advertisements.MetaData::set_category(System.String)
extern void MetaData_set_category_mCF262A667D874F3A5B758420FC6CE01521AAA300 (void);
// 0x0000005C System.Void UnityEngine.Advertisements.MetaData::.ctor(System.String)
extern void MetaData__ctor_mCC80A9A64EEEC5340746FEB1B2CF787F3891911B (void);
// 0x0000005D System.Void UnityEngine.Advertisements.MetaData::Set(System.String,System.Object)
extern void MetaData_Set_m997F03F70DE2BF19AE97002EDE047619CBD52C5C (void);
// 0x0000005E System.Object UnityEngine.Advertisements.MetaData::Get(System.String)
extern void MetaData_Get_m10178F85D09B26C16A9A086F8C947503368D7820 (void);
// 0x0000005F System.Collections.Generic.IDictionary`2<System.String,System.Object> UnityEngine.Advertisements.MetaData::Values()
extern void MetaData_Values_mD6CC2222CE2B1A863DA664E9678694229CB5B108 (void);
// 0x00000060 System.String UnityEngine.Advertisements.MetaData::ToJSON()
extern void MetaData_ToJSON_mC4791A88E21E2F982DE6C98C10C7588E4C4B0CD4 (void);
// 0x00000061 System.Void UnityEngine.Advertisements.AndroidInitializationListener::.ctor(UnityEngine.Advertisements.Platform.IPlatform,UnityEngine.Advertisements.IUnityAdsInitializationListener)
extern void AndroidInitializationListener__ctor_mC9528623EB1956439EA549F8979E10E524C422D9 (void);
// 0x00000062 System.Void UnityEngine.Advertisements.AndroidInitializationListener::onInitializationComplete()
extern void AndroidInitializationListener_onInitializationComplete_mB8E550641FAC0595B1CD802F9AA3C17291FA1343 (void);
// 0x00000063 System.Void UnityEngine.Advertisements.AndroidInitializationListener::onInitializationFailed(UnityEngine.AndroidJavaObject,System.String)
extern void AndroidInitializationListener_onInitializationFailed_m86A8A224EEA9892E268E20CAA1865D2004C6B8B4 (void);
// 0x00000064 System.Void UnityEngine.Advertisements.AndroidLoadListener::.ctor(UnityEngine.Advertisements.Platform.IPlatform,UnityEngine.Advertisements.IUnityAdsLoadListener)
extern void AndroidLoadListener__ctor_m95C4131257DD23B962143C5439DB5F0E27798F4A (void);
// 0x00000065 System.Void UnityEngine.Advertisements.AndroidLoadListener::onUnityAdsAdLoaded(System.String)
extern void AndroidLoadListener_onUnityAdsAdLoaded_m355BCCCFCE9A4FF7A4E022D8CE91B8CB8AA85325 (void);
// 0x00000066 System.Void UnityEngine.Advertisements.AndroidLoadListener::onUnityAdsFailedToLoad(System.String,UnityEngine.AndroidJavaObject,System.String)
extern void AndroidLoadListener_onUnityAdsFailedToLoad_m63DB5EBB1A54CBF0340F2B65B651EF40847CBA3D (void);
// 0x00000067 System.Void UnityEngine.Advertisements.AndroidShowListener::.ctor(UnityEngine.Advertisements.Platform.IPlatform,UnityEngine.Advertisements.IUnityAdsShowListener)
extern void AndroidShowListener__ctor_m255EBE9F98451576C0B56791B38F67D12E97671A (void);
// 0x00000068 System.Void UnityEngine.Advertisements.AndroidShowListener::onUnityAdsShowFailure(System.String,UnityEngine.AndroidJavaObject,System.String)
extern void AndroidShowListener_onUnityAdsShowFailure_m9F6D9564DB55A7038BC5D7DF175F98EC93A3782C (void);
// 0x00000069 System.Void UnityEngine.Advertisements.AndroidShowListener::onUnityAdsShowStart(System.String)
extern void AndroidShowListener_onUnityAdsShowStart_m074670050BEB5F88E8E343C5DC3E54E5CD533E55 (void);
// 0x0000006A System.Void UnityEngine.Advertisements.AndroidShowListener::onUnityAdsShowClick(System.String)
extern void AndroidShowListener_onUnityAdsShowClick_m0EA7453D92BD3D04FB38306FAC490FC2BE4B1197 (void);
// 0x0000006B System.Void UnityEngine.Advertisements.AndroidShowListener::onUnityAdsShowComplete(System.String,UnityEngine.AndroidJavaObject)
extern void AndroidShowListener_onUnityAdsShowComplete_m14096B1BB0D484F6DAA8070BB761B5A2C1D084CF (void);
// 0x0000006C System.Void UnityEngine.Advertisements.INativePlatform::SetupPlatform(UnityEngine.Advertisements.Platform.IPlatform)
// 0x0000006D System.Void UnityEngine.Advertisements.INativePlatform::Initialize(System.String,System.Boolean,System.Boolean,UnityEngine.Advertisements.IUnityAdsInitializationListener)
// 0x0000006E System.Void UnityEngine.Advertisements.INativePlatform::Load(System.String,UnityEngine.Advertisements.IUnityAdsLoadListener)
// 0x0000006F System.Void UnityEngine.Advertisements.INativePlatform::Show(System.String,UnityEngine.Advertisements.IUnityAdsShowListener)
// 0x00000070 System.Void UnityEngine.Advertisements.INativePlatform::SetMetaData(UnityEngine.Advertisements.MetaData)
// 0x00000071 System.Boolean UnityEngine.Advertisements.INativePlatform::GetDebugMode()
// 0x00000072 System.Void UnityEngine.Advertisements.INativePlatform::SetDebugMode(System.Boolean)
// 0x00000073 System.String UnityEngine.Advertisements.INativePlatform::GetVersion()
// 0x00000074 System.Boolean UnityEngine.Advertisements.INativePlatform::IsInitialized()
// 0x00000075 System.Boolean UnityEngine.Advertisements.INativePlatform::IsReady(System.String)
// 0x00000076 UnityEngine.Advertisements.PlacementState UnityEngine.Advertisements.INativePlatform::GetPlacementState(System.String)
// 0x00000077 System.Action`1<UnityEngine.Advertisements.ShowResult> UnityEngine.Advertisements.ShowOptions::get_resultCallback()
extern void ShowOptions_get_resultCallback_m0BFEA29C0C9F5A32390477D00AEE67BA83D1B5AA (void);
// 0x00000078 System.Void UnityEngine.Advertisements.ShowOptions::set_resultCallback(System.Action`1<UnityEngine.Advertisements.ShowResult>)
extern void ShowOptions_set_resultCallback_mA9255B0E4EBF3EEAD519A2080E7A2C93E6683BB1 (void);
// 0x00000079 System.String UnityEngine.Advertisements.ShowOptions::get_gamerSid()
extern void ShowOptions_get_gamerSid_mD7509B7A64FDF3B1CC18C2FD1EA2048CDD7359FB (void);
// 0x0000007A System.Void UnityEngine.Advertisements.ShowOptions::set_gamerSid(System.String)
extern void ShowOptions_set_gamerSid_m8415326E01EEB67A42FA2098352368F2FD0F688E (void);
// 0x0000007B System.Void UnityEngine.Advertisements.ShowOptions::.ctor()
extern void ShowOptions__ctor_m5287C43DB7AC8A5CAF97F7ED143785E69B033A64 (void);
// 0x0000007C System.Void UnityEngine.Advertisements.Utilities.ApplicationQuit::add_OnApplicationQuitEventHandler(UnityEngine.Events.UnityAction)
extern void ApplicationQuit_add_OnApplicationQuitEventHandler_m2EA2F130925E01EBBA3D203A05239766823BA126 (void);
// 0x0000007D System.Void UnityEngine.Advertisements.Utilities.ApplicationQuit::remove_OnApplicationQuitEventHandler(UnityEngine.Events.UnityAction)
extern void ApplicationQuit_remove_OnApplicationQuitEventHandler_m8E78BA3977B4F5EF27F73F078296EDEB2A82392C (void);
// 0x0000007E System.Void UnityEngine.Advertisements.Utilities.ApplicationQuit::OnApplicationQuit()
extern void ApplicationQuit_OnApplicationQuit_m94BE0ECFE143EE6ADEF4449B8D40C57B8149815B (void);
// 0x0000007F System.Void UnityEngine.Advertisements.Utilities.ApplicationQuit::.ctor()
extern void ApplicationQuit__ctor_m48D6BA4AE7EEAFC80EA2CA17AE1368AC68091A7C (void);
// 0x00000080 System.Void UnityEngine.Advertisements.Utilities.CoroutineExecutor::Update()
extern void CoroutineExecutor_Update_m87AFDF181CF5686DE1FADB0C5060A3707B2BB0F2 (void);
// 0x00000081 System.Void UnityEngine.Advertisements.Utilities.CoroutineExecutor::.ctor()
extern void CoroutineExecutor__ctor_m10343728ADAF1FF7287FD55C3736A5B1D4121EA7 (void);
// 0x00000082 UnityEngine.Advertisements.ShowResult UnityEngine.Advertisements.Utilities.EnumUtilities::GetShowResultsFromCompletionState(UnityEngine.Advertisements.UnityAdsShowCompletionState)
extern void EnumUtilities_GetShowResultsFromCompletionState_m84B487DA01D22B415D70710AE17481DA13C48778 (void);
// 0x00000083 T UnityEngine.Advertisements.Utilities.EnumUtilities::GetEnumFromAndroidJavaObject(UnityEngine.AndroidJavaObject,T)
// 0x00000084 UnityEngine.Coroutine UnityEngine.Advertisements.Utilities.IUnityLifecycleManager::StartCoroutine(System.Collections.IEnumerator)
// 0x00000085 System.Void UnityEngine.Advertisements.Utilities.IUnityLifecycleManager::Post(System.Action)
// 0x00000086 System.Void UnityEngine.Advertisements.Utilities.IUnityLifecycleManager::SetOnApplicationQuitCallback(UnityEngine.Events.UnityAction)
// 0x00000087 System.Object UnityEngine.Advertisements.Utilities.Json::Deserialize(System.String)
extern void Json_Deserialize_mB7BD7F81801A9BF03C214624B2F627C505A0FD96 (void);
// 0x00000088 System.String UnityEngine.Advertisements.Utilities.Json::Serialize(System.Object)
extern void Json_Serialize_m8D287DACFCBDAB5CF11AB3EBDDD4D109591D443D (void);
// 0x00000089 System.Void UnityEngine.Advertisements.Utilities.UnityLifecycleManager::.ctor()
extern void UnityLifecycleManager__ctor_mEEAFC546E4A237D0793A7953AB562FA3FD96F8D7 (void);
// 0x0000008A System.Void UnityEngine.Advertisements.Utilities.UnityLifecycleManager::Initialize()
extern void UnityLifecycleManager_Initialize_m4F6ABEC49E9B2FC0A46BCDEB08625D1111BAFC9C (void);
// 0x0000008B UnityEngine.Coroutine UnityEngine.Advertisements.Utilities.UnityLifecycleManager::StartCoroutine(System.Collections.IEnumerator)
extern void UnityLifecycleManager_StartCoroutine_m5C59DD52F52791D9D34D07B8580DE7E5A5AF5D28 (void);
// 0x0000008C System.Void UnityEngine.Advertisements.Utilities.UnityLifecycleManager::Post(System.Action)
extern void UnityLifecycleManager_Post_mD9178EAFE99F3F89EA840AF064CAF55FE0F771F0 (void);
// 0x0000008D System.Void UnityEngine.Advertisements.Utilities.UnityLifecycleManager::Dispose()
extern void UnityLifecycleManager_Dispose_mCF9E89CBAD0085E00760F95245FC903FCE846D45 (void);
// 0x0000008E System.Void UnityEngine.Advertisements.Utilities.UnityLifecycleManager::SetOnApplicationQuitCallback(UnityEngine.Events.UnityAction)
extern void UnityLifecycleManager_SetOnApplicationQuitCallback_m7334EED46E388838750223ECB66F89C34E588719 (void);
// 0x0000008F System.Void UnityEngine.Advertisements.Purchasing.IPurchase::Initialize(UnityEngine.Advertisements.Purchasing.IPurchasingEventSender)
// 0x00000090 System.Void UnityEngine.Advertisements.Purchasing.IPurchase::SendEvent(System.String)
// 0x00000091 System.Void UnityEngine.Advertisements.Purchasing.IPurchase::onPurchasingCommand(System.String)
// 0x00000092 System.Void UnityEngine.Advertisements.Purchasing.IPurchase::onGetPurchasingVersion()
// 0x00000093 System.Void UnityEngine.Advertisements.Purchasing.IPurchase::onGetProductCatalog()
// 0x00000094 System.Void UnityEngine.Advertisements.Purchasing.IPurchase::onInitializePurchasing()
// 0x00000095 System.Void UnityEngine.Advertisements.Purchasing.IPurchasingEventSender::SendPurchasingEvent(System.String)
// 0x00000096 System.Void UnityEngine.Advertisements.Purchasing.Purchase::onPurchasingCommand(System.String)
extern void Purchase_onPurchasingCommand_mB9E644708ED8D8EFA8CD98F5D1958FA82C27977C (void);
// 0x00000097 System.Void UnityEngine.Advertisements.Purchasing.Purchase::onGetPurchasingVersion()
extern void Purchase_onGetPurchasingVersion_mD7C57FC1DBF00A587E7E7B05CF402073136DA811 (void);
// 0x00000098 System.Void UnityEngine.Advertisements.Purchasing.Purchase::onGetProductCatalog()
extern void Purchase_onGetProductCatalog_m1CAFF4E3ECF242B4463599BAB6740EB944471F1E (void);
// 0x00000099 System.Void UnityEngine.Advertisements.Purchasing.Purchase::onInitializePurchasing()
extern void Purchase_onInitializePurchasing_m3FB4CB1D7D6ADC2FB9EB1CD28F9BAD2291571B7F (void);
// 0x0000009A System.Void UnityEngine.Advertisements.Purchasing.Purchase::SendEvent(System.String)
extern void Purchase_SendEvent_m231E1E4E2C702B97DF3C50D40F1E58D112EE0D46 (void);
// 0x0000009B System.Void UnityEngine.Advertisements.Purchasing.Purchase::Initialize(UnityEngine.Advertisements.Purchasing.IPurchasingEventSender)
extern void Purchase_Initialize_m1DA9E4152F6B39CFD32FFF93F5AB01926A3614BB (void);
// 0x0000009C System.Void UnityEngine.Advertisements.Purchasing.Purchase::.ctor()
extern void Purchase__ctor_m3473E768807E79A5E05AC323F5F284FC53704F6D (void);
// 0x0000009D System.Boolean UnityEngine.Advertisements.Purchasing.Purchasing::Initialize(UnityEngine.Advertisements.Purchasing.IPurchasingEventSender)
extern void Purchasing_Initialize_mEE57A82CF79030301EAB7E832429DBFACACAD82D (void);
// 0x0000009E System.Boolean UnityEngine.Advertisements.Purchasing.Purchasing::InitiatePurchasingCommand(System.String)
extern void Purchasing_InitiatePurchasingCommand_m66FE9833C18F368298094F57ECC8A4677E2405E3 (void);
// 0x0000009F System.String UnityEngine.Advertisements.Purchasing.Purchasing::GetPurchasingCatalog()
extern void Purchasing_GetPurchasingCatalog_m4AE9FEEDCB3280E1EA93126B8483CC606347D437 (void);
// 0x000000A0 System.String UnityEngine.Advertisements.Purchasing.Purchasing::GetPromoVersion()
extern void Purchasing_GetPromoVersion_m250CC4254BBB79FBBF560A6BB326026328FEE2A7 (void);
// 0x000000A1 System.Boolean UnityEngine.Advertisements.Purchasing.Purchasing::SendEvent(System.String)
extern void Purchasing_SendEvent_m9AD5D002983892D293F1139CF2687FF15086BB0C (void);
// 0x000000A2 System.Void UnityEngine.Advertisements.Purchasing.Purchasing::.cctor()
extern void Purchasing__cctor_m38CF437C93B02C3A5012D76D6534F65A8B4EB4E3 (void);
// 0x000000A3 System.Void UnityEngine.Advertisements.Platform.IPlatform::add_OnStart(System.EventHandler`1<UnityEngine.Advertisements.Events.StartEventArgs>)
// 0x000000A4 System.Void UnityEngine.Advertisements.Platform.IPlatform::remove_OnStart(System.EventHandler`1<UnityEngine.Advertisements.Events.StartEventArgs>)
// 0x000000A5 System.Void UnityEngine.Advertisements.Platform.IPlatform::add_OnFinish(System.EventHandler`1<UnityEngine.Advertisements.Events.FinishEventArgs>)
// 0x000000A6 System.Void UnityEngine.Advertisements.Platform.IPlatform::remove_OnFinish(System.EventHandler`1<UnityEngine.Advertisements.Events.FinishEventArgs>)
// 0x000000A7 UnityEngine.Advertisements.IBanner UnityEngine.Advertisements.Platform.IPlatform::get_Banner()
// 0x000000A8 UnityEngine.Advertisements.Utilities.IUnityLifecycleManager UnityEngine.Advertisements.Platform.IPlatform::get_UnityLifecycleManager()
// 0x000000A9 UnityEngine.Advertisements.INativePlatform UnityEngine.Advertisements.Platform.IPlatform::get_NativePlatform()
// 0x000000AA System.Boolean UnityEngine.Advertisements.Platform.IPlatform::get_IsInitialized()
// 0x000000AB System.Boolean UnityEngine.Advertisements.Platform.IPlatform::get_IsShowing()
// 0x000000AC System.String UnityEngine.Advertisements.Platform.IPlatform::get_Version()
// 0x000000AD System.Boolean UnityEngine.Advertisements.Platform.IPlatform::get_DebugMode()
// 0x000000AE System.Void UnityEngine.Advertisements.Platform.IPlatform::set_DebugMode(System.Boolean)
// 0x000000AF System.Collections.Generic.HashSet`1<UnityEngine.Advertisements.IUnityAdsListener> UnityEngine.Advertisements.Platform.IPlatform::get_Listeners()
// 0x000000B0 System.Void UnityEngine.Advertisements.Platform.IPlatform::Initialize(System.String,System.Boolean,System.Boolean,UnityEngine.Advertisements.IUnityAdsInitializationListener)
// 0x000000B1 System.Void UnityEngine.Advertisements.Platform.IPlatform::Load(System.String,UnityEngine.Advertisements.IUnityAdsLoadListener)
// 0x000000B2 System.Void UnityEngine.Advertisements.Platform.IPlatform::Show(System.String,UnityEngine.Advertisements.ShowOptions,UnityEngine.Advertisements.IUnityAdsShowListener)
// 0x000000B3 System.Void UnityEngine.Advertisements.Platform.IPlatform::AddListener(UnityEngine.Advertisements.IUnityAdsListener)
// 0x000000B4 System.Void UnityEngine.Advertisements.Platform.IPlatform::RemoveListener(UnityEngine.Advertisements.IUnityAdsListener)
// 0x000000B5 System.Boolean UnityEngine.Advertisements.Platform.IPlatform::IsReady(System.String)
// 0x000000B6 UnityEngine.Advertisements.PlacementState UnityEngine.Advertisements.Platform.IPlatform::GetPlacementState(System.String)
// 0x000000B7 System.Void UnityEngine.Advertisements.Platform.IPlatform::SetMetaData(UnityEngine.Advertisements.MetaData)
// 0x000000B8 System.Void UnityEngine.Advertisements.Platform.IPlatform::UnityAdsReady(System.String)
// 0x000000B9 System.Void UnityEngine.Advertisements.Platform.IPlatform::UnityAdsDidError(System.String)
// 0x000000BA System.Void UnityEngine.Advertisements.Platform.IPlatform::UnityAdsDidStart(System.String)
// 0x000000BB System.Void UnityEngine.Advertisements.Platform.IPlatform::UnityAdsDidFinish(System.String,UnityEngine.Advertisements.ShowResult)
// 0x000000BC System.Void UnityEngine.Advertisements.Platform.Platform::add_OnStart(System.EventHandler`1<UnityEngine.Advertisements.Events.StartEventArgs>)
extern void Platform_add_OnStart_m04511443723766BAFF495DAC298754E51EA5D584 (void);
// 0x000000BD System.Void UnityEngine.Advertisements.Platform.Platform::remove_OnStart(System.EventHandler`1<UnityEngine.Advertisements.Events.StartEventArgs>)
extern void Platform_remove_OnStart_m3E857ED12DB9D9C971082D7694F1E43D5046F61B (void);
// 0x000000BE System.Void UnityEngine.Advertisements.Platform.Platform::add_OnFinish(System.EventHandler`1<UnityEngine.Advertisements.Events.FinishEventArgs>)
extern void Platform_add_OnFinish_m4A7073AEC10FD6A2F1786DE8B52E4CF1CD132EA3 (void);
// 0x000000BF System.Void UnityEngine.Advertisements.Platform.Platform::remove_OnFinish(System.EventHandler`1<UnityEngine.Advertisements.Events.FinishEventArgs>)
extern void Platform_remove_OnFinish_m8EA12F234481EFF5A53A1C1D1BB57DEADA2CF037 (void);
// 0x000000C0 UnityEngine.Advertisements.IBanner UnityEngine.Advertisements.Platform.Platform::get_Banner()
extern void Platform_get_Banner_mBEE78892FFA8C866082541224CFB8A92E4101311 (void);
// 0x000000C1 UnityEngine.Advertisements.Utilities.IUnityLifecycleManager UnityEngine.Advertisements.Platform.Platform::get_UnityLifecycleManager()
extern void Platform_get_UnityLifecycleManager_mB9294779C0B76789EBA9F9D5A17ECA0BBAAB27C6 (void);
// 0x000000C2 UnityEngine.Advertisements.INativePlatform UnityEngine.Advertisements.Platform.Platform::get_NativePlatform()
extern void Platform_get_NativePlatform_m947A379FFBD64F5CEA65376985C7222235B65A97 (void);
// 0x000000C3 System.Boolean UnityEngine.Advertisements.Platform.Platform::get_IsInitialized()
extern void Platform_get_IsInitialized_m19A3C638BFD4482F3AE83A2E56F25F1E35A9968B (void);
// 0x000000C4 System.Boolean UnityEngine.Advertisements.Platform.Platform::get_IsShowing()
extern void Platform_get_IsShowing_m514552FE27C701698D6355E09C8851DD4E5CE122 (void);
// 0x000000C5 System.Void UnityEngine.Advertisements.Platform.Platform::set_IsShowing(System.Boolean)
extern void Platform_set_IsShowing_mD31DBEAAEB77EE3516C8EC04FF31D9377DE1C2DD (void);
// 0x000000C6 System.String UnityEngine.Advertisements.Platform.Platform::get_Version()
extern void Platform_get_Version_m36E3FCBE6B23DE5BB29C5314B840E2A324B0BDFC (void);
// 0x000000C7 System.Boolean UnityEngine.Advertisements.Platform.Platform::get_DebugMode()
extern void Platform_get_DebugMode_m368948B55F5A11B39A30E390F9DC43D67E890AAA (void);
// 0x000000C8 System.Void UnityEngine.Advertisements.Platform.Platform::set_DebugMode(System.Boolean)
extern void Platform_set_DebugMode_m365E6C59F32F9C56C3595DDB2966EAD00B349970 (void);
// 0x000000C9 System.Collections.Generic.HashSet`1<UnityEngine.Advertisements.IUnityAdsListener> UnityEngine.Advertisements.Platform.Platform::get_Listeners()
extern void Platform_get_Listeners_m33769B37E8564946E3D9C317E1BB968894674C2F (void);
// 0x000000CA System.Void UnityEngine.Advertisements.Platform.Platform::.ctor(UnityEngine.Advertisements.INativePlatform,UnityEngine.Advertisements.IBanner,UnityEngine.Advertisements.Utilities.IUnityLifecycleManager)
extern void Platform__ctor_m05B42FE74A794E239AF260BD724BFDDFF8967E13 (void);
// 0x000000CB System.Void UnityEngine.Advertisements.Platform.Platform::Initialize(System.String,System.Boolean,System.Boolean,UnityEngine.Advertisements.IUnityAdsInitializationListener)
extern void Platform_Initialize_m02E4C03E17A810D1C4077D8C22F626C1D9EE9C8C (void);
// 0x000000CC System.Void UnityEngine.Advertisements.Platform.Platform::Load(System.String)
extern void Platform_Load_m793712044AE700F7706568DCBEB96B696EDDB2D6 (void);
// 0x000000CD System.Void UnityEngine.Advertisements.Platform.Platform::Load(System.String,UnityEngine.Advertisements.IUnityAdsLoadListener)
extern void Platform_Load_m0390E439C77D3D1771A9494E13D9CA3B278506DA (void);
// 0x000000CE System.Void UnityEngine.Advertisements.Platform.Platform::Show(System.String,UnityEngine.Advertisements.ShowOptions,UnityEngine.Advertisements.IUnityAdsShowListener)
extern void Platform_Show_mC1908C3954266E79D4E4BD29B960DD383C646400 (void);
// 0x000000CF System.Void UnityEngine.Advertisements.Platform.Platform::AddListener(UnityEngine.Advertisements.IUnityAdsListener)
extern void Platform_AddListener_mF745F32B3601EF8C4A4EB521808112BFF7EE544F (void);
// 0x000000D0 System.Void UnityEngine.Advertisements.Platform.Platform::RemoveListener(UnityEngine.Advertisements.IUnityAdsListener)
extern void Platform_RemoveListener_m7808950BCB103AB1464B1A27B2E18968C6635974 (void);
// 0x000000D1 System.Boolean UnityEngine.Advertisements.Platform.Platform::IsReady(System.String)
extern void Platform_IsReady_m1E8F6D115C7B01FDF6D7FBD098D34AC407EC471C (void);
// 0x000000D2 UnityEngine.Advertisements.PlacementState UnityEngine.Advertisements.Platform.Platform::GetPlacementState(System.String)
extern void Platform_GetPlacementState_mA0C6E36D9E7BA7880BDE681FE9C3171B28985CD2 (void);
// 0x000000D3 System.Void UnityEngine.Advertisements.Platform.Platform::SetMetaData(UnityEngine.Advertisements.MetaData)
extern void Platform_SetMetaData_mEF57467DE4554E6E2E5A5DFBC00490CFBDE679A3 (void);
// 0x000000D4 System.Void UnityEngine.Advertisements.Platform.Platform::UnityAdsReady(System.String)
extern void Platform_UnityAdsReady_m366535E3FE1BA8075169E85988A381CC8C1194DA (void);
// 0x000000D5 System.Void UnityEngine.Advertisements.Platform.Platform::UnityAdsDidError(System.String)
extern void Platform_UnityAdsDidError_m3AE8D1935F67D15EBC6400F7364104370908096E (void);
// 0x000000D6 System.Void UnityEngine.Advertisements.Platform.Platform::UnityAdsDidStart(System.String)
extern void Platform_UnityAdsDidStart_m6EA3AFCE2F0EC636B96AE359D3C1FEBA4266E390 (void);
// 0x000000D7 System.Void UnityEngine.Advertisements.Platform.Platform::UnityAdsDidFinish(System.String,UnityEngine.Advertisements.ShowResult)
extern void Platform_UnityAdsDidFinish_mBF602D6B7CBDF7A3D8A4BAC7CB75E7FC4854AC07 (void);
// 0x000000D8 System.Collections.Generic.HashSet`1<UnityEngine.Advertisements.IUnityAdsListener> UnityEngine.Advertisements.Platform.Platform::GetClonedHashSet(System.Collections.Generic.HashSet`1<UnityEngine.Advertisements.IUnityAdsListener>)
extern void Platform_GetClonedHashSet_m1372A4D374629411CFBDD416832C3C2FF38F56C7 (void);
// 0x000000D9 System.Void UnityEngine.Advertisements.Platform.Platform::<Initialize>b__30_0(System.Object,UnityEngine.Advertisements.Events.StartEventArgs)
extern void Platform_U3CInitializeU3Eb__30_0_m7A2E7A473A4F1D97AAD3B9FEA06EA0452FCF91AF (void);
// 0x000000DA System.Void UnityEngine.Advertisements.Platform.Platform::<Initialize>b__30_1(System.Object,UnityEngine.Advertisements.Events.FinishEventArgs)
extern void Platform_U3CInitializeU3Eb__30_1_m825D6AFF0E1E398216A1EE6796370B170219008A (void);
// 0x000000DB System.Boolean UnityEngine.Advertisements.Platform.Unsupported.UnsupportedBanner::get_IsLoaded()
extern void UnsupportedBanner_get_IsLoaded_mAC20597B72255A43422A65B92B0D6DB321380383 (void);
// 0x000000DC System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedBanner::SetupBanner(UnityEngine.Advertisements.IBanner)
extern void UnsupportedBanner_SetupBanner_mC98C55C69EED503F1DA8B22B5307FFA17337C2B4 (void);
// 0x000000DD System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedBanner::Load(System.String,UnityEngine.Advertisements.BannerLoadOptions)
extern void UnsupportedBanner_Load_m2D4478E96AD0F9C94A38DC821A70E51C277D56C8 (void);
// 0x000000DE System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedBanner::Show(System.String,UnityEngine.Advertisements.BannerOptions)
extern void UnsupportedBanner_Show_m861743CBEDFC9A9688FE2525EECA30E831BA6942 (void);
// 0x000000DF System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedBanner::Hide(System.Boolean)
extern void UnsupportedBanner_Hide_m8BDA1734AD214C9B1876D7831B1221F382869A8D (void);
// 0x000000E0 System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedBanner::SetPosition(UnityEngine.Advertisements.BannerPosition)
extern void UnsupportedBanner_SetPosition_m3F83D2F79AB7B55390BFD6709F7DD4B4B3271F68 (void);
// 0x000000E1 System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedBanner::.ctor()
extern void UnsupportedBanner__ctor_mC20B1B08B0CD39488831DBFE168A35A355F3DE34 (void);
// 0x000000E2 System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::SetupPlatform(UnityEngine.Advertisements.Platform.IPlatform)
extern void UnsupportedPlatform_SetupPlatform_mF077E22C1EDF0CC6CC6279DBC130C84FE7723817 (void);
// 0x000000E3 System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::Initialize(System.String,System.Boolean,System.Boolean,UnityEngine.Advertisements.IUnityAdsInitializationListener)
extern void UnsupportedPlatform_Initialize_m0AAD08F28892D94C7AF7D7A84BE604CFD7C000BD (void);
// 0x000000E4 System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::Load(System.String,UnityEngine.Advertisements.IUnityAdsLoadListener)
extern void UnsupportedPlatform_Load_m5B2A9F19A25AF29E297AAE694DC68CA681854038 (void);
// 0x000000E5 System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::Show(System.String,UnityEngine.Advertisements.IUnityAdsShowListener)
extern void UnsupportedPlatform_Show_mDC748A64CF758DB864B98A018FB880D88E2E9C5D (void);
// 0x000000E6 System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::SetMetaData(UnityEngine.Advertisements.MetaData)
extern void UnsupportedPlatform_SetMetaData_mA0CDAD6010FEF07BE5AC19264BBD89540FB0985D (void);
// 0x000000E7 System.Boolean UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::GetDebugMode()
extern void UnsupportedPlatform_GetDebugMode_m29965A5CBA58F05ED4D9703502EA369C90DD85EF (void);
// 0x000000E8 System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::SetDebugMode(System.Boolean)
extern void UnsupportedPlatform_SetDebugMode_mC61BD5EFB1E14D4E701EF030C99EB689183EC381 (void);
// 0x000000E9 System.String UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::GetVersion()
extern void UnsupportedPlatform_GetVersion_mABD397AE640F7AECA986A91916C2FC32B745D5A4 (void);
// 0x000000EA System.Boolean UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::IsInitialized()
extern void UnsupportedPlatform_IsInitialized_m1378AA8DDC6FE7DDAF2EF06092BEE8DA42C0E8F4 (void);
// 0x000000EB System.Boolean UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::IsReady(System.String)
extern void UnsupportedPlatform_IsReady_mC4BC7A8534BE6B6E82FCFB74697B44F255FF62A9 (void);
// 0x000000EC UnityEngine.Advertisements.PlacementState UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::GetPlacementState(System.String)
extern void UnsupportedPlatform_GetPlacementState_m0A347897C170FA834DCD24F0547451839F4E9979 (void);
// 0x000000ED System.Void UnityEngine.Advertisements.Platform.Unsupported.UnsupportedPlatform::.ctor()
extern void UnsupportedPlatform__ctor_m7958280F42833563A46C31ADB774DBA9DB7DB7A5 (void);
// 0x000000EE System.Void UnityEngine.Advertisements.Platform.Editor.BannerPlaceholder::Awake()
extern void BannerPlaceholder_Awake_m392559752B7D79220B534EBC2F84D634034F4B11 (void);
// 0x000000EF System.Void UnityEngine.Advertisements.Platform.Editor.BannerPlaceholder::OnGUI()
extern void BannerPlaceholder_OnGUI_m17CB0B088C168FBC82051455D4BD10813E5F2675 (void);
// 0x000000F0 System.Void UnityEngine.Advertisements.Platform.Editor.BannerPlaceholder::ShowBanner(UnityEngine.Advertisements.BannerPosition,UnityEngine.Advertisements.BannerOptions)
extern void BannerPlaceholder_ShowBanner_m7DD53DDFBDFFD078AC2D50325E93CFB52F8AC088 (void);
// 0x000000F1 System.Void UnityEngine.Advertisements.Platform.Editor.BannerPlaceholder::HideBanner()
extern void BannerPlaceholder_HideBanner_m8516A412A3CD20FB4D5F66CFEF233ACA340BC135 (void);
// 0x000000F2 UnityEngine.Texture2D UnityEngine.Advertisements.Platform.Editor.BannerPlaceholder::BackgroundTexture(System.Int32,System.Int32,UnityEngine.Color)
extern void BannerPlaceholder_BackgroundTexture_m3571682A4CA0E00BEA263954EE87DF8FADFB8516 (void);
// 0x000000F3 UnityEngine.Rect UnityEngine.Advertisements.Platform.Editor.BannerPlaceholder::GetBannerRect(UnityEngine.Advertisements.BannerPosition)
extern void BannerPlaceholder_GetBannerRect_m0F10D44EE32D80CB79954C98F8EF0A73BEF4601A (void);
// 0x000000F4 System.Void UnityEngine.Advertisements.Platform.Editor.BannerPlaceholder::.ctor()
extern void BannerPlaceholder__ctor_m3ECE869D6AFA5D19B732F44D72F21850C9EB52D8 (void);
// 0x000000F5 System.Boolean UnityEngine.Advertisements.Platform.Android.AndroidBanner::get_IsLoaded()
extern void AndroidBanner_get_IsLoaded_m3A7700F8A5205BA7709EB4801853E601D0D797F0 (void);
// 0x000000F6 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::.ctor()
extern void AndroidBanner__ctor_m734C09F272FE7BD60B1235513B9C071CF3D2FE0E (void);
// 0x000000F7 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::SetupBanner(UnityEngine.Advertisements.IBanner)
extern void AndroidBanner_SetupBanner_m8EAA0F0C3F1ED5FAAB6D0BCCD34887396EAB2516 (void);
// 0x000000F8 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::Load(System.String,UnityEngine.Advertisements.BannerLoadOptions)
extern void AndroidBanner_Load_m62999B7510DEDE7A341DB8FD2810147C726AD561 (void);
// 0x000000F9 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::Show(System.String,UnityEngine.Advertisements.BannerOptions)
extern void AndroidBanner_Show_m6E99841CFD49F93AFAD677BFD9FDC52303C94232 (void);
// 0x000000FA System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::Hide(System.Boolean)
extern void AndroidBanner_Hide_mB1E7741E9807F1AECAFD971C6F36A8F5555E8697 (void);
// 0x000000FB System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::SetPosition(UnityEngine.Advertisements.BannerPosition)
extern void AndroidBanner_SetPosition_m16185AD21C3D7D5F1542E23401D9CA0633BF26F0 (void);
// 0x000000FC System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::onUnityBannerShow(System.String)
extern void AndroidBanner_onUnityBannerShow_m8D172FC53B43DB542DB6F6A33871A90E7747F34E (void);
// 0x000000FD System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::onUnityBannerHide(System.String)
extern void AndroidBanner_onUnityBannerHide_m77A79A43FED9B7DB90F7962975F3ECA738CD2D71 (void);
// 0x000000FE System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::onUnityBannerLoaded(System.String,UnityEngine.AndroidJavaObject)
extern void AndroidBanner_onUnityBannerLoaded_m6017EE911350AC3BE006342764C4D478DAF73914 (void);
// 0x000000FF System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::onUnityBannerUnloaded(System.String)
extern void AndroidBanner_onUnityBannerUnloaded_m32662FDF703D4F351FEDC645EE1C3D80413E831B (void);
// 0x00000100 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::onUnityBannerClick(System.String)
extern void AndroidBanner_onUnityBannerClick_m01905C342D9102EE8B25640EC08A6B7E4396AAF1 (void);
// 0x00000101 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::onUnityBannerError(System.String)
extern void AndroidBanner_onUnityBannerError_m8E5DD5AACD964A0E69C0456C4CD95A424435D721 (void);
// 0x00000102 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::<Hide>b__13_0()
extern void AndroidBanner_U3CHideU3Eb__13_0_mAB882C8070DC10996541AA95791DDF35A7D3F9A8 (void);
// 0x00000103 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::<Hide>b__13_1()
extern void AndroidBanner_U3CHideU3Eb__13_1_mD15360DE3E36F405B8A47ABAFDB23D612167EF95 (void);
// 0x00000104 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::<onUnityBannerLoaded>b__17_1()
extern void AndroidBanner_U3ConUnityBannerLoadedU3Eb__17_1_mCB7A72E7A65C77AF78D2DDD94DFA950AE9FC3AC9 (void);
// 0x00000105 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::<onUnityBannerLoaded>b__17_0()
extern void AndroidBanner_U3ConUnityBannerLoadedU3Eb__17_0_mB0AF2D12F816E40A5C11FD95EDE2C22E18A4AE5B (void);
// 0x00000106 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner::<onUnityBannerClick>b__19_0()
extern void AndroidBanner_U3ConUnityBannerClickU3Eb__19_0_m7B6FC4F679F7F4903BB7FDB468FD1C111C21E399 (void);
// 0x00000107 System.Void UnityEngine.Advertisements.Platform.Android.AndroidPlatform::.ctor()
extern void AndroidPlatform__ctor_m5D667B39B959DDFEB1C821705E4D8FD4D91811EC (void);
// 0x00000108 System.Void UnityEngine.Advertisements.Platform.Android.AndroidPlatform::SetupPlatform(UnityEngine.Advertisements.Platform.IPlatform)
extern void AndroidPlatform_SetupPlatform_m2A31F16DDE02C7AC58514BDDDA945B3184470508 (void);
// 0x00000109 System.Void UnityEngine.Advertisements.Platform.Android.AndroidPlatform::Initialize(System.String,System.Boolean,System.Boolean,UnityEngine.Advertisements.IUnityAdsInitializationListener)
extern void AndroidPlatform_Initialize_m65FF211170FBBB8E79EE8A507366C11E29144144 (void);
// 0x0000010A System.Void UnityEngine.Advertisements.Platform.Android.AndroidPlatform::Load(System.String,UnityEngine.Advertisements.IUnityAdsLoadListener)
extern void AndroidPlatform_Load_m2B4C3F4AF02470D8D5D5D759F4E937F2D4371EB7 (void);
// 0x0000010B System.Void UnityEngine.Advertisements.Platform.Android.AndroidPlatform::Show(System.String,UnityEngine.Advertisements.IUnityAdsShowListener)
extern void AndroidPlatform_Show_m787BCE53E812EB340388FF343F937E72B8F96A82 (void);
// 0x0000010C System.Void UnityEngine.Advertisements.Platform.Android.AndroidPlatform::SetMetaData(UnityEngine.Advertisements.MetaData)
extern void AndroidPlatform_SetMetaData_m81C927FD8E29866B95DA104BF9F35C4138D174E3 (void);
// 0x0000010D System.Boolean UnityEngine.Advertisements.Platform.Android.AndroidPlatform::GetDebugMode()
extern void AndroidPlatform_GetDebugMode_m1ED72CA58F3DF253C03216459C2AC7E7AD12C104 (void);
// 0x0000010E System.Void UnityEngine.Advertisements.Platform.Android.AndroidPlatform::SetDebugMode(System.Boolean)
extern void AndroidPlatform_SetDebugMode_m18A3E0EE2CE09E20C596491C4A0B235BA249E197 (void);
// 0x0000010F System.String UnityEngine.Advertisements.Platform.Android.AndroidPlatform::GetVersion()
extern void AndroidPlatform_GetVersion_m591D9181C0E6AFA100BC79EF9BFC0F9925D28241 (void);
// 0x00000110 System.Boolean UnityEngine.Advertisements.Platform.Android.AndroidPlatform::IsInitialized()
extern void AndroidPlatform_IsInitialized_mA501F1339E6E6E64C0A8BDB4C9461D295ED2EE7B (void);
// 0x00000111 System.Boolean UnityEngine.Advertisements.Platform.Android.AndroidPlatform::IsReady(System.String)
extern void AndroidPlatform_IsReady_m6C93143C253793CEF1310B9E171FBD6374EDE0AE (void);
// 0x00000112 System.Void UnityEngine.Advertisements.Platform.Android.AndroidPlatform::RemoveListener()
extern void AndroidPlatform_RemoveListener_m2A18237BD604377583E41BACED668F65209C5BFD (void);
// 0x00000113 UnityEngine.Advertisements.PlacementState UnityEngine.Advertisements.Platform.Android.AndroidPlatform::GetPlacementState(System.String)
extern void AndroidPlatform_GetPlacementState_m41C5D4E0CCC299FD83367835715900DF778227EA (void);
// 0x00000114 UnityEngine.AndroidJavaObject UnityEngine.Advertisements.Platform.Android.AndroidPlatform::GetCurrentAndroidActivity()
extern void AndroidPlatform_GetCurrentAndroidActivity_m30A1BB0C7C5520B51B8F5E45F27F586A8C7BD68C (void);
// 0x00000115 System.Void UnityEngine.Advertisements.Platform.Android.AndroidPlatform::UnityEngine.Advertisements.Purchasing.IPurchasingEventSender.SendPurchasingEvent(System.String)
extern void AndroidPlatform_UnityEngine_Advertisements_Purchasing_IPurchasingEventSender_SendPurchasingEvent_m6650EDE5A3299DE08FF0975CF43922566AFFB3DB (void);
// 0x00000116 UnityEngine.AndroidJavaObject UnityEngine.Advertisements.Platform.Android.BannerBundle::get_bannerView()
extern void BannerBundle_get_bannerView_mA6309659C791621AA4EC30ADCD94331041586BDD (void);
// 0x00000117 System.String UnityEngine.Advertisements.Platform.Android.BannerBundle::get_bannerPlacementId()
extern void BannerBundle_get_bannerPlacementId_m789DB548258FCAE45F571D630961D7101EF1CE1C (void);
// 0x00000118 System.Void UnityEngine.Advertisements.Platform.Android.BannerBundle::.ctor(System.String,UnityEngine.AndroidJavaObject)
extern void BannerBundle__ctor_mDFCB4F3D083FDFFC6FC1A605D6B106D8E2D71E05 (void);
// 0x00000119 System.String UnityEngine.Advertisements.Events.FinishEventArgs::get_placementId()
extern void FinishEventArgs_get_placementId_m14031742D9FC9A405AE2E4348E7F650464EB6500 (void);
// 0x0000011A UnityEngine.Advertisements.ShowResult UnityEngine.Advertisements.Events.FinishEventArgs::get_showResult()
extern void FinishEventArgs_get_showResult_m933F173D07675D7C20C05D07623BE25398257C52 (void);
// 0x0000011B System.Void UnityEngine.Advertisements.Events.FinishEventArgs::.ctor(System.String,UnityEngine.Advertisements.ShowResult)
extern void FinishEventArgs__ctor_m496AD66C8D626BF0EFB33EEB2CEA2D2EDF42ADBB (void);
// 0x0000011C System.String UnityEngine.Advertisements.Events.StartEventArgs::get_placementId()
extern void StartEventArgs_get_placementId_mC0C9FF59F36D53397DBE16BABB94F3C11B8C3319 (void);
// 0x0000011D System.Void UnityEngine.Advertisements.Events.StartEventArgs::.ctor(System.String)
extern void StartEventArgs__ctor_m28465529BB59ED9A94D78ECEEF36F1FA601B87B0 (void);
// 0x0000011E System.Void UnityEngine.Advertisements.Advertisement/Banner::Load()
extern void Banner_Load_m2B7A9D57BE812ACE56BD6FBB556C64275A5108D8 (void);
// 0x0000011F System.Void UnityEngine.Advertisements.Advertisement/Banner::Load(UnityEngine.Advertisements.BannerLoadOptions)
extern void Banner_Load_m51016602F237F1C5FFFB4A9F8CC78027ABFBD465 (void);
// 0x00000120 System.Void UnityEngine.Advertisements.Advertisement/Banner::Load(System.String)
extern void Banner_Load_m4B1707F275B6824F09AC6CA0400BAB31B84447D7 (void);
// 0x00000121 System.Void UnityEngine.Advertisements.Advertisement/Banner::Load(System.String,UnityEngine.Advertisements.BannerLoadOptions)
extern void Banner_Load_m0B16B2F910D9AEA6F3C484E0A840B325BA604533 (void);
// 0x00000122 System.Void UnityEngine.Advertisements.Advertisement/Banner::Show()
extern void Banner_Show_m0867EEFB56F00AD7CB56DDE26D113DE8841E92E0 (void);
// 0x00000123 System.Void UnityEngine.Advertisements.Advertisement/Banner::Show(UnityEngine.Advertisements.BannerOptions)
extern void Banner_Show_m58ADAC207C46D38FB1D0F6C382760FAB7B03020A (void);
// 0x00000124 System.Void UnityEngine.Advertisements.Advertisement/Banner::Show(System.String)
extern void Banner_Show_mA9C697F2D14854F4D63B73CE200C302BC001B95C (void);
// 0x00000125 System.Void UnityEngine.Advertisements.Advertisement/Banner::Show(System.String,UnityEngine.Advertisements.BannerOptions)
extern void Banner_Show_m39F52F443284A243479F2C4C8CEB65D311BE577E (void);
// 0x00000126 System.Void UnityEngine.Advertisements.Advertisement/Banner::Hide(System.Boolean)
extern void Banner_Hide_m34F6689FFF8EADF49DA305D28D8DB5EA546D47C8 (void);
// 0x00000127 System.Void UnityEngine.Advertisements.Advertisement/Banner::SetPosition(UnityEngine.Advertisements.BannerPosition)
extern void Banner_SetPosition_m22764F6738A68978315AE1F3E24C62504431C195 (void);
// 0x00000128 System.Boolean UnityEngine.Advertisements.Advertisement/Banner::get_isLoaded()
extern void Banner_get_isLoaded_mC869D39CF2B67DFC4C62E89997597FB66D346F31 (void);
// 0x00000129 System.Void UnityEngine.Advertisements.Advertisement/<>c__DisplayClass32_0::.ctor()
extern void U3CU3Ec__DisplayClass32_0__ctor_m1C91BFEF8EE8A0ECD9425543B019D89270DEE796 (void);
// 0x0000012A System.Void UnityEngine.Advertisements.Advertisement/<>c__DisplayClass32_0::<CreatePlatform>b__0()
extern void U3CU3Ec__DisplayClass32_0_U3CCreatePlatformU3Eb__0_mC86B1175C401A8C939E6DB8B2443A8674DBB8068 (void);
// 0x0000012B System.Void UnityEngine.Advertisements.Banner/<>c__DisplayClass15_0::.ctor()
extern void U3CU3Ec__DisplayClass15_0__ctor_m0DB0934942645FDEE57D700579847E250B8B9D6D (void);
// 0x0000012C System.Void UnityEngine.Advertisements.Banner/<>c__DisplayClass15_0::<UnityAdsBannerDidShow>b__0()
extern void U3CU3Ec__DisplayClass15_0_U3CUnityAdsBannerDidShowU3Eb__0_mD13EA19B4F6F57C9D9F7B7C217D1F8C98FC5A910 (void);
// 0x0000012D System.Void UnityEngine.Advertisements.Banner/<>c__DisplayClass16_0::.ctor()
extern void U3CU3Ec__DisplayClass16_0__ctor_m20C0C90217B1373752842BB8599CC2476FBC2621 (void);
// 0x0000012E System.Void UnityEngine.Advertisements.Banner/<>c__DisplayClass16_0::<UnityAdsBannerDidHide>b__0()
extern void U3CU3Ec__DisplayClass16_0_U3CUnityAdsBannerDidHideU3Eb__0_mF4E325C0BBA19A39DCA32D8785F5143E1857E314 (void);
// 0x0000012F System.Void UnityEngine.Advertisements.Banner/<>c__DisplayClass17_0::.ctor()
extern void U3CU3Ec__DisplayClass17_0__ctor_mFF6DE5D6673EFE25526201D8D9534F65B44B8D97 (void);
// 0x00000130 System.Void UnityEngine.Advertisements.Banner/<>c__DisplayClass17_0::<UnityAdsBannerClick>b__0()
extern void U3CU3Ec__DisplayClass17_0_U3CUnityAdsBannerClickU3Eb__0_m4BF21D2B15528306906D50AABACF2EC97D833442 (void);
// 0x00000131 System.Void UnityEngine.Advertisements.Banner/<>c__DisplayClass18_0::.ctor()
extern void U3CU3Ec__DisplayClass18_0__ctor_m5650F32CAFB0E8540ECB8C817DA50BB5C541E5FE (void);
// 0x00000132 System.Void UnityEngine.Advertisements.Banner/<>c__DisplayClass18_0::<UnityAdsBannerDidLoad>b__0()
extern void U3CU3Ec__DisplayClass18_0_U3CUnityAdsBannerDidLoadU3Eb__0_m5E289A637853D4A68E819535A95EE58A0DECDA46 (void);
// 0x00000133 System.Void UnityEngine.Advertisements.Banner/<>c__DisplayClass19_0::.ctor()
extern void U3CU3Ec__DisplayClass19_0__ctor_mDFAA32A1AF754C5D4E96D6B080F395220A3783DB (void);
// 0x00000134 System.Void UnityEngine.Advertisements.Banner/<>c__DisplayClass19_0::<UnityAdsBannerDidError>b__0()
extern void U3CU3Ec__DisplayClass19_0_U3CUnityAdsBannerDidErrorU3Eb__0_m8DB8BD5CC86C48514D5F4DC0796AA6161135CC80 (void);
// 0x00000135 System.Void UnityEngine.Advertisements.BannerLoadOptions/LoadCallback::.ctor(System.Object,System.IntPtr)
extern void LoadCallback__ctor_m88910C05F7F71C398B7C12E68959C19D805BA703 (void);
// 0x00000136 System.Void UnityEngine.Advertisements.BannerLoadOptions/LoadCallback::Invoke()
extern void LoadCallback_Invoke_m2E4DAA16907300D497321EB545722747501062A0 (void);
// 0x00000137 System.IAsyncResult UnityEngine.Advertisements.BannerLoadOptions/LoadCallback::BeginInvoke(System.AsyncCallback,System.Object)
extern void LoadCallback_BeginInvoke_mED3103EDAAD26402C9C6CD01387AE13114220A6F (void);
// 0x00000138 System.Void UnityEngine.Advertisements.BannerLoadOptions/LoadCallback::EndInvoke(System.IAsyncResult)
extern void LoadCallback_EndInvoke_mAD24BE381D4801F52672FF1A2AC29E7B32A66EB9 (void);
// 0x00000139 System.Void UnityEngine.Advertisements.BannerLoadOptions/ErrorCallback::.ctor(System.Object,System.IntPtr)
extern void ErrorCallback__ctor_m1AE24068DF33FED857A07398BD89201E51EC94F6 (void);
// 0x0000013A System.Void UnityEngine.Advertisements.BannerLoadOptions/ErrorCallback::Invoke(System.String)
extern void ErrorCallback_Invoke_m41AE86ABF14CA916455DF97513DDAF5402E264CB (void);
// 0x0000013B System.IAsyncResult UnityEngine.Advertisements.BannerLoadOptions/ErrorCallback::BeginInvoke(System.String,System.AsyncCallback,System.Object)
extern void ErrorCallback_BeginInvoke_m80BA836285ABFDC1B4EDBC9A8EBEE541078BC129 (void);
// 0x0000013C System.Void UnityEngine.Advertisements.BannerLoadOptions/ErrorCallback::EndInvoke(System.IAsyncResult)
extern void ErrorCallback_EndInvoke_m2FDF0B737F2C234072C8E8591F0D97EA07ED659C (void);
// 0x0000013D System.Void UnityEngine.Advertisements.BannerOptions/BannerCallback::.ctor(System.Object,System.IntPtr)
extern void BannerCallback__ctor_m6A768B225071699C43901DFE357D7BE97098332A (void);
// 0x0000013E System.Void UnityEngine.Advertisements.BannerOptions/BannerCallback::Invoke()
extern void BannerCallback_Invoke_mF722B8CEBFA15A581C3C6BA430274DDAEF99FDBB (void);
// 0x0000013F System.IAsyncResult UnityEngine.Advertisements.BannerOptions/BannerCallback::BeginInvoke(System.AsyncCallback,System.Object)
extern void BannerCallback_BeginInvoke_m5112C5B0A18873B78449BD8006A164DBC2030F1A (void);
// 0x00000140 System.Void UnityEngine.Advertisements.BannerOptions/BannerCallback::EndInvoke(System.IAsyncResult)
extern void BannerCallback_EndInvoke_m72CE39272B31D92EF21FB6C1FB4A74D3987D6490 (void);
// 0x00000141 System.Boolean UnityEngine.Advertisements.Utilities.Json/Parser::IsWordBreak(System.Char)
extern void Parser_IsWordBreak_mBC7F99163008E6C756C565EE26952DCB3DB975D3 (void);
// 0x00000142 System.Void UnityEngine.Advertisements.Utilities.Json/Parser::.ctor(System.String)
extern void Parser__ctor_mED91F5842DC9DAB1CED670C4945DF74A1AE14D43 (void);
// 0x00000143 System.Object UnityEngine.Advertisements.Utilities.Json/Parser::Parse(System.String)
extern void Parser_Parse_m455BF22D71DAF9C70F824B709E776CE56CA26246 (void);
// 0x00000144 System.Void UnityEngine.Advertisements.Utilities.Json/Parser::Dispose()
extern void Parser_Dispose_mBEA826262FA72601E50657D0F98669A83CD88E32 (void);
// 0x00000145 System.Collections.Generic.Dictionary`2<System.String,System.Object> UnityEngine.Advertisements.Utilities.Json/Parser::ParseObject()
extern void Parser_ParseObject_m69DF5865D4D6314D49108710E25F61ED845D69E6 (void);
// 0x00000146 System.Collections.Generic.List`1<System.Object> UnityEngine.Advertisements.Utilities.Json/Parser::ParseArray()
extern void Parser_ParseArray_m1B77320E7DF0FDF01F7ECCB60E2936D9C82BF3D1 (void);
// 0x00000147 System.Object UnityEngine.Advertisements.Utilities.Json/Parser::ParseValue()
extern void Parser_ParseValue_m4EF4FAD14AB6DD2A17316C1B45D280C65BB3B5C6 (void);
// 0x00000148 System.Object UnityEngine.Advertisements.Utilities.Json/Parser::ParseByToken(UnityEngine.Advertisements.Utilities.Json/Parser/TOKEN)
extern void Parser_ParseByToken_mDD48A3EE9CEBD6286BDCE59B29AF627E37BC2FAB (void);
// 0x00000149 System.String UnityEngine.Advertisements.Utilities.Json/Parser::ParseString()
extern void Parser_ParseString_m2E3583D23DE5857CA5C19C13922432B6EBF9FBAE (void);
// 0x0000014A System.Object UnityEngine.Advertisements.Utilities.Json/Parser::ParseNumber()
extern void Parser_ParseNumber_mD2F2C6FE407817F2FD0CAA763CE6764B71A00CD1 (void);
// 0x0000014B System.Void UnityEngine.Advertisements.Utilities.Json/Parser::EatWhitespace()
extern void Parser_EatWhitespace_mA74B9E144EDD360BCFEBF913408E881D7A5FDEE7 (void);
// 0x0000014C System.Char UnityEngine.Advertisements.Utilities.Json/Parser::get_PeekChar()
extern void Parser_get_PeekChar_m333316E4051C051273C8ACE99D34A350A6FB06C1 (void);
// 0x0000014D System.Char UnityEngine.Advertisements.Utilities.Json/Parser::get_NextChar()
extern void Parser_get_NextChar_mCD30E77C9F504EEAB1AA27AD916B27521F391E9C (void);
// 0x0000014E System.String UnityEngine.Advertisements.Utilities.Json/Parser::get_NextWord()
extern void Parser_get_NextWord_m159583A5AB29CEC7C3D3AD8319223A987AFA857F (void);
// 0x0000014F UnityEngine.Advertisements.Utilities.Json/Parser/TOKEN UnityEngine.Advertisements.Utilities.Json/Parser::get_NextToken()
extern void Parser_get_NextToken_m4542BE5F6909E7A9DAA59478392DE86EDB03EC2C (void);
// 0x00000150 System.Void UnityEngine.Advertisements.Utilities.Json/Serializer::.ctor()
extern void Serializer__ctor_m852569F7E1DD20662D69C211447716364DB9A84B (void);
// 0x00000151 System.String UnityEngine.Advertisements.Utilities.Json/Serializer::Serialize(System.Object)
extern void Serializer_Serialize_m2E1F879077D37E96F852FF0EA0086A27FDA97848 (void);
// 0x00000152 System.Void UnityEngine.Advertisements.Utilities.Json/Serializer::SerializeValue(System.Object)
extern void Serializer_SerializeValue_mEBF5DD1B16B0086AAF2D2C24604C60D843541CE3 (void);
// 0x00000153 System.Void UnityEngine.Advertisements.Utilities.Json/Serializer::SerializeObject(System.Collections.IDictionary)
extern void Serializer_SerializeObject_m3FCC46BC471E448EF78619AA270F645DF692302D (void);
// 0x00000154 System.Void UnityEngine.Advertisements.Utilities.Json/Serializer::SerializeArray(System.Collections.IList)
extern void Serializer_SerializeArray_m547C55EB2B89139D92CF4498C303BF8B78BAAE98 (void);
// 0x00000155 System.Void UnityEngine.Advertisements.Utilities.Json/Serializer::SerializeString(System.String)
extern void Serializer_SerializeString_m7A5E2E16F76B57A6BA87D45A7349F24AEDCE5D10 (void);
// 0x00000156 System.Void UnityEngine.Advertisements.Utilities.Json/Serializer::SerializeOther(System.Object)
extern void Serializer_SerializeOther_mF9ED4B926475C5064B30BEE500041C3C0D3E0B26 (void);
// 0x00000157 System.Void UnityEngine.Advertisements.Platform.Platform/<>c__DisplayClass33_0::.ctor()
extern void U3CU3Ec__DisplayClass33_0__ctor_m5A43676152D4F57A59A0D52298A03ACFE4EF54EB (void);
// 0x00000158 System.Void UnityEngine.Advertisements.Platform.Platform/<>c__DisplayClass33_0::<Show>b__0(System.Object,UnityEngine.Advertisements.Events.FinishEventArgs)
extern void U3CU3Ec__DisplayClass33_0_U3CShowU3Eb__0_m9012914A2A16CD095D6B46177AFF3B5C3CB93145 (void);
// 0x00000159 System.Void UnityEngine.Advertisements.Platform.Platform/<>c__DisplayClass39_0::.ctor()
extern void U3CU3Ec__DisplayClass39_0__ctor_m0FC8804305346AEB7A362ADECA1AC44A46D7CC89 (void);
// 0x0000015A System.Void UnityEngine.Advertisements.Platform.Platform/<>c__DisplayClass39_0::<UnityAdsReady>b__0()
extern void U3CU3Ec__DisplayClass39_0_U3CUnityAdsReadyU3Eb__0_m28B8003670F5637B40F27E0B51F58D13B953F7C3 (void);
// 0x0000015B System.Void UnityEngine.Advertisements.Platform.Platform/<>c__DisplayClass40_0::.ctor()
extern void U3CU3Ec__DisplayClass40_0__ctor_m0128F39B29139E36655B7E6266ED5DCDD7F76FC0 (void);
// 0x0000015C System.Void UnityEngine.Advertisements.Platform.Platform/<>c__DisplayClass40_0::<UnityAdsDidError>b__0()
extern void U3CU3Ec__DisplayClass40_0_U3CUnityAdsDidErrorU3Eb__0_mABDE371586C21B6C5A4AB8A5F2C33C42A7301119 (void);
// 0x0000015D System.Void UnityEngine.Advertisements.Platform.Platform/<>c__DisplayClass41_0::.ctor()
extern void U3CU3Ec__DisplayClass41_0__ctor_m49FE78F3CFE52FBFDE4DF77AC6C8342850D863F5 (void);
// 0x0000015E System.Void UnityEngine.Advertisements.Platform.Platform/<>c__DisplayClass41_0::<UnityAdsDidStart>b__0()
extern void U3CU3Ec__DisplayClass41_0_U3CUnityAdsDidStartU3Eb__0_mF6E648E0E1C3D00F3D88E61F040BC0FE905B8B13 (void);
// 0x0000015F System.Void UnityEngine.Advertisements.Platform.Platform/<>c__DisplayClass42_0::.ctor()
extern void U3CU3Ec__DisplayClass42_0__ctor_m8FF1D1F8C068D89D0DBCA25E34BEDE8A2CEB7A1C (void);
// 0x00000160 System.Void UnityEngine.Advertisements.Platform.Platform/<>c__DisplayClass42_0::<UnityAdsDidFinish>b__0()
extern void U3CU3Ec__DisplayClass42_0_U3CUnityAdsDidFinishU3Eb__0_m2307DD2AF08F36B79D29B4050B1B6F1B1E048852 (void);
// 0x00000161 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner/<>c__DisplayClass11_0::.ctor()
extern void U3CU3Ec__DisplayClass11_0__ctor_mC415D8C16ADFC539F325538D30E81965A946A793 (void);
// 0x00000162 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner/<>c__DisplayClass11_0::<Load>b__0()
extern void U3CU3Ec__DisplayClass11_0_U3CLoadU3Eb__0_m949FFACE737D6CC4C752851E5F7406BF22B4998D (void);
// 0x00000163 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner/<>c__DisplayClass12_0::.ctor()
extern void U3CU3Ec__DisplayClass12_0__ctor_m30C2A848A50D2A065D20149EC691368359C13B60 (void);
// 0x00000164 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner/<>c__DisplayClass12_0::<Show>b__0()
extern void U3CU3Ec__DisplayClass12_0_U3CShowU3Eb__0_m0359D20E86119FDB7B89DD07E5CE3DF1F7F54BFA (void);
// 0x00000165 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner/<>c__DisplayClass12_0::<Show>b__1()
extern void U3CU3Ec__DisplayClass12_0_U3CShowU3Eb__1_m28B3F96473008FF1719DBF80C47C1E9670AA0791 (void);
// 0x00000166 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner/<>c__DisplayClass20_0::.ctor()
extern void U3CU3Ec__DisplayClass20_0__ctor_m7003CA38A85CB5D71D566630B55C90C822F875E7 (void);
// 0x00000167 System.Void UnityEngine.Advertisements.Platform.Android.AndroidBanner/<>c__DisplayClass20_0::<onUnityBannerError>b__0()
extern void U3CU3Ec__DisplayClass20_0_U3ConUnityBannerErrorU3Eb__0_mCA5AD7066A9870DF7FD69A8179B89C51970C6EF9 (void);
static Il2CppMethodPointer s_methodPointers[359] = 
{
	Advertisement__cctor_mEB30FD458B55E90811E113F894F17BADF3ED8658,
	Advertisement_get_isInitialized_mBC2B086E4633C893C4EED6FBDC1AB0615CD1B33A,
	Advertisement_get_isSupported_m4ECFA817DC2CBFBF90A66AE629C38396E75B925E,
	Advertisement_get_debugMode_m316718060F88EC9B2B64EA9E853D7E87A965881B,
	Advertisement_set_debugMode_mE8C7E59B822ABF1F8DC7ECA7244F61A2A77558E6,
	Advertisement_get_version_m0988FB62997EB8938B580B5EE3D37CDDFAEBAFA7,
	Advertisement_get_isShowing_m37CBCF24417233A4B6BD625A523CD849878F854F,
	Advertisement_Initialize_m082D5F19215B919660A314D4E72A7ED61AF8F704,
	Advertisement_Initialize_mD4FCDAE8D46A87C062B95FDA009085530825A66E,
	Advertisement_Initialize_mBF05AEF7A6C7E63B9111EB57E266A26E314F9C15,
	Advertisement_Initialize_m0FD976ECEA4D74BE3D60C60E6CCAB3027959C1AA,
	Advertisement_IsReady_mFDE54C7D1C9D98D205C7DC5BF16E36E7041401C3,
	Advertisement_IsReady_m7C3F32AA27ECF25EBED4FA413C77BFEF7B3DD858,
	Advertisement_Load_m2E6903F8D053377A617C356FB804FE8F8BB71756,
	Advertisement_Load_mA1912CED23684003576EC04DD6E14E06E1E47387,
	Advertisement_Show_m138EDDD0BF8B3C822671234133F31C237E2F0988,
	Advertisement_Show_m4809AB1F96CC586C69AE6C6C94E036DEE956D670,
	Advertisement_Show_m72C78A977F924A61DAF5B5D92D88200DE9F9C98B,
	Advertisement_Show_m5ACBA6D0BF333D43B7EC3B7C36F8E55A10741C1D,
	Advertisement_Show_m59789CD39DAC86DA1171BD01ABC0821AE1164D3D,
	Advertisement_Show_m51879852E416A638DC3789BBF67E36B33E2305D7,
	Advertisement_SetMetaData_mEDD78DDB6A5FB827E6060F446E485B67562BF7E1,
	Advertisement_AddListener_mF100E2DDC96B1395807E4B43F7FE1159F7FAE76C,
	Advertisement_RemoveListener_mCBBCDA53A28FF8BD03BB168F6AEF5703D70BD3CC,
	Advertisement_GetPlacementState_mA1A327D28199EBA054FD361E22FF6AF2FBE7CDBE,
	Advertisement_GetPlacementState_m0EBA0959072DBF04306C5C75BDE4A7C2E44E3C05,
	Advertisement_CreatePlatform_m7E8486F406293DCD2AA0A3690A6B2FD481BB5C63,
	Advertisement_IsSupported_mE13C31757D843704A28EBD64C1878F9E359C6FD0,
	Banner_get_UnityLifecycleManager_m8D1890BD72B78FB9DFF2E5351A146882DC4EE675,
	Banner_get_IsLoaded_m9381D3F941EBA8E7071C46ED56A3057CFAFFC0A2,
	Banner_get_ShowAfterLoad_m0149C039231E658B625C6DBA0AE981B1111C9F09,
	Banner_set_ShowAfterLoad_m645CDCDB38358BFCE6030CD3064140CA287F4794,
	Banner__ctor_m5FA80FC5D6F4028AFDBBBAA5DD18517A53A3CD81,
	Banner_Load_m7FB027911B94845C9AB4E5EEFEB2D98C3CC18544,
	Banner_Show_m92C480622ECB96A9EEC110B9C6D9B69618C01913,
	Banner_Hide_m46D677B526C45AADF7C18E7311EA7988271DBF34,
	Banner_SetPosition_m796058CCB4891D57CD4B1668A60566E11E296B0D,
	Banner_UnityAdsBannerDidShow_m8A09CD159A21B06FFD8D8734F13E2DE04929D2CB,
	Banner_UnityAdsBannerDidHide_m64A40CE58299FEB1EC2AF5B8E8DD5587701A0CBF,
	Banner_UnityAdsBannerClick_m589DEF392D6EA31211B7D152F200DC82EBECB951,
	Banner_UnityAdsBannerDidLoad_m67E46A4082BDB8E38BF6BB671DDB62FE506BBC6D,
	Banner_UnityAdsBannerDidError_mBAEDDC9BEEE536C436B8F2870883D011E09EFFD7,
	BannerLoadOptions_get_loadCallback_mAB4AE91F6982F5C4E06F2168C9F3D26BD6906578,
	BannerLoadOptions_set_loadCallback_mF981A9D9527258C8F1524F4AAE178C0786B6518E,
	BannerLoadOptions_get_errorCallback_m53924413EB917C85AABAD9B66FDEC9C84F08A4EB,
	BannerLoadOptions_set_errorCallback_mD8598AF5C0A7BE058286EEB566E5912B6AC7D6B1,
	BannerLoadOptions__ctor_m46ACB4C63B65D85BCB79D0B5FDA716E3A6957B04,
	BannerOptions_get_showCallback_m3EC0B33B8E5D4E959BE9572B3F57A549E8331C2F,
	BannerOptions_set_showCallback_mE32BBAD1A431BD6DB60AFDEA2828BDA3BAD75045,
	BannerOptions_get_hideCallback_m8FA965F59B65278AF69BFEB39B2FEDB17A511FE9,
	BannerOptions_set_hideCallback_m5F492B1009CDC83A26AC89B24A74CE6349B0E716,
	BannerOptions_get_clickCallback_mC79C625FB89086CF9C4DE112EF464D207B35D197,
	BannerOptions_set_clickCallback_m4919E420A2CCFCF1321E8BC6D4141B7386999134,
	BannerOptions__ctor_mA3423377C564DF63B24BD7217C157639E4013218,
	Configuration_get_enabled_m7BE1C8F2EA4B6C3B9CD8E821C999BF6BA1F3E982,
	Configuration_get_defaultPlacement_mACD234B00B1A8445668BA25D92E91142F21B9B65,
	Configuration_get_placements_m6787C062082EEABCEE45A1B809819650C2090BF4,
	Configuration__ctor_mFE8855E479EC18DDF7612712DD5D2A6E83A5430E,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	MetaData_get_category_m88A7B16198B9F13FDE4BFA0D81B3476E245CEE82,
	MetaData_set_category_mCF262A667D874F3A5B758420FC6CE01521AAA300,
	MetaData__ctor_mCC80A9A64EEEC5340746FEB1B2CF787F3891911B,
	MetaData_Set_m997F03F70DE2BF19AE97002EDE047619CBD52C5C,
	MetaData_Get_m10178F85D09B26C16A9A086F8C947503368D7820,
	MetaData_Values_mD6CC2222CE2B1A863DA664E9678694229CB5B108,
	MetaData_ToJSON_mC4791A88E21E2F982DE6C98C10C7588E4C4B0CD4,
	AndroidInitializationListener__ctor_mC9528623EB1956439EA549F8979E10E524C422D9,
	AndroidInitializationListener_onInitializationComplete_mB8E550641FAC0595B1CD802F9AA3C17291FA1343,
	AndroidInitializationListener_onInitializationFailed_m86A8A224EEA9892E268E20CAA1865D2004C6B8B4,
	AndroidLoadListener__ctor_m95C4131257DD23B962143C5439DB5F0E27798F4A,
	AndroidLoadListener_onUnityAdsAdLoaded_m355BCCCFCE9A4FF7A4E022D8CE91B8CB8AA85325,
	AndroidLoadListener_onUnityAdsFailedToLoad_m63DB5EBB1A54CBF0340F2B65B651EF40847CBA3D,
	AndroidShowListener__ctor_m255EBE9F98451576C0B56791B38F67D12E97671A,
	AndroidShowListener_onUnityAdsShowFailure_m9F6D9564DB55A7038BC5D7DF175F98EC93A3782C,
	AndroidShowListener_onUnityAdsShowStart_m074670050BEB5F88E8E343C5DC3E54E5CD533E55,
	AndroidShowListener_onUnityAdsShowClick_m0EA7453D92BD3D04FB38306FAC490FC2BE4B1197,
	AndroidShowListener_onUnityAdsShowComplete_m14096B1BB0D484F6DAA8070BB761B5A2C1D084CF,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	ShowOptions_get_resultCallback_m0BFEA29C0C9F5A32390477D00AEE67BA83D1B5AA,
	ShowOptions_set_resultCallback_mA9255B0E4EBF3EEAD519A2080E7A2C93E6683BB1,
	ShowOptions_get_gamerSid_mD7509B7A64FDF3B1CC18C2FD1EA2048CDD7359FB,
	ShowOptions_set_gamerSid_m8415326E01EEB67A42FA2098352368F2FD0F688E,
	ShowOptions__ctor_m5287C43DB7AC8A5CAF97F7ED143785E69B033A64,
	ApplicationQuit_add_OnApplicationQuitEventHandler_m2EA2F130925E01EBBA3D203A05239766823BA126,
	ApplicationQuit_remove_OnApplicationQuitEventHandler_m8E78BA3977B4F5EF27F73F078296EDEB2A82392C,
	ApplicationQuit_OnApplicationQuit_m94BE0ECFE143EE6ADEF4449B8D40C57B8149815B,
	ApplicationQuit__ctor_m48D6BA4AE7EEAFC80EA2CA17AE1368AC68091A7C,
	CoroutineExecutor_Update_m87AFDF181CF5686DE1FADB0C5060A3707B2BB0F2,
	CoroutineExecutor__ctor_m10343728ADAF1FF7287FD55C3736A5B1D4121EA7,
	EnumUtilities_GetShowResultsFromCompletionState_m84B487DA01D22B415D70710AE17481DA13C48778,
	NULL,
	NULL,
	NULL,
	NULL,
	Json_Deserialize_mB7BD7F81801A9BF03C214624B2F627C505A0FD96,
	Json_Serialize_m8D287DACFCBDAB5CF11AB3EBDDD4D109591D443D,
	UnityLifecycleManager__ctor_mEEAFC546E4A237D0793A7953AB562FA3FD96F8D7,
	UnityLifecycleManager_Initialize_m4F6ABEC49E9B2FC0A46BCDEB08625D1111BAFC9C,
	UnityLifecycleManager_StartCoroutine_m5C59DD52F52791D9D34D07B8580DE7E5A5AF5D28,
	UnityLifecycleManager_Post_mD9178EAFE99F3F89EA840AF064CAF55FE0F771F0,
	UnityLifecycleManager_Dispose_mCF9E89CBAD0085E00760F95245FC903FCE846D45,
	UnityLifecycleManager_SetOnApplicationQuitCallback_m7334EED46E388838750223ECB66F89C34E588719,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Purchase_onPurchasingCommand_mB9E644708ED8D8EFA8CD98F5D1958FA82C27977C,
	Purchase_onGetPurchasingVersion_mD7C57FC1DBF00A587E7E7B05CF402073136DA811,
	Purchase_onGetProductCatalog_m1CAFF4E3ECF242B4463599BAB6740EB944471F1E,
	Purchase_onInitializePurchasing_m3FB4CB1D7D6ADC2FB9EB1CD28F9BAD2291571B7F,
	Purchase_SendEvent_m231E1E4E2C702B97DF3C50D40F1E58D112EE0D46,
	Purchase_Initialize_m1DA9E4152F6B39CFD32FFF93F5AB01926A3614BB,
	Purchase__ctor_m3473E768807E79A5E05AC323F5F284FC53704F6D,
	Purchasing_Initialize_mEE57A82CF79030301EAB7E832429DBFACACAD82D,
	Purchasing_InitiatePurchasingCommand_m66FE9833C18F368298094F57ECC8A4677E2405E3,
	Purchasing_GetPurchasingCatalog_m4AE9FEEDCB3280E1EA93126B8483CC606347D437,
	Purchasing_GetPromoVersion_m250CC4254BBB79FBBF560A6BB326026328FEE2A7,
	Purchasing_SendEvent_m9AD5D002983892D293F1139CF2687FF15086BB0C,
	Purchasing__cctor_m38CF437C93B02C3A5012D76D6534F65A8B4EB4E3,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	Platform_add_OnStart_m04511443723766BAFF495DAC298754E51EA5D584,
	Platform_remove_OnStart_m3E857ED12DB9D9C971082D7694F1E43D5046F61B,
	Platform_add_OnFinish_m4A7073AEC10FD6A2F1786DE8B52E4CF1CD132EA3,
	Platform_remove_OnFinish_m8EA12F234481EFF5A53A1C1D1BB57DEADA2CF037,
	Platform_get_Banner_mBEE78892FFA8C866082541224CFB8A92E4101311,
	Platform_get_UnityLifecycleManager_mB9294779C0B76789EBA9F9D5A17ECA0BBAAB27C6,
	Platform_get_NativePlatform_m947A379FFBD64F5CEA65376985C7222235B65A97,
	Platform_get_IsInitialized_m19A3C638BFD4482F3AE83A2E56F25F1E35A9968B,
	Platform_get_IsShowing_m514552FE27C701698D6355E09C8851DD4E5CE122,
	Platform_set_IsShowing_mD31DBEAAEB77EE3516C8EC04FF31D9377DE1C2DD,
	Platform_get_Version_m36E3FCBE6B23DE5BB29C5314B840E2A324B0BDFC,
	Platform_get_DebugMode_m368948B55F5A11B39A30E390F9DC43D67E890AAA,
	Platform_set_DebugMode_m365E6C59F32F9C56C3595DDB2966EAD00B349970,
	Platform_get_Listeners_m33769B37E8564946E3D9C317E1BB968894674C2F,
	Platform__ctor_m05B42FE74A794E239AF260BD724BFDDFF8967E13,
	Platform_Initialize_m02E4C03E17A810D1C4077D8C22F626C1D9EE9C8C,
	Platform_Load_m793712044AE700F7706568DCBEB96B696EDDB2D6,
	Platform_Load_m0390E439C77D3D1771A9494E13D9CA3B278506DA,
	Platform_Show_mC1908C3954266E79D4E4BD29B960DD383C646400,
	Platform_AddListener_mF745F32B3601EF8C4A4EB521808112BFF7EE544F,
	Platform_RemoveListener_m7808950BCB103AB1464B1A27B2E18968C6635974,
	Platform_IsReady_m1E8F6D115C7B01FDF6D7FBD098D34AC407EC471C,
	Platform_GetPlacementState_mA0C6E36D9E7BA7880BDE681FE9C3171B28985CD2,
	Platform_SetMetaData_mEF57467DE4554E6E2E5A5DFBC00490CFBDE679A3,
	Platform_UnityAdsReady_m366535E3FE1BA8075169E85988A381CC8C1194DA,
	Platform_UnityAdsDidError_m3AE8D1935F67D15EBC6400F7364104370908096E,
	Platform_UnityAdsDidStart_m6EA3AFCE2F0EC636B96AE359D3C1FEBA4266E390,
	Platform_UnityAdsDidFinish_mBF602D6B7CBDF7A3D8A4BAC7CB75E7FC4854AC07,
	Platform_GetClonedHashSet_m1372A4D374629411CFBDD416832C3C2FF38F56C7,
	Platform_U3CInitializeU3Eb__30_0_m7A2E7A473A4F1D97AAD3B9FEA06EA0452FCF91AF,
	Platform_U3CInitializeU3Eb__30_1_m825D6AFF0E1E398216A1EE6796370B170219008A,
	UnsupportedBanner_get_IsLoaded_mAC20597B72255A43422A65B92B0D6DB321380383,
	UnsupportedBanner_SetupBanner_mC98C55C69EED503F1DA8B22B5307FFA17337C2B4,
	UnsupportedBanner_Load_m2D4478E96AD0F9C94A38DC821A70E51C277D56C8,
	UnsupportedBanner_Show_m861743CBEDFC9A9688FE2525EECA30E831BA6942,
	UnsupportedBanner_Hide_m8BDA1734AD214C9B1876D7831B1221F382869A8D,
	UnsupportedBanner_SetPosition_m3F83D2F79AB7B55390BFD6709F7DD4B4B3271F68,
	UnsupportedBanner__ctor_mC20B1B08B0CD39488831DBFE168A35A355F3DE34,
	UnsupportedPlatform_SetupPlatform_mF077E22C1EDF0CC6CC6279DBC130C84FE7723817,
	UnsupportedPlatform_Initialize_m0AAD08F28892D94C7AF7D7A84BE604CFD7C000BD,
	UnsupportedPlatform_Load_m5B2A9F19A25AF29E297AAE694DC68CA681854038,
	UnsupportedPlatform_Show_mDC748A64CF758DB864B98A018FB880D88E2E9C5D,
	UnsupportedPlatform_SetMetaData_mA0CDAD6010FEF07BE5AC19264BBD89540FB0985D,
	UnsupportedPlatform_GetDebugMode_m29965A5CBA58F05ED4D9703502EA369C90DD85EF,
	UnsupportedPlatform_SetDebugMode_mC61BD5EFB1E14D4E701EF030C99EB689183EC381,
	UnsupportedPlatform_GetVersion_mABD397AE640F7AECA986A91916C2FC32B745D5A4,
	UnsupportedPlatform_IsInitialized_m1378AA8DDC6FE7DDAF2EF06092BEE8DA42C0E8F4,
	UnsupportedPlatform_IsReady_mC4BC7A8534BE6B6E82FCFB74697B44F255FF62A9,
	UnsupportedPlatform_GetPlacementState_m0A347897C170FA834DCD24F0547451839F4E9979,
	UnsupportedPlatform__ctor_m7958280F42833563A46C31ADB774DBA9DB7DB7A5,
	BannerPlaceholder_Awake_m392559752B7D79220B534EBC2F84D634034F4B11,
	BannerPlaceholder_OnGUI_m17CB0B088C168FBC82051455D4BD10813E5F2675,
	BannerPlaceholder_ShowBanner_m7DD53DDFBDFFD078AC2D50325E93CFB52F8AC088,
	BannerPlaceholder_HideBanner_m8516A412A3CD20FB4D5F66CFEF233ACA340BC135,
	BannerPlaceholder_BackgroundTexture_m3571682A4CA0E00BEA263954EE87DF8FADFB8516,
	BannerPlaceholder_GetBannerRect_m0F10D44EE32D80CB79954C98F8EF0A73BEF4601A,
	BannerPlaceholder__ctor_m3ECE869D6AFA5D19B732F44D72F21850C9EB52D8,
	AndroidBanner_get_IsLoaded_m3A7700F8A5205BA7709EB4801853E601D0D797F0,
	AndroidBanner__ctor_m734C09F272FE7BD60B1235513B9C071CF3D2FE0E,
	AndroidBanner_SetupBanner_m8EAA0F0C3F1ED5FAAB6D0BCCD34887396EAB2516,
	AndroidBanner_Load_m62999B7510DEDE7A341DB8FD2810147C726AD561,
	AndroidBanner_Show_m6E99841CFD49F93AFAD677BFD9FDC52303C94232,
	AndroidBanner_Hide_mB1E7741E9807F1AECAFD971C6F36A8F5555E8697,
	AndroidBanner_SetPosition_m16185AD21C3D7D5F1542E23401D9CA0633BF26F0,
	AndroidBanner_onUnityBannerShow_m8D172FC53B43DB542DB6F6A33871A90E7747F34E,
	AndroidBanner_onUnityBannerHide_m77A79A43FED9B7DB90F7962975F3ECA738CD2D71,
	AndroidBanner_onUnityBannerLoaded_m6017EE911350AC3BE006342764C4D478DAF73914,
	AndroidBanner_onUnityBannerUnloaded_m32662FDF703D4F351FEDC645EE1C3D80413E831B,
	AndroidBanner_onUnityBannerClick_m01905C342D9102EE8B25640EC08A6B7E4396AAF1,
	AndroidBanner_onUnityBannerError_m8E5DD5AACD964A0E69C0456C4CD95A424435D721,
	AndroidBanner_U3CHideU3Eb__13_0_mAB882C8070DC10996541AA95791DDF35A7D3F9A8,
	AndroidBanner_U3CHideU3Eb__13_1_mD15360DE3E36F405B8A47ABAFDB23D612167EF95,
	AndroidBanner_U3ConUnityBannerLoadedU3Eb__17_1_mCB7A72E7A65C77AF78D2DDD94DFA950AE9FC3AC9,
	AndroidBanner_U3ConUnityBannerLoadedU3Eb__17_0_mB0AF2D12F816E40A5C11FD95EDE2C22E18A4AE5B,
	AndroidBanner_U3ConUnityBannerClickU3Eb__19_0_m7B6FC4F679F7F4903BB7FDB468FD1C111C21E399,
	AndroidPlatform__ctor_m5D667B39B959DDFEB1C821705E4D8FD4D91811EC,
	AndroidPlatform_SetupPlatform_m2A31F16DDE02C7AC58514BDDDA945B3184470508,
	AndroidPlatform_Initialize_m65FF211170FBBB8E79EE8A507366C11E29144144,
	AndroidPlatform_Load_m2B4C3F4AF02470D8D5D5D759F4E937F2D4371EB7,
	AndroidPlatform_Show_m787BCE53E812EB340388FF343F937E72B8F96A82,
	AndroidPlatform_SetMetaData_m81C927FD8E29866B95DA104BF9F35C4138D174E3,
	AndroidPlatform_GetDebugMode_m1ED72CA58F3DF253C03216459C2AC7E7AD12C104,
	AndroidPlatform_SetDebugMode_m18A3E0EE2CE09E20C596491C4A0B235BA249E197,
	AndroidPlatform_GetVersion_m591D9181C0E6AFA100BC79EF9BFC0F9925D28241,
	AndroidPlatform_IsInitialized_mA501F1339E6E6E64C0A8BDB4C9461D295ED2EE7B,
	AndroidPlatform_IsReady_m6C93143C253793CEF1310B9E171FBD6374EDE0AE,
	AndroidPlatform_RemoveListener_m2A18237BD604377583E41BACED668F65209C5BFD,
	AndroidPlatform_GetPlacementState_m41C5D4E0CCC299FD83367835715900DF778227EA,
	AndroidPlatform_GetCurrentAndroidActivity_m30A1BB0C7C5520B51B8F5E45F27F586A8C7BD68C,
	AndroidPlatform_UnityEngine_Advertisements_Purchasing_IPurchasingEventSender_SendPurchasingEvent_m6650EDE5A3299DE08FF0975CF43922566AFFB3DB,
	BannerBundle_get_bannerView_mA6309659C791621AA4EC30ADCD94331041586BDD,
	BannerBundle_get_bannerPlacementId_m789DB548258FCAE45F571D630961D7101EF1CE1C,
	BannerBundle__ctor_mDFCB4F3D083FDFFC6FC1A605D6B106D8E2D71E05,
	FinishEventArgs_get_placementId_m14031742D9FC9A405AE2E4348E7F650464EB6500,
	FinishEventArgs_get_showResult_m933F173D07675D7C20C05D07623BE25398257C52,
	FinishEventArgs__ctor_m496AD66C8D626BF0EFB33EEB2CEA2D2EDF42ADBB,
	StartEventArgs_get_placementId_mC0C9FF59F36D53397DBE16BABB94F3C11B8C3319,
	StartEventArgs__ctor_m28465529BB59ED9A94D78ECEEF36F1FA601B87B0,
	Banner_Load_m2B7A9D57BE812ACE56BD6FBB556C64275A5108D8,
	Banner_Load_m51016602F237F1C5FFFB4A9F8CC78027ABFBD465,
	Banner_Load_m4B1707F275B6824F09AC6CA0400BAB31B84447D7,
	Banner_Load_m0B16B2F910D9AEA6F3C484E0A840B325BA604533,
	Banner_Show_m0867EEFB56F00AD7CB56DDE26D113DE8841E92E0,
	Banner_Show_m58ADAC207C46D38FB1D0F6C382760FAB7B03020A,
	Banner_Show_mA9C697F2D14854F4D63B73CE200C302BC001B95C,
	Banner_Show_m39F52F443284A243479F2C4C8CEB65D311BE577E,
	Banner_Hide_m34F6689FFF8EADF49DA305D28D8DB5EA546D47C8,
	Banner_SetPosition_m22764F6738A68978315AE1F3E24C62504431C195,
	Banner_get_isLoaded_mC869D39CF2B67DFC4C62E89997597FB66D346F31,
	U3CU3Ec__DisplayClass32_0__ctor_m1C91BFEF8EE8A0ECD9425543B019D89270DEE796,
	U3CU3Ec__DisplayClass32_0_U3CCreatePlatformU3Eb__0_mC86B1175C401A8C939E6DB8B2443A8674DBB8068,
	U3CU3Ec__DisplayClass15_0__ctor_m0DB0934942645FDEE57D700579847E250B8B9D6D,
	U3CU3Ec__DisplayClass15_0_U3CUnityAdsBannerDidShowU3Eb__0_mD13EA19B4F6F57C9D9F7B7C217D1F8C98FC5A910,
	U3CU3Ec__DisplayClass16_0__ctor_m20C0C90217B1373752842BB8599CC2476FBC2621,
	U3CU3Ec__DisplayClass16_0_U3CUnityAdsBannerDidHideU3Eb__0_mF4E325C0BBA19A39DCA32D8785F5143E1857E314,
	U3CU3Ec__DisplayClass17_0__ctor_mFF6DE5D6673EFE25526201D8D9534F65B44B8D97,
	U3CU3Ec__DisplayClass17_0_U3CUnityAdsBannerClickU3Eb__0_m4BF21D2B15528306906D50AABACF2EC97D833442,
	U3CU3Ec__DisplayClass18_0__ctor_m5650F32CAFB0E8540ECB8C817DA50BB5C541E5FE,
	U3CU3Ec__DisplayClass18_0_U3CUnityAdsBannerDidLoadU3Eb__0_m5E289A637853D4A68E819535A95EE58A0DECDA46,
	U3CU3Ec__DisplayClass19_0__ctor_mDFAA32A1AF754C5D4E96D6B080F395220A3783DB,
	U3CU3Ec__DisplayClass19_0_U3CUnityAdsBannerDidErrorU3Eb__0_m8DB8BD5CC86C48514D5F4DC0796AA6161135CC80,
	LoadCallback__ctor_m88910C05F7F71C398B7C12E68959C19D805BA703,
	LoadCallback_Invoke_m2E4DAA16907300D497321EB545722747501062A0,
	LoadCallback_BeginInvoke_mED3103EDAAD26402C9C6CD01387AE13114220A6F,
	LoadCallback_EndInvoke_mAD24BE381D4801F52672FF1A2AC29E7B32A66EB9,
	ErrorCallback__ctor_m1AE24068DF33FED857A07398BD89201E51EC94F6,
	ErrorCallback_Invoke_m41AE86ABF14CA916455DF97513DDAF5402E264CB,
	ErrorCallback_BeginInvoke_m80BA836285ABFDC1B4EDBC9A8EBEE541078BC129,
	ErrorCallback_EndInvoke_m2FDF0B737F2C234072C8E8591F0D97EA07ED659C,
	BannerCallback__ctor_m6A768B225071699C43901DFE357D7BE97098332A,
	BannerCallback_Invoke_mF722B8CEBFA15A581C3C6BA430274DDAEF99FDBB,
	BannerCallback_BeginInvoke_m5112C5B0A18873B78449BD8006A164DBC2030F1A,
	BannerCallback_EndInvoke_m72CE39272B31D92EF21FB6C1FB4A74D3987D6490,
	Parser_IsWordBreak_mBC7F99163008E6C756C565EE26952DCB3DB975D3,
	Parser__ctor_mED91F5842DC9DAB1CED670C4945DF74A1AE14D43,
	Parser_Parse_m455BF22D71DAF9C70F824B709E776CE56CA26246,
	Parser_Dispose_mBEA826262FA72601E50657D0F98669A83CD88E32,
	Parser_ParseObject_m69DF5865D4D6314D49108710E25F61ED845D69E6,
	Parser_ParseArray_m1B77320E7DF0FDF01F7ECCB60E2936D9C82BF3D1,
	Parser_ParseValue_m4EF4FAD14AB6DD2A17316C1B45D280C65BB3B5C6,
	Parser_ParseByToken_mDD48A3EE9CEBD6286BDCE59B29AF627E37BC2FAB,
	Parser_ParseString_m2E3583D23DE5857CA5C19C13922432B6EBF9FBAE,
	Parser_ParseNumber_mD2F2C6FE407817F2FD0CAA763CE6764B71A00CD1,
	Parser_EatWhitespace_mA74B9E144EDD360BCFEBF913408E881D7A5FDEE7,
	Parser_get_PeekChar_m333316E4051C051273C8ACE99D34A350A6FB06C1,
	Parser_get_NextChar_mCD30E77C9F504EEAB1AA27AD916B27521F391E9C,
	Parser_get_NextWord_m159583A5AB29CEC7C3D3AD8319223A987AFA857F,
	Parser_get_NextToken_m4542BE5F6909E7A9DAA59478392DE86EDB03EC2C,
	Serializer__ctor_m852569F7E1DD20662D69C211447716364DB9A84B,
	Serializer_Serialize_m2E1F879077D37E96F852FF0EA0086A27FDA97848,
	Serializer_SerializeValue_mEBF5DD1B16B0086AAF2D2C24604C60D843541CE3,
	Serializer_SerializeObject_m3FCC46BC471E448EF78619AA270F645DF692302D,
	Serializer_SerializeArray_m547C55EB2B89139D92CF4498C303BF8B78BAAE98,
	Serializer_SerializeString_m7A5E2E16F76B57A6BA87D45A7349F24AEDCE5D10,
	Serializer_SerializeOther_mF9ED4B926475C5064B30BEE500041C3C0D3E0B26,
	U3CU3Ec__DisplayClass33_0__ctor_m5A43676152D4F57A59A0D52298A03ACFE4EF54EB,
	U3CU3Ec__DisplayClass33_0_U3CShowU3Eb__0_m9012914A2A16CD095D6B46177AFF3B5C3CB93145,
	U3CU3Ec__DisplayClass39_0__ctor_m0FC8804305346AEB7A362ADECA1AC44A46D7CC89,
	U3CU3Ec__DisplayClass39_0_U3CUnityAdsReadyU3Eb__0_m28B8003670F5637B40F27E0B51F58D13B953F7C3,
	U3CU3Ec__DisplayClass40_0__ctor_m0128F39B29139E36655B7E6266ED5DCDD7F76FC0,
	U3CU3Ec__DisplayClass40_0_U3CUnityAdsDidErrorU3Eb__0_mABDE371586C21B6C5A4AB8A5F2C33C42A7301119,
	U3CU3Ec__DisplayClass41_0__ctor_m49FE78F3CFE52FBFDE4DF77AC6C8342850D863F5,
	U3CU3Ec__DisplayClass41_0_U3CUnityAdsDidStartU3Eb__0_mF6E648E0E1C3D00F3D88E61F040BC0FE905B8B13,
	U3CU3Ec__DisplayClass42_0__ctor_m8FF1D1F8C068D89D0DBCA25E34BEDE8A2CEB7A1C,
	U3CU3Ec__DisplayClass42_0_U3CUnityAdsDidFinishU3Eb__0_m2307DD2AF08F36B79D29B4050B1B6F1B1E048852,
	U3CU3Ec__DisplayClass11_0__ctor_mC415D8C16ADFC539F325538D30E81965A946A793,
	U3CU3Ec__DisplayClass11_0_U3CLoadU3Eb__0_m949FFACE737D6CC4C752851E5F7406BF22B4998D,
	U3CU3Ec__DisplayClass12_0__ctor_m30C2A848A50D2A065D20149EC691368359C13B60,
	U3CU3Ec__DisplayClass12_0_U3CShowU3Eb__0_m0359D20E86119FDB7B89DD07E5CE3DF1F7F54BFA,
	U3CU3Ec__DisplayClass12_0_U3CShowU3Eb__1_m28B3F96473008FF1719DBF80C47C1E9670AA0791,
	U3CU3Ec__DisplayClass20_0__ctor_m7003CA38A85CB5D71D566630B55C90C822F875E7,
	U3CU3Ec__DisplayClass20_0_U3ConUnityBannerErrorU3Eb__0_mCA5AD7066A9870DF7FD69A8179B89C51970C6EF9,
};
static const int32_t s_InvokerIndices[359] = 
{
	4494,
	4485,
	4485,
	4485,
	4394,
	4467,
	4485,
	4391,
	4060,
	3690,
	3414,
	4485,
	4337,
	4391,
	4057,
	4494,
	4391,
	4391,
	4057,
	4057,
	3686,
	4391,
	4391,
	4391,
	4460,
	4216,
	4467,
	4485,
	2768,
	2792,
	2792,
	2276,
	1326,
	1326,
	1326,
	2276,
	2240,
	1326,
	1326,
	1326,
	1326,
	1326,
	2768,
	2257,
	2768,
	2257,
	2822,
	2768,
	2257,
	2768,
	2257,
	2768,
	2257,
	2822,
	2792,
	2768,
	2768,
	2257,
	2768,
	2792,
	2792,
	2276,
	1326,
	1326,
	2276,
	2240,
	1326,
	1326,
	1326,
	1326,
	1326,
	2792,
	2257,
	1326,
	1326,
	2276,
	2240,
	2257,
	2257,
	2257,
	1321,
	2822,
	1201,
	2257,
	754,
	754,
	2257,
	2257,
	1321,
	2768,
	2257,
	2257,
	1326,
	1744,
	2768,
	2768,
	1326,
	2822,
	1326,
	1326,
	2257,
	763,
	1326,
	763,
	2257,
	2257,
	1326,
	2257,
	499,
	1326,
	1326,
	2257,
	2792,
	2276,
	2768,
	2792,
	1933,
	1602,
	2768,
	2257,
	2768,
	2257,
	2822,
	2257,
	2257,
	2822,
	2822,
	2822,
	2822,
	4212,
	-1,
	1744,
	2257,
	2257,
	4281,
	4281,
	2822,
	2822,
	1744,
	2257,
	2822,
	2257,
	2257,
	2257,
	2257,
	2822,
	2822,
	2822,
	2257,
	2257,
	2822,
	2822,
	2822,
	2257,
	2257,
	2822,
	4337,
	4337,
	4467,
	4467,
	4337,
	4494,
	2257,
	2257,
	2257,
	2257,
	2768,
	2768,
	2768,
	2792,
	2792,
	2768,
	2792,
	2276,
	2768,
	499,
	1326,
	763,
	2257,
	2257,
	1933,
	1602,
	2257,
	2257,
	2257,
	2257,
	1321,
	2257,
	2257,
	2257,
	2257,
	2768,
	2768,
	2768,
	2792,
	2792,
	2276,
	2768,
	2792,
	2276,
	2768,
	763,
	499,
	2257,
	1326,
	763,
	2257,
	2257,
	1933,
	1602,
	2257,
	2257,
	2257,
	2257,
	1321,
	4281,
	1326,
	1326,
	2792,
	2257,
	1326,
	1326,
	2276,
	2240,
	2822,
	2257,
	499,
	1326,
	1326,
	2257,
	2792,
	2276,
	2768,
	2792,
	1933,
	1602,
	2822,
	2822,
	2822,
	1201,
	2822,
	3507,
	4323,
	2822,
	2792,
	2822,
	2257,
	1326,
	1326,
	2276,
	2240,
	2257,
	2257,
	1326,
	2257,
	2257,
	2257,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2257,
	499,
	1326,
	1326,
	2257,
	2792,
	2276,
	2768,
	2792,
	1933,
	2822,
	1602,
	4467,
	2257,
	2768,
	2768,
	1326,
	2768,
	2750,
	1321,
	2768,
	2257,
	4494,
	4391,
	4391,
	4057,
	4494,
	4391,
	4391,
	4057,
	4394,
	4389,
	4485,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	1323,
	2822,
	930,
	2257,
	1323,
	2257,
	588,
	2257,
	1323,
	2822,
	930,
	2257,
	4333,
	2257,
	4281,
	2822,
	2768,
	2768,
	2768,
	1737,
	2768,
	2768,
	2822,
	2749,
	2749,
	2768,
	2750,
	2822,
	4281,
	2257,
	2257,
	2257,
	2257,
	2257,
	2822,
	1326,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
	2822,
};
static const Il2CppTokenRangePair s_rgctxIndices[1] = 
{
	{ 0x06000083, { 0, 2 } },
};
static const Il2CppRGCTXDefinition s_rgctxValues[2] = 
{
	{ (Il2CppRGCTXDataType)1, 148 },
	{ (Il2CppRGCTXDataType)2, 148 },
};
extern const CustomAttributesCacheGenerator g_UnityEngine_Advertisements_AttributeGenerators[];
IL2CPP_EXTERN_C const Il2CppCodeGenModule g_UnityEngine_Advertisements_CodeGenModule;
const Il2CppCodeGenModule g_UnityEngine_Advertisements_CodeGenModule = 
{
	"UnityEngine.Advertisements.dll",
	359,
	s_methodPointers,
	0,
	NULL,
	s_InvokerIndices,
	0,
	NULL,
	1,
	s_rgctxIndices,
	2,
	s_rgctxValues,
	NULL,
	g_UnityEngine_Advertisements_AttributeGenerators,
	NULL, // module initializer,
	NULL,
	NULL,
	NULL,
};
